({
    doInit : function(component, event, helper) {
        component.find("navService").navigate({
            type: "standard__component",
            attributes: {
                componentName: "c__ccQuickProductAddContainer" 
            },
            state: {
                "c__id": component.get('v.recordId')
            }
        });
    }
})