/**
 * Created by Matt on 6/10/2022.
 */

({
    redirectToRecord: function (recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
        });
        navEvt.fire();
    }

});