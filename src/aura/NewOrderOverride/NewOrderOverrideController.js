// auraNewAccountOverrideController.js
({
    handleCreateLoad: function (component, event, helper) {
        component.set("v.isLoading", false);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");

        var pageRef = component.get("v.pageReference");


        var statusField = component.find("Status").set("v.value", "Draft");
        var orderField = component.find("OrderDate").set("v.value", today);
        // MParrella | Coastal Cloud | 03-07-2023 | TSK-00236400 | Referencing new Custom MDT for PricebookId instead of custom label: START
//        let pricebookId =  $A.get("$Label.c.cc_standard_pricebook_id");

        // MParrella | Coastal Cloud | 03-07-2023 | TSK-00236400 | Referencing new Custom MDT for PricebookId instead of custom label: STOP
    },

    handleSuccess : function (component, event, helper) {
        var record = event.getParam("response");
        var recordId = record.id;
        component.set("v.recordId", recordId);
        helper.redirectToRecord(recordId);
    },

    handleSubmit : function (component, event, helper) {
        component.set("v.isLoading", true);
    },

    handleError : function (component, event, helper) {
        console.log(JSON.parse(JSON.stringify(event.getParam("error"))));
        component.set("v.isLoading", false);
    },

    handleAccountChange : function (component, event, helper) {
        // Call Aura Method to get Account Data
        var accountId = component.find("Account").get("v.value");
        if (!!accountId) {
            component.set("v.isLoading", true);
            var accountData;
            component.accountMethod(accountId)
            .then((result) => {
                accountData = result;
                if (!!accountData) {
                    var isoCodeField = component.find("CurrencyIsoCode").set("v.value", accountData[accountId].CurrencyIsoCode);
                    var salesOrgField = component.find("SalesOrg").set("v.value", accountData[accountId].Sales_Org__c);
                    // MParrella | Coastal Cloud | 03-08-2023 | TSK-00236400 | Calling new logic to get Pricebook data from Custom MDT: START
                    var evt = component.get("c.getPricebookData");
                    $A.enqueueAction(evt);
                    // MParrella | Coastal Cloud | 03-08-2023 | TSK-00236400 | Calling new logic to get Pricebook data from Custom MDT: STOP
                }
            })
            .catch(error => {
                console.log('Error getting account data: ', error);
            });
        }
    },

    getAccountData : function (component, event) {
        return new Promise(
            $A.getCallback((resolve, reject) => {
                var params = event.getParam('arguments');
                if (params) {
                    var acctId = params.accountId;
                }
                  var action = component.get("c.getAccountFieldData");
                  action.setParams({
                      accountId : acctId
                  });
                  action.setCallback(this, function(response) {
                      var state = response.getState();
                      if (state === 'SUCCESS') {
                          component.set("v.isLoading", false);
                          resolve(response.getReturnValue());
                      } else if (state === 'ERROR') {
                          var errors = response.getError();
                          if (errors) {
                              if (errors[0] && errors[0].message) {
                                  console.log('Error message: ', errors[0].message);
                              }
                          } else {
                              console.log('Unknown error when retrieving Account data.');
                          }
                          component.set("v.isLoading", false);
                          reject('');
                      }
                  });
                  $A.enqueueAction(action);
            })
        );
    },

    // MParrella | Coastal Cloud | 03-07-2023 | TSK-00236400 | Creating new method to retrieve pricebook data from Custom MDT: START
    getPricebookData : function (component, event, helper) {
        return new Promise(
            $A.getCallback((resolve, reject) => {
                var action = component.get("c.getPricebookId");
                action.setParams({
                   salesOrgId : component.find("SalesOrg").get("v.value")
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        component.set("v.isLoading", false);
                        component.find("Pricebook").set("v.value", response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log('Error retrieving Pricebook Data: ', errors[0].message);
                            }
                        } else {
                            console.log('Unknown error when retrieving Pricebook data.');
                        }
                        component.set("v.isLoading", false);
                        reject('');
                    }
                });
                $A.enqueueAction(action);
            })
        );
    }
    // MParrella | Coastal Cloud | 03-07-2023 | TSK-00236400 | Creating new method to retrieve pricebook data from Custom MDT: STOP


})