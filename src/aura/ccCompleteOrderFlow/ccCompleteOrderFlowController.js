({
    doInit : function(component, event, helper) {
        component.set('v.id', component.get('v.pageReference').state.c__id);
        console.log('v.id: ' + component.get('v.id'));
        console.log('launchFlow');
        let flow = component.find("flowData");
        let inputs = [
            { 
                name: 'recordId',
                type: 'String',
                value: component.get('v.id')
            }
        ];
        flow.startFlow("B2B_Order_Payment", inputs);
    },

    onPageReferenceChange: function(component, event, helper) {
        component.set('v.id', component.get('v.pageReference').state.c__id);
        $A.get('e.force:refreshView').fire();
    },

    closeModalOnFinish : function(component, event, helper) {        
        if(event.getParam('status') === "FINISHED") {
            console.log('flow status: ' + event.getParam('status'));
            component.find("navService").navigate({
                type: 'standard__recordPage',
                attributes: {
                    recordId: component.get('v.id'),
                    objectApiName: 'ORDER',
                    actionName: 'view'
                }
            });
        }
    },

    handleNavigate : function(component, event, helper) {
        component.find("navService").navigate({
            type: 'standard__recordPage',
            attributes: {
                recordId: component.get('v.id'),
                objectApiName: 'ORDER',
                actionName: 'view'
            }
        });
    }
})