({
    doInit : function(component, event, helper) {
        component.set('v.id', component.get('v.pageReference').state.c__id);
        console.log('v.id: ' + component.get('v.id'));
    },

    onPageReferenceChange: function(component, event, helper) {
        component.set('v.id', component.get('v.pageReference').state.c__id);
        $A.get('e.force:refreshView').fire();
    },

    launchFlow : function(component, event, helper){
        console.log('launchFlow');
        // component.set('v.startFlow', true);
        
        let flow = component.find("flowData");
        let inputs = [
            { 
                name: 'recordId',
                type: 'String',
                value: component.get('v.id')
            }
        ];
        flow.startFlow("B2B_Order_Payment", inputs);
        component.find('quickProductAdd').launchFlow(true);
    },

    navigateToRecord : function(component, event, helper){
        console.log('navigateToRecord');
        component.find("navService").navigate({
            type: 'standard__recordPage',
                attributes: {
                    recordId: component.get('v.id'),
                    // objectApiName: 'Order',
                    actionName: 'view'
                }
        });
        $A.get('e.force:refreshView').fire();
    },

    closeTab : function(component, event, helper) {
      var workspaceAPI = component.find("workspace");
              workspaceAPI.getFocusedTabInfo().then(function(response) {
                  var focusedTabId = response.tabId;
                  workspaceAPI.closeTab({tabId: focusedTabId});
              })
              .catch(error => {
                  console.log('error: ', error);
                  component.find("navService").navigate({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: component.get('v.id'),
                        objectApiName: 'ORDER',
                        actionName: 'view'
                    }
                });
          });
    }
})