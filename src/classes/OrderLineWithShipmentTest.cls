@isTest
public with sharing class OrderLineWithShipmentTest {
    @testSetup static void setup() {
        Account account = new Account(Name='TestAccount');
        insert account;
        

		Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test',Model_Number__c='1265710',Material__c='1265710',Case_Quantity__c=3.0);
        insert Prod;
        Id Pricebook2Id=Test.getStandardPricebookId();
        Order Ord = new Order(ShippingFutureDate__c=Date.valueOf('2022-04-02'),Ship_To__c=null,Ship_To_Account__c=null,Reason_Code__c='100',name='Test',AccountId=account.Id,EffectiveDate=Date.valueOf('2022-04-02'),status='Draft',Pricebook2Id=Pricebook2Id,Distribution_Channel__c='00');
        insert Ord;
        PricebookEntry prcBuKInt = [Select Id from PricebookEntry where Product2Id=:Prod.id and Pricebook2Id=:Pricebook2Id and CurrencyIsoCode='USD' limit 1];
        
        OrderItem OrdItem = new OrderItem( 
            Quantity=3.0, 
            OrderId=Ord.id,
            Type='Order Product',
            PricebookEntryId=prcBuKInt.Id,
            UnitPrice=2,
            Quantity_Ordered__c=2,
            Quantity_Shipped__c=2,
            TotalLineAmount=20,
			product2Id = prod.Id
        );
        insert OrdItem;
        
        Shipment Shp = new Shipment();
        Shp.ShipToName='Test';
        insert Shp;
        Schema.ShipmentItem ShipItm= new Schema.ShipmentItem(); 
        ShipItm.Quantity=2.0;
        ShipItm.ShipmentId=Shp.Id;
        insert ShipItm;


    }
    @isTest static void testCartTaxForCartItemSuccessfullyInserted() {
        Test.startTest();
        Order Ord = [SELECT Id FROM Order LIMIT 1];
        List<Id> orderId = new List<Id>{Ord.Id};
        Id OrdItemId = [SELECT Id FROM OrderItem WHERE OrderId = :orderId LIMIT 1].Id;
        OrderLineWithShipment.getOrderLineWithShipment(Ord.Id,10);
        Test.stopTest();
    }
}