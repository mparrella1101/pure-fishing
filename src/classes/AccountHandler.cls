public class AccountHandler {
    public static Account insertAccountIfNotExists(Integer storeId, String accountName, String eComStoreID){
        Account account = null;
        try{
            List<Account> accounts = [SELECT Id FROM Account WHERE Magento_Store_ID__c = :storeId AND EComStoreID__c = :eComStoreID];
            if(accounts == NULL || accounts.size() == 0)
            {
                Account acc = new Account(Name=accountName);
                acc.Magento_Store_ID__c = storeId;
                acc.EComStoreID__c = eComStoreID;
                insert acc;
                account = acc;
            }
        } catch(DmlException e) {
            System.debug('A DML exception has occurred:'+
                         e.getMessage());
        }
        return account;
    }
    
    public static Boolean doesAccountExists(Integer storeId, String eComStoreID){
        Boolean account = false;
        try{
            List<Account> accounts = [SELECT Id FROM Account WHERE Magento_Store_ID__c = :storeId AND EComStoreID__c = :eComStoreID];
            if(accounts != NULL && accounts.size() > 0)
            {
                account =  true;
            }
        } catch(DmlException e) {
            System.debug('A DML exception has occurred:'+
                         e.getMessage());
        }
        return account;
    }
    
    public static Account getAccount(Integer storeId, String eComStoreID){
        Account account = null;
        try{
            List<Account> accounts = [SELECT Id FROM Account WHERE Magento_Store_ID__c = :storeId AND EComStoreID__c = :eComStoreID];
            if(accounts != NULL && accounts.size() > 0)
            {
                account = accounts.get(0);
            }
        } catch(DmlException e) {
            System.debug('A DML exception has occurred:'+
                         e.getMessage());
        }
        return account;
    }
}