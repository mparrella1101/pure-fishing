public class SalesOrderExtensionAttributes
{
    public List<SalesOrderShippingAssignment> shipping_assignments { get; set; }
}