@istest
    public class b2BCreateSearchIndexBatchTest {
        static testmethod void OrderTestMethod()
        {
            WebStore webStore = new WebStore(Name='TestWebStore');
            insert webStore;
            
            //List<WebStoreNetwork> WSN =[Select WebStoreId from WebStoreNetwork where WebStoreId=:webStore.Id ];
            List<WebStore> ws=[Select Id from WebStore where Id=:webStore.Id ];
            system.debug('ws '+ ws);
            String CRON_EXP = '0 0 0 15 3 ? *';
           
            // Create your test data
              Test.startTest();
                b2BCreateSearchIndexBatch CreateIndex = new b2BCreateSearchIndexBatch();
                database.executeBatch(CreateIndex,5);
                /*String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new b2BCreateSearchIndexSchedular());
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
                System.assertEquals(CRON_EXP, ct.CronExpression);
                System.assertEquals(0, ct.TimesTriggered);*/
    
            Test.stopTest();
       }
        
    }