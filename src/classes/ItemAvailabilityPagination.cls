public with sharing class ItemAvailabilityPagination {
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                if(size==null)
                    size = 200;
                List<Product2> productList = [SELECT JdeId__c FROM Product2 WHERE JdeId__c != NULL AND JdeId__c != '' AND MagentoId__c!= NULL];
                setCon = new ApexPages.StandardSetController(productList);
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }set;
    }
     
    Public List<Product2> getProducts(){
        List<Product2> prodList = new List<Product2>();
        for(Product2 p : (List<Product2>)setCon.getRecords())
            prodList.add(p);
        return prodList;
    }
     
    public pageReference refresh() {
        setCon = null;
        getProducts();
        setCon.setPageNumber(1);
        return null;
    }
     
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    /*public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }*/
  
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
  
    /*public void first() {
        setCon.first();
    }
  
    public void last() {
        setCon.last();
    }
  
    public void previous() {
        setCon.previous();
    }*/
  
    public void next() {
        setCon.next();
    }
}