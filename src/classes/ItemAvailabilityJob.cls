public class ItemAvailabilityJob implements Queueable {
    private String[] items{get;Set;}

    public ItemAvailabilityJob(String[] items){
        this.items = items;
    }
    
    public void execute(QueueableContext context) {
        if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
            System.debug('Making future calls from Queueable');
            EpJDESync.defensiveItemAvailability(items);
        } else {
            Notifier.sendEmail('ItemAvailabilityJob - Future from Queueable failed');
        }
    }
}