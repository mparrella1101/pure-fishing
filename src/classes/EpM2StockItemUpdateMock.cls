@isTest
global class EpM2StockItemUpdateMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json; charset=utf-8');
        response.setBody('"579"');
        response.setStatusCode(200);
        response.setStatus('Success');
        return response;
    }
}