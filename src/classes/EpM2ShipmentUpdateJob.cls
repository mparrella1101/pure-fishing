public class EpM2ShipmentUpdateJob implements Queueable {
    private List<ShipmentSalesOrderItem> shipments{get;Set;}
    private List<String> orderItemIDs{get;Set;}
    private List<String> ECRecordNames{get;Set;}
    private List<String> ShipmentNumbers{get;Set;}
    private List<String> order_ids{get;Set;}

    public EpM2ShipmentUpdateJob(List<ShipmentSalesOrderItem> shipments, List<String> orderItemIDs,
                                List<String> ECRecordNames, List<String> ShipmentNumbers, List<String> order_ids){
        this.shipments = shipments;
		this.orderItemIDs = orderItemIDs;
		this.ECRecordNames = ECRecordNames;
		this.ShipmentNumbers = ShipmentNumbers;
		this.order_ids = order_ids;
    }
    
    public void execute(QueueableContext context) {
		for(Integer index=0; index<shipments.size(); index++)
        {
            if(ECRecordNames.get(index) != null && ShipmentNumbers.get(index) == null) {
                ShipmentSalesOrderItem shpmnt = shipments.get(index);
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    Order order = [SELECT MagentoId__c, ECRecordName__c,EComStoreID__c FROM Order WHERE Id = :order_ids.get(index)];
                    ApexGovernorLimits.printLimitsUsed();
                    
                    String body = System.JSON.serialize(shpmnt);
                    System.debug('END JSON:'+body);
                    System.debug('ECRecordName:'+ECRecordNames.get(index));
                    
                    if (Limits.getQueries() > Limits.getLimitQueries()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit Queries governor limits');
                        MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :order.EComStoreID__c];
                        ApexGovernorLimits.printLimitsUsed();
                        String last_sync_time = store.LastSalesOrderSyncTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
        
                        List<String> itemList = new List<String>();
                        if(orderItemIDs.get(index).contains(','))
                            itemList = orderItemIDs.get(index).split(',');
                        else
                            itemList = new List<String>{orderItemIDs.get(index)};
                        
                        EpM2ShipmentUpdateSync.defensiveUpdateShipment(ECRecordNames.get(index), order.MagentoId__c,body, itemList, store.AccessToken__c, store.Name, last_sync_time, store.BaseURL__c, store.SupportEmail__c);   
                    }
                }
            }
        }
    }
}