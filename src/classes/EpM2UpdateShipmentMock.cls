@isTest
global class EpM2UpdateShipmentMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json; charset=utf-8');
        response.setBody('"2"');
        response.setStatusCode(200);
        response.setStatus('Success');
        return response;
    }
}