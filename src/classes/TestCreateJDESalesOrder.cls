@isTest(seeAllData=true)
public class TestCreateJDESalesOrder {
	static testMethod void testCreateJDESalesOrder() {
        Test.setMock(HttpCalloutMock.class, new EpJDECreateJDESalesOrderMock());
        Test.startTest();
		EpJDEOrderSync.defensiveCreateJDESalesOrder('8010B000000mZ0h','https://pfbridge.simple105.com/salesForceEndpoint.php');
        Test.stopTest();        
    }
}