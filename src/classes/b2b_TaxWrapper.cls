/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           04/04/2022            Initial Version
 */
// Created to have Tax payload wrapper.
public class b2b_TaxWrapper {
    public String division;	//00
	public String customerNumber;	//72142
	public String distributionChannel;	//00
	public String salesOrganization;	//0040
	public String orderType;	//OR
    public String shipTo;//72142
	public cls_products[] products;
	public String orderReason;	//100
	public String shipmentDate;	//2022-04-02
	public class cls_products {
		public String materialNumber;	//1265710
		public string quantity;	//2
	}

    public class response{
		public String materialNumber;	//1265710
		public string price;
		public string tax;
	}
	/*public static b2b_TaxWrapper parse(String json){
		return (b2b_TaxWrapper) System.JSON.deserialize(json, b2b_TaxWrapper.class);
	}*/

}