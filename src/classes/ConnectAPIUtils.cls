/**
 * @description Utility Class for accessing Connect API methods
 */
/* Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           April 19, 2022        Initial Version
    1.1         Matt Parrella           Coastal Cloud           October 12, 2022      Adding utility method to strip out '\t' characters from supplied text
 */

public without sharing class ConnectAPIUtils {

    /**
     * @description Get List, Unit, MAP and MSRP prices for multiple Products using the ProductId
     *
     * @param communityId The Salesforce 18-character Id associated with a Community
     * @param effectiveAccountId The Salesforce 18-character Id associated with the user's Account
     * @param productIds List of Salesforce 18-character Ids associated with the Product we are getting prices for
     *
     * @return Map of PriceWrapper objects where ProductId is the key and the PriceWrapper object is the value (containing all price values)
     */
    private static Map<String, ConnectApi.CartItemSortOrder> sortOrderCache = new Map<String, ConnectApi.CartItemSortOrder>();

    public static Map<String,PriceWrapper> getAllPriceData(String communityId, String webstoreId, String effectiveAccountId, List<String> productIds) {
        Map<String, PriceWrapper> priceWrapperMap = new Map<String, PriceWrapper>();

        // Get Webstore Id
        if (webstoreId == null || webstoreId == '') {
            webstoreId = ConnectAPIUtils.getWebstoreId(communityId);
        }


        // Get the Effective Account Id field value from the current user's record
        if (effectiveAccountId == null || effectiveAccountId == ''){
            try {
                effectiveAccountId = [SELECT Effective_Account_ID__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1].Effective_Account_ID__c;
            }
            catch(QueryException ex){
                effectiveAccountId = '';
            }
        }


        // Use Connect API to get the 'Unit' and 'List' pricing
        ConnectApi.PricingResult connectApiResult = getProductPricesCallout(webstoreId, effectiveAccountId, productIds);
        List<ConnectApi.PricingResultLineItem> connectApiLineItems = connectApiResult.pricingLineItemResults;

        // Start populating map of prices with List and Unit price values
        for(ConnectApi.PricingResultLineItem lineItem : connectApiLineItems){
            // The ConnectApi returns the 15-character product Id, so we need to modify the 18-character ID to remove the last 3 characters so we can successfully access the priceMap values
            String modifiedId = lineItem.productId.substring(0,15);
            if (priceWrapperMap.containsKey(modifiedId)){
                PriceWrapper currWrap = priceWrapperMap.get(modifiedId);
                currWrap.listPrice = lineItem.listPrice;
                currWrap.unitPrice = lineItem.unitPrice;
                priceWrapperMap.put(modifiedId, currWrap);
            } else {
                PriceWrapper priceWrap = new PriceWrapper();
                priceWrap.productId = modifiedId;
                priceWrap.listPrice = lineItem.listPrice;
                priceWrap.unitPrice = lineItem.unitPrice;
                priceWrapperMap.put(modifiedId, priceWrap);
            }
        }

        // Update Price Map with MSRP and MAP values
       getMapMsrpPrices(webstoreId, productIds, priceWrapperMap);


        return priceWrapperMap;
    }

    /**
     * @description  Helper method that will take in a Community ID and return the associated WebstoreId (There can be a max of 1 store
     * per community)
     *
     * @param communityId The 18-character Salesforce Community Id
     *
     * @return webstoreId The 18-character Salesforce Store Id
    */
    public static String getWebstoreId(String communityId){
        if (!Test.isRunningTest()) {
            if (communityId == null || communityId == ''){
                return null;
            }
        } else {
            // If we're running from a test perspective, there is no WebStoreNetwork record created
           return [SELECT Id FROM WebStore LIMIT 1].Id;
        }
        String webstoreId = null;

        // Check if the fields needed to acquire the webstoreId are accessible by the user
        if (Schema.SObjectType.WebStoreNetwork.fields.WebStoreId.isAccessible() &&
                Schema.SObjectType.WebStoreNetwork.fields.NetworkId.isAccessible()){
            List<WebStoreNetwork> webStoreList = new List<WebStoreNetwork>();
            webStoreList = [SELECT WebStoreId FROM WebStoreNetwork WHERE NetworkId =: communityId LIMIT 1];


            if (!webStoreList.isEmpty()){
                webstoreId = webStoreList[0].WebStoreId;
            }
        }
        return webstoreId;
    }

    /**
     * @description Get prices for multiple products using the Connect API
     *
     * @param webstoreId (Optional) The Salesforce 18-character ID associated with the WebStore
     * @param effectiveAccountId The Salesforce 18-character ID associated with the user's Account
     * @param productIds List of Salesforce 18-character IDs for Products that we wish to get prices for
     *
     * @return ConnectApi.PricingResult object containing product pricing information
     */
    public static ConnectApi.PricingResult getProductPricesCallout(String webstoreId, String effectiveAccountId, List<String> productIds){

        // Create list of PricingLineItemInput objects
        List<ConnectApi.PricingLineItemInput> priceLineInputs = new List<ConnectApi.PricingLineItemInput>();
        for (String currId : productIds) {
            ConnectApi.PricingLineItemInput itemInput = new ConnectApi.PricingLineItemInput();
            itemInput.productId = currId;
            priceLineInputs.add(itemInput);
        }

        // Assign the list of PricingLineItemInputs to the 'pricingLineItems' attribute of the 'PricingInput' object
        ConnectApi.PricingInput priceInput = new ConnectApi.PricingInput();
        priceInput.pricingLineItems = priceLineInputs;

        if (!Test.isRunningTest()) {
            return ConnectApi.CommerceStorePricing.getProductPrices(webstoreId, effectiveAccountId, priceInput);
        } else {
            ConnectApi.PricingResult mockResult = new ConnectApi.PricingResult();
            List<ConnectApi.PricingResultLineItem> mockLineItems = new List<ConnectApi.PricingResultLineItem>();
            ConnectApi.PricingResultLineItem mockLine = new ConnectApi.PricingResultLineItem();
            mockLine.unitPrice = '08.23';
            mockLine.listPrice = '07.17';
            mockLine.productId = productIds[0];
            mockLineItems.add(mockLine);
            mockResult.pricingLineItemResults = mockLineItems;
            mockResult.success = true;
            return mockResult;
        }
    }

    /**
     * @description Helper method to retrieve MAP and MSRP prices based on passed-in Product Ids
     *
     * @param webstoreId The Salesforce 18-character Id associated with the community's WebStore record
     * @param productIds A list of Salesforce 18-character Product Ids
     * @param priceWrapperMap A Map containing ProductIds and PriceWrapper objects to be updated with MSRP and MAP prices
     */
    @TestVisible
    private static void getMapMsrpPrices(String webstoreId, List<String> productIds, Map<String,PriceWrapper> priceWrapperMap){
        // Get Webstore SAP_Company__c value
        String companySAP = [SELECT SAP_Company__c FROM WebStore WHERE Id =: webstoreId LIMIT 1].SAP_Company__c;
        // Iterate over Company Product Info records and update the Price Map with MAP and MSRP price values
        for(Company_Product_Info__c companyInfo : [SELECT Id, Related_Product__c, MAP__c, MSRP__c FROM Company_Product_Info__c WHERE Related_Product__c IN :productIds AND SAP_Company__c =: companySAP]) {
            // The ConnectApi returns the 15-character product Id, so we need to modify the 18-character ID to remove the last 3 characters so we can successfully access the priceMap values
            String modifiedId = String.valueOf(companyInfo.Related_Product__c).substring(0,15);
            if (priceWrapperMap.containsKey(modifiedId)){
                PriceWrapper currWrap = priceWrapperMap.get(modifiedId);
                currWrap.unitPrice = currWrap.unitPrice;
                currWrap.listPrice = currWrap.listPrice;
                currWrap.mapPrice = String.valueOf(companyInfo.MAP__c);
                currWrap.msrpPrice = String.valueOf(companyInfo.MSRP__c);
                priceWrapperMap.put(modifiedId, currWrap);
            } else {
                PriceWrapper newWrap = new PriceWrapper();
                newWrap.productId = modifiedId;
                newWrap.mapPrice = companyInfo.MAP__c != null ? String.valueOf(companyInfo.MAP__c) : null;
                newWrap.msrpPrice = companyInfo.MSRP__c != null ? String.valueOf(companyInfo.MSRP__c) : null;
                priceWrapperMap.put(modifiedId, newWrap);
            }
        }
    }

    /**
     * @description Deletes a Suggested Order (Suggested_Order__c) record
     *
     * @param communityId The Salesforce 18-character Id associated with the community
     * @param suggestedOrderId The Id of the Suggested Order we wish to add a product to
     */

    @AuraEnabled
    public static void deleteSuggestedOrder(String communityId, String suggestedOrderId) {
        String webstoreId = getWebstoreId(communityId);
        Suggested_Order__c order;
        try {
            order = [SELECT Id FROM Suggested_Order__c WHERE WebStore__c =: webstoreId AND Id =: suggestedOrderId LIMIT 1];
            delete order;
        }
        catch(Exception ex){
            System.debug('*** ConnectAPIUtils.deleteSuggestedOrder() ERROR: ' + ex.getMessage());
        }
    }

    /**
     * @description Method to retrieve all Suggested_Order__c records associated with the user's Account and place them into a picklist field
     *
     * @param communityId The Salesforce 18-character Id associated with the community
     * @param webstoreId (optional) The Salesforce 18-character Id associated with the community's WebStore record
     * @param effectiveAccountId The Salesforce 18-character ID associated with the user's Account
     *
     * @return A list of SuggestedOrderOption objects used for displaying the lists in a drop down
     */
    @AuraEnabled
    public static List<SuggestedOrderOption> getSuggestedOrders(String communityId, String webstoreId, String effectiveAccountId) {
        if (webstoreId == null || webstoreId == ''){
            webstoreId = getWebstoreId(communityId);
        }

        if (effectiveAccountId == null || effectiveAccountId == '') {
            try {
                effectiveAccountId = [SELECT Effective_Account_Id__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1].Effective_Account_ID__c;
            }
            catch(QueryException ex) {
                System.debug('*** ConnectAPIUtils.getWishlists() ERROR: ' + ex.getMessage());
            }
        }

        List<Suggested_Order__c> orders = new List<Suggested_Order__c>();
        try {
            orders = [SELECT Id, Name FROM Suggested_Order__c WHERE Effective_Account__c =: effectiveAccountId AND WebStore__c =: webstoreId];
        }
        catch(QueryException ex) {
            System.debug('*** ConnectAPIUtils.getSuggestedOrders() found no records to return or encountered an error. ' + ex.getMessage());
        }

        List<SuggestedOrderOption> suggestedOrderOptions = new List<SuggestedOrderOption>();

        for (Suggested_Order__c order : orders) {
            SuggestedOrderOption wo = new SuggestedOrderOption();
            wo.label = order.Name;
            wo.value = order.Id;
            suggestedOrderOptions.add(wo);
        }

        return suggestedOrderOptions;
    }

    /**
     * @description Method to retrieve all Suggested_Order__c records associated with the user's Account and current Store
     *
     * @param communityId The Salesforce 18-character Id associated with the community
     * @param effectiveAccountId The Salesforce 18-character ID associated with the user's Account
     *
     * @return A list of Suggested_Order__c records
     */
    @AuraEnabled
    public static List<Suggested_Order__c> getAllSuggestedOrders(String communityId, String effectiveAccountId) {
        String webstoreId = getWebstoreId(communityId);
        return [SELECT Id, Name, CreatedDate, Expiration_Date__c, SuggestedOrderItemCount__c FROM Suggested_Order__c WHERE WebStore__c =: webstoreId AND Effective_Account__c =: effectiveAccountId];
    }

    /**
     * @description Method to create a new Suggested_Order__c. The Name of the Suggested Order will be entered by the user
     * on the front-end and passed into this function.
     *
     * @param orderName The provided name for the Suggested Order provided by the user
     * @param webstoreId The Salesforce 18-character Id associated with the community's WebStore record
     * @param effectiveAccountId The Salesforce 18-character ID associated with the user's Account
     * @param productQuantityMap A stringified array of 'productObj' objects containing a productId and quantity value (not used when adding to wishlist from cart)
     * @param expirationDate The selected expriation date for the wishlist
     *
     * @return The newly created Suggested_Order__c object
     */
    @AuraEnabled
    public static Suggested_Order__c createSuggestedOrder(String orderName, String webstoreId, String effectiveAccountId, String productQuantityMap, Date expirationDate)  {
        Suggested_Order__c newOrder;
        List<SuggestedOrderItemDataWrapper> items = new List<SuggestedOrderItemDataWrapper>();
        if (productQuantityMap != null){
            items = (List<SuggestedOrderItemDataWrapper>) JSON.deserialize(productQuantityMap, List<SuggestedOrderItemDataWrapper>.class);
        }
        try {
            // Create Suggested_Order__c record
            newOrder = new Suggested_Order__c(
                    Name = orderName,
                    Effective_Account__c = effectiveAccountId,
                    Expiration_Date__c = expirationDate,
                    WebStore__c = webstoreId
            );

            insert newOrder;

            // Create Suggested_Order_Item__c records
            List<Suggested_Order_Item__c> orderItems = new List<Suggested_Order_Item__c>();
            for (SuggestedOrderItemDataWrapper item : items) {
                orderItems.add(
                        new Suggested_Order_Item__c(
                                Quantity__c = item.quantity,
                                Product__c = item.productId,
                                Suggested_Order__c = newOrder.Id
                        )
                );
            }
            insert orderItems;
            return newOrder;
        }
        catch(DmlException ex) {
            System.debug('*** ConnectAPIUtils.createSuggestedOrder() ERROR: ' + ex.getMessage());
            return null;
        }
    }


    /**
     * @description Method used to add an item to a Suggested Order
     *
     * @param webstoreId The Salesforce 18-character Id associated with the community's WebStore record
     * @param effectiveAccountId The Salesforce 18-character ID associated with the user's Account
     * @param orderId The Id of the Suggested Order we wish to add a product to
     * @param productQuantityMap A stringified map containing the product's 18-character Salesforce Id as well as the quantity value
     *
     * @return The newly created Suggested_Order_Item__c record
     */
    @AuraEnabled
    public static Suggested_Order_Item__c addToSuggestedOrder(String webstoreId, String effectiveAccountId, String orderId, String productQuantityMap) {
        List<SuggestedOrderItemDataWrapper> items = new List<SuggestedOrderItemDataWrapper>();
        Product2 productRecord;
        if (productQuantityMap != null){
            items = (List<SuggestedOrderItemDataWrapper>) JSON.deserialize(productQuantityMap, List<SuggestedOrderItemDataWrapper>.class);
            productRecord = [SELECT Id, Name FROM Product2 WHERE Id =: items[0].productId LIMIT 1];
        }


        Boolean foundExisting = false;

        // Check if product is already in list
        Suggested_Order_Item__c newItem = new Suggested_Order_Item__c();
        for (Suggested_Order_Item__c currItem : [SELECT Id, Quantity__c, Product__c FROM Suggested_Order_Item__c WHERE Suggested_Order__c =: orderId]){
            if (currItem.Product__c == productRecord.Id) {
                // Just update quantity
                newItem.Quantity__c = items[0].quantity;
                newItem.Id = currItem.Id;
                foundExisting = true;
            }
        }
        if (!foundExisting){
            newItem = new Suggested_Order_Item__c(
                    Product__c = items[0].productId,
                    Quantity__c = items[0].quantity,
                    Suggested_Order__c = orderId
            );
        }

        try {
            upsert newItem;
            return newItem;
        }
        catch(DmlException ex){
            System.debug('*** ConnectAPIUtils.addToSugestedOrder ERROR: ' + ex.getMessage());
            return null;
        }
    }

    /**
   * @description Given a sortParam string, return null or the relavent ConnectApi.CartItemSortOrder enum value
   *              See https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/connectAPI_enums.htm#cartItemSortOrderEnum for the enum values
   * @param sortParam A string representing a sort Param.
   *
   * @return resolvedSortParam A ConnectApi.CartItemSortOrder enum value or null
   *
   * @example
   * ConnectApi.CartItemSortOrder sortParam = resolveSortParam('CreatedDateAsc');
   */
    public static ConnectApi.CartItemSortOrder resolveSortParam(String sortParam) {
        if (sortOrderCache.isEmpty()) {
            for (
                ConnectApi.CartItemSortOrder sortOrder : ConnectApi.CartItemSortOrder.values()
            ) {
                sortOrderCache.put(sortOrder.name(), sortOrder);
            }
        }
        return sortOrderCache.get(sortParam);
    }

    /**
     * @description Returns Suggested Order record data based on passed in Suggested Order Id
     *
     * @param communityId The Salesforce 18-character Id associated with the community
     * @param effectiveAccountId The Salesforce 18-character ID associated with the user's Account
     * @param orderId The Salesforce 18-character ID Associated with the Suggested Order
     * @param productFields A comma-separated String of product api field names to include in the request
     *
     * @return ConnectApi.Wishlist A Wishlist object containing all data
     */
    @AuraEnabled
    public static SuggestedOrderWrapper getSuggestedOrder(String communityId, String effectiveAccountId, String orderId, String productFields) {
        SuggestedOrderWrapper returnWrapper = new SuggestedOrderWrapper();
        String webstoreId = getWebstoreId(communityId);
        String[] productFieldsArray = productFields.split(',');

        // Get Suggested Order Data
        Suggested_Order__c order = [SELECT Id, CreatedDate, LastModifiedDate, Expiration_Date__c,  Name, SuggestedOrderItemCount__c FROM Suggested_Order__c WHERE Id =: orderId AND Effective_Account__c =: effectiveAccountId AND WebStore__c =: webstoreId LIMIT 1];
        returnWrapper.name = order.Name;
        returnWrapper.orderId = order.Id;
        returnWrapper.orderProductCount = Integer.valueOf(order.SuggestedOrderItemCount__c);
        returnWrapper.createdDate = order.CreatedDate.date();
        returnWrapper.modifiedDate = order.LastModifiedDate.date();
        returnWrapper.expirationDate = order.Expiration_Date__c;
        // Build dynamic query string for retrieving Product2 field data
        String queryString = 'SELECT Id, Quantity__c, Product__c, ';
        for(String productField : productFieldsArray) {
            queryString += 'Product__r.' + productField +', ';
        }
        queryString += ' Name FROM Suggested_Order_Item__c WHERE Suggested_Order__c = ' + '\'' + orderId + '\'';


        List<Suggested_Order_Item__c> orderItems = Database.query(queryString);
        List<SuggestedOrderItemDataWrapper> orderItemWrappers = new List<SuggestedOrderItemDataWrapper>();

        if (!orderItems.isEmpty()) {
            // Build list of product2Ids so we can get pricing
            List<String> productIds = new List<String>();
            for(Suggested_Order_Item__c item : orderItems){
                productIds.add(item.Product__c);
            }

            // Get Price Data
            Map<String,PriceWrapper> priceData = getAllPriceData(communityId, null, effectiveAccountId, productIds);


            // Build list of wrapper objects to send back to front-end
            for (Suggested_Order_Item__c orderItem : orderItems) {
                SuggestedOrderItemDataWrapper newWrap = new SuggestedOrderItemDataWrapper();
                newWrap.productId = orderItem.Product__c;

                String modifiedId = orderItem.Product__c.to15();

                newWrap.quantity = orderItem.Quantity__c;
                newWrap.imageAltText = orderItem.Product__r.Name;
                newWrap.photoURL = orderItem.Product__r.Hero_Image__c.split('"')[1];
                newWrap.modelNumber = orderItem.Product__r.Model_Number__c;
                newWrap.materialNumber = orderItem.Product__r.Material__c;
                newWrap.casePack = Integer.valueOf(orderItem.Product__r.Case_Quantity__c);
                newWrap.orderItemId = orderItem.Id;
                newWrap.isPlano = orderItem.Product__r.Product_Type__c == 'Plano Legacy' ? true : false;
                newWrap.salesPrice = priceData.get(modifiedId)?.unitPrice != null ? Decimal.valueOf(priceData.get(modifiedId)?.unitPrice) : 0.00;
                newWrap.totalPrice = orderItem.Quantity__c * newWrap.salesPrice;
                newWrap.productName = orderItem.Product__r.Name;
                newWrap.productUrl = '/product/' + orderItem.Product__c;
                orderItemWrappers.add(newWrap);
            }
            returnWrapper.orderItems = orderItemWrappers;
        }


        return returnWrapper;
    }

    /**
     * @description Using the ConnectApi.getCartItems() function, this method will process the returned Cart/Product data and package it
     *              into wrapper classes for ease of access from within the 'cc_cart_wishlist_item' LWC.
     *
     * @param communityId The 18-character Salesforce Community Id
     * @param effectiveAccountId The current user's 18-character Salesforce Account record Id
     * @param activeCartOrId The 18-character Salesforce Id representing the Cart object within the Community
     * @param productFields A comma-separated String of product api field names to include in the request
     * @param pageParam Specifies the page token to use to view a page of information. Page tokens are returned as part of the response class, such as currentPageToken or nextPageToken. If you pass in null, the first page is returned.
     * @param pageSize Specifics the number of records per page. Default is 25. (Accepted values: 1 - 100)
     * @param sortParam Sort order for items in a cart. If null, default is CreatedDateDesc (Accepted values: CreatedDateAsc, CreatedDateDesc, NameAsc, NameDesc, SalesPriceAsc, SalesPriceDesc)
     *
     * @return ConnectApi.CartItemCollection object containing Cart/Product Item data
     */
    @AuraEnabled
    public static ConnectApi.CartItemCollection getCartItems(String communityId, String effectiveAccountId, String activeCartOrId, String productFields, String pageParam, Integer pageSize, String sortParam) {
        // Resolve 'sortParam' into ConnectApi.CartItemSortOrder record
        ConnectApi.CartItemSortOrder resolvedSortParam = ConnectAPIUtils.resolveSortParam(sortParam);

        // Resolve CommunityId to webstoreId
        String webstoreId = ConnectAPIUtils.getWebstoreId(communityId);

        // Call ConnectApi.getCartItems() method to retrieve cart/product data
        ConnectApi.CartItemCollection cartItemCollection;
        try {
            if (!Test.isRunningTest()){
                return ConnectApi.CommerceCart.getCartItems(webstoreId,
                        effectiveAccountId,
                        activeCartOrId,
                        productFields,
                        pageParam,
                        pageSize,
                        resolvedSortParam);
            } else {
                return new ConnectApi.CartItemCollection();
            }

        }
        catch(Exception ex) {
            return null;
        }
    }

    /**
     * @description Using the ConnectApi.CommerceCart.updateCartItem() function, this method will attempt to update the
     * a CartItem record that is in a specific cart.
     *
     * @param cartItemId The 18-character Salesforce Id representing the CartItem object that we are updating
     * @param quantity The quantity value that will be applied to the CartItem
     *
     * @return ConnectApi.CartItem object representing the latest data for the CartItem record we just updated
     */

    @AuraEnabled
    public static String updateCartItem(String cartItemId, Decimal quantity) {
        CartItem currItem = [SELECT Id, Quantity, UnitAdjustedPrice FROM CartItem WHERE Id =: cartItemId LIMIT 1];
        currItem.Quantity = quantity;
        currItem.TotalPrice = currItem.Quantity * currItem.UnitAdjustedPrice;
        update currItem;

        return currItem.Id;
    }

    /**
     * @description  Helper method to update a Suggested Order Item record's Quantity__c value (from the Suggested Order detail page)
     *
     * @param orderItemId The 18-character Salesforce Id representing the Suggested Order Item object that we are updating
     * @param quantity The quantity value that will be applied to the Suggested Order Item
     *
     * @return The 18-character Salesforce Id associated to the Suggested Order Item record that was just modified
     */
    @AuraEnabled
    public static String updateOrderItemQuantity(String orderItemId, Decimal quantity) {
        Suggested_Order_Item__c currItem = [SELECT Id, Quantity__c  FROM Suggested_Order_Item__c WHERE Id =: orderItemId LIMIT 1];
        currItem.Quantity__c = quantity;
        try {
            update currItem;
            return currItem.Id;
        }
        catch(DmlException ex){
            System.debug(ex);
            return null;
        }
    }

    /**
     * @description Using the ConnectApi.CommerceCart.deleteCartItem function, this method will remove a selected
     *              CartItem from a Cart.
     *
     * @param communityId The 18-character Salesforce Community Id
     * @param effectiveAccountId The current user's 18-character Salesforce Account record Id
     * @param activeCartOrId The 18-character Salesforce Id representing the Cart object within the Community
     * @param cartItemId The 18-character Salesforce Id representing the CartItem object that we are updating
     */
    @AuraEnabled
    public static void deleteCartItem(String communityId, String effectiveAccountId, String activeCartOrId, String cartItemId) {
        String webstoreId = getWebstoreId(communityId);
        ConnectApi.CommerceCart.deleteCartItem(
                webstoreId,
                effectiveAccountId,
                activeCartOrId,
                cartItemId
        );
    }

    /**
     * @description Removes a Suggested Order Item from a Suggested Order
     *
     * @param communityId The 18-character Salesforce Community Id
     * @param effectiveAccountId The current user's 18-character Salesforce Account record Id
     * @param orderId The 18-character Salesforce Id associated to the Suggested order record
     * @param orderItemId The 18-character Salesforce Id associated with the Suggested Order Item we have to remove
     *
     * @return The 18-character Salesforce Id of the Suggested Order Item that was deleted
     */
    @AuraEnabled
    public static String deleteSuggestedOrderItem(String communityId, String effectiveAccountId, String orderId, String orderItemId) {
        String webstoreId = getWebstoreId(communityId);
        Suggested_Order_Item__c item;
        try {
            item = [SELECT Id FROM Suggested_Order_Item__c WHERE Id =: orderItemId LIMIT 1];
            delete item;
            return item.Id;
        }
        catch(Exception ex) {
            System.debug('*** ConnectAPIUtils.deleteSuggestedOrderItem() ERROR: ' + ex.getMessage());
            return null;
        }
    }

    /**
     * @description Using the ConnectApi.CommerceCate.deleteCart function, this method will delete a Cart record
     * from the store.
     *
     * @param communityId The 18-character Salesforce Community Id
     * @param effectiveAccountId The current user's 18-character Salesforce Account record Id
     * @param activeCartOrId The 18-character Salesforce Id representing the Cart object within the Community
     */
    @AuraEnabled
    public static void deleteCart(String communityId, String effectiveAccountId, String activeCartOrId) {
        String webstoreId = ConnectAPIUtils.getWebstoreId(communityId);
        ConnectApi.CommerceCart.deleteCart(
                webstoreId,
                effectiveAccountId,
                activeCartOrId
        );
    }

    /**
     * @description Using the ConnectApi.CommerceCart.createCart function, this method will create a new Cart record
     * within the store
     *
     * @param communityId The 18-character Salesforce Community Id
     * @param effectiveAccountId The current user's 18-character Salesforce Account record Id
     *
     * @return ConnectApi.CartSummary A CartSummary object containing detailed information about the cart
     */
    @AuraEnabled
    public static ConnectApi.CartSummary createCart(String communityId, String effectiveAccountId){
        String webstoreId = ConnectAPIUtils.getWebstoreId(communityId);
        ConnectApi.CartInput cartInput = new ConnectApi.CartInput();
        cartInput.effectiveAccountId = effectiveAccountId;
        return ConnectApi.CommerceCart.createCart(webstoreId, cartInput);
    }

    /**
     * @description Using the ConnectApi.CommerceCart.getCartSummary function, this method will retrieve the latest cart information
     *
     * @param communityId
     * @param effectiveAccountId
     * @param activeCartOrId
     *
     * @return ConnectApi.CartSummary object
     */
    @AuraEnabled
    public static ConnectApi.CartSummary getCartSummary(String communityId, String effectiveAccountId, String activeCartOrId) {
        String webstoreId = getWebstoreId(communityId);
        return ConnectApi.CommerceCart.getCartSummary(
                webstoreId,
                effectiveAccountId,
                activeCartOrId
        );
    }

    /**
     * @description     Helper method to parse out specific characters within a specified string of text
     *
     * @param           patternsToRemove (List<String>) The text pattern that we will be searching and removing from the text string
     * @param           sourceText (String) The text that we will be searching and modifying
     *
     * @return          The parsed string
     */
    public static String removeCharacters(List<String> patternsToRemove, String sourceText) {
        String modifiedText = '';
        if (patternsToRemove == null || patternsToRemove.size() == 0) {
            return sourceText;
        }
        else if (sourceText == '' || sourceText == null) {
            return '';
        }
        for(String pattern : patternsToRemove) {
            switch on pattern {
                when '\t' {
                    sourceText = sourceText.replaceAll(pattern, '    '); // Replaces Tab character with 4 spaces
                    System.debug('tab modifiedText: ' + modifiedText);
                }
                when else {
                    sourceText = sourceText.replaceAll(pattern, ''); // Replaces any other pattern listed in the 'patternsToRemove' list with a space
                    System.debug('else modifiedText: ' + modifiedText);
                }
            }
        }
        modifiedText = sourceText;

        return modifiedText;
    }

    /* WRAPPER CLASSES: START */
    public class PriceWrapper {
        @AuraEnabled public String productId;
        @AuraEnabled public String listPrice;
        @AuraEnabled public String unitPrice;
        @AuraEnabled public String msrpPrice;
        @AuraEnabled public String mapPrice;
        public PriceWrapper(String productId, String listP, String unit, String msrp, String mapP){
            this.productId = productId;
            this.listPrice = listP;
            this.unitPrice = unit;
            this.msrpPrice = msrp;
            this.mapPrice = mapP;
        }
        public PriceWrapper() {
            productId = '';
            listPrice = null;
            unitPrice = null;
            msrpPrice = null;
            mapPrice = null;
        }
    }

    public class SuggestedOrderOption {
        @AuraEnabled public String value;
        @AuraEnabled public String label;
    }

    public class SuggestedOrderWrapper {
        @AuraEnabled public String name;
        @AuraEnabled public String orderId;
        @AuraEnabled public Integer orderProductCount;
        @AuraEnabled public Date modifiedDate;
        @AuraEnabled public Date createdDate;
        @AuraEnabled public Date expirationDate;
        @AuraEnabled public List<SuggestedOrderItemDataWrapper> orderItems;
    }

    public class SuggestedOrderItemDataWrapper {
        @AuraEnabled public String productId;
        @AuraEnabled public String productName;
        @AuraEnabled public String modelNumber;
        @AuraEnabled public String materialNumber;
        @AuraEnabled public Integer casePack;
        @AuraEnabled public Decimal quantity;
        @AuraEnabled public Boolean isPlano;
        @AuraEnabled public String photoURL;
        @AuraEnabled public String productUrl;
        @AuraEnabled public Decimal salesPrice;
        @AuraEnabled public Decimal totalPrice;
        @AuraEnabled public Decimal listPrice;
        @AuraEnabled public String imageAltText;
        @AuraEnabled public String orderItemId;
    }

    /* WRAPPER CLASSES: STOP */
}