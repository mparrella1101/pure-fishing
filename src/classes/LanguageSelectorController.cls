/**
 * @description     Apex controller class for the 'cc_language_selector' LWC
 * @author          Coastal Cloud (Matt Parrella)
 * @date            February 14, 2023
 */

/* Author                  Company                 TaskRay                  Date            Description
-------------------------------------------------------------------------------------------------------------------------
    Matt Parrella           Coastal Cloud          TSK-00236425             02-14-2023      Initial version
 */

public with sharing class LanguageSelectorController {

    /**
     * @description     This method will take in a User record Id, retrieve that user's language setting (to be used as the default value), as well as
     *                  retrieve a list of supported/active languages for the B2B Experience
     *
     * @param           userId The Salesforce 18-character record Id associated with the User whom which we need to retrieve the default language for
     *                  (This is usually the running user)
     * @param           communityId The Salesforce 18-character Id associated with the B2B Experience for which we wish to retrieve active/supported languages
     *                  for
     *
     * @return          (ApexOperationData) A Wrapper object containing a list of SelectOption objects containing the language key as the value and the language label as the label:
     *                  'langOptions' => List<SelectOption>
     *                  'defaultLang' => String
     */
    @AuraEnabled
    public static LanguageData getLanguageSelections(String userId, String communityId) {
        LanguageData returnData = new LanguageData();
        List<LanguageOption> langOptions = new List<LanguageOption>();


        // Get User's language setting
        String userLang = [SELECT LanguageLocaleKey FROM User WHERE Id =: userId LIMIT 1]?.LanguageLocaleKey;

        // Get and validate the webstore Id using the passed-in community Id
        String webstoreId = ConnectAPIUtils.getWebstoreId(communityId);
        if (webstoreId == null || webstoreId == '') {
            System.debug('*** [LanguageSelectorController DEBUG] Error: No Webstore Id found for Community Id: ' + communityId + ' ***');
            return returnData;
        }

        // Get and validate the active/supported languages from the WebStore object
        String supportedLangs = [SELECT SupportedLanguages FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.SupportedLanguages;
        if (supportedLangs == null || supportedLangs == '') {
            System.debug('*** [LanguageSelectorController DEBUG] Error: No SupportedLanguage value found for Webstore Id: ' + webstoreId + ' ***');
            return returnData;
        }

        // Access the 'Language Mapping' Custom MDT to retrieve the language key value as well as the language label value
        Map<String,Language_Mapping__mdt> languageMDT = Language_Mapping__mdt.getAll();

        // Delimit the supported languages string to a list
        List<String> supportedLangDelim = supportedLangs.split(';');

        // Iterate over supported languages delimited string and get language values from custom metadata to pass back to front end
        for (String currString : supportedLangDelim) {
            if (languageMDT.containsKey(currString)) {
                LanguageOption newSelect = new LanguageOption(currString, languageMDT.get(currString).Language_Label__c);
                langOptions.add(newSelect);
            }
        }

        returnData.recordData.put('langOptions', langOptions);
        returnData.recordData.put('defaultLang', userLang);

        return returnData;
    }

    /**
     * @description         This method will be used to update the language setting on the current B2B user's record in order to re-render the
     *                      the B2B Store page(s) in that selected language
     *
     * @param               userId The Salesforce 18-character record Id associated with the User record we wish to update the language setting for
     * @param               selectedLanguage The LanguageLocaleKey value that will be set on the User's record
     *
     * @return              True - DML Successful; False - Error occurred
     */
    @AuraEnabled
    public static Boolean updateLanguageSetting(String userId, String selectedLanguage) {
        try {
            User usrRecord = [SELECT LanguageLocaleKey FROM User WHERE Id =: userId LIMIT 1];
            usrRecord.LanguageLocaleKey = selectedLanguage;
            update usrRecord;
            return true;
        }
        catch(DmlException ex) {
            System.debug('*** [LanguageSelectorController DEBUG] updateLanguageSetting error occurred when trying to update the Users language. Details: ' + ex.getMessage() + ' ***');
            return false;
        }
    }

    public class LanguageData {
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String errorMsg;
        @AuraEnabled public Map<String,Object> recordData;

        public LanguageData(){
            this.isSuccess = true;
            this.recordData = new Map<String,Object>();
            this.errorMsg = '';
        }
    }

    public class LanguageOption {
        @AuraEnabled public String value;
        @AuraEnabled public String label;

        public LanguageOption(String value, String label) {
            this.value = value;
            this.label = label;
        }
    }
}