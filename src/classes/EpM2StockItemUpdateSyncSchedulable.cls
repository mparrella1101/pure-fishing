public class EpM2StockItemUpdateSyncSchedulable implements Schedulable {
    private String  sku{get;Set;}
    private String qty{get;Set;}
    private String  eComStoreID{get;Set;}

    public EpM2StockItemUpdateSyncSchedulable(String  sku, String qty, String eComStoreID){
        this.sku = sku;
        this.qty = qty;
        this.eComStoreID = eComStoreID;
    }
    public void execute(SchedulableContext sc) {
        if(System.Test.isRunningTest()) {
            System.enqueueJob(new EpM2StockItemUpdateJob(sku, qty,eComStoreID));
        }
        if(this.sku != null) {
            if(System.isFuture() || System.isBatch() || Limits.getFutureCalls() < Limits.getLimitFutureCalls()) { // if future call limit is available
                EpM2StockItemUpdateSync.defensiveStockItemUpdateSynchronously(sku, qty, eComStoreID);
            } else if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) { // if queueable call limit is available
                System.enqueueJob(new EpM2StockItemUpdateJob(sku, qty,eComStoreID));
            } else {
                Notifier.sendEmail('EpM2StockItemUpdateSyncSchedulable - Limits reached - Cannot update sku:'+sku);
            }
        }
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}