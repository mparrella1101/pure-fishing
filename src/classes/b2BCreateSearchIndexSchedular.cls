/**
 * @description b2BCreateSearchIndexSchedular is Schedular class for b2BCreateSearchIndexBatch
 *
 *
 */
/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/24/2022            Initial Version
 */
global class b2BCreateSearchIndexSchedular implements Schedulable {
        global void execute(SchedulableContext SC)  {

            b2BCreateSearchIndex sqrb = new b2BCreateSearchIndex();
                sqrb.b2BCreateSearchIndex();
            
        }
    }