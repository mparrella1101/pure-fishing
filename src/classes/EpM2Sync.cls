global class EpM2Sync {
    @future(callout=true)
    public static void getCustomerById(String item, String id, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setEndpoint(baseURL+'?function=getCustomerByID&customer_id='+id);
        /*request.setHeader('authorization', 'Bearer ' + token);*/
        request.setMethod('GET');
        
        System.debug('EpM2Sync - Request: ' + request.getBody());
        try {
            response = http.send(request);

            if(response.getStatusCode() == 200) {
                String jsonString = response.getBody();
                System.debug('EpM2Sync - Response: ' + jsonString);
                if(System.Test.isRunningTest()) {
                    jsonString = '{"id":"5468703293600","email":"william@simple101.com","firstname":"William","lastname":"Leisey","phone":null,"addresses":[{"region":{"region_code":"PA","region":"Pennsylvania","region_id":0},"region_id":0,"country_id":"US","street":["133 Rouen Place",""],"telephone":"+17176395591","postcode":"17022","city":"Elizabethtown","firstname":"William","lastname":"Leisey","default_shipping":0}]}';
                }
                Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonString);
                
                String m_id = ((String)root.get('id'));
                System.debug('m_id: ['+m_id+']');
                if(m_id == NULL)
                    m_id = '0';
                
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    List<Contact> customers = [SELECT Id FROM Contact WHERE MagentoId__c = :m_id AND EComStoreID__c = :eComStoreID];
                    ApexGovernorLimits.printLimitsUsed();
                    System.debug(customers);
                    System.debug('getCustomerById customers size:'+customers.size());
                        
                    if(customers.size() == 0) {
                        ECRecord__c record = new ECRecord__c();
                        record.Dump__c = jsonString;
                        record.MagentoAPIStatus__c = true;
                        record.MagentoExecutionTime__c = DateTime.now();
                        record.MagentoResponse__c = 'Success';
                        record.Type__c = 'Customer Create';
                        record.EComStoreID__c = eComStoreID;
                        if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit DML governor limits');
                            insert record;
                            ApexGovernorLimits.printLimitsUsed();
                        }   
                    }
                    createECRecordOrder(JSON.deserializeUntyped(item), eComStoreID, supportEmail);
                }
            }/* else {
                 Notifier.sendEmail('EpM2Sync - getCustomerById - Error: '+ response.toString());
            }*/
        } catch(System.Exception e) {
            System.debug('EpM2Sync - getCustomerById - ' + id + ' - Exception error: '+ e);
            System.debug(response.toString());
            Notifier.sendEmail('EpM2Sync - getCustomerById - ' + id + ' - Exception error: '+ e, supportEmail);
        }
    }
    
    public static void defensiveSalesOrdersCreatedAfter(String now_sync_time, Double pageSize, Double currentPage, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        System.debug('defensiveSalesOrdersCreatedAfter Started');
        if(System.isFuture() || System.isBatch()){
            getSalesOrdersCreatedAfterSync(now_sync_time, pageSize, currentPage, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
        } else if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()) {
            getSalesOrdersCreatedAfter(now_sync_time, pageSize, currentPage, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
        } else {
            Notifier.sendEmail('EpM2Sync - defensiveSalesOrdersCreatedAfter - @future call was denied');
            System.debug('what happened');
        }
    }
    
    @future(callout=true)
    public static void getSalesOrdersCreatedAfter(String now_sync_time, Double pageSize, Double currentPage, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        getSalesOrdersCreatedAfterSync(now_sync_time, pageSize, currentPage, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
    }
    
    public static void getSalesOrdersCreatedAfterSync(String now_sync_time, Double pageSize, Double currentPage, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        System.debug('Get Sales Orders Created After Sync Started');
        ApexGovernorLimits.printMaxLimits();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();

        //request.setEndpoint(baseURL+'orders?searchCriteria[filter_groups][0][filters][0][field]=updated_at&searchCriteria[filter_groups][0][filters][0][value]='+lastSalesOrderSyncTime+'&searchCriteria[filter_groups][0][filters][0][condition_type]=from&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq&searchCriteria[filter_groups][2][filters][0][field]=updated_at&searchCriteria[filter_groups][2][filters][0][value]='+now_sync_time+'&searchCriteria[filter_groups][2][filters][0][condition_type]=to&searchCriteria[filter_groups][3][filters][0][field]=fraud_status&searchCriteria[filter_groups][3][filters][0][condition_type]=notnull&searchCriteria[filter_groups][4][filters][0][field]=fraud_status&searchCriteria[filter_groups][4][filters][0][value]&searchCriteria[filter_groups][4][filters][0][condition_type]=neq&searchCriteria[pageSize]='+((Integer)pageSize)+'&searchCriteria[currentPage]='+((Integer)currentPage));
        request.setEndpoint(baseURL+'?function=getOrdersSinceLastSync&lastSalesOrderSyncTime='+lastSalesOrderSyncTime);
        request.setHeader('authorization', 'Bearer ' + token);
        request.setMethod('GET');
        
        System.debug('EpM2Sync - Request: ' + request.getBody());
        try {
            
            response = http.send(request);

            String jsonString = response.getBody();
            if(System.Test.isRunningTest()){
                jsonString = '{"items":[{"applied_rule_ids":"13","base_currency_code":"USD","base_discount_amount":-5.39,"base_grand_total":48.55,"base_discount_tax_compensation_amount":0,"base_shipping_amount":0,"base_shipping_discount_amount":0,"base_shipping_discount_tax_compensation_amnt":0,"base_shipping_incl_tax":0,"base_shipping_tax_amount":0,"base_subtotal":53.94,"base_subtotal_incl_tax":53.94,"base_tax_amount":0,"base_total_due":48.55,"base_to_global_rate":1,"base_to_order_rate":1,"billing_address_id":23712,"coupon_code":"CaboodlesGet10","created_at":"2018-12-26 19:30:30","customer_email":"kthusband@gmail.com","customer_group_id":0,"customer_is_guest":1,"customer_note_notify":1,"discount_amount":-5.39,"discount_description":"CaboodlesGet10","email_sent":1,"entity_id":11856,"global_currency_code":"USD","grand_total":48.55,"discount_tax_compensation_amount":0,"increment_id":"40011197","is_virtual":0,"order_currency_code":"USD","protect_code":"6fce96a47a24bc7720efaa0e3d4fd0aa","quote_id":41784,"remote_ip":"65.217.185.66","shipping_amount":0,"shipping_description":"United Parcel Service - Ground","shipping_discount_amount":0,"shipping_discount_tax_compensation_amount":0,"shipping_incl_tax":0,"shipping_tax_amount":0,"state":"processing","status":"processing","store_currency_code":"USD","store_id":4,"store_name":"Caboodles Website Caboodles Stor","store_to_base_rate":0,"store_to_order_rate":0,"subtotal":53.94,"subtotal_incl_tax":53.94,"tax_amount":0,"total_due":48.55,"total_item_count":1,"total_qty_ordered":6,"updated_at":"2018-12-26 19:31:49","weight":2.4,"items":[{"amount_refunded":0,"applied_rule_ids":"13","base_amount_refunded":0,"base_discount_amount":5.39,"base_discount_invoiced":0,"base_discount_tax_compensation_amount":0,"base_original_price":8.99,"base_price":8.99,"base_price_incl_tax":8.99,"base_row_invoiced":0,"base_row_total":53.94,"base_row_total_incl_tax":53.94,"base_tax_amount":0,"base_tax_invoiced":0,"created_at":"2018-12-26 19:30:30","discount_amount":5.39,"discount_invoiced":0,"discount_percent":10,"free_shipping":0,"discount_tax_compensation_amount":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30969,"name":"Take It™","no_discount":0,"order_id":11856,"original_price":8.99,"price":8.99,"price_incl_tax":8.99,"product_id":433,"product_type":"configurable","qty_canceled":0,"qty_invoiced":0,"qty_ordered":6,"qty_refunded":0,"qty_returned":0,"qty_shipped":6,"quote_item_id":99867,"row_invoiced":0,"row_total":53.94,"row_total_incl_tax":53.94,"row_weight":2.4,"sku":"CAB56062A","store_id":4,"tax_amount":0,"tax_invoiced":0,"tax_percent":0,"updated_at":"2018-12-28 21:10:28","weight":0.4},{"amount_refunded":0,"base_amount_refunded":0,"base_discount_amount":0,"base_discount_invoiced":0,"base_price":0,"base_row_invoiced":0,"base_row_total":0,"base_tax_amount":0,"base_tax_invoiced":0,"created_at":"2018-12-26 19:30:30","discount_amount":0,"discount_invoiced":0,"discount_percent":0,"free_shipping":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30970,"name":"Take It™ - Sky Blue","no_discount":0,"order_id":11856,"original_price":0,"parent_item_id":30969,"price":0,"product_id":381,"product_type":"simple","qty_canceled":0,"qty_invoiced":0,"qty_ordered":6,"qty_refunded":0,"qty_returned":0,"qty_shipped":0,"quote_item_id":99868,"row_invoiced":0,"row_total":0,"row_weight":0,"sku":"CAB56062A","store_id":4,"tax_amount":0,"tax_invoiced":0,"tax_percent":0,"updated_at":"2018-12-26 19:30:30","weight":0.4,"parent_item":{"amount_refunded":0,"applied_rule_ids":"13","base_amount_refunded":0,"base_discount_amount":5.39,"base_discount_invoiced":0,"base_discount_tax_compensation_amount":0,"base_original_price":8.99,"base_price":8.99,"base_price_incl_tax":8.99,"base_row_invoiced":0,"base_row_total":53.94,"base_row_total_incl_tax":53.94,"base_tax_amount":0,"base_tax_invoiced":0,"created_at":"2018-12-26 19:30:30","discount_amount":5.39,"discount_invoiced":0,"discount_percent":10,"free_shipping":0,"discount_tax_compensation_amount":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30969,"name":"Take It™","no_discount":0,"order_id":11856,"original_price":8.99,"price":8.99,"price_incl_tax":8.99,"product_id":433,"product_type":"configurable","qty_canceled":0,"qty_invoiced":0,"qty_ordered":6,"qty_refunded":0,"qty_returned":0,"qty_shipped":6,"quote_item_id":99867,"row_invoiced":0,"row_total":53.94,"row_total_incl_tax":53.94,"row_weight":2.4,"sku":"CAB56062A","store_id":4,"tax_amount":0,"tax_invoiced":0,"tax_percent":0,"updated_at":"2018-12-28 21:10:28","weight":0.4}}],"billing_address":{"address_type":"billing","city":"NORWALK","country_id":"US","email":"kthusband@gmail.com","entity_id":23712,"firstname":"Kaitlin","lastname":"Husband","parent_id":11856,"postcode":"06850-1392","region":"Connecticut","region_code":"CT","region_id":14,"street":["34 OAKWOOD AVE UNIT 312"],"telephone":"2033135622"},"payment":{"account_status":null,"additional_information":["Credit Card","2e55d70f71969ccea9c4d8224ce72c2c","56964d6540ab4966bd961f2ef4aaccce","KvNj2Ae6vYkSaF4vNsW59Pg3j","04362P"],"amount_authorized":48.55,"amount_ordered":48.55,"base_amount_authorized":48.55,"base_amount_ordered":48.55,"base_shipping_amount":0,"cc_exp_month":"06","cc_exp_year":"20","cc_last4":"8288","cc_owner":"Kaitlin Husband","cc_type":"MC","entity_id":11856,"last_trans_id":"BP1UFA46006D","method":"payflow_link","parent_id":11856,"shipping_amount":0},"status_histories":[{"comment":"Authorized amount of $48.55. Transaction ID: BP1UFA46006D","created_at":"2018-12-26 19:31:47","entity_id":19804,"entity_name":"order","is_customer_notified":null,"is_visible_on_front":0,"parent_id":11856,"status":"processing"},{"comment":null,"created_at":"2018-12-26 19:30:30","entity_id":19803,"entity_name":"order","is_customer_notified":1,"is_visible_on_front":0,"parent_id":11856,"status":"pending_payment"}],"extension_attributes":{"shipping_assignments":[{"shipping":{"address":{"address_type":"shipping","city":"NORWALK","country_id":"US","email":"kthusband@gmail.com","entity_id":23711,"firstname":"Kaitlin","lastname":"Husband","parent_id":11856,"postcode":"06850-1392","region":"Connecticut","region_code":"CT","region_id":14,"street":["34 OAKWOOD AVE UNIT 312"],"telephone":"2033135622"},"method":"ups_GND","total":{"base_shipping_amount":0,"base_shipping_discount_amount":0,"base_shipping_discount_tax_compensation_amnt":0,"base_shipping_incl_tax":0,"base_shipping_tax_amount":0,"shipping_amount":0,"shipping_discount_amount":0,"shipping_discount_tax_compensation_amount":0,"shipping_incl_tax":0,"shipping_tax_amount":0}},"items":[{"amount_refunded":0,"applied_rule_ids":"13","base_amount_refunded":0,"base_discount_amount":5.39,"base_discount_invoiced":0,"base_discount_tax_compensation_amount":0,"base_original_price":8.99,"base_price":8.99,"base_price_incl_tax":8.99,"base_row_invoiced":0,"base_row_total":53.94,"base_row_total_incl_tax":53.94,"base_tax_amount":0,"base_tax_invoiced":0,"created_at":"2018-12-26 19:30:30","discount_amount":5.39,"discount_invoiced":0,"discount_percent":10,"free_shipping":0,"discount_tax_compensation_amount":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30969,"name":"Take It™","no_discount":0,"order_id":11856,"original_price":8.99,"price":8.99,"price_incl_tax":8.99,"product_id":433,"product_type":"configurable","qty_canceled":0,"qty_invoiced":0,"qty_ordered":6,"qty_refunded":0,"qty_returned":0,"qty_shipped":6,"quote_item_id":99867,"row_invoiced":0,"row_total":53.94,"row_total_incl_tax":53.94,"row_weight":2.4,"sku":"CAB56062A","store_id":4,"tax_amount":0,"tax_invoiced":0,"tax_percent":0,"updated_at":"2018-12-28 21:10:28","weight":0.4},{"amount_refunded":0,"base_amount_refunded":0,"base_discount_amount":0,"base_discount_invoiced":0,"base_price":0,"base_row_invoiced":0,"base_row_total":0,"base_tax_amount":0,"base_tax_invoiced":0,"created_at":"2018-12-26 19:30:30","discount_amount":0,"discount_invoiced":0,"discount_percent":0,"free_shipping":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30970,"name":"Take It™ - Sky Blue","no_discount":0,"order_id":11856,"original_price":0,"parent_item_id":30969,"price":0,"product_id":381,"product_type":"simple","qty_canceled":0,"qty_invoiced":0,"qty_ordered":6,"qty_refunded":0,"qty_returned":0,"qty_shipped":0,"quote_item_id":99868,"row_invoiced":0,"row_total":0,"row_weight":0,"sku":"CAB56062A","store_id":4,"tax_amount":0,"tax_invoiced":0,"tax_percent":0,"updated_at":"2018-12-26 19:30:30","weight":0.4,"parent_item":{"amount_refunded":0,"applied_rule_ids":"13","base_amount_refunded":0,"base_discount_amount":5.39,"base_discount_invoiced":0,"base_discount_tax_compensation_amount":0,"base_original_price":8.99,"base_price":8.99,"base_price_incl_tax":8.99,"base_row_invoiced":0,"base_row_total":53.94,"base_row_total_incl_tax":53.94,"base_tax_amount":0,"base_tax_invoiced":0,"created_at":"2018-12-26 19:30:30","discount_amount":5.39,"discount_invoiced":0,"discount_percent":10,"free_shipping":0,"discount_tax_compensation_amount":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30969,"name":"Take It™","no_discount":0,"order_id":11856,"original_price":8.99,"price":8.99,"price_incl_tax":8.99,"product_id":433,"product_type":"configurable","qty_canceled":0,"qty_invoiced":0,"qty_ordered":6,"qty_refunded":0,"qty_returned":0,"qty_shipped":6,"quote_item_id":99867,"row_invoiced":0,"row_total":53.94,"row_total_incl_tax":53.94,"row_weight":2.4,"sku":"CAB56062A","store_id":4,"tax_amount":0,"tax_invoiced":0,"tax_percent":0,"updated_at":"2018-12-28 21:10:28","weight":0.4}}]}],"gift_cards":[],"base_gift_cards_amount":0,"gift_cards_amount":0,"gw_base_price":"0.0000","gw_price":"0.0000","gw_items_base_price":"0.0000","gw_items_price":"0.0000","gw_card_base_price":"0.0000","gw_card_price":"0.0000"}},{"base_currency_code":"USD","base_discount_amount":0,"base_grand_total":33.09,"base_discount_tax_compensation_amount":0,"base_shipping_amount":11.64,"base_shipping_discount_amount":0,"base_shipping_discount_tax_compensation_amnt":0,"base_shipping_incl_tax":11.64,"base_shipping_tax_amount":0,"base_subtotal":20,"base_subtotal_incl_tax":21.45,"base_tax_amount":1.45,"base_total_due":33.09,"base_to_global_rate":1,"base_to_order_rate":1,"billing_address_id":23718,"created_at":"2018-12-26 19:58:34","customer_email":"justincummings40@hotmail.com","customer_group_id":0,"customer_is_guest":1,"customer_note_notify":1,"discount_amount":0,"email_sent":1,"entity_id":11859,"global_currency_code":"USD","grand_total":33.09,"discount_tax_compensation_amount":0,"increment_id":"90000566","is_virtual":0,"order_currency_code":"USD","protect_code":"9b5ad8bc424073984099b27eb511b56f","quote_id":41789,"remote_ip":"73.66.144.173","shipping_amount":11.64,"shipping_description":"United Parcel Service - Ground","shipping_discount_amount":0,"shipping_discount_tax_compensation_amount":0,"shipping_incl_tax":11.64,"shipping_tax_amount":0,"state":"processing","status":"processing","store_currency_code":"USD","store_id":9,"store_name":"Zink Calls Website Zink Calls St","store_to_base_rate":0,"store_to_order_rate":0,"subtotal":20,"subtotal_incl_tax":21.45,"tax_amount":1.45,"total_due":33.09,"total_item_count":1,"total_qty_ordered":4,"updated_at":"2018-12-26 19:59:24","weight":0.36,"items":[{"amount_refunded":0,"base_amount_refunded":0,"base_discount_amount":0,"base_discount_invoiced":0,"base_discount_tax_compensation_amount":0,"base_original_price":5,"base_price":5,"base_price_incl_tax":5.36,"base_row_invoiced":0,"base_row_total":20,"base_row_total_incl_tax":21.45,"base_tax_amount":1.45,"base_tax_invoiced":0,"created_at":"2018-12-26 19:58:34","discount_amount":0,"discount_invoiced":0,"discount_percent":0,"free_shipping":0,"discount_tax_compensation_amount":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30979,"name":"Paralyzer XR-2 Replacement Parts","no_discount":0,"order_id":11859,"original_price":5,"price":5,"price_incl_tax":5.36,"product_id":881,"product_type":"simple","qty_canceled":0,"qty_invoiced":0,"qty_ordered":4,"qty_refunded":0,"qty_returned":0,"qty_shipped":4,"quote_item_id":99879,"row_invoiced":0,"row_total":20,"row_total_incl_tax":21.45,"row_weight":0.36,"sku":"ZNK800329","store_id":9,"tax_amount":1.45,"tax_invoiced":0,"tax_percent":7.25,"updated_at":"2018-12-27 21:10:28","weight":0.09}],"billing_address":{"address_type":"billing","city":"ELK GROVE","country_id":"US","email":"justincummings40@hotmail.com","entity_id":23718,"firstname":"Justin","lastname":"Cummings","parent_id":11859,"postcode":"95624-3130","region":"California","region_code":"CA","region_id":12,"street":["8611 ALLISTER WAY"],"telephone":"9164708734"},"payment":{"account_status":null,"additional_information":["Credit Card","5c7145824fbea0e2b0d86f622586d1c8","b2f3ec4842924c29bbb0f4a68141ece1","EBBBC1CmxaECNuwUtwoJG9AN4","125593"],"amount_authorized":33.09,"amount_ordered":33.09,"base_amount_authorized":33.09,"base_amount_ordered":33.09,"base_shipping_amount":11.64,"cc_exp_month":"10","cc_exp_year":"21","cc_last4":"8309","cc_type":"VI","entity_id":11859,"last_trans_id":"BK0U6A91240E","method":"payflow_link","parent_id":11859,"shipping_amount":11.64},"status_histories":[{"comment":"Authorized amount of $33.09. Transaction ID: BK0U6A91240E","created_at":"2018-12-26 19:59:23","entity_id":19808,"entity_name":"order","is_customer_notified":null,"is_visible_on_front":0,"parent_id":11859,"status":"processing"},{"comment":null,"created_at":"2018-12-26 19:58:34","entity_id":19807,"entity_name":"order","is_customer_notified":1,"is_visible_on_front":0,"parent_id":11859,"status":"pending_payment"}],"extension_attributes":{"shipping_assignments":[{"shipping":{"address":{"address_type":"shipping","city":"ELK GROVE","country_id":"US","email":"justincummings40@hotmail.com","entity_id":23717,"firstname":"Justin","lastname":"Cummings","parent_id":11859,"postcode":"95624-3130","region":"California","region_code":"CA","region_id":12,"street":["8611 ALLISTER WAY"],"telephone":"9164708734"},"method":"ups_GND","total":{"base_shipping_amount":11.64,"base_shipping_discount_amount":0,"base_shipping_discount_tax_compensation_amnt":0,"base_shipping_incl_tax":11.64,"base_shipping_tax_amount":0,"shipping_amount":11.64,"shipping_discount_amount":0,"shipping_discount_tax_compensation_amount":0,"shipping_incl_tax":11.64,"shipping_tax_amount":0}},"items":[{"amount_refunded":0,"base_amount_refunded":0,"base_discount_amount":0,"base_discount_invoiced":0,"base_discount_tax_compensation_amount":0,"base_original_price":5,"base_price":5,"base_price_incl_tax":5.36,"base_row_invoiced":0,"base_row_total":20,"base_row_total_incl_tax":21.45,"base_tax_amount":1.45,"base_tax_invoiced":0,"created_at":"2018-12-26 19:58:34","discount_amount":0,"discount_invoiced":0,"discount_percent":0,"free_shipping":0,"discount_tax_compensation_amount":0,"is_qty_decimal":0,"is_virtual":0,"item_id":30979,"name":"Paralyzer XR-2 Replacement Parts","no_discount":0,"order_id":11859,"original_price":5,"price":5,"price_incl_tax":5.36,"product_id":881,"product_type":"simple","qty_canceled":0,"qty_invoiced":0,"qty_ordered":4,"qty_refunded":0,"qty_returned":0,"qty_shipped":4,"quote_item_id":99879,"row_invoiced":0,"row_total":20,"row_total_incl_tax":21.45,"row_weight":0.36,"sku":"ZNK800329","store_id":9,"tax_amount":1.45,"tax_invoiced":0,"tax_percent":7.25,"updated_at":"2018-12-27 21:10:28","weight":0.09}]}],"gift_cards":[],"base_gift_cards_amount":0,"gift_cards_amount":0,"gw_base_price":"0.0000","gw_price":"0.0000","gw_items_base_price":"0.0000","gw_items_price":"0.0000","gw_card_base_price":"0.0000","gw_card_price":"0.0000"}}],"search_criteria":{"filter_groups":[{"filters":[{"field":"updated_at","value":"2018-12-26T19:30:00","condition_type":"from"}]},{"filters":[{"field":"status","value":"processing","condition_type":"eq"}]},{"filters":[{"field":"updated_at","value":"2018-12-26T20:30:00","condition_type":"to"}]}],"page_size":1,"current_page":1},"total_count":2}';
            }
            System.debug('EpM2Sync - Response: ' + jsonString);
            if(response.getStatusCode() == 200) {
                Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonString);
                List<Object> items = (List<Object>)root.get('items');
                for (Object item : items) {
                    Datetime dt = Datetime.now();
                    Integer hour, min, ss;
                    hour = dt.hour();
                    min = dt.minute(); 
                    ss = dt.second() + 5;
                    
                    if(ss>59)
                    {
                        min += 1;ss = ss - 59;
                    }
                    
                    if(min>59)
                    {
                        hour += 1;min = min - 59;
                    }
                    
                    if(hour>23)
                    {
                        hour = 0;
                    }
                    //parse to cron expression
                    String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                    System.debug('nextFireTime: ' + nextFireTime);
                    if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){
                        System.enqueueJob(new EpM2SyncJob(item, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail));
                        if(System.Test.isRunningTest()) {
                            System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, new EpM2SyncSchedulable(item, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail));
                        }
                    } else {
                        System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, new EpM2SyncSchedulable(item, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail));
                    }
                }              
                String[] dt = now_sync_time.split('T');
                String[] d = dt[0].split('-');
                String[] t = dt[1].split(':');
                
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    MagentoStore__c mStore = [SELECT Id, Name, LastSalesOrderSyncTime__c, ECom_Store_Name__c FROM MagentoStore__c WHERE Name = :eComStoreID];
                    ApexGovernorLimits.printLimitsUsed();
                    mStore.LastSalesOrderSyncTime__c = DateTime.newInstanceGmt(Integer.valueOf(d[0]),Integer.valueOf(d[1]),Integer.valueOf(d[2]),Integer.valueOf(t[0]),Integer.valueOf(t[1]),Integer.valueOf(t[2]));
                    if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit DML governor limits');
                        update mStore;
                        ApexGovernorLimits.printLimitsUsed();
                    }
                }
            } else {
                Notifier.sendEmail('EpM2Sync - getSalesOrdersCreatedAfterSync: ' + response.toString() + ' > ' + jsonString, supportEmail);
            }
        } catch(System.Exception e) {
            System.debug('EpM2Sync - getSalesOrdersCreatedAfter - Exception error: '+ e);
            System.debug(response.toString());
            Notifier.sendEmail('EpM2Sync - getSalesOrdersCreatedAfter - Exception error: '+ e, supportEmail);
        }
    }
    
    public static void syncOrders(Object item, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        System.debug('Item is:' + item);
		Map<String,Object> itemObj = (Map<String,Object>)item;
        Integer store_id = ((Integer)itemObj.get('store_id'));
        String store_name = ((String)itemObj.get('store_name'));
        if(!AccountHandler.doesAccountExists(store_id, eComStoreID))
        {
            //EpM2Sync.getStoreViews();
            store_name = store_name.substringBefore('Website').trim();
            System.debug(store_id+'||'+store_name);
            AccountHandler.insertAccountIfNotExists(store_id, store_name, eComStoreID);
        }
        String m_id = ((String)itemObj.get('customer_id'));
        System.debug('m_id: ['+m_id+']');
        if(m_id == NULL)
            m_id = '0';
        
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            List<Contact> customers = [SELECT Id FROM Contact WHERE MagentoId__c = :m_id AND EComStoreID__c = :eComStoreID];
            ApexGovernorLimits.printLimitsUsed();
            System.debug(customers);
            System.debug('customers size:'+customers.size());
            if(System.Test.isRunningTest() || (customers == NULL || customers.size() == 0) && itemObj.get('customer_id') != NULL) {
                EpM2Sync.getCustomerById(JSON.serialize(item), (String)itemObj.get('customer_id'), token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
            } else {
                createECRecordOrder(item, eComStoreID, supportEmail);
            }
        }
    }
    
    private static void createECRecordOrder(Object item, String eComStoreID, String supportEmail) {
        Map<String,Object> itemObj = (Map<String,Object>)item;
        String increment_id = String.valueOf((String)itemObj.get('increment_id'));
        System.debug('increment_id: ['+increment_id+']');
        if(increment_id == NULL)
            increment_id = '0';
        
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            List<Order> orders = [SELECT Id FROM Order WHERE Magento_Order_Number__c = :increment_id AND EComStoreID__c = :eComStoreID];
            ApexGovernorLimits.printLimitsUsed();
            
            System.debug(orders);
            System.debug('orders size:'+orders.size());
            
            if(orders.size() == 0) {
                ECRecord__c record = new ECRecord__c();
                record.Dump__c = JSON.serialize(item);
                record.MagentoAPIStatus__c = true;
                record.MagentoExecutionTime__c = DateTime.now();
                record.MagentoResponse__c = 'Success';
                record.Type__c = 'Sales Order Create';
                record.EComStoreID__c = eComStoreID;
                if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit DML governor limits');
                    insert record;
                    ApexGovernorLimits.printLimitsUsed();
                }
            } else {
                Notifier.sendEmail('EpM2Sync - getSalesOrdersCreatedAfterSync: Duplicate order received: ' + orders, supportEmail);
            }
        }
    }
    
    //To sync one order
    public static void syncSpecificOrder(String store_id, String order_id) {
        ApexGovernorLimits.printMaxLimits();
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :store_id];
            ApexGovernorLimits.printLimitsUsed();
            
            DateTime dateTimeNow = DateTime.now();
            String now_sync_time = dateTimeNow.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
            System.debug('now_sync_time: ' + now_sync_time);
            
            String last_sync_time = store.LastSalesOrderSyncTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
            System.debug('last_sync_time: ' + last_sync_time);
            EpM2Sync.getSpecificSalesOrders(order_id, now_sync_time, store.AccessToken__c, store.Name, last_sync_time, store.BaseURL__c, store.SupportEmail__c);
        }
        ApexGovernorLimits.printLimitsLeft();
    }
    
    public static void getSpecificSalesOrders(String order_id, String now_sync_time,String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();

        //request.setEndpoint(baseURL+'orders?searchCriteria[filter_groups][0][filters][0][field]=increment_id&searchCriteria[filter_groups][0][filters][0][value]='+order_id+'&searchCriteria[filter_groups][0][filters][0][condition_type]=eq&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq&searchCriteria[filter_groups][2][filters][0][field]=fraud_status&searchCriteria[filter_groups][2][filters][0][condition_type]=notnull&searchCriteria[filter_groups][3][filters][0][field]=fraud_status&searchCriteria[filter_groups][3][filters][0][value]&searchCriteria[filter_groups][3][filters][0][condition_type]=neq');
        //request.setEndpoint(baseURL+'orders?searchCriteria[filter_groups][0][filters][0][field]=increment_id&searchCriteria[filter_groups][0][filters][0][value]='+order_id+'&searchCriteria[filter_groups][0][filters][0][condition_type]=eq&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq');
        //request.setHeader('authorization', 'Bearer ' + token);
        request.setEndpoint(baseURL+'?function=getSpecificOrder&order_id='+order_id);
        request.setMethod('GET');
        
        System.debug('EpM2Sync - Request: ' + request.getBody());
        try {
            
            response = http.send(request);

            String jsonString = response.getBody();       
            System.debug('EpM2Sync - Response: ' + jsonString);
            if(response.getStatusCode() == 200) {
                Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonString);
                List<Object> items = (List<Object>)root.get('items');
                for (Object item : items) {
                    Datetime dt = Datetime.now();
                    Integer hour, min, ss;
                    hour = dt.hour();
                    min = dt.minute(); 
                    ss = dt.second() + 5;
                    
                    if(ss>59)
                    {
                        min += 1;ss = ss - 59;
                    }
                    
                    if(min>59)
                    {
                        hour += 1;min = min - 59;
                    }
                    
                    if(hour>23)
                    {
                        hour = 0;
                    }
                    //parse to cron expression
                    String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                    System.debug('nextFireTime: ' + nextFireTime);
                    if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){
                        System.enqueueJob(new EpM2SyncJob(item, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail));
                        if(System.Test.isRunningTest()) {
                            System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, new EpM2SyncSchedulable(item, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail));
                        }
                    } else {
                        System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, new EpM2SyncSchedulable(item, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail));
                    }
                }
            } else {
                Notifier.sendEmail('EpM2Sync - getSalesOrdersCreatedAfterSync: ' + response.toString() + ' > ' + jsonString, supportEmail);
            }
        } catch(System.Exception e) {
            System.debug('EpM2Sync - getSalesOrdersCreatedAfter - Exception error: '+ e);
            System.debug(response.toString());
            Notifier.sendEmail('EpM2Sync - getSalesOrdersCreatedAfter - Exception error: '+ e, supportEmail);
        }
    }
}