/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           04/04/2022            Initial Version
 */
// Created to have Shipping payload wrapper.
public class b2b_ShippingWrapper {
    public String division;	//00
	public String customerNumber;	//72142
	public String distributionChannel;	//00
	public String salesOrganization;	//0040
    public String orderReason;
    public String orderValue;
    public String currencyType;
	

    public class response{
		public String price;	//1265710
		public string currencyType;
	}
	/*public static b2b_ShippingWrapper parse(String json){
		return (b2b_ShippingWrapper) System.JSON.deserialize(json, b2b_ShippingWrapper.class);
	}*/

}