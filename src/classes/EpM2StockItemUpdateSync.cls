public class EpM2StockItemUpdateSync {
    
    public static void defensiveStockItemUpdate(String sku, String qty, String eComStoreID) {
        if(System.isFuture() || System.isBatch()){
            stockItemUpdate(sku, qty, eComStoreID);
        } else {
            if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
                stockItemUpdateFuture(sku, qty, eComStoreID);
            } else {
                // Do some explicit notification so that you know your @future call was denied
                Notifier.sendEmail('EpM2StockItemUpdateSync - defensiveStockItemUpdate - @future call was denied');
            }
        }
    }
        
    @future(callout=true)
    public static void stockItemUpdateFuture(String sku, String qty, String eComStoreID) {
        stockItemUpdate(sku, qty, eComStoreID);
    }
    
    public static void stockItemUpdate(String sku, String qty, String eComStoreID) {
        MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :eComStoreID];
        
        String body = null;
        body = '{"stockItem":{"qty":'+ qty +', "isInStock": true, "sku":"'+ sku +'" }}';
        HttpRequest request = null;
        request = new HttpRequest();
        HttpResponse response = null;
        response = new HttpResponse();
        Http http = null;
        http = new Http();
        request.setEndpoint(store.BaseURL__c);
        request.setHeader('authorization', 'Bearer ' + store.AccessToken__c);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('PUT');
        request.setBody(body);
        
        System.debug('EpM2StockItemUpdateSync - Request: ' + request.getBody());
        try {
            response = http.send(request);
    
            String jsonString = response.getBody();
            System.debug('EpM2StockItemUpdateSync - Response: ' + jsonString); 
            
            if(response.getStatusCode() != 200) {
                Notifier.sendEmail('EpM2StockItemUpdateSync - stockItemUpdate: ' + sku + ' >' + response.toString() + ' > ' + jsonString);
            }
        } catch(System.Exception e) {
            System.debug('EpM2StockItemUpdateSync - stockItemUpdate - Exception error: '+ e);
            System.debug(response.toString());
            Notifier.sendEmail('EpM2StockItemUpdateSync - stockItemUpdate - Exception error: '+ e);
        }
    }
    
    public static void defensiveStockItemUpdateSynchronously(String sku, String qty, String eComStoreID) {
        if(System.isFuture() || System.isBatch()){
            System.debug('EpM2StockItemUpdateSync - defensiveStockItemUpdateSynchronously > System.isFuture():'+System.isFuture());
            stockItemUpdateSynchronously(sku, qty, eComStoreID);
        } else if(!System.isFuture() && (Limits.getFutureCalls() < Limits.getLimitFutureCalls())){
            System.debug('EpM2StockItemUpdateSync - defensiveStockItemUpdateSynchronously > Limits.getFutureCalls():'+Limits.getFutureCalls());
            stockItemUpdateSynchronouslyFuture(sku, qty, eComStoreID);
        } else {
            System.debug('EpM2StockItemUpdateSync - defensiveStockItemUpdateSynchronously - @future call was denied');
            Notifier.sendEmail('EpM2StockItemUpdateSync - defensiveStockItemUpdateSynchronously - @future call was denied');
        }
    }
    
    @future(callout=true)
    public static void stockItemUpdateSynchronouslyFuture(String sku, String qty, String eComStoreID) {
        stockItemUpdateSynchronously(sku, qty, eComStoreID);
    }
    
    public static void stockItemUpdateSynchronously(String sSKU, String sQTY, String sEComStoreID) {
        List<String> sku = (List<String>) JSON.deserialize(sSKU, List<String>.class);
        List<Decimal> qty = (List<Decimal>) JSON.deserialize(sQTY, List<Decimal>.class);
        List<String> eComStoreID = (List<String>) JSON.deserialize(sEComStoreID, List<String>.class);
        stockItemUpdateProcess(sku,qty,eComStoreID);
    }
    
    public static void stockItemUpdateProcess(List<String> sku,
                                                  List<Decimal> qty, List<String> eComStoreID) {
        if(sku!=null && sku.size() != 0) {
            EpM2StockItemUpdateSync.stockItemUpdateNotFutureSynchronously(JSON.serialize(sku), JSON.serialize(qty), JSON.serialize(eComStoreID));
        }
    }
        
    public static void stockItemUpdateNotFutureSynchronously(String sSKU, String sQTY, String sEComStoreID) {
        List<String> sku = (List<String>) JSON.deserialize(sSKU, List<String>.class);
        List<Decimal> qty = (List<Decimal>) JSON.deserialize(sQTY, List<Decimal>.class);
        List<String> eComStoreID = (List<String>) JSON.deserialize(sEComStoreID, List<String>.class);
        ApexGovernorLimits.printMaxLimits();
        
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :eComStoreID.get(0)];
            ApexGovernorLimits.printLimitsUsed();
            
            String body = null;
            body = '{"stockItem":{"qty":'+Integer.valueOf(qty.get(0))+', "isInStock": true, "sku":"'+ sku.get(0) +'"}}';
            HttpRequest request = null;
            request = new HttpRequest();
            HttpResponse response = null;
            response = new HttpResponse();
            Http http = null;
            http = new Http();
            request.setEndpoint(store.BaseURL__c);
            request.setHeader('authorization', 'Bearer ' + store.AccessToken__c);
            request.setHeader('Content-Type', 'application/json');
            request.setMethod('PUT');
            request.setBody(body);
            
            System.debug('EpM2StockItemUpdateSync - Request: ' + request.getBody());
            try {
                response = http.send(request);
                System.debug('EpM2StockItemUpdateSync - sku.size(): ' + sku.size());
                 if(sku.size()>0) {
                        sku.remove(0);
                        qty.remove(0);
                        eComStoreID.remove(0);
                        stockItemUpdateProcess(sku,qty,eComStoreID);
                }
                String jsonString = response.getBody();
                System.debug('EpM2StockItemUpdateSync - Response: ' + jsonString); 
                
                if(response.getStatusCode() != 200) {
                    Notifier.sendEmail('EpM2StockItemUpdateSync - stockItemUpdateNotFutureSynchronously: ' + sku + ' >' + response.toString() + ' > ' + jsonString);
                }
            } catch(System.Exception e) {
                System.debug('EpM2StockItemUpdateSync - stockItemUpdateNotFutureSynchronously - Exception error: '+ e + ' SKUs: ' + sku);
                System.debug(response.toString());
                Notifier.sendEmail('EpM2StockItemUpdateSync - stockItemUpdateNotFutureSynchronously - Exception error: '+ e + ' SKUs: ' + sku);
            }
        }

        ApexGovernorLimits.printLimitsLeft();
    }
}