/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/24/2022            Initial Version
 */
/*This Class will update order status to Draft of orders which are in Completing Order status and Lastmodified before 5 mins.
  orderStatusUpdateSchedular is the schedular for this class which will run this job in org every 5mins daily*/
global class orderStatusUpdateBatch implements Database.Batchable<SObject> {
    public String soqlquery;
   global Database.QueryLocator start(Database.BatchableContext BC) { 
     String ChkStatus='Completing Order';
     Datetime dt = System.now();
     dt = dt.addMinutes(-5);
     List<Order> OrderLists=[SELECT Id,Status FROM Order where Status=:ChkStatus AND LastModifiedDate<=: dt];
      
       
       if(Test.isRunningTest()){ // this is done to avoid the condition of last modified date as in test class we cannot have the date editable
			 soqlquery = 'SELECT Id,Status FROM Order where Status=:ChkStatus'; //changed query when the batch is run from test class
       }else{
           	soqlquery = 'SELECT Id,Status FROM Order where Status=:ChkStatus AND LastModifiedDate<=: dt';  //original query when the batch is run from real time scenario
       }	
       
       Database.QueryLocator qloc = database.getquerylocator(soqlquery);
       system.debug('the result is '+ qloc);
       return qloc;
        
       }
       global void execute(Database.BatchableContext BC, List<Order> OrderLists) { 
           
        system.debug('the values we got is '+ OrderLists);
        List<Order> UpdateOrderList = New List<Order>();
        for(Order Ord:OrderLists)
        {
          Ord.Status='Draft';
          UpdateOrderList.add(Ord);
        }
        update UpdateOrderList;
        System.debug('UpdateOrderList' + UpdateOrderList + ' OrderLists:' + OrderLists);
      }
      global void finish(Database.BatchableContext BC) { 
        System.debug('BatchApexClassExample Batch job completed successfully.');
      }
}