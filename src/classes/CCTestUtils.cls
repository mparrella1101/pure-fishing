/**
 *
 * @author Kevin Fleck - Coastal Cloud
 * kevin.fleck@coastalcloud.us
 *
 */
@IsTest
public without sharing class CCTestUtils {

    public static Account createAccount(String name, Boolean doInsert) {
        Account obj = new Account();
        obj.Name = name;

        // Callout required parameters: START
        obj.Division_SAP__c = '23';
        obj.SAP_Customer__c = '23';
        obj.Distribution_Channel__c = '00';
        obj.Sales_Org__c = '33';
        // Callout required parameters: STOP

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    public static Contact createContact(Account acc, Boolean doInsert) {

        Contact testContact = new Contact(
            FirstName = 'B2B',
            LastName = 'Testing',
            AccountId = acc != null ? acc.Id : null,
            Email = 'fullname@domain.com'
        );

        if(doInsert) {
            insert testContact;
        }
        return testContact;
    }

    public static User createTestUser(Contact contact, Boolean doInsert){
        // 'B2B Store User' profile isn't used in QAFull, but it is in Dev1
        // When executing tests in Dev1, use 'B2B Store User' profile (uncomment line)
        // When executing tests in QAFull, use 'B2B Customer Community Plus Login User' profile (uncomment line)
//        Profile b2bProfile = [SELECT Id FROM Profile WHERE Name = 'B2B Store User' LIMIT 1];
        Profile b2bProfile = [SELECT Id FROM Profile WHERE Name = 'B2B Customer Community Plus Login User' LIMIT 1];
        User obj = new User();
        obj.FirstName = contact.firstName;
        obj.LastName = contact.lastName;
        obj.Email = contact.email;
        obj.UserName = contact.email + '.b2b';
        obj.ContactId = contact.Id;
        obj.ProfileId = b2bProfile.Id;
        obj.TimeZoneSidKey = 'America/Chicago'; 
        obj.LocaleSidKey = 'en_US'; 
        obj.EmailEncodingKey = 'ISO-8859-1'; 
        obj.LanguageLocaleKey = 'en_US';
        obj.Alias ='tstUsr';
        obj.User_Role__c = 'Owner';

        if(doInsert) {
            insert obj;
        }

        return obj;

    }
    

    public static ContactPointAddress createContactPointAddress(String name, Account acc, String addressType, String street, Boolean isDefaultAddress, Boolean doInsert) {
        ContactPointAddress obj = new ContactPointAddress();
        obj.Name = name;
        obj.ParentId = acc.Id;
        obj.isDefault = isDefaultAddress;
        obj.AddressType = addressType;
        obj.Street = street;
        obj.City = 'Gainesville';
        obj.State = 'Florida';
        obj.StateCode = 'FL';
        obj.Country = 'United States';
        obj.CountryCode = 'US';
        obj.PostalCode = '32601';

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    public static Product2 createProduct(String name, String productCode, String materialNumber, Boolean doInsert, Boolean isPlano) {
        Product2 obj = (Product2)Product2.sObjectType.newSObject(
            null, // null will make it the default record type for the user
            true // loadDefaultValues
        );
        obj.Name = name;
        obj.Ecom_Status__c = '30';
        obj.IsActive = true;
        obj.ProductCode = productCode;
        obj.ShortItemNumber__c = productCode;
        obj.Material__c = materialNumber;
        obj.IsActive = true;

        // Set the Plano_Legacy__c checkbox to true, which will update the Product_Type__c formula field that is used for detecting different product types in cart
        if (isPlano) {
            obj.Plano_Legacy__c = true;
        }

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    /**
     * @description Will create a Product2 record that has a Product Class of 'Variation Parent' (a Master product that has associated variant
     *              Product2 records. Will also create 3 test variation products if the 'createVariantRecords' input parameter is TRUE. All Products
     *              created in this method will be added to the Entitlement Policy record whose Id was provided in the input parameters (see: entitlementPolicyId
     *              param)
     *
     * @param name The name of the Product
     * @param productCode The Product Code value to be assigned to the Product
     * @param materialNumber The Material_Number__c value to be assigned to the Product
     * @param entitlementPolicyId The 18-character Salesforce CommerceEntitlementPolicy record Id to be associated with this product (and variant products)
     * @param productCategoryId The 18-character Salesforce ProductCategory record Id
     * @param createVariantRecords When TRUE, creates 3 variant products related to the Variant Parent Product this method is creating
     *
     * @return The new Parent Variation Product sObject record
     */
    public static Product2 createMasterVariantProduct(String name, String productCode, String materialNumber, String entitlementPolicyId, String productCategoryId, Boolean createVariantRecords) {
        // List to store junction objects that associate Product2 records to Entitlement Policy records
        List<CommerceEntitlementProduct> entitlementJunctionObjs = new List<CommerceEntitlementProduct>();

        Product2 obj = (Product2)Product2.sObjectType.newSObject(
                null, // null will make it the default record type for the user
                true // loadDefaultValues
        );
        obj.Name = name;
        obj.IsActive = true;
        obj.ProductCode = productCode;
        obj.ShortItemNumber__c = productCode;
        obj.Material__c = materialNumber;

        // Set 'Type' field to 'Base' (required for a Master Product Variant record)
        obj.Type = 'Base';



        // Commit the new Product record to the DB, so we can use it's Id to relate records to it
        insert obj;

        entitlementJunctionObjs.add(
                new CommerceEntitlementProduct(
                        ProductId = obj.Id,
                        PolicyId = entitlementPolicyId
                )
        );

        // Get 'B2B_Variation' ProductAttributeSet
        Id prodAttSetId = [SELECT Id FROM ProductAttributeSet WHERE DeveloperName = 'B2B_Variation' LIMIT 1]?.Id;

        // Create ProductAttributeSetProduct record and relate to B2B_Variation ProductAttributeSet
        ProductAttributeSetProduct newAttribSet = new ProductAttributeSetProduct(
                ProductAttributeSetId = prodAttSetId,
                ProductId = obj.Id
        );
        insert newAttribSet;

        // Create Variant Records
        if (createVariantRecords) {
            List<Product2> variantRecords = new List<Product2>();

            // Create ProductCategoryProduct record to relate the Master Variant Product to a ProductCategory (so we can utilize the 'Grid_Fields__c' field)
            ProductCategoryProduct productCategory = new ProductCategoryProduct();
            productCategory.ProductId = obj.Id;
            productCategory.ProductCategoryId = productCategoryId;
            productCategory.IsPrimaryCategory = true;

            List<ProductCategoryProduct> categoryProducts = new List<ProductCategoryProduct>();
            for (Integer i = 0; i < 3; i++) {
                variantRecords.add(
                        new Product2(
                                Name = name + ' Variation ' + i,
                                IsActive = true,
                                ProductCode = 'Variation_' + i + '_' + productCode,
                                ShortItemNumber__c = 'v_' + i,
                                Material__c = 'Variation_' + i + '_' + materialNumber,
                                Rod_Butt__c = 'Rod Butt ' + i, // Category-specific field
                                Bait_Length__c = 'Bait length ' + i, // Category-specific field
                                Reel_Size__c = 'Reel Size ' + i, // Category-specific field
                                Case_Quantity__c = 2.0
                        )
                );
            }

            // Commit new Variant Products to DB so we can reference their Ids to relate records to them
            insert variantRecords;

            // Create ProductAttribute records for each variant product record
            List<ProductAttribute> variantAttributes = new List<ProductAttribute>();
            for (Integer i = 1; i < 4; i++) {
                variantAttributes.add(
                        new ProductAttribute(
                                ProductId = variantRecords.get(i-1).Id,
                                B2B_Variation__c = '00' + i,
                                VariantParentId = obj.Id
                        )
                );
                entitlementJunctionObjs.add(
                        new CommerceEntitlementProduct(
                                ProductId = variantRecords.get(i-1).Id,
                                PolicyId = entitlementPolicyId
                        )
                );
            }

            insert variantAttributes;
            insert entitlementJunctionObjs;
            insert productCategory;
        }
        return obj;
    }

    /**
     * @description Creates a CommerceEntitlementPolicy record
     *
     * @param name The name to be assigned to the new Buyer Group record
     * @param doInsert When TRUE, commits the new record to the DB for querying later
     *
     * @return The new CommerceEntitlementPolicy record
     */
    public static CommerceEntitlementPolicy createEntitlementPolicy(String name, Boolean doInsert) {
        CommerceEntitlementPolicy obj = (CommerceEntitlementPolicy)CommerceEntitlementPolicy.sObjectType.newSObject(
                null, // null will make it the default record type for the user
                true // loadDefaultValues
        );
        obj.Name = name;
        obj.IsActive = true;
        obj.CanViewPrice = true;
        obj.CanViewProduct = true;

        if (doInsert) {
            insert obj;
        }

        return obj;
    }

    /**
     * @description Creates a new BuyerGroupMember record
     *
     * @param accountId The Account that should be associated to the BuyerGroup record
     * @param buyerGroupId The 18-character record Id of the Buyer Group this member should be a member of
     * @param doInsert When TRUE, commits the record to the DB for querying later
     *
     * @return The new BuyerGroupMember object
     */
    public static BuyerGroupMember createBuyerGroupMember(String accountId, String buyerGroupId, Boolean doInsert) {
        BuyerGroupMember obj = (BuyerGroupMember)BuyerGroupMember.SObjectType.newSObject(
                null,
                true
        );

        obj.BuyerGroupId = buyerGroupId;
        obj.BuyerId = accountId;

        if (doInsert) {
            insert obj;
        }

        return obj;
    }


    /**
     * @description Creates a new BuyerGroup record
     *
     * @param name The name to be applied to the new BuyerGroup record
     * @param doInsert When TRUE, inserts the record into the DB for querying later
     *
     * @return The new BuyerGroup record
     */
    public static BuyerGroup createBuyerGroup(String name, Boolean doInsert){
        BuyerGroup obj = (BuyerGroup)BuyerGroup.sObjectType.newSObject();

        obj.Name = name;
        obj.Description = 'Apex Test Buyer Group';

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    /**
     * @description Creates a new BuyerGroupPricebook object that relates a Buyer Group to a Pricebook
     *
     * @param pricebookId The 18-character Salesforce record Id of the pricebook we want to associate with the Buyer Group
     * @param buyerGroupId The 18-character Salesforce record Id of the BuyerGroup we want to associated with the Pricebook
     * @param doInsert  When TRUE, commits the new record to the DB for querying later
     *
     * @return The new BuyerGroupPricebook record
     */
    public static BuyerGroupPricebook createBuyerGroupPricebook(String pricebookId, String buyerGroupId, Boolean doInsert) {
        BuyerGroupPricebook obj = (BuyerGroupPricebook)BuyerGroupPricebook.SObjectType.newSObject(
                null,
                true
        );

        obj.Pricebook2Id = pricebookId;
        obj.BuyerGroupId = buyerGroupId;

        if (doInsert) {
            insert obj;
        }

        return obj;
    }

    /**
     * @description Creates a new BuyerAccount record and associates it to the Account whose recordId is passed in via the 'accountId' parameter
     *
     * @param name The name that will be applied to the new BuyerAccount Record
     * @param accountId The Salesforce 18-character Record Id of the Account that we wish to associate this new BuyerAccount record with
     * @param doInsert When TRUE, commits the new record to teh DB for querying later
     *
     * @return The new BuyerGroup record
     */
    public static BuyerAccount createBuyerAccount(String name, String accountId, Boolean doInsert) {
        BuyerAccount obj = (BuyerAccount)BuyerAccount.SObjectType.newSObject(
                null,
                true
        );

        obj.Name = name;
        obj.BuyerId = accountId;
        obj.IsActive = true;

        if (doInsert) {
            insert obj;
        }

        return obj;
    }



    public static PriceBook2 createPriceBook(Boolean isActive, String name, Boolean isStandard, String currencyIsoCode, Boolean populateDefaultValues, Boolean doInsert){
        PriceBook2 obj = (PriceBook2)PriceBook2.sObjectType.newSObject(
            null, // null will make it the default record type for the user
            true // loadDefaultValues
        );
        if(isStandard){
            obj.Id = Test.getStandardPricebookId();
        } else {
            obj.Name = name;
        }
        obj.IsActive = isActive;

        // Note: decomment if multicurrency is enabled:
        //obj.CurrencyIsoCode = currencyIsoCode;

        if(doInsert && !isStandard) {
            insert obj;
        }
        return obj;
    }

    public static PricebookEntry createPriceBookEntry(Product2 product, Pricebook2 priceBook, Boolean populateDefaultValues, Boolean doInsert) {
        PricebookEntry obj = (PricebookEntry)PricebookEntry.sObjectType.newSObject(
            null, // null will make it the default record type for the user
            populateDefaultValues // loadDefaultValues
        );
        obj.UnitPrice = 100;
        obj.Pricebook2Id = priceBook == null ? Test.getStandardPricebookId() : priceBook.Id;
        obj.Product2Id = product.Id;
        obj.isActive = true;

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    public static Order createOrder(Account account, Pricebook2 pricebook, Boolean doInsert) {
            Order obj = new Order();
            obj.AccountId = account.Id;
            obj.BillingCity = 'Gainesville';
            obj.BillingCountry = 'United States';
            obj.BillingPostalCode = '32107';
            obj.BillingState = 'Florida';
            obj.BillingStreet = '4600 E University Avenue';
            obj.EffectiveDate = Date.today();
            obj.Pricebook2Id = priceBook == null ? Test.getStandardPricebookId() : priceBook.Id;
            obj.Reason_Code__c = '100';
            obj.ShippingCity = 'Gainesville';
            obj.ShippingCountry = 'United States';
            obj.ShippingPostalCode = '32107';
            obj.ShippingState = 'Florida';
            obj.ShippingStreet = '1000 Archer Road';
            obj.Status = 'Draft';
            obj.Type = 'OR';
        
            if(doInsert){ 
                insert obj;
            }

            return obj;
    }

    public static OrderItem createOrderItem(Order order, Product2 product, PricebookEntry pricebookEntry, Boolean doInsert){
        OrderItem obj = new OrderItem();
        obj.OrderId = order.Id;
        obj.Product2Id = product.Id;
        obj.PricebookEntryId = pricebookEntry.Id;
        obj.ListPrice = pricebookEntry.UnitPrice;
        obj.UnitPrice = 123;
        obj.Quantity = 3;
        obj.Type = 'Order Product';

        if(doInsert){ 
            insert obj;
        }

        return obj;
    }

    //*** b2b products START ***/

    public static WebStore createWebStore(String name, String pricebook2Id, Boolean doInsert){
        Webstore obj = (Webstore)Webstore.sObjectType.newSObject();

        obj.Name = name;
        obj.Description = name;
        obj.DefaultLanguage = 'en_US';
        obj.CheckoutTimeToLive = 2880;
        obj.GuestCartTimeToLive = 10080;
        obj.SAP_Company__c = 'apex_test';
        obj.OptionsGuestBrowsingEnabled = false;
        obj.OptionsSkipAdditionalEntitlementCheckForSearch = false;
        obj.PaginationSize = 20;
        obj.PricingStrategy = 'LowestPrice';
        obj.ProductGrouping = 'VariationParent';
        obj.SupportedLanguages = 'en_US';
        obj.Type = 'B2B';
        obj.StrikethroughPricebookId = pricebook2Id;

        if(doInsert) {
            insert obj;
        }

        return obj;

    }

    public static ProductCatalog createProductCatalog(String name, Boolean doInsert){
        ProductCatalog obj = (ProductCatalog)ProductCatalog.sObjectType.newSObject();

        obj.Name = name;
        obj.Sales_Org__c = '33';

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    public static WebStoreCatalog createWebStoreCatalog(ProductCatalog productCatalog, Boolean doInsert){
        WebStoreCatalog obj = (WebStoreCatalog)WebStoreCatalog.sObjectType.newSObject();

        obj.ProductCatalogId = productCatalog.Id;

        if(doInsert) {
            insert obj;
        }

        return obj;

    }

    public static ProductCategory createProductCategory(String name, ProductCatalog productCatalog, ProductCategory productCategory, List<String> productGridFields, Boolean doInsert){
        ProductCategory obj = new ProductCategory();

        obj.Name = name;
        obj.IsNavigational = true;
        obj.ParentCategoryId = productCategory != null ? productCategory.Id : null;
        obj.CatalogId = productCatalog.Id;
        if (productGridFields != null && !productGridFields.isEmpty()) {
            String productGridFieldsString = '';
            for (String productField : productGridFields) {
                productGridFieldsString += productField + '\n';
            }
            obj.Product_Grid_Fields__c = productGridFieldsString;
        }

        System.debug('product grid fields: ' + obj.Product_Grid_Fields__c);

        if(doInsert) {
            insert obj;
        }

        return obj;

    }

    public static ProductCategoryProduct createProductCategoryProduct(Product2 product, ProductCategory productCategory, Boolean doInsert){
        ProductCategoryProduct obj = new ProductCategoryProduct();

        obj.ProductId = product.Id;
        obj.ProductCategoryId = productCategory.Id;
        obj.IsPrimaryCategory = true;

        if(doInsert) {
            insert obj;
        }

        return obj;

    }

    public static WebCart createWebCart(String name, User user, WebStore webStore, Account account, Boolean doInsert){
        WebCart obj = (WebCart)WebCart.sObjectType.newSObject();

        obj.Name = name;
        obj.OwnerId = user != null ? user.Id : UserInfo.getUserId();
        obj.WebStoreId = webstore.Id;
        obj.AccountId = account.Id;
        obj.Status = 'Active';
        obj.Type = 'Cart';

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    public static CartDeliveryGroup createCartDeliveryGroup(String name, WebCart webCart, Boolean doInsert){
        CartDeliveryGroup obj = (CartDeliveryGroup)CartDeliveryGroup.sObjectType.newSObject();

        obj.Name = name;
        obj.CartId = webCart.Id;

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    public static CartItem createCartItem(Product2 product, Webcart webCart, CartDeliveryGroup cdg, Boolean doInsert){
        CartItem obj = (CartItem)CartItem.sObjectType.newSObject();

        obj.CartId = webCart.id;
        obj.Name = product.name;
        obj.Product2Id = product.Id;
        obj.SKU = product.StockKeepingUnit;
        obj.SalesPrice = 100;
        obj.Quantity = 2;
        obj.TotalPrice = 200;
        obj.CartDeliveryGroupId = cdg.id;
        obj.ListPrice = 100;
        obj.UnitAdjustedPrice = 100;
        obj.TotalListPrice = 200;
        obj.AdjustmentAmount = 0;
        obj.UnitAdjustmentAmount = 0;
        obj.TotalAdjustmentAmount = 0;
        obj.TotalLineAmount = 200;
        obj.TotalPriceAfterAllAdjustments = 200;
        obj.Type = 'Product';

        if(doInsert) {
            insert obj;
        }

        return obj;
    }

    public static Suggested_Order__c createSuggestedOrder(String name, Account account, WebStore webstore, Boolean doInsert) {
        Suggested_Order__c obj = (Suggested_Order__c)Suggested_Order__c.SObjectType.newSObject(
                null,
                true
        );

        obj.Name = name;
        obj.Effective_Account__c = account != null ? account.Id : '';
        obj.WebStore__c = webstore != null ? webstore.Id : '';
        obj.Expiration_Date__c = Date.today().addDays(3);

        if (doInsert) {
            insert obj;
        }

        return obj;
    }

    public static Suggested_Order_Item__c createSuggestedOrderItem(Product2 product, Suggested_Order__c suggestedOrder, Boolean doInsert) {
        Suggested_Order_Item__c obj =(Suggested_Order_Item__c)Suggested_Order_Item__c.SObjectType.newSObject(
                null,
                true
        );

        obj.Suggested_Order__c = suggestedOrder != null ? suggestedOrder.Id : '';
        obj.Product__c = product != null ? product.Id : '';
        obj.Quantity__c = Math.ceil(Math.random() * 10);

        if (doInsert) {
            insert obj;
        }

        return obj;
    }

    public static Product2 createPlanoProduct(String name, String productCode, String materialNumber, String entitlementPolicyId) {
        List<CommerceEntitlementProduct> entitlementJunctionObjs = new List<CommerceEntitlementProduct>();

        Product2 obj = (Product2)Product2.sObjectType.newSObject(
                null, // null will make it the default record type for the user
                true // loadDefaultValues
        );
        obj.Name = name;
        obj.Ecom_Status__c = '30';
        obj.IsActive = true;
        obj.ProductCode = productCode;
        obj.ShortItemNumber__c = productCode;
        obj.Material__c = materialNumber;
        obj.IsActive = true;

        // Set the Plano_Legacy__c checkbox to true, which will update the Product_Type__c formula field that is used for detecting different product types in cart
        obj.Plano_Legacy__c = true;

        insert obj;

        entitlementJunctionObjs.add(
                new CommerceEntitlementProduct(
                        ProductId = obj.Id,
                        PolicyId = entitlementPolicyId
                )
        );

        insert entitlementJunctionObjs;



        return obj;
    }

    public static Product2 createSparePartsProduct(String name, String productCode, String materialNumber, String entitlementPolicyId) {
        List<CommerceEntitlementProduct> entitlementJunctionObjs = new List<CommerceEntitlementProduct>();

        Product2 obj = (Product2)Product2.sObjectType.newSObject(
                null, // null will make it the default record type for the user
                true // loadDefaultValues
        );
        obj.Name = name;
        obj.Ecom_Status__c = '30';
        obj.IsActive = true;
        obj.ProductCode = productCode;
        obj.ShortItemNumber__c = productCode;
        obj.Material__c = materialNumber;
        obj.IsActive = true;

        // Set the Spare_Part__c checkbox to true, which will update the Product_Type__c formula field that is used for detecting different product types in cart
        obj.SubCategory__c = 'Spare Parts';

        insert obj;

        entitlementJunctionObjs.add(
                new CommerceEntitlementProduct(
                        ProductId = obj.Id,
                        PolicyId = entitlementPolicyId
                )
        );

        insert entitlementJunctionObjs;



        return obj;
    }


}