/**
 * @description     Test class to cover the 'LanguageSelectorController' Apex Class
 * @author          Coastal Cloud (Matt Parrella)
 * @date            February 15, 2023
 */

/* Author                  Company                 TaskRay                  Date            Description
-------------------------------------------------------------------------------------------------------------------------
    Matt Parrella           Coastal Cloud          TSK-00236425             02-15-2023      Initial version
 */

@IsTest
public with sharing class LanguageSelectorController_Test {
    @TestSetup
    static void setup_data() {
        // Create Test Account
        Account testAccount = CCTestUtils.createAccount('Aperture Labs',  true);

        // Create Buyer Account
        BuyerAccount testBuyerAccount = CCTestUtils.createBuyerAccount('Aperture Labs Buyer Account', testAccount.Id, true);

        // Create Buyer Group
        BuyerGroup testBuyerGroup = CCTestUtils.createBuyerGroup('Test Buyer Group', true);

        // Create Buyer Group Member
        BuyerGroupMember testBuyerGroupMember = CCTestUtils.createBuyerGroupMember(testAccount.Id, testBuyerGroup.Id, true);

        // Create Test Contact
        Contact testContact = CCTestUtils.createContact(testAccount, true);

        // Create Test Community User
        User commUser = CCTestUtils.createTestUser(testContact, true);

        // Create Strikethrough Pricebook
        Pricebook2 testStrikethroughPricebook = CCTestUtils.createPriceBook(true, 'Test Strikethrough Pricebook', false, 'USD', true, true);

        // Create Test WebStore
        WebStore testStore = CCTestUtils.createWebStore('Apex Test Store', testStrikethroughPricebook.Id, true);

        // Update WebStore's Supported Languages field
        testStore.SupportedLanguages = 'en_US;fr;';
        update testStore;
    }

    @IsTest
    static void getLanguageSelections_test() {
        String userId = UserInfo.getUserId();
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];

        System.runAs(commUser) {
            String communityId = [SELECT NetworkId FROM NetworkMember WHERE MemberId =: commUser.Id LIMIT 1].NetworkId;
            Test.startTest();
            LanguageSelectorController.LanguageData result = LanguageSelectorController.getLanguageSelections(userId, communityId);
            Assert.isTrue(result.recordData.size() == 2, 'Expected 2 entries in this map: langOptions and defaultLang');
            Assert.isTrue(result.recordData.get('defaultLang') == 'en_US', 'Expected default language to be en_US, but found: ' + result.recordData.get('defaultLang'));
            List<LanguageSelectorController.LanguageOption> returnedOptions = (List<LanguageSelectorController.LanguageOption>) result.recordData.get('langOptions');
            Assert.isTrue(returnedOptions.size() == 2, 'Expected 2 supported languages to have been returned.');
            Test.stopTest();
        }
    }

    @IsTest
    static void updateLanguageSettings_test() {
        String userId = UserInfo.getUserId();
        String selectedLanguage = 'fr';

        Test.startTest();
        Boolean result = LanguageSelectorController.updateLanguageSetting(userId, selectedLanguage);
        Assert.isTrue(result, 'Expected a successful DML operation on the User record.');
        Test.stopTest();
    }
}