public class SalesOrderItem
{
    public String base_currency_code { get; set; }
    public Double base_discount_amount { get; set; }
    public Double base_grand_total { get; set; }
    public Double base_shipping_amount { get; set; }
    public Double base_subtotal { get; set; }
    public Double base_subtotal_incl_tax { get; set; }
    public String created_at { get; set; }
    public String customer_id { get; set; }
    public Integer customer_is_guest { get; set; }
    public String entity_id { get; set; }
    public String increment_id { get; set; }
    public String shipping_description { get; set; }
    public Integer store_id { get; set; }
    public Double tax_amount { get; set; }
    public String coupon_code { get; set; }
    public List<SalesOrderProductItem> items { get; set; }
    public SalesOrderBillingAddress billing_address { get; set; }
    public SalesOrderPayment payment { get; set; }
    public SalesOrderExtensionAttributes extension_attributes { get; set; }
}