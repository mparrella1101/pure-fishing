@isTest
public with sharing class AuthorizeNetAdapterTest {
    @testSetup static void setup() {
        Account testAccount = new Account(Name='TestAccount',Sales_Org__c='0020');
        insert testAccount;
        WebStore testWebStore = new WebStore(Name='TestWebStore');
        insert testWebStore;
        
        Account account = [SELECT Id FROM Account WHERE Name='TestAccount' LIMIT 1];
        WebStore webStore = [SELECT Id FROM WebStore WHERE Name='TestWebStore' LIMIT 1];
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id,Order_Reference__c='Test');
        insert cart;
        Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test',Material__c='1265710',Case_Quantity__c=3.0);
        insert Prod;
        /*AuthorizeNetPayment__mdt AuthData = new AuthorizeNetPayment__mdt(Username__c='Test1', Trax_key__c='Test1', Sales_Org__c='0020');
        insert AuthData;*/
        
        CartDeliveryGroup cartDeliveryGroup = new CartDeliveryGroup(CartId=cart.Id, Name='Default Delivery 1',DeliverToCountry = 'United States',DeliverToName = 'Test Test');
        insert cartDeliveryGroup;
        
        CartItem cartItem = new CartItem(CartId=cart.Id, Type='Product', Name='Test Product', CartDeliveryGroupId=cartDeliveryGroup.Id,Quantity=3.0,product2Id = prod.Id,SalesPrice=10.00);
        insert cartItem;
        Order Ord = new Order(AccountId=account.Id,Order_Reference__c='Test',Completing_Order_User__c= UserInfo.getUserId(),EffectiveDate=Date.valueOf('2022-04-02'),status='Completing Order',Pricebook2Id=Test.getStandardPricebookId(),Name='Test',CC_details__c='Test');
        insert Ord;
        /*List<DmlOperation__e> dmlOperationEvents = new <DmlOperation__e>(Operation__c='Modify',Payload__c={Test,Tes1});
        Insert dmlOperationEvents;*/
        /*OrderItem OrdItem = new OrderItem(OrderId=Ord.Id, Product2Id=Prod.Id,Quantity=1,Type ='Order Product',ListPrice=0);
        insert OrdItem;*/
        
        
    }
    
    
    
    @isTest static void testIntegrationRunsSuccessfully() {
        // Because test methods don't support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        AuthorizeNetAdapter apexSample = new AuthorizeNetAdapter();
        
        commercepayments.PaymentMethodTokenizationRequest authRequest = new commercepayments.PaymentMethodTokenizationRequest();
        commercepayments.paymentGatewayContext gatewayContext = new commercepayments.paymentGatewayContext(authRequest, commercepayments.RequestType.Tokenize);
        
        // Test: execute the integration for the test cart ID.
        /*StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetAuthReq');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');*/
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.startTest();
        // Associate the callout with a mock response.
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        // Verify: the integration executed successfully
        System.assertEquals(200,200);
        String TokenAPIKey='Test';
        String TokenTranKey='Test';
        String Msg='';
        commercepayments.GatewayResponse integrationResult = apexSample.processRequest(gatewayContext);
        List<Order> OrderList=[Select id,OrderNumber,Order_Reference__c,AccountId,TotalTaxAmount,GrandTotalAmount,TotalAdjustedDeliveryAmount,status,Completing_Order_User__c from Order where Order_Reference__c='Test'];
        Msg= AuthorizeNetAdapter.getOrderDetails(OrderList,TokenAPIKey,TokenTranKey,(commercepayments.PaymentMethodTokenizationRequest)authRequest);
        Test.stopTest();
        
    }
    /* @isTest static void testFailed() {
// Because test methods do not support Web service callouts, we create a mock response based on a static resource.
// To create the static resource from the the Developer Console, select File | New | Static Resource
StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
mock.setStaticResource('GetAuthReq');
// The web service call returns an error code.
mock.setStatusCode(404);
mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
Test.startTest();
// Associate the callout with a mock response.
Test.setMock(HttpCalloutMock.class, mock);

// Test: execute the integration for the test cart ID and integration info.
AuthorizeNetAdapter apexSample = new AuthorizeNetAdapter();
commercepayments.RequestType requestType = commercepayments.RequestType.Authorize;
commercepayments.GatewayResponse integrationResult = apexSample.processRequest(requestType);
// Verify: the integration executed successfully
System.assertEquals(scommercepayments.GatewayResponse.getStatusCode(), integrationResult.getStatusCode());
Test.stopTest();
}*/
    public class MockHttpResponseGenerator implements HttpCalloutMock {
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('/xml/v1/request.api', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"transactionResponse":{"responseCode":"1","authCode":"HW617E","avsResultCode":"Y","cvvResultCode":"","cavvResultCode":"","transId":"2157047189","refTransID":"","transHash":"E7CEB0A9F1BECA32A02493E1B31D5955","testRequest":"0","accountNumber":"XXXX1111","accountType":"Visa","messages":[{"code":"1","description":"This transaction has been approved."}],"transHashSha2":"D0C4FFF5648511A5862B917CFD9BB78ABF8A6E1D90C119CBBC4C0B454F4FF40DED15B204E042F36ECA5FB15D02588E4E4A7B85B94E7279599CE6020462CB7DEE","SupplementalDataQualificationIndicator":0,"networkTransId":"123456789NNNH"},"messages":{"resultCode":"Ok","message":[{"code":"I00001","text":"Successful."}]}}');
            res.setStatusCode(200);
            return res;
        }
    }
}