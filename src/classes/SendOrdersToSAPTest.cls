@isTest
public class SendOrdersToSAPTest {
	static testmethod void OrderTestMethod()
    {
        Account account = new Account(
            Name = 'Test Account',
            BillingCity = 'Florida',
            BillingCountry = 'United States',
            BillingPostalCode = '71923',
            BillingState = 'Florida',
            BillingStreet = '231 S Adams Rd, Sand Springs, OK, 74063',
            CurrencyIsoCode='USD'
        );
        insert account;

		Id pricebookId = Test.getStandardPricebookId();
		 
		Product2 product1 = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test');
        insert product1;
        Product2 product2 = new Product2(Name='Test2', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test');
        insert product2;

		
        PricebookEntry entry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = product1.Id,
            UnitPrice = 33.0,
            IsActive = true
        );
        insert entry;

		OrderDeliveryMethod orderMethod=new OrderDeliveryMethod();
        orderMethod.Name='Name';
        orderMethod.ProductId=product1.Id;
        orderMethod.IsActive=true;
        insert orderMethod;
		
		Order Ord = new Order(AccountId=account.Id,Reason_Code__c='100',EffectiveDate=Date.valueOf('2022-04-02'),status='Draft',Pricebook2Id=pricebookId ,Order_Reference__c='123',Bill_To_Name__c='Test',Ship_To_Account__c='Test',PoNumber='test',Bill_To_Account__c='test',Ship_To_Name__c='Test');
        insert Ord;
		
        OrderDeliveryGroup orderDeliveryGroup = new OrderDeliveryGroup(
            Description = 'Test',
            DesiredDeliveryDate = Date.today(),
            DeliverToName = 'Default Delivery',
            DeliveryInstructions = '',
            DeliverToStreet = '8131 Hillcrest Drive',
            DeliverToCity ='New York',
            DeliverToState = 'New York',
            DeliverToPostalCode = '30705',
            DeliverToCountry = 'United States',
            
            OrderDeliveryMethodId = orderMethod.Id,
            OrderId = Ord.id
        );

		OrderItem orderItem = new OrderItem(
            OrderId = ord.Id,
            Product2Id = product1.Id,
            Quantity = 3,
            UnitPrice = 33.0,
            ListPrice = 33.0,
            PricebookEntryId=entry.id,
            type='Order Product',
            OrderDeliveryGroupId = orderDeliveryGroup.id
			);
        insert orderItem;
        OrderItem orderItem1 = new OrderItem(
            OrderId = ord.Id,
            Product2Id = product2.Id,
            Quantity = 3,
            UnitPrice = 33.0,
            ListPrice = 33.0,
            PricebookEntryId=entry.id,
            type='Delivery Charge',
            OrderDeliveryGroupId = orderDeliveryGroup.id
			);
        insert orderItem1;
		

        system.debug('the last mod date is '+Ord.LastModifiedDate);
        system.debug('the system now '+ system.now());
       
		Ord.status='Created';
        update Ord;

       
        // Create your test data
      	Test.startTest();
        SendOrdersToSAP ins = new SendOrdersToSAP();
        ins.query='Select Reason_Code__c,Distribution_Channel__c,Sales_Org__c,EffectiveDate,Account.SAP_Customer__c,Type,PO_Type__c,ShippingFutureDate__c,Placed_By__c,Order_Reference__c,BillingStreet,BillingCity,BillingPostalCode,BillingCountryCode,BillingPhoneNumber,ShippingStreet,ShippingCity,ShippingPostalCode,ShippingCountryCode,ShippingPhone__c,Ship_Cancel_Flag__c,PoNumber,Order_Notes__c,CC_Details__c,account.Division_SAP__c,account.Distribution_Channel__c,account.name,account.BillingStreet,account.BillingCity,account.BillingPostalCode,account.BillingCountryCode,account.phone,Bill_To_Account__c,Bill_To_Name__c,Ship_To_Account__c,Ship_To_Name__c,(SELECT UnitPrice,Product2.Material__c,Quantity,SF_External_ID__c, CurrencyIsoCode,Supplied_Line__c,Type FROM OrderItems) from Order where Status=:OrdStatus and Order_Reference__c<>null';
        Database.executeBatch(ins,1);
        Test.stopTest();
   }
}