/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/24/2022            Initial Version
 */
global class orderStatusUpdateSchedular implements Schedulable {
    global void execute(SchedulableContext SC)  {
        //String sch = '0 5 * * * ?';
        orderStatusUpdateBatch sqrb = new orderStatusUpdateBatch();
        //if(!Test.isRunningTest()){
			database.executeBatch(sqrb);
        //}
        //system.schedule('Every Hour plus 5 min', sch, sqrb);
    }
}