public with sharing class B2B_OrderPriceBookRemover {
    public List<Order> orderList = new List<Order>();
    public List<OrderItem> orderItemList = new List<OrderItem>();
    public List<OrderItem> masterOrderItemInsertList = new List<OrderItem>();
    public Map<String, String> orderPricebookMap = new Map<String, String>();
    public Map<String, String> orderItemPricebookEntryMap = new Map<String, String>();
    public Map<String, String> orderItemToOrderMap = new Map<String, String>(); 
    public String orderProduct = 'Order Product';
    @InvocableMethod(callout=true label='B2B_OrderPriceBookRemover' description='Runs a synchronously' category='B2B Commerce')
    public static void OrderPriceBookRemover(List<ID> OrderIds) {
        // Validate the input
        if (OrderIds == null || OrderIds.size() != 1) {
            String errorMessage = 'A Order id must be included to B2B_OrderPriceBookRemover';
            throw new CalloutException (errorMessage);
        }
        
        // Extract cart id and start processing
        Id OrdId = OrderIds[0];
        B2B_OrderPriceBookRemover OrdPrice=new B2B_OrderPriceBookRemover();
        OrdPrice.startOrderProcessSync(OrdId);
    }
    public void startOrderProcessSync(Id OrdId) {
        try {
            list<Order> orderList=[SELECT Id, Pricebook2Id , (SELECT PricebookEntryId, Product2Id, OrderId, ListPrice, UnitPrice, Product2.Material__c, Quantity, Quantity_Ordered__c, SF_External_ID__c, CurrencyIsoCode, Supplied_Line__c, Type, Customer_Expected_Price__c, SAP_External_ID__c  FROM OrderItems) FROM Order WHERE Id = :OrdId AND Pricebook2Id != null];
            if(orderList != null)
                {
                    System.debug('orderList: ' + orderList);
                    buildMaps(orderList); // set up maps for reference.
                    removeOrderLines(orderItemList); // store all order lines from order in map, then delete and reinsert later.
                    clearOrderPricebook(orderList); // remove pricebook2Id from all orders.
                    reinsertOrderLines(masterOrderItemInsertList); // reinsert order lines.        
                }
        
        
            }
            catch (Exception e) 
            {
                System.debug('Error is :' + e.getMessage() + 'LN:' + e.getLineNumber() );
            }
   }
   public void buildMaps(List<Order> orderList){
    for(Order order : orderList){
        System.debug('buildMaps');
        orderPricebookMap.put(order.Id, order.Pricebook2Id);
        for(OrderItem orderItem : order.OrderItems){ // assumes all orderItems have pricebook entries since the order would have a pricebook.
            orderItemPricebookEntryMap.put(orderItem.Id, orderItem.PricebookEntryId);
            orderItemToOrderMap.put(orderItem.Id, orderItem.OrderId);
            orderItemList.add(orderItem);
        }
        System.debug('orderItemToOrderMap: ' + orderItemToOrderMap);
        System.debug('orderItemList: ' + orderItemList);
        System.debug('orderItemPricebookEntryMap: ' + orderItemPricebookEntryMap);
    }
}
public void removeOrderLines(List<OrderItem> orderItemList){
    for(OrderItem orderItem : orderItemList){
        
            OrderItem newOrderItem = new OrderItem(
                OrderId = orderItem.OrderId,
                Product2Id = orderItem.Product2Id,
                ListPrice = orderItem.ListPrice,
                UnitPrice = orderItem.UnitPrice,
                Quantity = orderItem.Quantity,
                Quantity_Ordered__c = orderItem.Quantity_Ordered__c,
                SF_External_ID__c = orderItem.SF_External_ID__c,
                Supplied_Line__c = orderItem.Supplied_Line__c,
                Type = orderItem.Type, 
                Customer_Expected_Price__c = orderItem.Customer_Expected_Price__c,
                SAP_External_ID__c = orderItem.SAP_External_ID__c        
            );
            masterOrderItemInsertList.add(newOrderItem);
                   
    }
    System.debug('masterOrderItemInsertList: ' + masterOrderItemInsertList);
    runDelete(orderItemList, 'OrderItem', true);
}
public  void clearOrderPricebook(List<Order> orderList){
    System.debug('clearOrderPricebook');
    System.debug('orderList before: ' + orderList);
    for(Order order : orderList){
        order.Pricebook2Id = null;
    }
    System.debug('orderList after: ' + orderList);
    runUpdate(orderList, 'Order', true);
}
public void reinsertOrderLines(List<OrderItem> orderItemsToInsertList){
    System.debug('reinsertOrderLines');
    System.debug('orderItemsToInsertList: ' + orderItemsToInsertList);
    runInsert(orderItemsToInsertList, 'OrderItem', true);
}
public void runInsert(List<sObject> listToInsert, String objectType, Boolean continueOnError){
    System.debug('runInsert');
    System.debug(objectType + ' listToInsert: ' + listToInsert);
    Database.SaveResult[] srList = Database.insert(listToInsert, continueOnError);
    for (Database.SaveResult sr : srList) {
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully inserted ' + objectType + ', ID: ' + sr.getId());
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Account fields that affected this error: ' + err.getFields());
            }
        }
    }
}

public void runUpdate(List<sObject> listToUpdate, String objectType, Boolean continueOnError){
    System.debug('runUpdate');
    System.debug(objectType + ' listToUpdate: ' + listToUpdate);
    Database.SaveResult[] srList = Database.update(listToUpdate, continueOnError);
    for(Database.SaveResult sr : srList){
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully updated '+ objectType + ' record with ID: ' + sr.getId());
        } else {
            // Operation failed, so get all errors 
            for(Database.Error err : sr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug(objectType + ' fields that affected this error: ' + err.getFields());
            }
        }
    }
}

public void runDelete(List<sObject> listToDelete, String objectType, Boolean continueOnError){
    System.debug('runDelete');
    System.debug(objectType + ' listToDelete: ' + listToDelete);
    Database.DeleteResult[] drList = Database.delete(listToDelete, continueOnError);
    for(Database.DeleteResult dr : drList) {
        if (dr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully deleted ' + objectType + ' with ID: ' + dr.getId());
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : dr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug(objectType + ' fields that affected this error: ' + err.getFields());
            }
        }
    }
}
}