public class b2b_InventoryWrapper {
    public String division;	//00
	public String customerNumber;	//72142
	public String distributionChannel;	//00
	public String salesOrganization;	//0040
	public cls_products[] products;
//	public String orderReason;	//100
//	public String shipmentDate;	//2022-04-02
	public class cls_products {
		public String materialNumber;	//1265710
	}

    public class response{
		@AuraEnabled
		public String materialNumber;	//1265710
		@AuraEnabled
		public String quantity;
	}
	public static b2b_InventoryWrapper parse(String json){
		return (b2b_InventoryWrapper) System.JSON.deserialize(json, b2b_InventoryWrapper.class);
	}


}