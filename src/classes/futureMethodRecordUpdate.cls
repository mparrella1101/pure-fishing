global class futureMethodRecordUpdate
{
    @future
    public static void processRecords(string reqOrd,string authCode , String transId)
    {   
         // Get those records based on the IDs
        Order OrdUpd= [Select id,CC_AuthCode__c,CC_transId__c from Order where OrderNumber=:reqOrd LIMIT 1];
        OrdUpd.CC_AuthCode__c=authCode;
        OrdUpd.CC_transId__c=transId;
        update OrdUpd;
    }
}