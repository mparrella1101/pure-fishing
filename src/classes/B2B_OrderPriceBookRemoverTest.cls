@isTest
public class B2B_OrderPriceBookRemoverTest {
    @testSetup static void setup() {
        Account account = new Account(
            Name = 'Test Account',
            BillingCity = 'Florida',
            BillingCountry = 'United States',
            BillingPostalCode = '71923',
            BillingState = 'Florida',
            BillingStreet = '231 S Adams Rd, Sand Springs, OK, 74063',
            CurrencyIsoCode='USD'
        );
        insert account;

		Id pricebookId = Test.getStandardPricebookId();
		 
		Product2 product1 = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test');
        insert product1;

		
        PricebookEntry entry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = product1.Id,
            UnitPrice = 33.0,
            IsActive = true
        );
        insert entry;

		OrderDeliveryMethod orderMethod=new OrderDeliveryMethod();
        orderMethod.Name='Name';
        orderMethod.ProductId=product1.Id;
        orderMethod.IsActive=true;
        insert orderMethod;
		
		Order Ord = new Order(AccountId=account.Id,Reason_Code__c='100',EffectiveDate=Date.valueOf('2022-04-02'),status='Draft',Pricebook2Id=pricebookId ,Order_Reference__c='123',Bill_To_Name__c='Test',Ship_To_Account__c='Test',PoNumber='test',Bill_To_Account__c='test',Ship_To_Name__c='Test');
        insert Ord;
		
        OrderDeliveryGroup orderDeliveryGroup = new OrderDeliveryGroup(
            Description = 'Test',
            DesiredDeliveryDate = Date.today(),
            DeliverToName = 'Default Delivery',
            DeliveryInstructions = '',
            DeliverToStreet = '8131 Hillcrest Drive',
            DeliverToCity ='New York',
            DeliverToState = 'New York',
            DeliverToPostalCode = '30705',
            DeliverToCountry = 'United States',
            
            OrderDeliveryMethodId = orderMethod.Id,
            OrderId = Ord.id
        );

		OrderItem orderItem = new OrderItem(
            OrderId = ord.Id,
            Product2Id = product1.Id,
            Quantity = 3,
            UnitPrice = 33.0,
//            ListPrice = 33.0,
            PricebookEntryId=entry.id,
            type='Order Product',
            OrderDeliveryGroupId = orderDeliveryGroup.id,
            Customer_Expected_Price__c = 33.0
			);
        insert orderItem;
		
    }
    @isTest static void testIntegrationRunsSuccessfully() {
		Test.startTest();
        Order Ord = [SELECT Id FROM Order LIMIT 1];
        List<Id> orderId = new List<Id>{Ord.Id};
        B2B_OrderPriceBookRemover.OrderPriceBookRemover(orderId);
        Test.stopTest();
    }
    
}