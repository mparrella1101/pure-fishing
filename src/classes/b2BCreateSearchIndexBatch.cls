/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/24/2022            Initial Version
 */
//This Class will create the Search index for all active webstore in org .It is nightly schedule in b2BCreateSearchIndexSchedular class.
global class b2BCreateSearchIndexBatch implements Database.Batchable<SObject> {
        public String soqlquery;
       global Database.QueryLocator start(Database.BatchableContext BC) { 
         List<WebStoreNetwork> storeList=[select WebStoreId from WebStoreNetwork];
           if(Test.isRunningTest()){ // this is done to avoid the condition of last modified date as in test class we cannot have the date editable
			 soqlquery = 'select Id from webStore'; //changed query when the batch is run from test class
           }else{
                soqlquery = 'select WebStoreId from WebStoreNetwork';  //original query when the batch is run from real time scenario
           }
         //soqlquery='select WebStoreId from WebStoreNetwork';
        Database.QueryLocator qloc = database.getquerylocator(soqlquery);
           system.debug('the result is '+ qloc);
           return qloc;
            
           }
           global void execute(Database.BatchableContext BC, List<WebStoreNetwork> storeList) { 
            system.debug('storeList '+ storeList);
            for(WebStoreNetwork WSN:storeList)
            {
                ConnectApi.CommerceSearchIndex searchIndex =ConnectApi.CommerceSearchSettings.createCommerceSearchIndex(WSN.WebStoreId);
            }
            
          }
          global void finish(Database.BatchableContext BC) { 
            System.debug('BatchApexClassExample Batch job completed successfully.');
          }
    }