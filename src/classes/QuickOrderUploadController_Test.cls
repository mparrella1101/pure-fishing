/**
 * Test class to cover the code within the 'QuickOrderUploadController' Apex Class
 */
 /*
  Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           Mar 28, 2022            Initial version
 */

@IsTest
public without sharing class QuickOrderUploadController_Test {
    private static final String COMMUNITY_PROFILE = 'B2B Customer Community Plus Login User';
    private static Id effectiveAccountId = null;
    private static Id contactId = null;
    private static Id webstoreId;
    private static Id cartId;
    private static Id stndPricebookId;
    private static User storeUser;
    private static Id productId;
    private static List<Product2> productList;

    @TestSetup
    static void setup_data() {


        // Create Test Account
        Account testAccount = CCTestUtils.createAccount('Aperture Labs',  true);

        // Create Test Contact
        Contact testContact = CCTestUtils.createContact(testAccount, true);

        // Create Test Community User
        User commUser = CCTestUtils.createTestUser(testContact, true);

        try {
            stndPricebookId = Test.getStandardPricebookId();
            effectiveAccountId = B2BCartUploadTestUtils.createAccount();
            contactId = B2BCartUploadTestUtils.createContact(effectiveAccountId);
            storeUser = B2BCartUploadTestUtils.createCommunityUser(contactId);
            webstoreId = B2BCartUploadTestUtils.createWebStore(stndPricebookId);
            cartId = B2BCartUploadTestUtils.createWebCart(effectiveAccountId, webstoreId);
        }
        catch(Exception ex) {
            System.debug('### Test Setup Error: ' + ex.getMessage());
        }
    }

    @IsTest
    static void getTemplateId_Test(){
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        NetworkMember commMember = [SELECT NetworkId FROM NetworkMember WHERE MemberId =: commUser.Id LIMIT 1];

        System.runAs(commUser) {
            Test.startTest();
            QuickOrderUploadController.getTemplateId(commMember.NetworkId);
            Test.stopTest();
        }
    }

    @IsTest
    static void getInfo_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        // Get the NetworkMember record to obtain the networkid
        NetworkMember commMember = [SELECT NetworkId FROM NetworkMember WHERE MemberId =: commUser.Id LIMIT 1];
        System.assertNotEquals(null, commMember, 'Couldn\'t find a NetworkMember record.');

        String networkId = commMember.NetworkId;

        System.runAs(commUser) {
            Test.startTest();
            Map<String,Object> result = QuickOrderUploadController.getInfo(null, null, networkId, null);
            System.assertNotEquals(null, result);
            Test.stopTest();
        }
    }

    @IsTest
    static void processData_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        String effectiveAccountId = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1].Id;

        String cartId = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1].Id;

        String webStoreId = [SELECT Id FROM WebStore WHERE Name = 'Aperture Labs' LIMIT 1].Id;

        // Build testData
        List<QuickOrderUploadUtils.ImportItem> items = helper_createImportItems();


        String testData = JSON.serialize(items);
        Test.startTest();
        System.runAs(commUser) {
            QuickOrderUploadController.processData(testData, commUser.Id, webstoreId, effectiveAccountId, cartId);
        }
        Test.stopTest();
    }

    @IsTest
    static void createInputItems_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');


        List<QuickOrderUploadController.UploadedItem> testItems = helper_createUploadedItems();

        System.runAs(commUser) {
            Test.startTest();
            QuickOrderUploadUtils.createInputItems(testItems);
            Test.stopTest();
        }
    }


    @IsTest
    static void verifySearchTerms_Test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        Map<String,String> testValidTerms = new Map<String,String>();
        testValidTerms.put('123', 'abc');
        testValidTerms.put('789', 'ghi');

        List<QuickOrderUploadController.UploadedItem> testItems = helper_createUploadedItems();
        Map<String,Object> responseMap = new Map<String,Object>();
        Map<String,QuickOrderUploadUtils.ImportItem> testImportItems = QuickOrderUploadUtils.createInputItems(testItems);

        System.runAs(commUser) {
            Test.startTest();
            List<QuickOrderUploadUtils.ImportItem> importItems = QuickOrderUploadController.verifySearchTerms(testImportItems.values(), testValidTerms, responseMap);
            Test.stopTest();
        }

    }

    @IsTest
    static void createCartItems_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        List<QuickOrderUploadUtils.ImportItem> testItems = helper_createImportItems();
        Map<String,Object> responseMap = new Map<String,Object>();

        System.runAs(commUser) {
            Test.startTest();
            List<ConnectApi.CartItemInput> result = QuickOrderUploadController.createCartItems(testItems, responseMap);
            Test.stopTest();
        }
    }

    @IsTest
    static void createBatchInputLines_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        List<ConnectApi.CartItemInput> cartItems = helper_createCartItemInputs();
        Map<String,Object> responseMap = new Map<String,Object>();

        System.runAs(commUser) {
            Test.startTest();
            List<ConnectApi.BatchInput> batchInputs = QuickOrderUploadController.createBatchInputLines(cartItems, responseMap);
            Test.stopTest();
        }
    }

    @IsTest
    static void processAddToCartErrors_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        List<QuickOrderUploadUtils.ImportItem> testImports = helper_createImportItems();

        // Modify data to fit test
        for (QuickOrderUploadUtils.ImportItem item : testImports) {
            item.isValid = true;
            item.isSuccess = false;
            item.errorMsg = 'test msg';
        }

        Map<String,Object> responseMap = new Map<String,Object>();

        System.runAs(commUser) {
            Test.startTest();
            QuickOrderUploadController.processAddToCartErrors(testImports, responseMap);
            Test.stopTest();
        }
    }

    @IsTest
    static void processInvalidItems_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        List<QuickOrderUploadUtils.ImportItem> testItems = helper_createImportItems();
        for (QuickOrderUploadUtils.ImportItem item : testItems) {
            item.isValid = false;
        }
        Map<String,Object> responseMap = new Map<String,Object>();

        System.runAs(commUser) {
            Test.startTest();
            QuickOrderUploadController.processInvalidItems(testItems, responseMap);
            Test.stopTest();
        }
    }

    @IsTest
    static void addItemsToCartInBatches_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        String effectiveAccountId = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1].Id;

        String cartId = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1].Id;

        String webStoreId = [SELECT Id FROM WebStore WHERE Name = 'Aperture Labs' LIMIT 1].Id;

        Map<String,Object> responseMap = new Map<String,Object>();

        List<ConnectApi.CartItemInput> testCartItems = helper_createCartItemInputs();

        List<ConnectApi.BatchInput> batchInputs = QuickOrderUploadController.createBatchInputLines(testCartItems, responseMap);

        List<QuickOrderUploadUtils.ImportItem> testItems = helper_createImportItems();

        System.runAs(commUser) {
            Test.startTest();
            List<QuickOrderUploadUtils.ImportItem> resultItems = QuickOrderUploadController.addItemsToCartInBatches(webstoreId, effectiveAccountId, cartId, batchInputs, testItems, responseMap);
            Test.stopTest();
        }
    }

    @IsTest
    static void processData_large_input_test() {
        // Get a community user to run this test as
        User commUser = [SELECT Id FROM User WHERE Username = 'Chris.Parachute@test.com' LIMIT 1];
        System.assertNotEquals(null, commUser, 'Couldn\'t find a Community User');

        List<QuickOrderUploadController.UploadedItem> importItems = helper_createUploadedItems_large();
        String testData =  JSON.serialize(importItems);

        String effectiveAccountId = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1].Id;

        String cartId = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1].Id;

        String webStoreId = [SELECT Id FROM WebStore WHERE Name = 'Aperture Labs' LIMIT 1].Id;

        System.runAs(commUser) {
            Test.startTest();
            Map<String,Object> response = QuickOrderUploadController.processData(testData, null, webstoreId, effectiveAccountId, cartId);
            Test.stopTest();
        }
    }

    @IsTest
    static void processBatchResults_test() {
        String testResponse = '{"hasErrors":false,"results":[{"statusCode":201,"result":{"categories":{"category":{"description":"All Categories","id":null,"name":"All Categories"},"children":[{"category":{"description":null,"id":"0ZG560000008OSFGA2","name":"Products"},"children":[],"productCount":2}],"productCount":2},"facets":[{"attributeType":"ProductAttribute","displayName":"B2B Variation","displayRank":2,"displayType":"MultiSelect","facetType":"DistinctValue","nameOrId":"B2B_Variation__c","values":[{"displayName":"002","nameOrId":"002","productCount":1,"type":"DistinctValue"}]}],"locale":"en_US","productsPage":{"currencyIsoCode":null,"pageSize":50,"products":[{"defaultImage":{"alternateText":"","contentVersionId":null,"id":null,"mediaType":"Image","sortOrder":0,"thumbnailUrl":null,"title":"image","url":"/img/b2b/default-product-image.svg"},"fields":{"Model_Number__c":{"value":"CLAW SPRING"},"UPC_Code__c":{"value":null},"Material__c":{"value":"1099914"}},"id":"01t56000009vMQzAAM","name":"Mitchell Parts - Springs","prices":null,"productClass":"Variation","purchaseQuantityRule":null,"variationAttributeSet":{"apiName":"B2B_Variation","attributes":[{"apiName":"B2B_Variation__c","label":"B2B Variation","sequence":0,"value":"032"}],"label":"B2B Variation"}},{"defaultImage":{"alternateText":"","contentVersionId":null,"id":null,"mediaType":"Image","sortOrder":0,"thumbnailUrl":null,"title":"image","url":"/img/b2b/default-product-image.svg"},"fields":{"Model_Number__c":{"value":"7000/802MH"},"UPC_Code__c":{"value":"036282615922"},"Material__c":{"value":"1324606"}},"id":"01t56000009vRUjAAM","name":"Abu Garcia Ambassadeur® 7000 Combo","prices":null,"productClass":"Variation","purchaseQuantityRule":null,"variationAttributeSet":{"apiName":"B2B_Variation","attributes":[{"apiName":"B2B_Variation__c","label":"B2B Variation","sequence":0,"value":"002"}],"label":"B2B Variation"}}],"total":2}}},{"statusCode":201,"result":{"categories":{"category":{"description":"All Categories","id":null,"name":"All Categories"},"children":[{"category":{"description":null,"id":"0ZG560000008OSFGA2","name":"Products"},"children":[],"productCount":3}],"productCount":3},"facets":[],"locale":"en_US","productsPage":{"currencyIsoCode":null,"pageSize":50,"products":[{"defaultImage":{"alternateText":"","contentVersionId":null,"id":null,"mediaType":"Image","sortOrder":0,"thumbnailUrl":null,"title":"image","url":"/img/b2b/default-product-image.svg"},"fields":{"Model_Number__c":{"value":"A/R LVR SPRING        IM30/30T"},"UPC_Code__c":{"value":null},"Material__c":{"value":"1071283"}},"id":"01t56000009vSr5AAE","name":"Mitchell Parts - Springs","prices":null,"productClass":"Variation","purchaseQuantityRule":null,"variationAttributeSet":{"apiName":"B2B_Variation","attributes":[{"apiName":"B2B_Variation__c","label":"B2B Variation","sequence":0,"value":"014"}],"label":"B2B Variation"}},{"defaultImage":{"alternateText":"","contentVersionId":null,"id":null,"mediaType":"Image","sortOrder":0,"thumbnailUrl":null,"title":"image","url":"/img/b2b/default-product-image.svg"},"fields":{"Model_Number__c":{"value":"SPRING AVS500UL"},"UPC_Code__c":{"value":null},"Material__c":{"value":"1109437"}},"id":"01t56000009vdDWAAY","name":"Mitchell Parts - Springs","prices":null,"productClass":"Variation","purchaseQuantityRule":null,"variationAttributeSet":{"apiName":"B2B_Variation","attributes":[{"apiName":"B2B_Variation__c","label":"B2B Variation","sequence":0,"value":"053"}],"label":"B2B Variation"}},{"defaultImage":{"alternateText":"","contentVersionId":null,"id":null,"mediaType":"Image","sortOrder":0,"thumbnailUrl":null,"title":"image","url":"/img/b2b/default-product-image.svg"},"fields":{"Model_Number__c":{"value":"DRAG SPRING"},"UPC_Code__c":{"value":null},"Material__c":{"value":"1099917"}},"id":"01t56000009vHgcAAE","name":"Mitchell Parts - Springs","prices":null,"productClass":"Variation","purchaseQuantityRule":null,"variationAttributeSet":{"apiName":"B2B_Variation","attributes":[{"apiName":"B2B_Variation__c","label":"B2B Variation","sequence":0,"value":"033"}],"label":"B2B Variation"}}],"total":3}}},{"statusCode":201,"result":{"categories":{"category":{"description":"All Categories","id":null,"name":"All Categories"},"children":[{"category":{"description":null,"id":"0ZG560000008OSFGA2","name":"Products"},"children":[],"productCount":2}],"productCount":2},"facets":[{"attributeType":"ProductAttribute","displayName":"B2B Variation","displayRank":2,"displayType":"MultiSelect","facetType":"DistinctValue","nameOrId":"B2B_Variation__c","values":[{"displayName":"001","nameOrId":"001","productCount":1,"type":"DistinctValue"}]}],"locale":"en_US","productsPage":{"currencyIsoCode":null,"pageSize":50,"products":[{"defaultImage":{"alternateText":"","contentVersionId":null,"id":null,"mediaType":"Image","sortOrder":0,"thumbnailUrl":null,"title":"image","url":"/img/b2b/default-product-image.svg"},"fields":{"Model_Number__c":{"value":"A/R SPRING"},"UPC_Code__c":{"value":null},"Material__c":{"value":"1276897"}},"id":"01t56000009vOSRAA2","name":"Mitchell Parts - Springs","prices":null,"productClass":"Variation","purchaseQuantityRule":null,"variationAttributeSet":{"apiName":"B2B_Variation","attributes":[{"apiName":"B2B_Variation__c","label":"B2B Variation","sequence":0,"value":"081"}],"label":"B2B Variation"}},{"defaultImage":{"alternateText":"","contentVersionId":null,"id":null,"mediaType":"Image","sortOrder":0,"thumbnailUrl":null,"title":"image","url":"/img/b2b/default-product-image.svg"},"fields":{"Model_Number__c":{"value":"BAIL SPRING FOR MCH200UL SPIN REEL"},"UPC_Code__c":{"value":null},"Material__c":{"value":"1065696"}},"id":"01t56000009vZGLAA2","name":"Mitchell Parts - Springs","prices":null,"productClass":"Variation","purchaseQuantityRule":null,"variationAttributeSet":{"apiName":"B2B_Variation","attributes":[{"apiName":"B2B_Variation__c","label":"B2B Variation","sequence":0,"value":"001"}],"label":"B2B Variation"}}],"total":2}}}]}';
        Map<String,QuickOrderUploadUtils.ImportItem> testMap = new Map<String,QuickOrderUploadUtils.ImportItem>();
        Test.startTest();
        Map<String,String> response = QuickOrderUploadUtils.processBatchResults(testResponse, testMap);
        Test.stopTest();
    }



    /* Helper Methods: START */
    @IsTest
    static List<QuickOrderUploadController.UploadedItem> helper_createUploadedItems() {
        // Create test data
        List<QuickOrderUploadController.UploadedItem> testItems = new List<QuickOrderUploadController.UploadedItem>();
        for(Integer i = 0; i < 3; i++) {
            QuickOrderUploadController.UploadedItem newItem = new QuickOrderUploadController.UploadedItem();
            switch on i {
                when 0 {
                    newItem.material = '123';
                    newItem.model = '';
                    newItem.upc = '';
                    testItems.add(newItem);
                }
                when 1 {
                    newItem.material = '';
                    newItem.model = '456';
                    newItem.upc = '';
                    testItems.add(newItem);
                }
                when 2 {
                    newItem.material = '';
                    newItem.model = '';
                    newItem.upc = '789';
                    testItems.add(newItem);
                }
                when else {
                    // do nothing
                }
            }
        }
        return testItems;
    }

    @IsTest
    static List<QuickOrderUploadController.UploadedItem> helper_createUploadedItems_large() {
        // Create test data
        List<QuickOrderUploadController.UploadedItem> testItems = new List<QuickOrderUploadController.UploadedItem>();
        for(Integer i = 0; i < 500; i++) {
            QuickOrderUploadController.UploadedItem newItem = new QuickOrderUploadController.UploadedItem();
            newItem.material = '999' + i ;
            newItem.model = '';
            newItem.upc = '';
            testItems.add(newItem);
        }
        return testItems;
    }

    @IsTest
    static List<QuickOrderUploadUtils.ImportItem> helper_createImportItems() {
        List<QuickOrderUploadController.UploadedItem> testItems = helper_createUploadedItems();
        Map<String,QuickOrderUploadUtils.ImportItem> testImportItems = QuickOrderUploadUtils.createInputItems(testItems);
        for(QuickOrderUploadUtils.ImportItem item : testImportItems.values()) {
            item.isValid = true;
        }
        return testImportItems.values();
    }

    @IsTest
    static List<ConnectApi.CartItemInput> helper_createCartItemInputs() {
        List<QuickOrderUploadUtils.ImportItem> testItems = helper_createImportItems();
        Map<String,Object> responseMap = new Map<String,Object>();
        List<ConnectApi.CartItemInput> cartItems = QuickOrderUploadController.createCartItems(testItems, responseMap);
        return cartItems;
    }




    /* Helper Methods: STOP */



}