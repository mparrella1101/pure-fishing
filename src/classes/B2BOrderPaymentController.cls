/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/09/2022            Initial Version
 */
//This class is used in Salesrep order payment flow.Its work on order.
public with sharing class B2BOrderPaymentController {

    private static final String PAYMENT_STATE = 'Payment And Billing Address';
    @AuraEnabled
    public static POCheckWrapper checkPO(String recordId)
     {
        System.debug('OrderId' + recordId);
        try {
            Set<String> poNumberSet = new Set<String>();
            string POLists='';
            POCheckWrapper poCheckWrapper = new POCheckWrapper();
            Order ord = [Select Id,AccountId,PONumber from Order where Id =: recordId];
            Account OrderList = [Select Id,(Select Id,PoNumber from Orders) from Account where Id=:ord.AccountId];
            for(Order record : OrderList.Orders){
                if(record.Id != ord.Id && record.PoNumber != null){
                    //poCheckWrapper.POList.add(record.PoNumber);
                    poNumberSet.add(record.PONumber);
                }
            }
            List<String> lst=new List<String>();
            lst.addAll(poNumberSet);
            system.debug('lst Org' + lst);
            for(Integer i=0; i<lst.size(); i++){
                lst[i] = lst[i].toLowerCase();
            }
            system.debug('lst Lowercase' + lst);
            poCheckWrapper.POList=lst;
            poCheckWrapper.POValue=ord.PONumber?.toLowerCase();
            return poCheckWrapper;
        } catch (Exception e) {
            System.debug(' value'+e.getMessage()+e.getLineNumber());
            throw new AuraHandledException(e.getMessage()+e.getLineNumber());
        }
     }
    
    @AuraEnabled
    public static Map<String, Object> getPaymentInfo(String recordId) {
        // Get the effectiveAccountId
        //String accountId = B2BUtils.getUserAccountID();
		system.debug('recordId '+recordId);
        List<Order> ActInfo =[Select id,AccountId from Order where id=: recordId];
        String accountId;
        if(test.isRunningTest()){
			accountId = [Select id from account][0].Id;
        }else{
			accountId=ActInfo[0]?.AccountId;
		}
        //recordId='80156000002Xw05AAC';
        System.debug('OrderId' + recordId);

        /////////////////////////////////////////////////////////////////////////////////////
        // When debugging within the flow, calls to this controller cannot be impersonated,
        // meaning the buyer won't be used. Therefore, the getUserAccountID() will not return
        // correctly. For debugging purposes, one can temporarily specify the account ID of
        // the buyer under test to get around this limitation. This will impact any user
        // trying to use this method, so this is only suggested on non-production orgs.
        // 
        // Example:
        // String accountId = '001xx000003GZBjAAO';
        /////////////////////////////////////////////////////////////////////////////////////
        
        // Get the 'purchaseOrderNumber' from the WebCart
        List<Order> OrderDetails = [SELECT Id, BillingAddress, PoNumber FROM Order WHERE Id=:recordId];
        if(!OrderDetails.isEmpty()){
        Map<String, Object> paymentInfo = new Map<String, Object>();
        paymentInfo.put('purchaseOrderNumber', OrderDetails[0].PoNumber);

        // Get the billingAddresses
        List<Map<String, Object>> addresses = getAddresses(OrderDetails[0],accountId);
        paymentInfo.put('addresses', addresses);
        return paymentInfo;
        }
        return null;
        
    }

    public static List<Map<String, Object>> getAddresses(Order OrderDetails, String accountId) {
        // Get the billingAddresses
        List<ContactPointAddress> addresses = 
        [
         SELECT Id, IsDefault, City, Street, State, Country, PostalCode, GeocodeAccuracy, Latitude, Longitude, Name ,SAP_Customer__c,Billing_Shipping_Account__c
         FROM ContactPointAddress 
         WHERE AddressType='Billing' AND ParentId=:accountId
        ];
        System.debug('addresses' + addresses);
        Address selectedBillingAddress = OrderDetails.BillingAddress;

        boolean anySelected = false;
        List<Map<String, Object>> billingAddresses = new List<Map<String, Object>>();

        // Add each contact point address to the list of addresses
        for (ContactPointAddress cpa : addresses) {
            // See if this address is the selected addresses, then regardless add to the list
            boolean selected = selectedBillingAddress != null && cpa.Street == selectedBillingAddress.Street &&
                    cpa.City == selectedBillingAddress.City && cpa.PostalCode == selectedBillingAddress.PostalCode &&
                    cpa.State == selectedBillingAddress.State && cpa.Country == selectedBillingAddress.Country;
            addContactPointAddressToList(billingAddresses, cpa, selected);

            // Keep track if there was a match on any contact point address. It's possible for none to match
            anySelected = (selected) ? true : anySelected;
        }

        // If there were no matches for the selected address, add it as the first item in the list
        if (!anySelected && selectedBillingAddress != null && selectedBillingAddress.Street != null) {
            prependAddressToList(billingAddresses, selectedBillingAddress, true, OrderDetails.Id);
        }

        return billingAddresses;

    }
    
    /**
     * Appends the contact point address to the end of the list, marking it selected if indicated to do so.
     * 
    **/
    private static void addContactPointAddressToList(List<Map<String, Object>> billingAddresses, ContactPointAddress cpa, boolean selected) {
        Map<String, Object> bill = new Map<String, Object>();
        bill.put('id', cpa.Id);
        bill.put('name', cpa.Name);
        bill.put('street', cpa.Street);
        bill.put('city', cpa.City);
        bill.put('state', cpa.State);
        bill.put('country', cpa.Country);
        bill.put('postalCode', cpa.PostalCode);
        bill.put('latitude', cpa.Latitude);
        bill.put('longitude', cpa.Longitude);
        bill.put('geocodeAccuracy', cpa.GeocodeAccuracy);
        bill.put('default', cpa.IsDefault);
        bill.put('selected', selected);
        bill.put('BillActName', cpa.Billing_Shipping_Account__c);
		bill.put('sap', cpa.SAP_Customer__c);
		bill.put('accountName', cpa.Name);

        billingAddresses.add(bill);
    }

    /**
     * Prepends the address to the start of the list, marking it selected if indicated to do so.
     * 
    **/
    @TestVisible
    private static void prependAddressToList(List<Map<String, Object>> billingAddresses, Address address, boolean selected, Id id) {
        Map<String, Object> bill = new Map<String, Object>();
        bill.put('id', id);
        bill.put('street', address.Street);
        bill.put('city', address.City);
        bill.put('state', address.State);
        bill.put('country', address.Country);
        bill.put('postalCode', address.PostalCode);
        bill.put('latitude', address.Latitude);
        bill.put('longitude', address.Longitude);
        bill.put('geocodeAccuracy', address.GeocodeAccuracy);
        bill.put('default', false);
        bill.put('selected', selected);

        billingAddresses.add(0, bill);
    }
    
    /**
    * Tokenizes the payment information, returns a paymentMethod that is then saved to the current WebCart.
    * It also updates the WebCart with the selected billing address.
    **/
    @AuraEnabled
    public static void setPayment(String paymentType,
                                  String recordId,
                                  Map<String, Object> billingAddress,
                                  Map<String, Object> paymentInfo) {
        try {
            System.debug('recordId: ' + recordId);
            String queryFields = 'PONumber,Payment_Method__c,AccountId';

            // Billing Address might have been hidden
            boolean billAddressSet = billingAddress != null && !billingAddress.isEmpty();
            if (billAddressSet) {
               queryFields += ', BillingAddress';
            }
            //recordId='80156000002Xw05AAC';
            String soqlQuery = 'SELECT ' + queryFields + ' FROM Order WHERE Id=:recordId LIMIT 1';
            Order OrderList = Database.query(soqlQuery);
            paymentInfo.put('accountId',OrderList.accountId);
            List<Account> getWebstoreInfo=[select id,Sales_Org__c from Account where id=:OrderList.accountId];
            //AuthorizeNetPayment__mdt AuthInfo = AuthorizeNetPayment__mdt.getInstance(getWebstoreInfo[0].Sales_Org__c);
            list<AuthorizeNetPayment__mdt> AuthData=[Select WebstoreId__c from AuthorizeNetPayment__mdt where Sales_Org__c=:getWebstoreInfo[0].Sales_Org__c ];
            //AuthorizeNetPayment__mdt AuthInfo = AuthorizeNetPayment__mdt.getInstance('Autherization_Net');
            String webStoreId=AuthData[0].WebstoreId__c;
            System.debug('webStoreId: ' + webStoreId + 'getWebstoreInfo' + getWebstoreInfo + 'AuthData' + AuthData);
            // Update the billingAddress
            // If it was not set, then set the cart.BillingAddress to empty
            OrderList.BillingStreet = billAddressSet ? (String) billingAddress.get('street') : '';
            OrderList.BillingCity = billAddressSet ? (String) billingAddress.get('city') : '';
            OrderList.BillingState = billAddressSet ? (String) billingAddress.get('state') : '';
            OrderList.BillingCountry = billAddressSet ? (String) billingAddress.get('country') : '';
            OrderList.BillingPostalCode = billAddressSet ? (String) billingAddress.get('postalCode') : '';
            OrderList.BillingLatitude = billAddressSet ? (Double) billingAddress.get('latitude') : null;
            OrderList.BillingLongitude = billAddressSet ? (Double) billingAddress.get('longitude') : null;
            OrderList.Bill_To_Name__c =  billAddressSet?(String) billingAddress.get('name') : '';
            OrderList.Bill_To_Account__c =  billAddressSet?(String) billingAddress.get('sap') : '';
            OrderList.Bill_To__c =  billAddressSet?(String) billingAddress.get('BillActName') : '';            
            // Update the payment information
            if (paymentType == 'PurchaseOrderNumber') {
                OrderList.PONumber = (String) paymentInfo.get('poNumber');
                OrderList.Payment_Method__c = null;
            } else {
                OrderList.PONumber = null;
                // Let's first check that paymentGateway has been setup
                String paymentGatewayId = getPaymentGatewayId(webStoreId);
                System.debug('paymentGatewayId: ' + paymentGatewayId + 'paymentInfo' + paymentInfo);

                // Cannot proceed if paymentGatewayId does not exist
                if (paymentGatewayId == null || paymentGatewayId.equals('')) {
                    throw new AuraHandledException('This store is not authorized to process payments.');
                }

                // Tokenize the paymentInfo
                ConnectApi.PaymentMethodTokenizationRequest tokenizeRequest = new ConnectApi.PaymentMethodTokenizationRequest();
                tokenizeRequest.paymentGatewayId = paymentGatewayId;
                System.debug('paymentInfo:' + paymentInfo);
                tokenizeRequest.cardPaymentMethod = getCardPaymentMethod(paymentInfo);
                if (billAddressSet) {
                    tokenizeRequest.address = getAddress(billingAddress); 
                    System.debug('billingAddress: ' + billingAddress);                   
                }
                System.debug('tokenizeRequest.cardPaymentMethod :'+ tokenizeRequest.cardPaymentMethod +'tokenizeRequest: ' + tokenizeRequest); 
                ConnectApi.PaymentMethodTokenizationResponse tokenizeResponse = ConnectApi.Payments.tokenizePaymentMethod(tokenizeRequest);                
                System.debug('tokenizeResponse:' + tokenizeResponse);
                if (tokenizeResponse.error != null) {
                    System.debug('TokenizeResponse has an error: ' + tokenizeResponse.error.message);
                    throw new AuraHandledException('There are issues finalizing your payment. Try contacting your account rep.');
                }
                // Tokenize was successful, assign the new payment method to the cart.
                /*OrderList.paymentMethodId = tokenizeResponse.paymentMethod.Id;
                System.debug('cart.paymentMethodId: ' + cart.paymentMethodId); */
                OrderList.Payment_Method__c = tokenizeResponse.paymentMethod.Id;
               // String CCdata=string.valueof(tokenizeRequest.cardPaymentMethod);
                //OrderList.CC_Details__c=CCdata.length() >255?CCdata.Substring(0,254) :CCdata; 
            }

           // Now update the webCart
           if(Schema.sObjectType.Order.isAccessible()) {
               update OrderList; 
               System.debug('OrderList:' +OrderList.Id);                    
           }
        } catch (AuraHandledException e) {
            System.debug('AuraHandledException:' + e.getMessage()); 
            throw e;
        } catch (DmlException e) {
            System.debug('DmlException:' + e.getMessage()); 
            throw new AuraHandledException('Unexpected error occurred while updating the cart: ' + e.getMessage());
        } catch (ConnectApi.ConnectApiException e) {
            System.debug('ConnectApi.ConnectApiException:' + e.getMessage() + e.getStackTraceString()); 
            throw new AuraHandledException('Unexpected error occurred while processing payment: ' + e.getMessage());
        } catch (Exception e) {
            System.debug('Exception:' + e.getMessage()); 
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    
    private static String getPaymentGatewayId(String webStoreId) {
        String paymentGatewayId = null;
        try {
            if(test.isRunningTest()){
				paymentGatewayId = '0b056000000008BAAQ';
            }else{
            paymentGatewayId = [SELECT Integration FROM StoreIntegratedService WHERE ServiceProviderType='Payment' AND StoreId=:webStoreId].Integration;
            }
        } catch (Exception e) {
            // For debug logs
            System.debug('Error querying the payment integration from StoreIntegratedService: ' + e.getMessage());
        }
        return paymentGatewayId;
    }
    
    private static ConnectApi.CardPaymentMethodRequest getCardPaymentMethod(Map<String, Object> paymentInfo) {
        System.debug('paymentInfo:' + paymentInfo);
        ConnectApi.CardPaymentMethodRequest paymentMethod = new ConnectApi.CardPaymentMethodRequest();
        paymentMethod.cardCategory = ConnectApi.CardCategory.CreditCard;
        paymentMethod.cardHolderName = (String) paymentInfo.get('cardHolderName');
        paymentMethod.cardType = (String) paymentInfo.get('cardType');
        paymentMethod.cardHolderFirstName = paymentMethod.cardHolderName.contains(' ') ? paymentMethod.cardHolderName.substringBefore(' ') : '';
        paymentMethod.cardHolderLastName = paymentMethod.cardHolderName.contains(' ') ? paymentMethod.cardHolderName.substringAfter(' ') : paymentMethod.cardHolderName;
        paymentMethod.cardNumber = (String) paymentInfo.get('cardNumber');
        paymentMethod.cardType = (String) paymentInfo.get('cardType');
        paymentMethod.AccountId =paymentInfo.containsKey('accountId') ? String.valueOf(paymentInfo.get('accountId')) : '';
        if (paymentInfo.get('cvv') != null) { //throws special error if this is missing
            paymentMethod.cvv = (String) paymentInfo.get('cvv');            
        }
        paymentMethod.expiryMonth = Integer.valueOf(paymentInfo.get('expiryMonth'));
        paymentMethod.expiryYear = Integer.valueOf(paymentInfo.get('expiryYear'));
        
        return paymentMethod;
    }
    @TestVisible
    private static ConnectApi.AddressRequest getAddress(Map<String, Object> billingAddress){
        ConnectApi.AddressRequest address = new ConnectApi.AddressRequest();
        address.street = (String) billingAddress.get('street');
        address.city = (String) billingAddress.get('city');
        address.state = (String) billingAddress.get('state');
        address.country = (String) billingAddress.get('country');
        address.postalCode = (String) billingAddress.get('postalCode');
        
        return address;
    }
    public class POCheckWrapper{
        @AuraEnabled
        public List<String> POList = new List<String>();
        @AuraEnabled
        public String POValue;
       // @AuraEnabled
        //public String OrdMin;

    }
}