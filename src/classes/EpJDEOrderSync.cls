public class EpJDEOrderSync {
    private static Integer TIMEOUT = 120000;
    private static String METHOD = 'POST';
    private static String CONTENT_TYPE = 'text/xml;charset=UTF-8';
    
    public static void defensiveCreateJDESalesOrder(String order_id, String baseURL) {
        if(System.Test.isRunningTest()) {
            String s_code;
            String s_to;
            String s_increase;
            String s_coverage;
            String s_of;
            String s_file;
        }
        if(System.isFuture() || System.isBatch()){
            createJDESalesOrder(order_id, baseURL);
        } else if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
            createJDESalesOrderFuture(order_id, baseURL);
        } else {
            Notifier.sendEmail('EpJDEOrderSync - defensiveCreateJDESalesOrder - @future call was denied');
        }
    }
    
    @future(callout=true)
    public static void createJDESalesOrderFuture(String order_id, String baseURL) {
        createJDESalesOrder(order_id, baseURL);
    }
    
	public static HttpResponse createJDESalesOrder(String order_id, String baseURL) {
        ApexGovernorLimits.printMaxLimits();
        System.debug('EpJDEOrderSync:'+order_id);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            Order order = [SELECT BillingAddress,BillingCity,BillingCountry,BillingCountryCode,BillingPostalCode,BillingState,BillingStateCode,BillingStreet,CreatedDate,ECRecordName__c,Id,JDE_Order_Number__c,OrderNumber,ShippingAddress,ShippingCity,ShippingCountry,ShippingCountryCode,ShippingPostalCode,ShippingState,ShippingStateCode,ShippingStreet,TotalAmount,MagentoCustomerId__c,MagentoId__c,Customer_Is_Guest__c,Coupon_Code__c,Discount_Amount__c,Shipping_Amount__c, Sub_Total__c, Sub_Total_Including_Tax__c, Shipping_Method__c, Billing_First_Name__c, Billing_Last_Name__c, BillingPhone__c, Shipping_First_Name__c, Shipping_Last_Name__c, ShippingPhone__c, Magento_Order_Number__c, CC_EXP_YEAR__c, CC_LAST4__c, CC_EXP_MONTH__c, CC_TYPE__c, Auth_Code__c, LAST_TRANSACTION_ID__c, Currency_code__c, Grand_Total__c,EComStoreID__c,Payment_Method__c,PNREF01__c,PNREF02__c,PROVID__c FROM Order WHERE Id = :order_id];
            ApexGovernorLimits.printLimitsUsed();

            req.setEndpoint('callout:Plano_JDE_Credentials/EcomSalesOrderProcessor');
            req.setMethod(METHOD);
            req.setHeader('Content-Type', CONTENT_TYPE);
            req.setHeader('SOAPAction', 'http://oracle.e1.bssv.JP574201/EcomSalesOrderProcessor/createSalesOrderRequest');
            req.setTimeout(TIMEOUT);
           
            req.setBody(createSalesOrderEnvelope(order));
    
            System.debug('req: ' + req.getBody());
            try {
                res = http.send(req);
                if(res.getStatusCode() == 200)
                {
                    String doc = res.getBody();
                    System.debug('JDEComm: ' + doc);
                    
                    Dom.Document docx = new Dom.Document();
                    docx.load(doc);
                    
                    dom.XmlNode xroot = docx.getrootelement() ;
            
                    dom.XmlNode body = xroot.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
                    system.debug(body);
                    dom.XmlNode createSalesOrderResponse = body.getChildElement('createSalesOrderResponse', 'http://oracle.e1.bssv.JP574201/');
                    system.debug(createSalesOrderResponse);
                
                    Boolean status = false;
                    String errors = '';
                    String customer_id = '';
                    List<OrderItem> orderItems = new List<OrderItem>();
                    for(dom.XmlNode child: createSalesOrderResponse.getChildElements())
                    {
                        if(child.getName() == 'items') {
                            dom.XmlNode documentLineNumber = child.getChildElement('documentLineNumber', null);
                            system.debug(documentLineNumber.getText());
                            
                            dom.XmlNode itemId = child.getChildElement('itemId', null);
                            system.debug(itemId.getText());
                            
                            dom.XmlNode itemProduct = child.getChildElement('itemProduct', null);
                            system.debug(itemProduct.getText());
                            
                            dom.XmlNode quantityOrdered = child.getChildElement('quantityOrdered', null);
                            system.debug(quantityOrdered.getText());
                            
                            String iProduct = itemProduct.getText();
                            if(!iProduct.contains('BILLABLE FREIGHT'))
                            {
                                if (Limits.getQueries() > Limits.getLimitQueries()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                } else {
                                    System.debug('Continue processing. Not going to hit Queries governor limits');
                                    Product2 product = [SELECT Id,ProductCode,EComStoreID__c FROM Product2 WHERE ProductCode = :itemProduct.getText()];
                                    ApexGovernorLimits.printLimitsUsed();
                                    
                                    if (Limits.getQueries() > Limits.getLimitQueries()) {
                                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                                    } else {
                                        System.debug('Continue processing. Not going to hit Queries governor limits');
                                        OrderItem orderItem = [SELECT Id,JDEId__c,EComStoreID__c FROM OrderItem WHERE OrderId = :order_id AND Product2Id = :product.Id];
                                        ApexGovernorLimits.printLimitsUsed();
                                        
                                        String lineItemNumber = documentLineNumber.getText();
                                        if(lineItemNumber.contains('.')) {
                                            lineItemNumber = String.valueOf(Integer.valueOf(lineItemNumber));
                                        }
                                        orderItem.JDEId__c = lineItemNumber;
                                        orderItems.add(orderItem);
                                    }
                                }
                            }
                        } else if(child.getName() == 'customerId') {
                            system.debug(child.getText());
                            customer_id = child.getText();
                        } else if(child.getName() == 'e1MessageList') {
                            for(dom.XmlNode e1Message: child.getChildElements()) {
                                system.debug(e1Message.getChildElement('message', null).getText());
                                errors += e1Message.getChildElement('message', null).getText();
                            }
                        } else if(child.getName() == 'SOId') {
                            system.debug(child.getText());
                            order.JDE_Order_Number__c = child.getText();
                            
                            HttpRequest request = new HttpRequest();
                            HttpResponse response = new HttpResponse();
                            Http http2 = new Http();
                    
                            //request.setEndpoint(baseURL+'orders?searchCriteria[filter_groups][0][filters][0][field]=increment_id&searchCriteria[filter_groups][0][filters][0][value]='+order_id+'&searchCriteria[filter_groups][0][filters][0][condition_type]=eq&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq&searchCriteria[filter_groups][2][filters][0][field]=fraud_status&searchCriteria[filter_groups][2][filters][0][condition_type]=notnull&searchCriteria[filter_groups][3][filters][0][field]=fraud_status&searchCriteria[filter_groups][3][filters][0][value]&searchCriteria[filter_groups][3][filters][0][condition_type]=neq');
                            //request.setEndpoint(baseURL+'orders?searchCriteria[filter_groups][0][filters][0][field]=increment_id&searchCriteria[filter_groups][0][filters][0][value]='+order_id+'&searchCriteria[filter_groups][0][filters][0][condition_type]=eq&searchCriteria[filter_groups][1][filters][0][field]=status&searchCriteria[filter_groups][1][filters][0][value]=processing&searchCriteria[filter_groups][1][filters][0][condition_type]=eq');
                            //request.setHeader('authorization', 'Bearer ' + token);
                            request.setEndpoint(baseURL+'?function=setJDEOrderNumber&order_id='+order.MagentoId__c+'&jde_order_number='+child.getText());
                            request.setMethod('GET');
                            response = http2.send(request);
                            
                        } else if(child.getName() == 'status') {
                            system.debug(child.getText());
                            if(child.getText() == 'Success')
                            {
                                status = true;
                            } else {
                                status = false;
                            }
                        }
                    }
                    ECRecord__c record = [SELECT Id,BSSVStatus__c,JDEExecutionTime__c,JDEResponse__c,SalesForceID__c,EComStoreID__c FROM ECRecord__c WHERE Name = :order.ECRecordName__c];
                    record.JDEExecutionTime__c = DateTime.now();
                    System.debug('RecordCSO: ' + record);
                    if(status)
                    {
                        if(order.Customer_Is_Guest__c == 0)
                        {
                            if (Limits.getQueries() > Limits.getLimitQueries()) {
                                System.debug('Need to stop processing to avoid hitting a governor limit.');
                            } else {
                                System.debug('Continue processing. Not going to hit Queries governor limits');
                                Contact contact = [SELECT Id, JDEId__c, ECRecordName__c, MagentoId__c,EComStoreID__c FROM Contact WHERE MagentoId__c = :order.MagentoCustomerId__c AND EComStoreID__c = :order.EComStoreID__c];
                                ApexGovernorLimits.printLimitsUsed();
                                if(contact != null) {
                                    contact.JDEId__c = customer_id;
                                    if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                                    } else {
                                        System.debug('Continue processing. Not going to hit DML governor limits');
                                        update contact;
                                        ApexGovernorLimits.printLimitsUsed();
                                    }
                                }
                            }
                        }
                        if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit DML governor limits');
                            update order;
                            ApexGovernorLimits.printLimitsUsed();
                        }
                        if(!orderItems.IsEmpty())
                        {
                            if (orderItems.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                                System.debug('Need to stop processing to avoid hitting a governor limit.');
                            } else {
                                System.debug('Continue processing. Not going to hit DML governor limits');
                                update orderItems;
                                ApexGovernorLimits.printLimitsUsed();
                            }
                        }
                        
                        record.BSSVStatus__c = true;
                        record.JDEResponse__c = 'Success';
                    } else {
                        record.BSSVStatus__c = false;
                        record.JDEResponse__c = errors;
                        //Notifier.sendEmail('EpJDEOrderSync - createJDESalesOrder: Receiving some errors: '+ errors);
                    }
                    if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit DML governor limits');
                        update record;
                        ApexGovernorLimits.printLimitsUsed();
                    }
                } else {
                    Notifier.sendEmail('EpJDEOrderSync - createJDESalesOrder: '+ res.getStatusCode() + '|' + res.getStatus());
                    if (Limits.getQueries() > Limits.getLimitQueries()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit Queries governor limits');
                        ECRecord__c record = [SELECT Id,BSSVStatus__c,JDEExecutionTime__c,JDEResponse__c,SalesForceID__c,EComStoreID__c FROM ECRecord__c WHERE Name = :order.ECRecordName__c];
                        ApexGovernorLimits.printLimitsUsed();
                        
                        record.JDEExecutionTime__c = DateTime.now();
                        record.BSSVStatus__c = false;
                        record.JDEResponse__c = res.getStatusCode() + '|' + res.getStatus();
                        if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit DML governor limits');
                            update record;
                            ApexGovernorLimits.printLimitsUsed();
                        }
                    }
                }
            } catch(System.Exception e) {
                System.debug('EpJDEOrderSync - createJDESalesOrder - Exception error: '+ e);
                Notifier.sendEmail('EpJDEOrderSync - createJDESalesOrder - Exception error: '+ e);
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    ECRecord__c record = [SELECT Id,BSSVStatus__c,JDEExecutionTime__c,JDEResponse__c,SalesForceID__c,EComStoreID__c FROM ECRecord__c WHERE Name = :order.ECRecordName__c];
                    ApexGovernorLimits.printLimitsUsed();
                    record.JDEExecutionTime__c = DateTime.now();
                    record.BSSVStatus__c = false;
                    record.JDEResponse__c = e.getMessage();
                    if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit DML governor limits');
                        update record;
                        ApexGovernorLimits.printLimitsUsed();
                    }
                }
                

            } finally {
                System.debug(res.toString());
            }
        }
        return res;
    }
    
    private static String createSalesOrderEnvelope(Order salesOrder) {
        Contact contact = null;
        if(salesOrder.Customer_Is_Guest__c == 0) {
            if (Limits.getQueries() > Limits.getLimitQueries()) {
                System.debug('Need to stop processing to avoid hitting a governor limit.');
            } else {
                System.debug('Continue processing. Not going to hit Queries governor limits');
                contact = [SELECT Id,MagentoId__c,ECRecordName__c,Email,FirstName,HomePhone,JDEId__c,LastName,Phone,OtherPhone,EComStoreID__c FROM Contact WHERE  MagentoId__c = : salesOrder.MagentoCustomerId__c AND EComStoreID__c = : salesOrder.EComStoreID__c];
                ApexGovernorLimits.printLimitsUsed();
            }
        }
        Dom.Document doc = new Dom.Document();
        
        dom.XmlNode envelope = doc.createRootElement('Envelope', 'http://schemas.xmlsoap.org/soap/envelope/', 'soapenv');
        envelope.setNamespace('orac', 'http://oracle.e1.bssv.JP574201/');
        
        dom.XmlNode header = envelope.addChildElement('Header', 'http://schemas.xmlsoap.org/soap/envelope/', 'soapenv');
        
        dom.XmlNode security = header.addChildElement('Security', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        security.setAttributeNS('mustUnderstand', '1', 'http://schemas.xmlsoap.org/soap/envelope/', null);
        
        dom.XmlNode usernameToken = security.addChildElement('UsernameToken', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        
        dom.XmlNode username = usernameToken.addChildElement('Username', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        username.addTextNode('{!$Credential.Username}');
        
        dom.XmlNode password = usernameToken.addChildElement('Password', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        password.setAttribute('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
        password.addTextNode('{!$Credential.Password}');
        
        dom.XmlNode body = envelope.addChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/', 'soapenv');
        
        dom.XmlNode createSalesOrder = body.addChildElement('createSalesOrder', 'http://oracle.e1.bssv.JP574201/', 'orac');
        
        dom.XmlNode billing = createSalesOrder.addChildElement('billing', null, null);
        
        String[] billingStreet = salesOrder.BillingStreet.split(',');
        
        dom.XmlNode addressLine1 = billing.addChildElement('addressLine1', null, null);
        if(billingStreet.size()>0)
        	addressLine1.addTextNode(billingStreet[0]);
        
        dom.XmlNode addressLine2 = billing.addChildElement('addressLine2', null, null);
        if(billingStreet.size()>1)
        	addressLine2.addTextNode(billingStreet[1]);
        
        dom.XmlNode addressLine3 = billing.addChildElement('addressLine3', null, null);
        if(billingStreet.size()>2)
        	addressLine3.addTextNode(billingStreet[2]);
        
        dom.XmlNode addressLine4 = billing.addChildElement('addressLine4', null, null);
        if(billingStreet.size()>3)
        	addressLine4.addTextNode(billingStreet[3]);
        
        dom.XmlNode city = billing.addChildElement('city', null, null);
        if(salesOrder.BillingCity!=null)
            city.addTextNode(salesOrder.BillingCity);
        
        dom.XmlNode country = billing.addChildElement('country', null, null);
        if(salesOrder.BillingCountryCode!=null)
            country.addTextNode(salesOrder.BillingCountryCode);
        
        dom.XmlNode firstName = billing.addChildElement('firstName', null, null);
        if(salesOrder.Billing_First_Name__c!=null)
        	firstName.addTextNode(salesOrder.Billing_First_Name__c);
        
        dom.XmlNode lastName = billing.addChildElement('lastName', null, null);
        if(salesOrder.Billing_Last_Name__c!=null)
        	lastName.addTextNode(salesOrder.Billing_Last_Name__c);
        
        dom.XmlNode phone = billing.addChildElement('phone', null, null);
        if(salesOrder.BillingPhone__c!=null)
        	phone.addTextNode(salesOrder.BillingPhone__c);
        
        dom.XmlNode postalCode = billing.addChildElement('postalCode', null, null);
        if(salesOrder.BillingPostalCode!=null)
            postalCode.addTextNode(salesOrder.BillingPostalCode);
        
        dom.XmlNode state = billing.addChildElement('state', null, null);
        state.addTextNode(salesOrder.BillingStateCode);
        
        dom.XmlNode customer = createSalesOrder.addChildElement('customer', null, null);
        
        dom.XmlNode customerId = customer.addChildElement('customerId', null, null);
        if(contact!=null && contact.JDEId__c != NULL)
        {
        	customerId.addTextNode(contact.JDEId__c);
        } else if (contact!=null)
        {
            dom.XmlNode email = customer.addChildElement('email', null, null);
            email.addTextNode(contact.Email);
            
            dom.XmlNode custFirstName = customer.addChildElement('firstName', null, null);
            custFirstName.addTextNode(contact.FirstName);
            
            dom.XmlNode custLastName = customer.addChildElement('lastName', null, null);
            custLastName.addTextNode(contact.LastName);
        }
        dom.XmlNode isGuest = customer.addChildElement('isGuest', null, null);
		isGuest.addTextNode(String.ValueOf(salesOrder.Customer_Is_Guest__c));
        
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            List<OrderItem> orderItems = [SELECT ECRecordName__c,Id,JDEId__c,MagentoId__c,OrderId,OrderItemNumber,Quantity,Quantity_Ordered__c,Shipment_Number__c,TotalPrice,UnitPrice,Product2Id,Discount_Amount__c,EComStoreID__c FROM OrderItem WHERE OrderId = :salesOrder.Id];
            ApexGovernorLimits.printLimitsUsed();
            
            Decimal orderWeight = 0;
            for(OrderItem orderItem: orderItems)
            {
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    Product2 product = [SELECT Description,Id,JdeId__c,MagentoId__c,Name,ProductCode,Weight__c,EComStoreID__c FROM Product2 WHERE Id = :orderItem.Product2Id];
                    ApexGovernorLimits.printLimitsUsed();
                    
                    dom.XmlNode items = createSalesOrder.addChildElement('items', null, null);
                    
                    dom.XmlNode itemId = items.addChildElement('itemId', null, null);
                    itemId.addTextNode(String.valueOf(product.JdeId__c));
                    Decimal weight = 0;
                    if(product.Weight__c != null)
                        weight = product.Weight__c;
                    orderWeight += weight;
                    
                    dom.XmlNode itemPrice = items.addChildElement('itemPrice', null, null);
                    /*if(salesOrder.Coupon_Code__c == null) {
                        itemPrice.addTextNode(String.valueOf(orderItem.UnitPrice));
                    } else {
                        itemPrice.addTextNode(String.valueOf(orderItem.UnitPrice-orderItem.Discount_Amount__c));
                    }*/
                    itemPrice.addTextNode(String.valueOf(orderItem.UnitPrice-orderItem.Discount_Amount__c));
                    
                    dom.XmlNode itemPriceIncTax = items.addChildElement('itemPriceIncTax', null, null);
                    itemPriceIncTax.addTextNode(String.valueOf(orderItem.TotalPrice));
                    
                    dom.XmlNode quantity = items.addChildElement('quantity', null, null);
                    quantity.addTextNode(String.valueOf(Integer.valueOf(orderItem.Quantity_Ordered__c)));
                }
            }
            
            dom.XmlNode order = createSalesOrder.addChildElement('order', null, null);
            
            dom.XmlNode createdDate = order.addChildElement('createdDate', null, null);
            createdDate.addTextNode(String.valueOf(salesOrder.CreatedDate));//'2018-01-05T00:00:00-06:00'
            
            dom.XmlNode grandTotal = order.addChildElement('grandTotal', null, null);
            grandTotal.addTextNode(String.valueOf(salesOrder.Grand_Total__c));
            
            dom.XmlNode discountAmount = order.addChildElement('discountAmount', null, null);
            Decimal disc = salesOrder.Discount_Amount__c * -1;
            discountAmount.addTextNode(String.valueOf(disc));
                                          
            dom.XmlNode shippingAmount = order.addChildElement('shippingAmount', null, null);
            shippingAmount.addTextNode(String.valueOf(salesOrder.Shipping_Amount__c));
            
            dom.XmlNode subTotal = order.addChildElement('subTotal', null, null);
            subTotal.addTextNode(String.valueOf(salesOrder.Sub_Total__c));
                                          
            dom.XmlNode subTotalIncTax = order.addChildElement('subTotalIncTax', null, null);
            subTotalIncTax.addTextNode(String.valueOf(salesOrder.Sub_Total_Including_Tax__c));
                                      
            dom.XmlNode SFOrderId = order.addChildElement('SFOrderId', null, null);
            if(salesOrder.Magento_Order_Number__c != null)
                SFOrderId.addTextNode(String.ValueOf(salesOrder.Magento_Order_Number__c));
            
            dom.XmlNode weight = order.addChildElement('weight', null, null);
            weight.addTextNode(String.ValueOf(orderWeight));
    
            dom.XmlNode couponCode = order.addChildElement('couponCode', null, null);
            if(salesOrder.Coupon_Code__c!=null)
                couponCode.addTextNode(String.ValueOf(salesOrder.Coupon_Code__c));
                    
            dom.XmlNode orderCurrency = order.addChildElement('orderCurrency', null, null);
            if(salesOrder.Currency_code__c!=null)
                orderCurrency.addTextNode(salesOrder.Currency_code__c);
            
            dom.XmlNode payment = createSalesOrder.addChildElement('payment', null, null);
            
            dom.XmlNode CCExpYear = payment.addChildElement('CCExpYear', null, null);
            if(salesOrder.CC_EXP_YEAR__c!=null)
                CCExpYear.addTextNode(salesOrder.CC_EXP_YEAR__c);
            
            dom.XmlNode CCLast4 = payment.addChildElement('CCLast4', null, null);
            if(salesOrder.CC_LAST4__c!=null)
                CCLast4.addTextNode(salesOrder.CC_LAST4__c);
            
            dom.XmlNode CCExpMonth = payment.addChildElement('CCExpMonth', null, null);
            if(salesOrder.CC_EXP_MONTH__c!=null)
                CCExpMonth.addTextNode(salesOrder.CC_EXP_MONTH__c);
            
            dom.XmlNode method = payment.addChildElement('method', null, null);
            if(salesOrder.CC_TYPE__c!=null)
                method.addTextNode(salesOrder.CC_TYPE__c);
            
            dom.XmlNode payMethod = payment.addChildElement('payMethod', null, null);
            if(salesOrder.Payment_Method__c!=null)
                payMethod.addTextNode(salesOrder.Payment_Method__c);
            
            if(salesOrder.Payment_Method__c == 'payflow_express') {
                dom.XmlNode PNREF01 = payment.addChildElement('PNREF01', null, null);
                if(salesOrder.PNREF01__c!=null)
                    PNREF01.addTextNode(salesOrder.PNREF01__c);
                
                dom.XmlNode PNREF02 = payment.addChildElement('PNREF02', null, null);
                if(salesOrder.PNREF02__c!=null)
                    PNREF02.addTextNode(salesOrder.PNREF02__c);
                
                dom.XmlNode PROVID = payment.addChildElement('PROVID', null, null);
                if(salesOrder.PROVID__c!=null)
                    PROVID.addTextNode(salesOrder.PROVID__c);
            }
            
            dom.XmlNode authCode = payment.addChildElement('authCode', null, null);
            if(salesOrder.Auth_Code__c!=null)
                authCode.addTextNode(salesOrder.Auth_Code__c);
            
            dom.XmlNode freightUpCharge = payment.addChildElement('freightUpCharge', null, null);
            if(salesOrder.Shipping_Amount__c!=null)
                freightUpCharge.addTextNode(String.valueOf(salesOrder.Shipping_Amount__c));
            
            dom.XmlNode lastTransId = payment.addChildElement('lastTransId', null, null);
            if(salesOrder.LAST_TRANSACTION_ID__c!=null)
                lastTransId.addTextNode(salesOrder.LAST_TRANSACTION_ID__c);
      
            dom.XmlNode shipping = createSalesOrder.addChildElement('shipping', null, null);
            
            String[] shippingStreet = salesOrder.ShippingStreet.split(',');
            
            addressLine1 = shipping.addChildElement('addressLine1', null, null);
            if(shippingStreet.size()>0)
                addressLine1.addTextNode(shippingStreet[0]);
            
            addressLine2 = shipping.addChildElement('addressLine2', null, null);
            if(shippingStreet.size()>1)
                addressLine2.addTextNode(shippingStreet[1]);
            
            addressLine3 = shipping.addChildElement('addressLine3', null, null);
            if(shippingStreet.size()>2)
                addressLine3.addTextNode(shippingStreet[2]);
            
            addressLine4 = shipping.addChildElement('addressLine4', null, null);
            if(shippingStreet.size()>3)
                addressLine4.addTextNode(shippingStreet[3]);
            
            city = shipping.addChildElement('city', null, null);
            if(salesOrder.ShippingCity!=null)
                city.addTextNode(salesOrder.ShippingCity);
            
            country = shipping.addChildElement('country', null, null);
            if(salesOrder.ShippingCountryCode!=null)
                country.addTextNode(salesOrder.ShippingCountryCode);
            
            firstName = shipping.addChildElement('firstName', null, null);
            if(salesOrder.Shipping_First_Name__c!=null)
                firstName.addTextNode(salesOrder.Shipping_First_Name__c);
            
            lastName = shipping.addChildElement('lastName', null, null);
            if(salesOrder.Shipping_Last_Name__c!=null)
                lastName.addTextNode(salesOrder.Shipping_Last_Name__c);
            
            phone = shipping.addChildElement('phone', null, null);
            if(salesOrder.ShippingPhone__c!=null)
                phone.addTextNode(salesOrder.ShippingPhone__c);
            
            postalCode = shipping.addChildElement('postalCode', null, null);
            if(salesOrder.ShippingPostalCode!=null)
                postalCode.addTextNode(salesOrder.ShippingPostalCode);
            
            state = shipping.addChildElement('state', null, null);
            if(salesOrder.ShippingStateCode!=null)
                state.addTextNode(salesOrder.ShippingStateCode);
    
            method = shipping.addChildElement('method', null, null);
            if(salesOrder.Shipping_Method__c!=null)
                method.addTextNode(salesOrder.Shipping_Method__c);
            
        }

        System.debug(doc.toXmlString());
        return doc.toXmlString();
    }
    
}