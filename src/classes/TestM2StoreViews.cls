@isTest(seeAllData=true)
public class TestM2StoreViews {
	static testMethod void testStoreViewsSync() {
		Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new EpM2StoreViewsMock());
        EpM2StoreViewsSync.getStoreViewsFuture('orx161y845xry84i4b219fuy6ig70fjr', '00000000', '2/24/2019 11:20 AM', 'https://www.halooptics.com/rest/all/V1/', 'logsdebug@gmail.com');
        Test.stopTest();        
    }
}