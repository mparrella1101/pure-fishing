public class CustomerAddresses {
    public String id {get;set;}
    public CustomerRegion region {get;set;}
    public String country_id {get;set;}
    public List<String> street {get;set;}
    public String telephone {get;set;}
    public String postcode {get;set;}
    public String city {get;set;}
}