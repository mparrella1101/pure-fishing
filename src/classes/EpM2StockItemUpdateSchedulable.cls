public class EpM2StockItemUpdateSchedulable implements Schedulable {
    private List<String> sku{get;Set;}
    private List<String> qty{get;Set;}
    private List<String> eComStoreID{get;Set;}

    public EpM2StockItemUpdateSchedulable(List<String>  sku, List<String> qty, List<String>  eComStoreID){
        this.sku = sku;
        this.qty = qty;
        this.eComStoreID = eComStoreID;
    }
    public void execute(SchedulableContext sc) {
        for(Integer index=0; index<sku.size(); index++) {
            if(this.sku.get(index) != null) {
                if(System.Test.isRunningTest()) {
                    scheduleItemSyncJob(index);
                }
                
                if(System.isFuture() || System.isBatch() || Limits.getFutureCalls() < Limits.getLimitFutureCalls()) { // if future call limit is available
                    EpM2StockItemUpdateSync.defensiveStockItemUpdate(sku.get(index), qty.get(index), eComStoreID.get(index));
                } else if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){ // if queueable call limit is available
                    System.enqueueJob(new EpM2StockItemUpdateJob(sku.get(index), qty.get(index),eComStoreID.get(index)));
                } else {
					scheduleItemSyncJob(index);
                }
            }
        }     
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
    
    public void scheduleItemSyncJob(Integer index) {
        Integer scheduledJobCount = [SELECT COUNT() FROM CronJobDetail WHERE JobType = '7'];
        if(scheduledJobCount<100) {
            try {
                Datetime dt = Datetime.now().addMinutes(2);
                Integer hour, min, ss;
                hour = dt.hour();
                min = dt.minute(); 
                ss = dt.second();
                if(System.Test.isRunningTest()) {
                    hour = 24;
                    min = 59; 
                    ss = 59 + 5;
                }
                while(ss>59)
                {
                    min += 1;
                    ss = ss - 59;
                }
                
                while(min>59)
                {
                    hour += 1;
                    min = min - 59;
                }
                
                if(hour>23)
                {
                    hour = 0;
                }
                //parse to cron expression
                String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                System.debug('nextFireTime: ' + nextFireTime);
                System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, new EpM2StockItemUpdateSyncSchedulable(sku.get(index), qty.get(index),eComStoreID.get(index)));
            } catch(Exception ex) {
                System.debug('EpM2StockItemUpdateSchedulable Exception: ' + ex.getMessage());
                Notifier.sendEmail('EpM2StockItemUpdateSchedulable Exception: ' + ex);
            }
        } else {
            Notifier.sendEmail('EpM2StockItemUpdateSchedulable - Limits reached - Cannot update sku:'+sku.get(index));
        }
    }
}