public class EpJDECreateSalesOrderSchedulable implements Schedulable {
    private String order_id{get;Set;}
    private String baseURL{get;Set;}
    /*
     * Constructor
     */
    public EpJDECreateSalesOrderSchedulable(String order_id, String baseURL){
        this.order_id = order_id;
        this.baseURL = baseURL;
    }
    public void execute(SchedulableContext sc) {
        System.enqueueJob(new EpJDECreateSalesOrderJob(order_id, baseURL));
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}