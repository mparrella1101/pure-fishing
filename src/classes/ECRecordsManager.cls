@RestResource(urlMapping='/ECRecordsManager/*')
global with sharing class ECRecordsManager {

    @HttpPost
    global static ID createECRecord(String dump, String type) {
        ECRecord__c record = new ECRecord__c();
        record.Dump__c = dump;
        record.Type__c = type;
        record.JDEExecutionTime__c = DateTime.now();
        record.BSSVStatus__c = true;
        record.JDEResponse__c = 'Success';
        record.MagentoResponse__c = null;
        insert record;
        return record.Id;
    }
}