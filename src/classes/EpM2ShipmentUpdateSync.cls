global class EpM2ShipmentUpdateSync {

	private static List<ShipmentSalesOrderItem> shipments = new List<ShipmentSalesOrderItem>();
    private static List<String> orderItemIDs = new List<String>();
    private static List<String> ECRecordNames = new List<String>();
    private static List<String> ShipmentNumbers = new List<String>();
    private static List<String> order_ids = new List<String>();
    
    public static void defensiveUpdateShipment(String ecRecordName, String order_id, String body, List<String> orderItemIDs, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        if(System.isFuture() || System.isBatch()){
            updateShipment(ecRecordName, order_id, body, orderItemIDs, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
        } else if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()) {
            updateShipmentFuture(ecRecordName, order_id, body, orderItemIDs, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
        } else {
            Notifier.sendEmail('EpM2ShipmentUpdateSync - defensiveUpdateShipment - @future call was denied', supportEmail);
        }
    }
    
    @future(callout=true)
    public static void updateShipmentFuture(String ecRecordName, String order_id, String body, List<String> orderItemIDs, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        updateShipment(ecRecordName, order_id, body, orderItemIDs, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
    }
    
    public static void updateShipment(String ecRecordName, String order_id, String body, List<String> orderItemIDs, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
		ApexGovernorLimits.printMaxLimits();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setEndpoint(baseURL+'?function=updateShipment&order_id='+order_id);
        request.setHeader('authorization', 'Bearer ' + token);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST');
        request.setBody(body);
        
        System.debug('EpM2ShipmentUpdateSync - Request: ' + request.getBody());
        try {
            response = http.send(request);

             if(EpM2ShipmentUpdateSync.shipments.size()>0) {
                    EpM2ShipmentUpdateSync.shipments.remove(0);
                    EpM2ShipmentUpdateSync.orderItemIDs.remove(0);
                    EpM2ShipmentUpdateSync.ECRecordNames.remove(0);
                    EpM2ShipmentUpdateSync.ShipmentNumbers.remove(0);
                    EpM2ShipmentUpdateSync.order_ids.remove(0);
                    updateShipmentProcess();
            }
            String jsonString = response.getBody();
            System.debug('EpM2ShipmentUpdateSync - Response: ' + jsonString); 
            
            if (Limits.getQueries() > Limits.getLimitQueries()) {
                System.debug('Need to stop processing to avoid hitting a governor limit.');
            } else {
                System.debug('Continue processing. Not going to hit Queries governor limits');
                ECRecord__c record = [SELECT Id,MagentoAPIStatus__c,MagentoExecutionTime__c,MagentoResponse__c,EComStoreID__c FROM ECRecord__c WHERE Name = :ecRecordName];
                ApexGovernorLimits.printLimitsUsed();
                
                System.debug('EpM2ShipmentUpdateSync updateShipment: ' +record);
                String m2Response = '';
                if(record.MagentoResponse__c!=null)
                    m2Response = record.MagentoResponse__c;
    			System.debug('response.getStatusCode():'+response.getStatusCode());
                System.debug('response.getStatusCode() == 200:'+(response.getStatusCode() == 200));
                if(response.getStatusCode() == 200) {
                    m2Response += 'Success:'+order_id+',';
                    record.MagentoAPIStatus__c = true;
                    
                    List<OrderItem> orderItems = new List<OrderItem>();
                    for(String orderItemID : orderItemIDs)
                    {
                        if(orderItemID != null) 
                        {
                            if(orderItemID.contains(','))
                            {
                                List<String> itemIDs = orderItemID.split(',');
                                for(String itemID : itemIDs) {
                                    if (Limits.getQueries() > Limits.getLimitQueries()) {
                                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                                    } else {
                                        System.debug('Continue processing. Not going to hit Queries governor limits');
                                        OrderItem orderItem = [SELECT Id, Shipment_Number__c,EComStoreID__c FROM OrderItem WHERE Id= :itemID];
                                        ApexGovernorLimits.printLimitsUsed();
                                        
                                        orderItem.Shipment_Number__c = jsonString.replaceAll('"', '');
                                        orderItems.add(orderItem);
                                    }
                                }
                            } else {
                                if (Limits.getQueries() > Limits.getLimitQueries()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                } else {
                                    System.debug('Continue processing. Not going to hit Queries governor limits');
                                    OrderItem orderItem = [SELECT Id, Shipment_Number__c,EComStoreID__c FROM OrderItem WHERE Id= :orderItemID];
                                    ApexGovernorLimits.printLimitsUsed();
                                    
                                    orderItem.Shipment_Number__c = jsonString.replaceAll('"', '');
                                    orderItems.add(orderItem);
                                }
                            }
                        }
                    }
                    if(!orderItems.isEmpty())
                    {
                        if (orderItems.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit DML governor limits');
                            update orderItems;
                            ApexGovernorLimits.printLimitsUsed();
                        }	
                    }
                } else {
                    Notifier.sendEmail('EpM2ShipmentUpdateSync - updateShipment - M2OrderId:- '+ order_id + ' - ' + jsonString);
                    m2Response += 'Failed:'+order_id+',';record.MagentoAPIStatus__c = false;
                }
                
                record.MagentoResponse__c = m2Response;
                record.MagentoExecutionTime__c = DateTime.now();
                if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit DML governor limits');
                    update record;
                    ApexGovernorLimits.printLimitsUsed();
                }	
            }
        } catch(System.Exception e) {
            System.debug('EpM2ShipmentUpdateSync - updateShipment - Exception error: '+ e);
            System.debug(response.toString());
            //Notifier.sendEmail('EpM2ShipmentUpdateSync - updateShipment - Exception error: '+ e,supportEmail);
            
            if (Limits.getQueries() > Limits.getLimitQueries()) {
                System.debug('Need to stop processing to avoid hitting a governor limit.');
            } else {
                System.debug('Continue processing. Not going to hit Queries governor limits');
                ECRecord__c record = [SELECT Id,MagentoAPIStatus__c,MagentoExecutionTime__c,MagentoResponse__c,EComStoreID__c FROM ECRecord__c WHERE Name = :ecRecordName];
                ApexGovernorLimits.printLimitsUsed();
                
                record.MagentoAPIStatus__c = false;
                record.MagentoExecutionTime__c = DateTime.now();
                record.MagentoResponse__c = response.toString();
                if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit DML governor limits');
                    update record;
                    ApexGovernorLimits.printLimitsUsed();
                }
            }
        }
    }
    
    public static void updateShipmentSynchronously(List<ShipmentSalesOrderItem> shipments,
                                                  List<String> orderItemIDs, List<String> ECRecordNames,
                                                   List<String> ShipmentNumbers, List<String> order_ids) {
        EpM2ShipmentUpdateSync.shipments.addAll(shipments);
        EpM2ShipmentUpdateSync.orderItemIDs.addAll(orderItemIDs);
        EpM2ShipmentUpdateSync.ECRecordNames.addAll(ECRecordNames);
        EpM2ShipmentUpdateSync.ShipmentNumbers.addAll(ShipmentNumbers);
        EpM2ShipmentUpdateSync.order_ids.addAll(order_ids);
        
        updateShipmentProcess();
    }
    
    public static void updateShipmentProcess() {
        
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.shipments: '+EpM2ShipmentUpdateSync.shipments.size());
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.orderItemIDs: '+EpM2ShipmentUpdateSync.orderItemIDs.size());
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.ECRecordNames: '+EpM2ShipmentUpdateSync.ECRecordNames.size());
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.ShipmentNumbers: '+EpM2ShipmentUpdateSync.ShipmentNumbers.size());
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.order_ids: '+EpM2ShipmentUpdateSync.order_ids.size());

            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.shipments: '+EpM2ShipmentUpdateSync.shipments);
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.orderItemIDs: '+EpM2ShipmentUpdateSync.orderItemIDs);
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.ECRecordNames: '+EpM2ShipmentUpdateSync.ECRecordNames);
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.ShipmentNumbers: '+EpM2ShipmentUpdateSync.ShipmentNumbers);
            System.debug('updateShipmentProcess > EpM2ShipmentUpdateSync.order_ids: '+EpM2ShipmentUpdateSync.order_ids);
        
 		if(EpM2ShipmentUpdateSync.shipments!=null && EpM2ShipmentUpdateSync.shipments.size() != 0) {
            if(EpM2ShipmentUpdateSync.ECRecordNames.get(0) != null && EpM2ShipmentUpdateSync.ShipmentNumbers.get(0) == null) {
                ShipmentSalesOrderItem shpmnt = EpM2ShipmentUpdateSync.shipments.get(0);
                String ecRecordName = EpM2ShipmentUpdateSync.ECRecordNames.get(0);
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    Order order = [SELECT MagentoId__c, ECRecordName__c,EComStoreID__c FROM Order WHERE Id = :order_ids.get(0)];
                    ApexGovernorLimits.printLimitsUsed();
                    
                    String body = System.JSON.serialize(shpmnt);
                    
                    System.debug('END JSON:'+body);
                    System.debug('ECRecordName:'+ecRecordName);
                    
                    if (Limits.getQueries() > Limits.getLimitQueries()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit Queries governor limits');
                        MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :order.EComStoreID__c];
                        ApexGovernorLimits.printLimitsUsed();
                        
                        String last_sync_time = store.LastSalesOrderSyncTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
                        
                        List<String> itemList = new List<String>();
                        if(EpM2ShipmentUpdateSync.orderItemIDs.get(0).contains(','))
                            itemList = EpM2ShipmentUpdateSync.orderItemIDs.get(0).split(',');
                        else
                            itemList = new List<String>{EpM2ShipmentUpdateSync.orderItemIDs.get(0)};
        
                        EpM2ShipmentUpdateSync.defensiveUpdateShipment(ecRecordName, order.MagentoId__c,body, itemList, store.AccessToken__c, store.Name, last_sync_time, store.BaseURL__c, store.SupportEmail__c);   
                    }
                }
            }
        }    
    }
}