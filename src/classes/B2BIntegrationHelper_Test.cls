/**
 * @description Test Class to cover the 'B2BIntegrationHelper' class
 */

 /* Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud            06-17-2022      Initial version
 */

@IsTest
public with sharing class B2BIntegrationHelper_Test {
    @IsTest
    static void getIntegrationData_Test() {
        B2BIntegrationHelper.IntegrationHelperWrapper testWrap = B2BIntegrationHelper.getIntegrationData();
        System.assertEquals(true, testWrap.hasData);
    }
}