public class SalesOrderPayment
{
    public List<string> additional_information { get; set; }
    public String cc_exp_year { get; set; }
    public String cc_last4 { get; set; }
    public String cc_type { get; set; }
    public String cc_owner { get; set; }
    public String cc_exp_month { get; set; }
    public String last_trans_id { get; set; }
    public String method { get; set; }
}