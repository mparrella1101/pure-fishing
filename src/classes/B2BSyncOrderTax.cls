/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/04/2022            Initial Version
 */
// Determines the taxes for the Order
public class B2BSyncOrderTax {
    // This invocable method only expects one ID
    @InvocableMethod(callout=true label='Prepare the Order taxes' description='Runs a synchronous version of taxes' category='B2B Commerce')
    public static void syncTax(List<ID> orderIds) {
        // Validate the input
        if (orderIds == null || orderIds.size() != 1) {
            String errorMessage = 'A Order id must be included to B2BSyncOrderTax'; // Get wording from doc!!!
            // Sync non-user errors skip saveCartValidationOutputError
            throw new CalloutException (errorMessage);
        }
        
        // Extract cart id and start processing
        Id orderId = orderIds[0];
        startOrderProcessSync(orderId);
    }

    private static void startOrderProcessSync(Id orderId) {
        
        Date ShipDate;
        String ActId;
        String shipAct;
        String Shipto;
        String reasonCode;
        String disChannel;
         for( Order ActInfo:[Select AccountId,ShippingFutureDate__c,Ship_To__c,Ship_To_Account__c,Reason_Code__c,Distribution_Channel__c from Order where Id = : orderId])
            {
                ActId=ActInfo.AccountId;
                ShipDate= ActInfo.ShippingFutureDate__c;
                shipAct=ActInfo.Ship_To__c;
                Shipto=ActInfo.Ship_To_Account__c;
                reasonCode=ActInfo.Reason_Code__c;
                disChannel=ActInfo.Distribution_Channel__c;
                
            }
        /*List<Account> shipCust = [Select SAP_Customer__c from Account where Id = : shipAct];
        String Shipto;
        if(shipCust.size() > 0)
        {
           Shipto= shipCust[0].SAP_Customer__c;
        }*/
        String Shipdateformat;
        DateTime stConverted;
        if(ShipDate == null)
        {
            ShipDate= system.today();
            DateTime dtConverted = Datetime.newInstance(ShipDate.year(), ShipDate.month(),ShipDate.day());
            Shipdateformat=dtConverted.format('yyyy-MM-dd');
        }
        else
        {
            stConverted = Datetime.newInstance(ShipDate.year(), ShipDate.month(),ShipDate.day());
            Shipdateformat=stConverted.format('yyyy-MM-dd');
        }
        // Get all SKUs, the cart item IDs, and the total prices from the cart items.
        Account ActInfos = [Select Division_SAP__c,SAP_Customer__c,Distribution_Channel__c,Sales_Org__c,Currency__c  from Account where Id = : ActId][0];
        list<b2b_TaxWrapper.cls_products> proList = new list<b2b_TaxWrapper.cls_products>();
        Map<String, Id> ordItemIdsBySKU = new Map<String, Id>();
        Map<String, Integer> orderItemByQant = new Map<String, Integer>();
        Integer Quant;
        //Map<String, Decimal> ordItemTotalPriceBySKU = new Map<String, Decimal>();
        for (OrderItem orderItem : [SELECT TotalPrice,Product2.Material__c,Quantity, Type FROM OrderItem WHERE OrderId = :orderId]) {
            String orderItemSKU = '';
            if (orderItem.Type == 'Order Product') {
                orderItemSKU = orderItem.Product2.Material__c;
                b2b_TaxWrapper.cls_products product = new b2b_TaxWrapper.cls_products();
                product.materialNumber = orderItem.Product2.Material__c;
                Quant = Integer.valueOf(orderItem.Quantity);
                product.quantity = string.valueOf(Quant);
                //product.quantity='1';
                proList.add(product);
                system.debug('proList:' + proList + ' product:' + product);
                ordItemIdsBySKU.put(orderItemSKU, orderItem.Id);
                orderItemByQant.put(orderItemSKU,Quant);
            }
            else if (orderItem.Type == 'Delivery Charge') {
                // This is an example for a Cart Item of type shipping charge.
                // For simplicity and testing purposes, we just assign some SKU to this charge so that the taxation external service returns some value.
                //cartItemSKU = 'ChargeSKU';
            }
            //cartItemTotalPriceBySKU.put(cartItemSKU, cartItem.TotalPrice);
        }      
        string OrdType = 'OR';
        b2b_TaxWrapper req = new b2b_TaxWrapper();
        //b2b_pricingWrapper.cls_products product = new b2b_pricingWrapper.cls_products();
        //product.materialNumber = '1265710';
        //product.quantity = '2';
        //list<b2b_pricingWrapper.cls_products> proList = new list<b2b_pricingWrapper.cls_products>();
       // proList.add(product);
        req.division = '00' + ActInfos.Division_SAP__c;
        req.customerNumber = ActInfos.SAP_Customer__c;
        req.salesOrganization = ActInfos.Sales_Org__c;
        //req.shipTo = shipCust[0]?.SAP_Customer__c;
        req.shipTo=Shipto;
        //req.orderType = 'OR';
        req.orderReason = reasonCode;
        switch on reasonCode
				 {
					 when '400'{OrdType='ZSMP';}
					 when '401'{OrdType='ZSMP';}
					 when '402'{OrdType='ZSMP';}
					 when '403'{OrdType='ZSMP';}
					 when '404'{OrdType='ZSMP';}
					 when '405'{OrdType='ZSMP';}
					 when else{}
				 }
        req.orderType =OrdType;
        req.shipmentDate = Shipdateformat;
        req.products = proList;
        req.distributionChannel = disChannel;
       /* req.division = '00';
        req.customerNumber = '72142';
        req.salesOrganization = '0040';
        req.shipTo='72142';
        req.orderType = 'OR';
        req.orderReason = '100';
        req.shipmentDate = '2022-04-02';
        req.products = proList;
        req.distributionChannel = '00';*/
        System.debug('json' + JSON.serialize(req));
        List<b2b_TaxWrapper.response> TaxFromExternalService = b2b_IntegrationService.Taxcal(req);            
        system.debug('TaxFromExternalService '+TaxFromExternalService);

        //convert list to map
        Map<String,b2b_TaxWrapper.response > mapMaterialResp = new Map<String,b2b_TaxWrapper.response>();
        for(b2b_TaxWrapper.response resp : TaxFromExternalService){
            mapMaterialResp.put(resp.materialNumber, resp);
        }
        
        // If there are taxes from a previously cancelled checkout, delete them.
       
        List<Id> ordItemIds = ordItemIdsBySKU.values();
        delete [SELECT Id FROM OrderItemTaxLineItem WHERE OrderItemId IN :ordItemIds];
        // For each cart item, insert a new tax line in the CartTax entity.
        // The total tax is automatically rolled up to TotalLineTaxAmount in the corresponding CartItem line.
        //OrderItemTaxLineItem[] ordTaxestoInsert = new OrderItemTaxLineItem[]{};
        list<sobject> payload = new list<sobject>();
        list<orderItem> UpdOrdLine = new list<orderItem>();
        for (String sku : ordItemIdsBySKU.keySet()) {
    
            b2b_TaxWrapper.response taxResp = mapMaterialResp.get(sku);
            //String rateAndAmountFromExternalService = TaxFromExternalService.get(sku);
            if (taxResp == null) {
                String errorMessage = 'The product with sku ' + sku + ' could not be found in the external system';
                //saveCartValidationOutputError(errorMessage, cartId);
                throw new CalloutException (errorMessage);
            }
            //OrderItem OrdItemList=[SELECT PricebookEntryId FROM OrderItem WHERE Id = :ordItemIdsBySKU.get(sku)];
            orderItem OI = new orderItem();
            OI.Id = ordItemIdsBySKU.get(sku);
            OI.unitPrice = (decimal.valueOf(taxResp.price)/orderItemByQant.get(sku)).setScale(4);
           // OI.unitPrice = decimal.valueOf(taxResp.price);
            //OI.PricebookEntryId=OrdItemList.PricebookEntryId;
            UpdOrdLine.add(OI)  ;       

            OrderItemTaxLineItem tax = new OrderItemTaxLineItem( 
                // update 8/12/22 SM - do not multiply tax amount (tax was being overstated)
                Amount = taxResp.tax==null?0:Double.valueOf(taxResp.tax),
                // Amount = taxResp.tax==null?0:Double.valueOf(taxResp.tax)*orderItemByQant.get(sku),
               // Amount = taxResp.tax==Double.valueOf(taxResp.tax)*orderItemByQant.get(sku),
                OrderItemId = ordItemIdsBySKU.get(sku),
                Name = 'Tax',
                TaxEffectiveDate = Date.today(),
                Type = 'Actual'
            );
            payload.add(tax);
            
        }

        //upsert(payload);
        upsert(UpdOrdLine);
        insert(payload);
    }
    
    
}