public class EpM2StockItemUpdateJob implements Queueable {
    private String sku{get;Set;}
    private String qty{get;Set;}
    private String eComStoreID{get;Set;}
    /*
     * Constructor
     */
    public EpM2StockItemUpdateJob(String sku, String qty, String eComStoreID){
        this.sku = sku;
        this.qty = qty;
        this.eComStoreID = eComStoreID;
    }
    
    public void execute(QueueableContext context) {
        if(this.sku != null) {
            EpM2StockItemUpdateSync.defensiveStockItemUpdateSynchronously(this.sku, this.qty, this.eComStoreID);
        }
    }
}