/**
 * @description Apex Test class to cover the 'MasterGridController' Apex Class
 */
/*  Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud            06-14-2022      Initial version
*/

@IsTest
public without sharing class MasterGridController_Test {
    @TestSetup
    static void setup_data() {

        // Create Test Account
        Account testAccount = CCTestUtils.createAccount('Aperture Labs',  true);

        // Create Buyer Account
        BuyerAccount testBuyerAccount = CCTestUtils.createBuyerAccount('Aperture Labs Buyer Account', testAccount.Id, true);

        // Create Buyer Group
        BuyerGroup testBuyerGroup = CCTestUtils.createBuyerGroup('Test Buyer Group', true);

        // Create Buyer Group Member
        BuyerGroupMember testBuyerGroupMember = CCTestUtils.createBuyerGroupMember(testAccount.Id, testBuyerGroup.Id, true);

        // Create Strikethrough Pricebook
        Pricebook2 testStrikethroughPricebook = CCTestUtils.createPriceBook(true, 'Test Strikethrough Pricebook', false, 'USD', true, true);

        // Create Discounted Pricebook
        Pricebook2 testDiscountedPricebook = CCTestUtils.createPriceBook(true, 'Test Discounted Pricebook', false, 'USD', true, true);

        // Create Buyer Group Pricebook
        BuyerGroupPricebook testBuyerGroupPricebook = CCTestUtils.createBuyerGroupPricebook(testDiscountedPricebook.Id, testBuyerGroup.Id, true);

        // Create EntitlementPolicy
        CommerceEntitlementPolicy testPolicy = CCTestUtils.createEntitlementPolicy('Test Entitlement Policy', true);

        // Create Test Contact
        Contact testContact = CCTestUtils.createContact(testAccount, true);

        // Create Test Community User
        User commUser = CCTestUtils.createTestUser(testContact, true);

        // Create Catalog
        ProductCatalog testProductCatalog = CCTestUtils.createProductCatalog('Apex Test Catalog', true);

        // Create list of product grid fields
        List<String> productGridFields = new List<String>{'Rod_Butt__c', 'Bait_Length__c', 'Reel_Size__c' };

        // Create Product Category
        ProductCategory testProductCategory = CCTestUtils.createProductCategory('Apex Test Category', testProductCatalog, null, productGridFields, true );

        // Create Product2 Master Record and related Variant records
        Product2 masterVariantProduct = CCTestUtils.createMasterVariantProduct('Test Master Product 1', '1101', 'abc123', testPolicy.Id, testProductCategory.Id, true);

        // Create Test Store
        WebStore testStore = CCTestUtils.createWebStore('Apex Test Store', testStrikethroughPricebook.Id, true);

        // Create Test Cart
        WebCart testCart = CCTestUtils.createWebCart('Apex Test Cart', commUser, testStore, testAccount, true);

        // Create Test Cart Delivery Group
        CartDeliveryGroup testDelivGroup = CCTestUtils.createCartDeliveryGroup('Apex Test Cart Delivery Group', testCart, true);

        // Create Test Plano Product
        Product2 planoTestProduct = CCTestUtils.createPlanoProduct('Apex Test Plano Product', 'PLANO123', 'PLANO123', testPolicy.Id);

        // Create Test Spare Parts Product
        Product2 sparePartTestPRoduct = CCTestUtils.createSparePartsProduct('Apex Test Spare Parts Product', 'SP123', 'SP123', testPolicy.Id);

    }

    @IsTest
    static void apexOpearationDataConst_test() {
        MasterGridController.ApexOperationData errorOperation = new MasterGridController.ApexOperationData('Test Error');
        System.assertEquals('Test Error', errorOperation.errorMsg);
        System.assertEquals(false, errorOperation.isSuccess);
    }

    @IsTest
    static void showMasterGrid_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        System.assertNotEquals(null, testProduct, 'Couldn\'t locate Master Variant Product record.');

        Test.startTest();
        System.runAs(commUser) {
            Boolean result = MasterGridController.showMasterGrid(testProduct.Id);
            System.assertEquals(true, result, 'Unexpected value returned from MasterGridController.showMasterGrid() method.');
        }
        Test.stopTest();
    }

    @IsTest
    static void getProductVariantData_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];


        Test.startTest();
        System.runAs(commUser) {
            MasterGridController.ApexOperationData result = MasterGridController.getProductVariantData(testProduct.Id, testStore.Id, testAccount.Id, null);
            System.assertNotEquals(null, result.recordData, 'Variant Product data was not returned as expected.');
        }
        Test.stopTest();
    }

    @IsTest
    static void addProductsToCart_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];

        // Get Master Variant Product2 record
        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];

        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: testProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        String itemsListString = '';

        // Build Items to Stringify!
        List<MasterGridController.ProductItemWrapper> itemWrappers = new List<MasterGridController.ProductItemWrapper>();
        for (Integer i = 0; i < 3; i++) {
            MasterGridController.ProductItemWrapper itemWrap = new MasterGridController.ProductItemWrapper();
            itemWrap.productId = variantProducts.get(i).Product.Id;
            itemWrap.qty = String.valueOf(i + 1);
            itemWrap.inventoryLabel = 'Test Data';
            itemWrap.materialNo = variantProducts.get(i).Product.Material__c;
            itemWrap.type = 'Product';
            itemWrap.productName = variantProducts.get(i).Product.Name;
            itemWrap.modelNumber = variantProducts.get(i).Product.Model_Number__c;
            itemWrappers.add(itemWrap);
        }

        itemsListString = JSON.serialize(itemWrappers);

        Test.startTest();
        System.runAs(commUser) {
            Map<String,Object> result = MasterGridController.addProductsToCart(itemsListString, testStore.Id, testAccount.Id, testCart.Id);
            System.assertEquals(0, result.get('totErrors'), 'Expected no errors to be encountered.');
        }
        Test.stopTest();

    }

    @IsTest
    static void addItemsToCartInBatches_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];

        // Get Master Variant Product2 record
        Product2 testProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];

        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: testProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        String itemsListString = '';

        // Build Items to Stringify!
        List<MasterGridController.ProductItemWrapper> itemWrappers = new List<MasterGridController.ProductItemWrapper>();
        for (Integer i = 0; i < 3; i++) {
            MasterGridController.ProductItemWrapper itemWrap = new MasterGridController.ProductItemWrapper();
            itemWrap.productId = variantProducts.get(i).Product.Id;
            itemWrap.qty = String.valueOf(i + 1);
            itemWrap.inventoryLabel = 'Test Data';
            itemWrap.materialNo = variantProducts.get(i).Product.Material__c;
            itemWrap.type = 'Product';
            itemWrap.productName = variantProducts.get(i).Product.Name;
            itemWrap.modelNumber = variantProducts.get(i).Product.Model_Number__c;
            itemWrappers.add(itemWrap);
        }

        itemsListString = JSON.serialize(itemWrappers);

        Map<String,Object> responseMap = new Map<String,Object>();
        List<ConnectApi.CartItemInput> cartInputs = MasterGridController.createCartItems(itemWrappers, responseMap);
        List<ConnectApi.BatchInput> batchInputs = MasterGridController.createBatchInputLines(cartInputs);

        Test.startTest();
        System.runAs(commUser) {
            List<MasterGridController.ProductItemWrapper> result = MasterGridController.addItemsToCartInBatches(testStore.Id, testAccount.Id, testCart.Id, batchInputs, itemWrappers, responseMap);
            System.assertEquals(3, result.size(), 'Expected 3 items to be processed.');
        }
        Test.stopTest();
    }

    @IsTest
    static void processAddToCartErrors_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];

        // Build Items to Stringify!
        List<MasterGridController.ProductItemWrapper> itemWrappers = new List<MasterGridController.ProductItemWrapper>();
        for (Integer i = 0; i < 3; i++) {
            MasterGridController.ProductItemWrapper itemWrap = new MasterGridController.ProductItemWrapper();
            itemWrap.productId = '123';
            itemWrap.qty = String.valueOf(i + 1);
            itemWrap.inventoryLabel = 'Test Data';
            itemWrap.materialNo = '456';
            itemWrap.type = 'Product';
            itemWrap.productName = 'test product ' + i;
            itemWrap.modelNumber = '789';
            itemWrap.isSuccess = false;
            itemWrappers.add(itemWrap);
        }

        Map<String,Object> responseMap = new Map<String,Object>();

        Test.startTest();
        System.runAs(commUser) {
            MasterGridController.processAddToCartErrors(itemWrappers, responseMap);
        }
        Test.stopTest();
    }

    @IsTest
    static void addBulkItemsToWishList_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];

        // Create Test Data
        Map<String,Decimal> testDataMap = new Map<String,Decimal>();
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        String stringifiedData = '[';
        for (Integer i = 0; i < 3; i++ ) {
            stringifiedData += '{ "productId":"' + variantProducts.get(i).ProductId + '","quantity":"' + Decimal.valueOf(i) + '"}';
            if (i != 2) {
                stringifiedData += ',';
            }
        }
        stringifiedData += ' ]';
        String serializedMap = JSON.serialize(testDataMap);

        Suggested_Order__c testOrder = new Suggested_Order__c();

        insert testOrder;

        // Build Suggested Order Items
        List<Suggested_Order_Item__c> orderItems = new List<Suggested_Order_Item__c>();
        for (Integer i = 0; i < 3; i++) {
            Suggested_Order_Item__c newItem = new Suggested_Order_Item__c();
            if (i == 2) {
                newItem.Product__c =  variantProducts.get(2).ProductId;
                newItem.Quantity__c = 2.0;
                newItem.Suggested_Order__c = testOrder.Id;
                orderItems.add(newItem);
            }
        }
        insert orderItems;

        Test.startTest();
        System.runAs(commUser) {
            MasterGridController.ApexOperationData result = MasterGridController.addBulkItemsToWishList(testStore.Id, testAccount.Id, testOrder.Id, stringifiedData);
            System.assertEquals(true, result.isSuccess, 'Expected no errors to occur.');
        }
        Test.stopTest();
    }

    @IsTest
    static void addCartToNewOrder_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        CartDeliveryGroup testGroup = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];

        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');


        // Create Test CartItem records
        CartItem testCartItem = CCTestUtils.createCartItem(variantProducts.get(0).Product, testCart, testGroup, true);

        Suggested_Order__c testOrder = new Suggested_Order__c();
        testOrder.Name = 'Apex Test Suggested Order';
        insert testOrder;

        Test.startTest();
        System.runAs(commUser) {
            MasterGridController.ApexOperationData result = MasterGridController.addCartToNewOrder('blahblah', testAccount.Id, testCart.Id, testOrder.Id);
            System.assertEquals(true, result.isSuccess, 'Expected no errors to occur.');
        }
        Test.stopTest();
    }

    @IsTest
    static void addCartToExistingOrder_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        CartDeliveryGroup testGroup = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];


        // Create Test CartItem records
        CartItem testCartItem = CCTestUtils.createCartItem(variantProducts.get(0).Product, testCart, testGroup, true);

        Suggested_Order__c testOrder = new Suggested_Order__c();
        testOrder.Name = 'Apex Test Suggested Order';
        insert testOrder;

        Test.startTest();
        System.runAs(commUser) {
            MasterGridController.ApexOperationData result = MasterGridController.addCartToExistingOrder('not_needed', testAccount.Id, testCart.Id, testOrder.Id);
            System.assertEquals(true, result.isSuccess, 'Expected no errors to occur.');
        }
        Test.stopTest();
    }

    @IsTest
    static void getProductDetail_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];

        Test.startTest();
        System.runAs(commUser) {
            MasterGridController.ApexOperationData result = MasterGridController.getProductDetail(masterProduct.Id);
            System.assertNotEquals(null, result.recordData, 'Should\'ve returned product data.');
        }
        Test.stopTest();
    }

    @IsTest
    static void getProductPriceData_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertNotEquals(null, variantProducts, 'Needed records to be returned here.');

        // Build Company Product Info records so we can specify specific MAP/MSRP prices and assert that the code is gathering data from the correct source

       Company_Product_Info__c testInfo =  new Company_Product_Info__c (
                    MAP__c = 59.99,
                    MSRP__c = 49.99,
               SAP_Company__c = 'apex_test',
               Related_Product__c = variantProducts[0].Product.Id
        );


        insert testInfo;


        System.runAs(commUser) {
            String communityId = [SELECT NetworkId FROM NetworkMember WHERE MemberId =: commUser.Id LIMIT 1].NetworkId;
            // Get AccountId
            Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];


            Test.startTest();
            MasterGridController.ApexOperationData result = MasterGridController.getProductPriceData(communityId, variantProducts[0].Product.Id, testAccount.Id);
            System.assertNotEquals(null, result.recordData, 'Expected some price data to be returned.');

            System.assertEquals(08.23, Decimal.valueOf(String.valueOf(result.recordData.get('your_price'))), 'Unexpected Unit Price returned.');
            System.assertEquals(49.99, Decimal.valueOf(String.valueOf(result.recordData.get('msrp'))), 'Unexpected MSRP Price returned.');
            System.assertEquals(59.99, Decimal.valueOf(String.valueOf(result.recordData.get('map'))), 'Unexpected MAP Price returned.');
            System.debug(result.recordData);
            Test.stopTest();
        }

    }

    @IsTest
    static void doInventoryCallout200_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];

        // Set Mock response for inventory callout
        Test.setMock(HttpCalloutMock.class, new GetInventory200Mock());

        System.runAs(commUser) {
            Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
            System.assertNotEquals(null, variantProducts, 'Needed records to be returned here.');

            Test.startTest();
            MasterGridController.ApexOperationData result = MasterGridController.doInventoryCallout(String.valueOf(testAccount.Id), new List<String>{variantProducts.get(0).Product.Id});
//            System.assertNotEquals(null, result.recordData, 'Expected inventory data.');
//            System.assertEquals('In Stock', result.recordData.get(variantProducts[0].Product.Id), 'Unexpected inventory value returned.');
            Test.stopTest();
        }
    }

    @IsTest
    static void doInventoryCallout500_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];

        // Set Mock response for inventory callout
        Test.setMock(HttpCalloutMock.class, new GetInventory500Mock());

        System.runAs(commUser) {
            Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];

            System.assertNotEquals(null, variantProducts, 'Needed records to be returned here.');

            Test.startTest();
            MasterGridController.ApexOperationData result = MasterGridController.doInventoryCallout(String.valueOf(testAccount.Id), new List<String>{variantProducts.get(0).Product.Id});
//            System.assertEquals(null, result.recordData, 'Expected no inventory data to be returned.');
//            System.assertEquals(true, result.errorMsg.contains('500 Response from Inventory endpoint.'), 'Unexpected callout error returned');
            Test.stopTest();
        }
    }

    @IsTest
    static void generateCartData_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        System.assertNotEquals(null, commUser, 'A test community user should have been found.');
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        CartDeliveryGroup cartGroup = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];

        Company_Product_Info__c testInfo =  new Company_Product_Info__c (
                MAP__c = 59.99,
                MSRP__c = 49.99,
                Related_Product__c = variantProducts[0].Product.Id
        );

        insert testInfo;

        // Create CartItem records
        List<CartItem> cartItems = new List<CartItem>();
        Integer i = 0;
        for (ProductAttribute pa : variantProducts) {
            cartItems.add(
                    CCTestUtils.createCartItem(
                            variantProducts.get(i).Product,
                            testCart,
                            cartGroup,
                            false)
            );
            i++;
        }
        insert cartItems;

        System.runAs(commUser){
            Test.startTest();
            String result = MasterGridController.generateCartData(testCart.Id, testStore.Id);
            Test.stopTest();
        }
    }

    @IsTest
    static void addProductsToCart_Plano_Standard_Mixed_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        CartDeliveryGroup cartGroup = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];
        List<Product2> allProducts = new List<Product2>();

        // Get Master Variant Product2 record
        allProducts.add([SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1]);

        // Get Plano Test Product
        allProducts.add([SELECT id, Name, StockKeepingUnit FROM Product2 WHERE Name = 'Apex Test Plano Product' LIMIT 1]);

        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: allProducts[0].Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');


        Test.startTest();
        System.runAs(commUser) {
            List<CartItem> cartItems = new List<CartItem>();
            Integer i = 0;
            for (ProductAttribute pa : variantProducts) {
                cartItems.add(
                        CCTestUtils.createCartItem(
                                variantProducts.get(i).Product,
                                testCart,
                                cartGroup,
                                false)
                );
                i++;
            }

            cartItems.add(
                    CCTestUtils.createCartItem (
                            allProducts[1],
                            testCart,
                            cartGroup,
                            false
                    )
            );

            insert cartItems;


            // Check for correct CVO to have been created
            List<CartValidationOutput> cvoList = [SELECT Id, Message FROM CartValidationOutput WHERE RelatedEntityId =: testCart.Id];
            System.assertNotEquals(0, cvoList.size(), 'Expected CVO record(s) to be created.');
            System.debug(cvoList);
            System.assertEquals(cvoList[0].Message.contains('Your order cannot contain Plano Products and other Products.'), true, 'Unexpected CVO created.');
        }
        Test.stopTest();

    }

    @IsTest
    static void addProductsToCart_Standard_SpareParts_Mixed_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        CartDeliveryGroup cartGroup = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];
        List<Product2> allProducts = new List<Product2>();

        // Get Master Variant Product2 record
        allProducts.add([SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1]);

        // Get Spare Parts Test Product
        allProducts.add([SELECT id, Name, StockKeepingUnit FROM Product2 WHERE Name = 'Apex Test Spare Parts Product' LIMIT 1]);

        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: allProducts[0].Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');


        Test.startTest();
        System.runAs(commUser) {
            List<CartItem> cartItems = new List<CartItem>();
            Integer i = 0;
            for (ProductAttribute pa : variantProducts) {
                cartItems.add(
                        CCTestUtils.createCartItem(
                                variantProducts.get(i).Product,
                                testCart,
                                cartGroup,
                                false)
                );
                i++;
            }

            cartItems.add(
                    CCTestUtils.createCartItem (
                            allProducts[1],
                            testCart,
                            cartGroup,
                            false
                    )
            );

            insert cartItems;


            // Check for correct CVO to have been created
            List<CartValidationOutput> cvoList = [SELECT Id, Message FROM CartValidationOutput WHERE RelatedEntityId =: testCart.Id];
            System.assertNotEquals(0, cvoList.size(), 'Expected CVO record(s) to be created.');
            System.debug(cvoList);
            System.assertEquals(cvoList[0].Message.contains('Your order cannot contain Spare Parts and Products.'), true, 'Unexpected CVO created.');
        }
        Test.stopTest();

    }

    @IsTest
    static void addProductsToCart_Plano_SpareParts_Mixed_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        CartDeliveryGroup cartGroup = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];
        List<Product2> allProducts = new List<Product2>();
        // Get Plano Test Product
        allProducts.add([SELECT id, Name, StockKeepingUnit FROM Product2 WHERE Name = 'Apex Test Plano Product' LIMIT 1]);

        // Get Spare Parts Test Product
        allProducts.add([SELECT id, Name, StockKeepingUnit FROM Product2 WHERE Name = 'Apex Test Spare Parts Product' LIMIT 1]);

        Test.startTest();
        System.runAs(commUser) {
            List<CartItem> cartItems = new List<CartItem>();

            cartItems.add(
                    CCTestUtils.createCartItem (
                            allProducts[1],
                            testCart,
                            cartGroup,
                            false
                    )
            );cartItems.add(
                    CCTestUtils.createCartItem (
                            allProducts[0],
                            testCart,
                            cartGroup,
                            false
                    )
            );

            insert cartItems;


            // Check for correct CVO to have been created
            List<CartValidationOutput> cvoList = [SELECT Id, Message FROM CartValidationOutput WHERE RelatedEntityId =: testCart.Id];
            System.assertNotEquals(0, cvoList.size(), 'Expected CVO record(s) to be created.');
            System.debug(cvoList);
            System.assertEquals(cvoList[0].Message.contains('Your order cannot contain Plano Products and Spare Parts.'), true, 'Unexpected CVO created.');
        }
        Test.stopTest();

    }

    @IsTest
    static void addProductsToCart_Plano_SpareParts_Standard_Mixed_test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        CartDeliveryGroup cartGroup = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];
        List<Product2> allProducts = new List<Product2>();

        // Get Master Variant Product2 record
        allProducts.add([SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1]);

        // Get Variant products
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: allProducts[0].Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');


        // Get Plano Test Product
        allProducts.add([SELECT id, Name, StockKeepingUnit FROM Product2 WHERE Name = 'Apex Test Plano Product' LIMIT 1]);

        // Get Spare Parts Test Product
        allProducts.add([SELECT id, Name, StockKeepingUnit FROM Product2 WHERE Name = 'Apex Test Spare Parts Product' LIMIT 1]);

        Test.startTest();
        System.runAs(commUser) {
            List<CartItem> cartItems = new List<CartItem>();
            Integer i = 0;
            for (ProductAttribute pa : variantProducts) {
                cartItems.add(
                        CCTestUtils.createCartItem(
                                variantProducts.get(i).Product,
                                testCart,
                                cartGroup,
                                false)
                );
                i++;
            }
            cartItems.add(
                    CCTestUtils.createCartItem (
                            allProducts[2],
                            testCart,
                            cartGroup,
                            false
                    )
            );cartItems.add(
                    CCTestUtils.createCartItem (
                            allProducts[1],
                            testCart,
                            cartGroup,
                            false
                    )
            );

            insert cartItems;


            // Check for correct CVO to have been created
            List<CartValidationOutput> cvoList = [SELECT Id, Message FROM CartValidationOutput WHERE RelatedEntityId =: testCart.Id];
            System.assertNotEquals(0, cvoList.size(), 'Expected CVO record(s) to be created.');
            System.debug(cvoList);
            System.assertEquals(cvoList[0].Message.contains('Your order cannot contain Spare Parts, Plano Products and Products.'), true, 'Unexpected CVO created.');
        }
        Test.stopTest();

    }


    /*  Inventory Callout Mock Response: START */
    public class GetInventory200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            String mockInventory200Resp = '';

            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('materialNumber', 'Variation_0_abc123');
            gen.writeStringField('quantity', '823');
            gen.writeEndObject();
            gen.writeEndArray();

            System.debug('200 resp mock: ' + gen.getAsString());
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setBody(gen.getAsString());

            return resp;
        }
    }

    public class GetInventory500Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('status', 'Error');
            gen.writeStringField('errorSource', 'SAP');
            gen.writeStringField('errorMessage', 'Apex Test Callout Error');
            gen.writeEndObject();

            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(500);
            resp.setBody(gen.getAsString());

            return resp;
        }
    }
    /*  Inventory Callout Mock Response: STOP */
}