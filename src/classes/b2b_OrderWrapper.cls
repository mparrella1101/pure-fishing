/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           06/01/2022            Initial Version
 */
// Created to have Order payload wrapper.
public class b2b_OrderWrapper {
    public String division;
	public String shippingFutureDate;
	public String poDate;
	public String divisionChannel;
	public String salesOrg;
	public String poType;
    public String placedBy;
	public String otherEmail;
    public String orderType;
    public String reasonCode;
	public String accountSAPCustomer;
    public String accountName;
	public String accountBillingStreet;
    public String accountBillingCity;
	public String accountBillingPostalCode;
    public String accountBillingCountryCode;
    public String accountPhone;
	public String orderReference;
    public String billToSAPCustomer;
    public String billToName;
	public String billingStreet;
    public String billingCity;
    public String billingPostalCode;
	public String billingCountryCode;
    public String shipToSAPCustomer;
    public String shipToName;
	public String shippingStreet;
    public String shippingCity;
	public String shippingPostalCode;
    public String shippingCountryCode;
    public String shippingPhone;
	public String shipCancelFlag;
    public String poNumber;
    public String orderNotes;
	public String ccDetails;
	public cls_orderitem[] OrderItem;
	public class cls_orderitem {
		public String materialNumber;
		public string quantity;
        public String price;
		public string currencyType;
        public string suppliedLineNumber;
	}

    public class response{
		public String materialNumber;	//1265710
		public string price;
		public string tax;
	}
	/*public static b2b_OrderWrapper parse(String json){
		return (b2b_OrderWrapper) System.JSON.deserialize(json, b2b_OrderWrapper.class);
	}*/
}