global class scheduledSync implements Schedulable {
    private Double pageSize = 4;
    private Double currentPage = 1;
    
    global void execute(SchedulableContext SC) {
        ApexGovernorLimits.printMaxLimits();

        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
            Notifier.sendEmail('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            List<MagentoStore__c> stores = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Active__c = true];
            if(System.Test.isRunningTest()) {
                stores = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c];
            }
            ApexGovernorLimits.printLimitsUsed();
            for(MagentoStore__c store : stores) {
                DateTime dateTimeNow = DateTime.now();
                String now_sync_time = dateTimeNow.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
                System.debug('now_sync_time: ' + now_sync_time);
                
                String last_sync_time = store.LastSalesOrderSyncTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
                System.debug('last_sync_time: ' + last_sync_time);
                EpM2Sync.defensiveSalesOrdersCreatedAfter(now_sync_time, pageSize, currentPage, store.AccessToken__c, store.Name, last_sync_time, store.BaseURL__c, store.SupportEmail__c);
            }
        }
        ApexGovernorLimits.printLimitsLeft();
    }
}