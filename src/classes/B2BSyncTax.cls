/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           04/08/2022            Initial Version
 */

// Determines the taxes for the cart
public class B2BSyncTax {
    // This invocable method only expects one ID
    @InvocableMethod(callout=true label='Prepare the taxes' description='Runs a synchronous version of taxes' category='B2B Commerce')
    public static void syncTax(List<ID> cartIds) {
        // Validate the input
        if (cartIds == null || cartIds.size() != 1) {
            String errorMessage = 'A cart id must be included to B2BSyncTax'; // Get wording from doc!!!
            // Sync non-user errors skip saveCartValidationOutputError
            throw new CalloutException (errorMessage);
        }
        
        // Extract cart id and start processing
        Id cartId = cartIds[0];
        startCartProcessSync(cartId);
    }

    private static void startCartProcessSync(Id cartId) {
        // In the Spring '20 release, there should be one delivery group per cart.
        // In the future, when multiple delivery groups can be created,
        // this sample should be updated to loop through all delivery groups.

        // We need to get the ID of the delivery group in order to get the DeliverTo info.
        Id cartDeliveryGroupId = [SELECT CartDeliveryGroupId FROM CartItem WHERE CartId = :cartId][0].CartDeliveryGroupId;
        CartDeliveryGroup deliveryGroup = [SELECT DeliverToState, DeliverToCountry,ShippingFutureDate__c,SAP_Customer__c  FROM CartDeliveryGroup WHERE Id = :cartDeliveryGroupId WITH SECURITY_ENFORCED][0];
        Date ShipDate = deliveryGroup.ShippingFutureDate__c;
        String Shipdateformat;
        if(ShipDate == null)
        {
            ShipDate= system.today();
            DateTime dtConverted = Datetime.newInstance(ShipDate.year(), ShipDate.month(),ShipDate.day());
            Shipdateformat=dtConverted.format('yyyy-MM-dd');
        }
        else
        {
            DateTime dtConverted = Datetime.newInstance(ShipDate.year(), ShipDate.month(),ShipDate.day());
            Shipdateformat=dtConverted.format('yyyy-MM-dd');
        }
        // Get all SKUs, the cart item IDs, and the total prices from the cart items.
        String ActId;
        String OrdReason;
        String DC;
        for( WebCart ActInfo:[Select AccountId,Cart_Order_Reason__c,Distribution_Channel__c from WebCart where Id = : cartId])
        {
            ActId=ActInfo.AccountId;
            OrdReason=ActInfo.Cart_Order_Reason__c;
            DC=ActInfo.Distribution_Channel__c;
        }
        /*String Div;
        string Cust;
        String Dis;
        String Cur;
        String Sal;
        for( Account ActInfos:[Select Division_SAP__c,SAP_Customer__c,Distribution_Channel__c,Sales_Org__c,Currency__c  from Account where Id = : ActId WITH SECURITY_ENFORCED])
        {
            Div=ActInfos.Division_SAP__c;
            Cust=ActInfos.SAP_Customer__c;
            Dis=ActInfos.Distribution_Channel__c;
            Cur=ActInfos.Currency__c;
            Sal=ActInfos.Sales_Org__c;
        }*/
		Account ActInfos = [Select Division_SAP__c,SAP_Customer__c,Distribution_Channel__c,Sales_Org__c,Currency__c  from Account where Id = : ActId][0];
		list<b2b_TaxWrapper.cls_products> proList = new list<b2b_TaxWrapper.cls_products>();
        Map<String, Id> cartItemIdsBySKU = new Map<String, Id>();
        Map<String, Decimal> cartItemTotalPriceBySKU = new Map<String, Decimal>();
        for (CartItem cartItem : [SELECT Sku, TotalPrice,Product2.Material__c,Quantity, Type FROM CartItem WHERE CartId = :cartId]) {
            String cartItemSKU = '';
            if (cartItem.Type == 'Product') {
                if (String.isBlank(cartItem.Sku)) {
                    String errorMessage = 'The SKUs for all products in your cart must be defined.';
                    saveCartValidationOutputError(errorMessage, cartId);
                    throw new CalloutException (errorMessage);
                }
                //cartItemSKU = cartItem.Sku;
				cartItemSKU = cartItem.Product2.Material__c;
				b2b_TaxWrapper.cls_products product = new b2b_TaxWrapper.cls_products();
				product.materialNumber = cartItem.Product2.Material__c;
        		product.quantity = String.valueof(cartItem.Quantity);
        		proList.add(product);
				cartItemIdsBySKU.put(cartItemSKU, cartItem.Id);
            }
            else if (cartItem.Type == 'Charge') {
                // This is an example for a Cart Item of type shipping charge.
                // For simplicity and testing purposes, we just assign some SKU to this charge so that the taxation external service returns some value.
                //cartItemSKU = 'ChargeSKU';
            }
            //cartItemTotalPriceBySKU.put(cartItemSKU, cartItem.TotalPrice);
        }      
        
        // Get the tax rates and tax amounts from an external service
        // Other parameters will be passed here, like ship_from, bill_to, more details about the ship_to, etc.
       // Map<String, TaxDataFromExternalService> rateAndAmountFromExternalServicePerSku = getTaxRatesAndAmountsFromExternalService(
          //  cartId, cartItemTotalPriceBySKU, deliveryGroup.DeliverToState, deliveryGroup.DeliverToCountry
        //);
        b2b_TaxWrapper req = new b2b_TaxWrapper();
        //b2b_pricingWrapper.cls_products product = new b2b_pricingWrapper.cls_products();
        //product.materialNumber = '1265710';
        //product.quantity = '2';
        //list<b2b_pricingWrapper.cls_products> proList = new list<b2b_pricingWrapper.cls_products>();
       // proList.add(product);
        req.division = '00' + ActInfos.Division_SAP__c;
        req.customerNumber = ActInfos.SAP_Customer__c;
        req.salesOrganization = ActInfos.Sales_Org__c;
        req.shipTo = deliveryGroup.SAP_Customer__c;
        req.orderType = 'OR';
        req.orderReason =OrdReason;
        req.shipmentDate = Shipdateformat;
        req.products = proList;
        req.distributionChannel = DC;
       /* req.division = '00';
        req.customerNumber = '72142';
        req.salesOrganization = '0040';
        req.shipTo='72142';
        req.orderType = 'OR';
        req.orderReason = '100';
        req.shipmentDate = '2022-04-02';
        req.products = proList;
        req.distributionChannel = '00';*/
        System.debug('json' + JSON.serialize(req));
        List<b2b_TaxWrapper.response> TaxFromExternalService = b2b_IntegrationService.Taxcal(req);            
		system.debug('TaxFromExternalService '+TaxFromExternalService);

        //convert list to map
        Map<String,b2b_TaxWrapper.response > mapMaterialResp = new Map<String,b2b_TaxWrapper.response>();
        for(b2b_TaxWrapper.response resp : TaxFromExternalService){
            mapMaterialResp.put(resp.materialNumber, resp);
        }
        
        // If there are taxes from a previously cancelled checkout, delete them.
        List<Id> cartItemIds = cartItemIdsBySKU.values();
        delete [SELECT Id FROM CartTax WHERE CartItemId IN :cartItemIds];
        
        // For each cart item, insert a new tax line in the CartTax entity.
        // The total tax is automatically rolled up to TotalLineTaxAmount in the corresponding CartItem line.
        CartTax[] cartTaxestoInsert = new CartTax[]{};
        for (String sku : cartItemIdsBySKU.keySet()) {
	
            b2b_TaxWrapper.response taxResp = mapMaterialResp.get(sku);
            //String rateAndAmountFromExternalService = TaxFromExternalService.get(sku);
            if (taxResp == null) {
                String errorMessage = 'The product with sku ' + sku + ' could not be found in the external system';
                saveCartValidationOutputError(errorMessage, cartId);
                throw new CalloutException (errorMessage);
            }
            // If the sku was found in the external system, add a new CartTax line for that sku
            // The following fields from CartTax can be filled in:
            // Amount (required): Calculated tax amount.
            // CartItemId (required): ID of the cart item.
            // Description (optional): Description of CartTax.
            // Name (required): Name of the tax.
            // TaxCalculationDate (required): Calculation date for this tax line.
            // TaxRate (optional): The percentage value of the tax. Null if the tax is a flat amount.
            // TaxType (required): The type of tax, e.g. Actual or Estimated.

            CartTax tax = new CartTax( 
                Amount = Double.valueOf(taxResp.tax),
                CartItemId = cartItemIdsBySKU.get(sku),
                Name = 'Tax',
                TaxCalculationDate = Date.today(),
                TaxType = 'Actual'
            );
            cartTaxestoInsert.add(tax);
        }

        insert(cartTaxestoInsert);
    }  
    private static void saveCartValidationOutputError(String errorMessage, Id cartId) {
            // For the error to be propagated to the user, we need to add a new CartValidationOutput record.
            // The following fields must be populated:
            // BackgroundOperationId: Foreign Key to the BackgroundOperation
            // CartId: Foreign key to the WebCart that this validation line is for
            // Level (required): One of the following - Info, Error, or Warning
            // Message (optional): Message displayed to the user (maximum 255 characters)
            // Name (required): The name of this CartValidationOutput record. For example CartId
            // RelatedEntityId (required): Foreign key to WebCart, CartItem, CartDeliveryGroup
            // Type (required): One of the following - SystemError, Inventory, Taxes, Pricing, Shipping, Entitlement, Other
            CartValidationOutput cartValidationError = new CartValidationOutput(
                CartId = cartId,
                Level = 'Error',
                Message = errorMessage.left(255),
                Name = (String)cartId,
                RelatedEntityId = cartId,
                Type = 'Taxes'
            );
            insert(cartValidationError);
    }
}