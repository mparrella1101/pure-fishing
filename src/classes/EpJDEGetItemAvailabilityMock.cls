@isTest
global class EpJDEGetItemAvailabilityMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        response.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ns2:getItemAvailabilityResponse xmlns:ns2="http://oracle.e1.bssv.JP574101/"><e1MessageList/><items><identifier2ndItem>CAB56260M</identifier2ndItem><identifierShortItem>54653</identifierShortItem><itemQuantity>2907</itemQuantity></items><status>SUCCESS</status></ns2:getItemAvailabilityResponse></soapenv:Body></soapenv:Envelope>');
        response.setStatusCode(200);
        response.setStatus('Success');
        return response;
    }
}