global class ItemAvailabilityPaginationSync implements Schedulable {
    global Integer PRODUCTS_PER_REQUEST{get;Set;}
    global Integer PRODUCTS_PER_QUERY{get;Set;}
    global Integer MINUTES_OFFSET{get;Set;}
    global static ItemAvailabilityPagination itemAvailabilityPagination{get;Set;}
    
    public ItemAvailabilityPaginationSync(Integer PRODUCTS_PER_REQUEST, Integer PRODUCTS_PER_QUERY, Integer MINUTES_OFFSET) {
        this.PRODUCTS_PER_REQUEST = PRODUCTS_PER_REQUEST;
        this.PRODUCTS_PER_QUERY = PRODUCTS_PER_QUERY;
        this.MINUTES_OFFSET = MINUTES_OFFSET;
        itemAvailabilityPagination = new ItemAvailabilityPagination();
    }
    
    public ItemAvailabilityPaginationSync() {
        this.PRODUCTS_PER_REQUEST = 10;
        this.PRODUCTS_PER_QUERY = 200;
        this.MINUTES_OFFSET = 3;
        itemAvailabilityPagination = new ItemAvailabilityPagination();
    }
    
    global void execute(SchedulableContext SC) {  
        itemAvailabilityPagination.size = PRODUCTS_PER_QUERY;
		itemAvailabilityPagination.refresh();
        syncItems(PRODUCTS_PER_REQUEST, PRODUCTS_PER_QUERY, MINUTES_OFFSET);
    }
    
    private void syncItems(Integer PRODUCTS_PER_REQUEST, Integer PRODUCTS_PER_QUERY, Integer MINUTES_OFFSET) {
        System.debug('syncItems: PRODUCTS_PER_REQUEST='+PRODUCTS_PER_REQUEST+', PRODUCTS_PER_QUERY='+PRODUCTS_PER_QUERY+', MINUTES_OFFSET='+MINUTES_OFFSET);
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            List<Product2> prods = itemAvailabilityPagination.getProducts();
            ApexGovernorLimits.printLimitsUsed();
            
            System.debug('ItemAvailabilityPaginationSync > syncItems > prods.size():'+prods.size());
            EpJDESync.stockItemUpdateSynchronously(JSON.serialize(prods),PRODUCTS_PER_REQUEST);
            
            if(itemAvailabilityPagination.hasNext) {
                itemAvailabilityPagination.next();
                syncItems(PRODUCTS_PER_REQUEST, PRODUCTS_PER_QUERY, MINUTES_OFFSET);
            }
        }      
    }
}