public class ShipmentItem
{
    public String order_item_id { get; set; }
    public Decimal qty { get; set; }
}