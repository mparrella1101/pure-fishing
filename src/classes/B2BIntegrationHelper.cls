/**
 * @description Helper class that will parse the current environment's domain and from that value
 * determining the correct 'MulesoftIntInfo__mdt' Custom Metadata to use for callouts to Mulesoft APIs.
 */

/* Version      Author                  Company                 Date                Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           April 29, 2022      Initial version
 */

public with sharing class B2BIntegrationHelper {

    /**
     * @description This method will parse the current Salesforce org's url to find the Domain, and using that Domain value, select
     * the appropriate 'MulesoftIntInfo__mdt' Custom Metadata record populated with all endpoint information.
     *
     * @return An IntegrationHelperWrapper object with all endpoint/integration information needed
     */
    public static IntegrationHelperWrapper getIntegrationData() {
        IntegrationHelperWrapper integrateWrapper = new IntegrationHelperWrapper();
        MulesoftIntInfo__mdt mulesoftInfo;
        String recordLabel; // Holds the Master Label of the Custom MDT record we are trying to obtain

        // Determine if current org is a Sandbox or not
        Boolean isSandbox = [SELECT Id, isSandbox FROM Organization LIMIT 1].isSandbox;

        // Assign correct value to 'recordLabel' (since base url naming != Custom MDT records naming)
        if (isSandbox){
            String urlString = String.valueOf(URL.getSalesforceBaseUrl());
            if (urlString.contains('--')){
                recordLabel = String.valueOf(URL.getSalesforceBaseUrl()).substringBetween('--','.').toUpperCase();
            } else {
                // community URL
                recordLabel = String.valueOf(URL.getSalesforceBaseUrl()).substringBetween('//','-').toUpperCase();
            }

            System.debug('#### Extracted URL String: ' + recordLabel + ' ####');

            switch on recordLabel {
                when 'B2BDEV1' {
                    recordLabel = 'Dev1';
                }
                when 'QAFULL' {
                    recordLabel = 'QAFull';
                }
                when else {
                    recordLabel = 'Dev1'; // Defaulting to 'Dev1' if this is deployed in an unknown/new org to prevent test failures
                }
            }


        } else {
            if (Test.isRunningTest()){
                recordLabel = 'Dev1';
            } else {
                recordLabel = 'Production';
            }
        }

        try {
            mulesoftInfo = MulesoftIntInfo__mdt.getInstance(recordLabel);
            System.debug('B2BIntegrationHelper.getIntegrationData() has selected the following MulesoftIntInfo__mdt CustomMetadata Record: ' + recordLabel);
        }
        catch(Exception ex){
            System.debug('*** B2BIntegrationHelper.getIntegrationData() ERROR: ' + ex.getMessage());
            integrateWrapper.hasData = false;
            return integrateWrapper;
        }

        // Populate the response wrapper with integration data
        integrateWrapper.baseUrl = mulesoftInfo.Base_URL__c;
        integrateWrapper.client_key = mulesoftInfo.Key__c;
        integrateWrapper.client_secret = mulesoftInfo.Secret__c;
        integrateWrapper.freight_endpoint = mulesoftInfo.Freight_Endpoint__c;
        integrateWrapper.inventory_endpoint = mulesoftInfo.Inventory_Endpoint__c;
        integrateWrapper.pricing_endpoint = mulesoftInfo.Pricing_Endpoint__c;
        integrateWrapper.tax_endpoint = mulesoftInfo.Tax_Endpoint__c;
        integrateWrapper.order_endpoint = mulesoftInfo.Order_Endpoint__c;
        

        return integrateWrapper;
    }


    /* Wrapper Class: START */
    /** @description Wrapper class that will be returned from this class, containing environment-specific integration information */
    public class IntegrationHelperWrapper {
        /** @description Boolean variable that is TRUE when integration data is returned, FALSE otherwise (i.e. when an error has occurred */
        public Boolean hasData;

        /** @description Stores the base url used in callouts (other _endpoint fields will be appended to this field to build the full endpoint url) */
        public String baseUrl;

        /** @description Stores the environment-specific key for authenticating with the Mulesoft endpoint */
        public String client_key;

        /** @description Stores the environment-specific secret fro authenticating with the Mulesoft endpoint */
        public String client_secret;

        /** @description Stores the endpoint path for retrieving Price information */
        public String pricing_endpoint;

        /** @description  Stores the endpoint path for retrieving Inventory information */
        public String inventory_endpoint;

        /** @description  Stores the endpoint path for retrieving Sales Tax information */
        public String tax_endpoint;

        /** @description Stores the endpoint path for retrieving Freight information */
        public String freight_endpoint;
        
        /** @description Stores the endpoint path for retrieving order information */
        public String order_endpoint;

        /** @description Constructor method used to set the 'hasData' attribute to TRUE */
        public IntegrationHelperWrapper() {
            this.hasData = true;
        }
    }
    /* Wrapper Class: STOP */
}