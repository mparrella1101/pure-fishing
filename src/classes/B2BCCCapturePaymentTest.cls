@isTest
public class B2BCCCapturePaymentTest {
    @testSetup static void setup() {
        Account account = new Account(Name='TestAccount');
        insert account;
        

		Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test',Material__c='1265710',Case_Quantity__c=3.0);
        insert Prod;
        Pricebook2 pb=new Pricebook2(SAP_ExternalID__c='STD',Name='Standard Price Book');
        insert pb;
        //Pricebook2Id=Test.getStandardPricebookId()
        Order Ord = new Order(ShippingFutureDate__c=Date.valueOf('2022-04-02'),Ship_To__c=null,Ship_To_Account__c=null,Reason_Code__c='100',name='Test',AccountId=account.Id,EffectiveDate=Date.valueOf('2022-04-02'),status='Draft',Pricebook2Id=pb.id,Distribution_Channel__c='00');
        insert Ord;
        PricebookEntry prcBuKInt = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(),Product2Id=Prod.id,UnitPrice=0);
        insert prcBuKInt;
        PricebookEntry prcBuKInt1 = new PricebookEntry(Pricebook2Id=pb.id,Product2Id=Prod.id,UnitPrice=0);
        insert prcBuKInt1;
        OrderItem OrdItem = new OrderItem( 
            Quantity=3.0, 
            OrderId=Ord.id,
            Type='Order Product',
            PricebookEntryId=prcBuKInt1.Id,
            UnitPrice=2,
			product2Id = prod.Id
        );
        insert OrdItem;
    }
    @isTest static void B2BCCCapturePaymentSuccessfullyInserted() {

        
        Test.startTest();
        Order Ord = [SELECT Id FROM Order LIMIT 1];
        List<Id> orderId = new List<Id>{Ord.Id};
        Id OrdItemId = [SELECT Id FROM OrderItem WHERE OrderId = :orderId LIMIT 1].Id;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        B2BCCCapturePayment.CCCapturePay(orderId);
        System.assertEquals(200,200);
        Test.stopTest();
    }
    
    public class MockHttpResponseGenerator implements HttpCalloutMock {
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            System.assertEquals('https://apitest.authorize.net/xml/v1/request.api', req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"transactionResponse":{"responseCode":"3","authCode":"","avsResultCode":"P","cvvResultCode":"","cavvResultCode":"","transId":"40106706394","refTransID":"40106706394","transHash":"","testRequest":"0","accountNumber":"","accountType":"Visa","errors":[{"errorCode":"47","errorText":"The amount requested for settlement cannot be greater than the original amount authorized."}],"transHashSha2":"","SupplementalDataQualificationIndicator":0},"refId":"US00011330","messages":{"resultCode":"Error","message":[{"code":"E00027","text":"The transaction was unsuccessful."}]}}');
            res.setStatusCode(200);
            return res;
        }
    }
    
}