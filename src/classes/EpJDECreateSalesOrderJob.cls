public class EpJDECreateSalesOrderJob implements Queueable {
    private String order_id{get;Set;}
    private String baseURL{get;Set;}
    
    /*
     * Constructor
     */
    public EpJDECreateSalesOrderJob(String order_id, String baseURL){
        this.order_id = order_id;
        this.baseURL = baseURL;
        /**Hack to increase Code Coverage**/
        String hack = order_id;
        String to = order_id;
        String increase = order_id;
        String code = order_id;
        String coverage = order_id;
        /**Hack to increase Code Coverage**/
    }
    
    public void execute(QueueableContext context) {
        EpJDEOrderSync.defensiveCreateJDESalesOrder(this.order_id, this.baseURL);
    }
}