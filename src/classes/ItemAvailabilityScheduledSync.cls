global class ItemAvailabilityScheduledSync implements Schedulable {
    global Integer PRODUCTS_PER_REQUEST{get;Set;}
    global Integer PRODUCTS_PER_QUERY{get;Set;}
    global Integer PRODUCTS_OFFSET{get;Set;}
    global Integer MINUTES_OFFSET{get;Set;}
    global Integer ITERATION{get;Set;}
    
    public ItemAvailabilityScheduledSync(Integer ITERATION, Integer PRODUCTS_PER_REQUEST, Integer PRODUCTS_PER_QUERY, Integer PRODUCTS_OFFSET, Integer MINUTES_OFFSET) {
        this.PRODUCTS_PER_REQUEST = PRODUCTS_PER_REQUEST;
        this.PRODUCTS_PER_QUERY = PRODUCTS_PER_QUERY;
        this.PRODUCTS_OFFSET = PRODUCTS_OFFSET;
        this.MINUTES_OFFSET = MINUTES_OFFSET;
        this.ITERATION = ITERATION;
    }
    
    public ItemAvailabilityScheduledSync() {
        this.PRODUCTS_PER_REQUEST = 10;
        this.PRODUCTS_PER_QUERY = 200;
        this.PRODUCTS_OFFSET = 0;
        this.MINUTES_OFFSET = 3;
        this.ITERATION = 0;
    }
    
    global void execute(SchedulableContext SC) {  
        syncItems(ITERATION, PRODUCTS_PER_REQUEST, PRODUCTS_PER_QUERY, PRODUCTS_OFFSET, MINUTES_OFFSET);
    }
    
    private void syncItems(Integer ITERATION, Integer PRODUCTS_PER_REQUEST, Integer nLimit, Integer nOffset, Integer minOffcet) {
        System.debug('syncItems: PRODUCTS_PER_REQUEST='+PRODUCTS_PER_REQUEST+', nLimit='+nLimit+', nOffset='+nOffset+', minOffcet='+minOffcet);
        if (Limits.getQueries() > Limits.getLimitQueries()) {
            System.debug('Need to stop processing to avoid hitting a governor limit.');
        } else {
            System.debug('Continue processing. Not going to hit Queries governor limits');
            List<Product2> prods = [SELECT JdeId__c FROM Product2 WHERE JdeId__c != NULL AND JdeId__c != '' AND  MagentoId__c!= NULL LIMIT :nLimit OFFSET :nOffset];
            ApexGovernorLimits.printLimitsUsed();
            
            System.debug('prods.size():'+prods.size());
            for(Integer index=0; index<=(prods.size()/PRODUCTS_PER_REQUEST) && prods.size()!=0; index++)
            {
                String[] items = new String[] {};
                for(Integer position=(index*PRODUCTS_PER_REQUEST); (position<(index*PRODUCTS_PER_REQUEST)+PRODUCTS_PER_REQUEST) && position<prods.size() ; position++)
                {
                    Product2 prod = prods.get(position);
                    items.add(prod.JdeId__c);
                }
                //System.debug('index:'+index);
                System.debug('items.size():'+items.size());
                if(items.size()>0) {
                    if(System.isFuture() || System.isBatch() || Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
                        System.debug('Making future calls');
                        EpJDESync.defensiveItemAvailability(items);
                    } else if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){
                        System.debug('Queueing calls');
                        System.enqueueJob(new ItemAvailabilityJob(items));
                    } else {//TODO
                        //Notifier.sendEmail('ItemAvailabilityScheduledSync - Future and Queueable failed');
                        Datetime dt = Datetime.now();
                        Integer hour, min, ss;
                        hour = dt.hour();
                        min = dt.minute();
                        ss = dt.second() + 5;
                        
                        min = min + (ITERATION*minOffcet); 
                        
                        while(ss>59)
                        {
                            min += 1;
                            ss = ss - 59;
                        }
                        
                        while(min>59)
                        {
                            hour += 1;
                            min = min - 59;
                        }
                        
                        if(hour>23)
                        {
                            hour = 0;
                        }
                        //parse to cron expression
                        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                        System.debug('nextFireTime: ' + nextFireTime);
                        System.schedule('ScheduledItemJob#'+ index + '-' + String.valueOf(Math.random()), nextFireTime, new ItemAvailabilitySchedulable(items));
                    }
                } 
            }
            
            if(prods.size() >= nLimit && ITERATION>=0)
                syncItems(ITERATION++, PRODUCTS_PER_REQUEST, nLimit, nOffset+nLimit, minOffcet);
        }      
    }
}