@isTest
public class B2BSyncOrderTaxTest {
    @testSetup static void setup() {
        Account account = new Account(Name='TestAccount');
        insert account;
        

		Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test',Material__c='1265710',Case_Quantity__c=3.0);
        insert Prod;
        Pricebook2 pb=new Pricebook2(SAP_ExternalID__c='STD',Name='Standard Price Book');
        insert pb;
        //Pricebook2Id=Test.getStandardPricebookId()
        Order Ord = new Order(ShippingFutureDate__c=Date.valueOf('2022-04-02'),Ship_To__c=null,Ship_To_Account__c=null,Reason_Code__c='100',name='Test',AccountId=account.Id,EffectiveDate=Date.valueOf('2022-04-02'),status='Draft',Pricebook2Id=pb.id,Distribution_Channel__c='00');
        insert Ord;
        PricebookEntry prcBuKInt = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(),Product2Id=Prod.id,UnitPrice=0);
        insert prcBuKInt;
        PricebookEntry prcBuKInt1 = new PricebookEntry(Pricebook2Id=pb.id,Product2Id=Prod.id,UnitPrice=0);
        insert prcBuKInt1;
        OrderItem OrdItem = new OrderItem( 
            Quantity=3.0, 
            OrderId=Ord.id,
            Type='Order Product',
            PricebookEntryId=prcBuKInt1.Id,
            UnitPrice=2,
			product2Id = prod.Id
        );
        insert OrdItem;
    }
    
    @isTest static void testCartTaxForCartItemSuccessfullyInserted() {
        // Because test methods don't support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        

        // Associate the callout with a mock response.
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        Test.startTest();

        // Test: execute the integration for the test cart ID.
        /*Id OrdId = [SELECT Id FROM Order WHERE Name = 'Test' LIMIT 1].Id;
        Id OrdItemId = [SELECT Id FROM OrderItem WHERE OrderId = :OrdId LIMIT 1].Id;
        
        B2BSyncOrderTax.syncTax(OrdId); */
        
        Order Ord = [SELECT Id FROM Order LIMIT 1];
        List<Id> orderId = new List<Id>{Ord.Id};
        Id OrdItemId = [SELECT Id FROM OrderItem WHERE OrderId = :orderId LIMIT 1].Id;
        B2BSyncOrderTax.syncTax(orderId);
        // Verify: the integration executed successfully
        // and the new CartTax record is inserted.
        List<OrderItemTaxLineItem> cartTaxesForCartItem = [SELECT Id FROM OrderItemTaxLineItem WHERE OrderItemId = :OrdItemId];
        System.assertEquals(1, cartTaxesForCartItem.size());
        Test.stopTest();
    }
    
   /* @isTest static void testWhenExternalServiceCallFailsAFailedStatusIsReturnedAndACartValidationOutputEntryIsNotCreated() {
        // Because test methods don't support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetInventoryResource');
        // The web service call returns an error code.
        mock.setStatusCode(404);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.startTest();
        // Associate the callout with a mock response.
        Test.setMock(HttpCalloutMock.class, mock);
        
        // Test: execute the integration for the test cart ID and integration info.
        Id OrdId = [SELECT Id FROM Order WHERE Name = 'Test' LIMIT 1];
        List<Id> OrdIds = new List<Id>{OrdId.Id};
        
        String expectedErrorMessage = 'There was a problem with the request. Error: 404';
        executeAndEnsureFailure(expectedErrorMessage, OrdIds, false);
        
        Test.stopTest();
    }

    @isTest static void testProductsWithNoSkuHasError() {
        Test.startTest();

        Id OrdId = [SELECT Id FROM Order WHERE Name = 'Test' LIMIT 1];
        List<Id> OrdIds = new List<Id>{OrdId.Id};
        Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test',Material__c='1265710',Case_Quantity__c=3.0);
        insert Prod;
        

        // Insert a cart item without a SKU
        OrderItem OrdItem = new OrderItem( 
            Quantity=3.0, 
            Type='Order Product', 
            Name='TestProduct', 
			product2Id = prod.Id
        );
        insert OrdItem;

        String expectedErrorMessage = 'The SKUs for all products in your cart must be defined.';
        executeAndEnsureFailure(expectedErrorMessage, OrdIds, true);

        Test.stopTest();

        // Remove the invalid cart item
        delete OrdItem;
    }
    
    // Executes taxes check and ensures an error is correctly triggered
    static void executeAndEnsureFailure(String expectedErrorMessage, List<Id> webCarts, Boolean userError) {
        try {
            B2BSyncTax.syncTax(webCarts);
            
            // An exception should have been thrown before getting to this point:
            System.assert(false);
        } catch (CalloutException e) {
            System.assertEquals(expectedErrorMessage, e.getMessage());
        }
        
        // A new CartValidationOutput record with level 'Error' was created.
        List<CartValidationOutput> cartValidationOutputs = [SELECT Id, Message FROM CartValidationOutput WHERE Level = 'Error'];
        if (userError) {
            System.assertEquals(2, cartValidationOutputs.size());
            //System.assertEquals(expectedErrorMessage, cartValidationOutputs.get(0).Message);
        } else {
            System.assertEquals(0, cartValidationOutputs.size());
        }
    }*/

	public class MockHttpResponseGenerator implements HttpCalloutMock {
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('https://purefishing-salesforce-qa-exp-api.us-e1.cloudhub.io/salesforce-exp-api/v1/price', req.getEndpoint());
			system.debug(req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[{"materialNumber":"1265710","price":"47.6900","tax":"2.3800"},{"materialNumber":"1011974","price":"12.3500","tax":"0.6200"}]');
            res.setStatusCode(200);
            return res;
        }
    }
}