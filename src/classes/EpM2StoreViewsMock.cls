@isTest
global class EpM2StoreViewsMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json; charset=utf-8');
        response.setBody('[{"id":1,"code":"halooptics_store_view","name":"Halo Optics Store View","website_id":1,"store_group_id":1},{"id":0,"code":"admin","name":"Admin","website_id":0,"store_group_id":0},{"id":2,"code":"barnettcrossbows_store_view","name":"Barnett Crossbows Store View","website_id":2,"store_group_id":2},{"id":3,"code":"ameristep_store_view","name":"Ameristep Store View","website_id":3,"store_group_id":3},{"id":4,"code":"caboodles_store_view","name":"Caboodles Store View","website_id":4,"store_group_id":4},{"id":5,"code":"bloodsportarchery_view","name":"Bloodsport Archery Store View","website_id":5,"store_group_id":5},{"id":6,"code":"flextonegamecalls_store_view","name":"Flextone Game Calls Store View","website_id":6,"store_group_id":6},{"id":7,"code":"evolved_store_view","name":"Evolved Store View","website_id":7,"store_group_id":7},{"id":8,"code":"tenzingoutdoors_store_view","name":"Tenzing Outdoors Store View","website_id":8,"store_group_id":8}]');
        response.setStatusCode(200);
        response.setStatus('Success');
        return response;
    }
}