/**
 * @description Used to retrieve Account information for the 'cc_account_display' LWC
 * @date July 7, 2022
 */
 /* Version      Author                  Company                 Date                 Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud            July 7, 2022         Initial version
 */

public with sharing class AccountDisplayController {


    @AuraEnabled
    public static String getAccountName(String accountId){
        return [SELECT Name FROM Account WHERE Id = :accountId LIMIT 1].Name;
    }

}