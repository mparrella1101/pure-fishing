@isTest(seeAllData=true)
public class TestItemAvailabilityScheduled {
    /*
	static testMethod void testItemAvailabilitySync() {
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EpJDEGetItemAvailabilityMock());
        ItemAvailabilityScheduledSync job = new ItemAvailabilityScheduledSync();
        job.execute(null);
        Test.stopTest();        
    }
    */
	static testMethod void testItemAvailabilityPagination() {
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EpJDEGetItemAvailabilityMock());
        Integer PRODUCTS_PER_REQUEST = 10;
        Integer PRODUCTS_PER_QUERY = 300;
        Integer MINUTES_OFFSET = 1;
        ItemAvailabilityPaginationSync job = new ItemAvailabilityPaginationSync(PRODUCTS_PER_REQUEST,PRODUCTS_PER_QUERY,MINUTES_OFFSET);
        job.execute(null);
        Test.stopTest();        
    }
}