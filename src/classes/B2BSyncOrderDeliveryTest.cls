@isTest
public class B2BSyncOrderDeliveryTest {
    @testSetup static void setup() {
        Account testAccount = new Account(Name='TestAccount');
        insert testAccount;
        WebStore testWebStore = new WebStore(Name='TestWebStore');
        insert testWebStore;
        
        Account account = [SELECT Id FROM Account WHERE Name='TestAccount' LIMIT 1];
        WebStore webStore = [SELECT Id FROM WebStore WHERE Name='TestWebStore' LIMIT 1];
        
        Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test',Material__c='1265710',Case_Quantity__c=3.0);
        insert Prod;
        
        PriceBook2 PriBook= new PriceBook2(name='Standard Price Book');
        insert PriBook;
        //Pricebook2 PriBook=[Select Id from Pricebook2 where name='Standard Price Book'];
        
        PricebookEntry PrBukEntry= new PricebookEntry(Product2Id=Prod.id,Pricebook2Id=Test.getStandardPricebookId(),UnitPrice=0);
        insert PrBukEntry;
        Order Ord = new Order(AccountId=account.Id,EffectiveDate=Date.valueOf('2022-04-02'),status='Draft',Pricebook2Id=Test.getStandardPricebookId(),Distribution_Channel__c='00');
        insert Ord;
       // PricebookEntry PricBukId=[Select Id from PricebookEntry where Pricebook2Id=PriBook.Id LIMIT 1 ];
        /*OrderItem OrdItem = new OrderItem(OrderId=Ord.Id, Product2Id=Prod.Id,Quantity=1,Type ='Delivery Charge',ListPrice=0);
        insert OrdItem;*/

    }
    
    @isTest static void testIntegrationRunsSuccessfully() {
        // Because test methods don't support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		Test.startTest();
        // Test: execute the integration for the test cart ID.
        Order Ord = [SELECT Id FROM Order LIMIT 1];
        List<Id> orderId = new List<Id>{Ord.Id};
        B2BSyncOrderDelivery.syncDelivery(orderId);

        // No status is returned from the syncCheckInventory check, but if no exception is thrown, the test passes

        Test.stopTest();
    }
    

   /* @isTest static void testWhenExternalServiceCallFailsAFailedStatusIsReturnedAndACartValidationOutputEntryIsNotCreated() {
        // Because test methods do not support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the the Developer Console, select File | New | Static Resource
       
        // Test: execute the integration for the test cart ID.
        Order Ord = [SELECT Id FROM Order LIMIT 1];
        List<Id> orderId = new List<Id>{Ord.Id};
        String expectedErrorMessage = 'There was a problem with the request. Error: 404';
        Test.startTest();
        try {
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            B2BSyncDelivery.syncDelivery(orderId);
            // An exception should have been thrown before getting to this point:
           // System.assert(false);
        } catch (CalloutException e) {
            //System.assertEquals(expectedErrorMessage, e.getMessage());
        }
        
        // A new CartValidationOutput record with level 'Error' was created.
        List<CartValidationOutput> cartValidationOutputs = [SELECT Id, Message FROM CartValidationOutput WHERE Level = 'Error'];
        System.assertEquals(0, cartValidationOutputs.size());
        
        Test.stopTest();
    }*/

    public class MockHttpResponseGenerator implements HttpCalloutMock {
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('https://purefishing-salesforce-qa-exp-api.us-e1.cloudhub.io/salesforce-exp-api/v1/freight', req.getEndpoint());
			system.debug(req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            b2b_ShippingWrapper.response responseMap = new b2b_ShippingWrapper.response();
            responseMap.price='12';
            responseMap.currencyType='USD';
            res.setBody(JSON.serialize(responseMap));
            res.setStatusCode(200);
            return res;
        }
    }
}