public class b2b_PricingIntegrationService {
    public static List<b2b_pricingWrapper.response> doPost(b2b_pricingWrapper request){
        HttpResponse res = new HttpResponse();
        Integer SuccessfulHttpRequest = 200;
        
        try {
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            
            req.setEndpoint('https://purefishing-salesforce-dev-exp-api.us-e1.cloudhub.io/salesforce-exp-api/v1/price');
            req.setMethod('POST');
            
            //these parts of the POST you may want to customize
            req.setBody(JSON.serialize(request));
            req.setHeader('Content-Type', 'application/json');  
            req.setHeader('client_id', '404ca6a1d11d4185931e24f3af23c581');  
            req.setHeader('client_secret', '4D746b710f3242a282A6EC90C61fE187');  
            
            res = http.send(req);
        } catch(exception e) {
            System.debug('Callout error: '+ e);
        }
        System.debug(res.getBody());
        if (res.getStatusCode() == SuccessfulHttpRequest) 
        {
    		List<b2b_pricingWrapper.response> responseMap = (List<b2b_pricingWrapper.response>)JSON.deserialize('[{"materialNumber": "1265710","price": "71.5300","tax": "3.5800"}]', List<b2b_pricingWrapper.response>.class);
            System.debug('responseMap' + responseMap);
            return responseMap;  
        }
        else
        {
        String errorMessage = 'There was a problem with the request. Error: ' + res.getStatusCode();
                throw new CalloutException (errorMessage);
        }
        
    }
}