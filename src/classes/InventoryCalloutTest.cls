/**
 * @author      Matt Parrella
 * @date        August 25, 2022
 * @description This class will provide test coverage for the 'InventoryCallout' Apex Class
 */

 /* Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud            08-25-2022      Initial version
 */

@IsTest
public with sharing class InventoryCalloutTest {
    @TestSetup
    static void setup_data() {
        // Create Test Account
        Account testAccount = CCTestUtils.createAccount('Apex Test Account', false);
        testAccount.SAP_Customer__c = '123';
        testAccount.Division_SAP__c = '01';
        testAccount.Distribution_Channel__c = '22';
        testAccount.Sales_Org__c = 'test';
        insert testAccount;


        // Create Test Product
        Product2 testProduct = CCTestUtils.createProduct('Apex Test Product', 'ABC123', 'MAT123', true, false);
    }

    @IsTest
    static void getInventoryRealTime200_Test() {
        // Set Mock Response
        Test.setMock(HttpCalloutMock.class, new MasterGridController_Test.GetInventory200Mock());

        // Get AccountId and Product2Id
        String accountId = [SELECT Id FROM Account WHERE Name = 'Apex Test Account' LIMIT 1]?.Id;
        System.assertNotEquals(null, accountId, 'No test Account record found.');

        String product2Id =[SELECT Id FROM Product2 WHERE Name = 'Apex Test Product' LIMIT 1]?.Id;
        System.assertNotEquals(null, product2Id, 'Test Product record not found.');

        // Build List<InventoryInput> objects to pass into the Invocable Method
        List<InventoryCallout.InventoryInput> testInputList = new List<InventoryCallout.InventoryInput>();
        InventoryCallout.InventoryInput testInput = new InventoryCallout.InventoryInput();
        testInput.accountId = accountId;
        testInput.productId = product2Id;
        testInputList.add(testInput);

        Test.startTest();
        List<InventoryCallout.InventoryOutput> result = InventoryCallout.getInventoryRealTime(testInputList);
        System.debug(result[0].errorMsg);
        System.assertEquals(823.0, result[0].inventoryResult, 'Unexpected quantity value returned.');
        System.assertEquals(true, result[0].isSuccess, 'Expected a value of \'true\' here.');
        System.assertEquals('', result[0].errorMsg, 'Expected an empty string here.');
        Test.stopTest();

    }

    @IsTest
    static void getInventoryRealTime_Exception_Test() {
        // Build List<InventoryInput> objects to pass into the Invocable Method
        List<InventoryCallout.InventoryInput> testInputList = new List<InventoryCallout.InventoryInput>();
        InventoryCallout.InventoryInput testInput = new InventoryCallout.InventoryInput();
        testInput.accountId = '123';
        testInput.productId = '123';
        testInputList.add(testInput);

        Test.startTest();
        List<InventoryCallout.InventoryOutput> result;
        try {
            result = InventoryCallout.getInventoryRealTime(testInputList);
        }
        catch(Exception ex) {
            System.assertNotEquals('', result[0].errorMsg);
        }
        Test.stopTest();
    }
}