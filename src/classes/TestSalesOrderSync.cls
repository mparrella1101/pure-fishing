@isTest(seeAllData=true)
public class TestSalesOrderSync {
	static testMethod void testSalesOrderSync() {
		System.Test.startTest();
        System.Test.setMock(HttpCalloutMock.class, new EpM2SalesOrderMock());
        scheduledSync job = new scheduledSync();
        job.execute(null);
        System.Test.stopTest();        
    }
    
    static testMethod void testSpecificSalesOrderSync() {
		System.Test.startTest();
        System.Test.setMock(HttpCalloutMock.class, new EpM2SalesOrderMock());
        String order_id = '4322640658592';
        String store_id = '00000000';
        EpM2Sync.syncSpecificOrder(store_id, order_id);
        System.Test.stopTest();        
    }
}