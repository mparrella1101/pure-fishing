//Arsee: Updated this class for Translated values on 19th Dec,2022
public  class productDetailService {
    
    @AuraEnabled(Cacheable=true)
    public static Map<String , Object> getProductDetails(string recordId){
        
       // recordId = '01t4P00000AA84tQAD';
        
        Map<String, Product_Fields__mdt> mcs = Product_Fields__mdt.getAll();
        
        Map<String,string> fieldsMap = new Map<String,string> ();
        
        string userLanguage = UserInfo.getLanguage();
        system.debug('userLanguage' + userLanguage + 'mcs' + mcs + 'recordId' + recordId);
        
        for(String Data: mcs.keySet())
        {
            //System.debug(Data+' --> '+mcs.get(Data).APIName__c);
            boolean pdpCheck=mcs.get(Data).Include_in_PDP_Specs__c;
           if(pdpCheck==true)
            { 
                if(userLanguage=='en_US')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).MasterLabel);
                else if(userLanguage=='fr')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).FR_Label__C);
                else if(userLanguage=='de')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).DE_Label__C);
                else if(userLanguage=='nl_NL')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).NL_Label__C);
                else if(userLanguage=='da')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).DA_Label__C);
                else if(userLanguage=='fi')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).FI_Label__C);
                else if(userLanguage=='hu')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).HU_Label__C);
                else if(userLanguage=='it')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).IT_Label__C);
                else if(userLanguage=='no')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).NO_Label__C);
                else if(userLanguage=='pl')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).PL_Label__C);
                else if(userLanguage=='sl')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).SL_Label__C);
                else if(userLanguage=='sv')
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).SV_Label__C);
                else
                fieldsMap.put(mcs.get(Data).APIName__c,mcs.get(Data).MasterLabel); 

            }
            else
            {
                system.debug('Nothing');
            }
        }

        
        
        list<String> fieldList = new List<String>();
        List<product2> products=new List<product2>();
        List<Product2DataTranslation> prodlist= new List<Product2DataTranslation>();
        Map<String, Object> fieldsToValue= new Map<String, Object>();
        Map<String , Object> productLabelValueMap = new Map<String , Object>();
        fieldList.addAll(fieldsMap.keySet());
        String query;
        if(userLanguage=='en_US')//Arsee: Changed for Translation
        {
            query = 'select ' +  String.join(fieldList,',') + ' from Product2 where Id=:recordId';
            products =Database.query(query);
            fieldsToValue = products[0].getPopulatedFieldsAsMap();
        }
        else
        {
           query = 'select ' +  String.join(fieldList,',') + ' from Product2DataTranslation where ParentId=:recordId and Language=:userLanguage';
           prodlist =Database.query(query);
           fieldsToValue = prodlist[0].getPopulatedFieldsAsMap();
           if(fieldsToValue == null || fieldsToValue.isEmpty())
           {
                query = 'select ' +  String.join(fieldList,',') + ' from Product2 where Id=:recordId';
                products =Database.query(query);
                fieldsToValue = products[0].getPopulatedFieldsAsMap();
           }
        }
		system.debug('products = '+products + ' prodlist:' + prodlist);
        
        for(String field : fieldsMap.keySet())
        {
            //below logic will populate key=" multilangauge label" and value = "fieldValue"
            if(fieldsToValue.get(field)!=false)
            productLabelValueMap.put(fieldsMap.get(field),fieldsToValue.get(field));
        }
        system.debug('productLabelValueMap = '+productLabelValueMap);
        return productLabelValueMap;
        
    }
    
    
}