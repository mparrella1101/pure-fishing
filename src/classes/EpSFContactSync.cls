public class EpSFContactSync {
    public static void createSFContact(ECRecord__c record) {
        try {
            CustomerItems item = (CustomerItems) System.JSON.deserialize(record.Dump__c, CustomerItems.class);
            
            Account account = AccountHandler.getAccount(item.store_id,record.EComStoreID__c);
            
            Contact c = new Contact();
            c.ECRecordName__c = record.Name;
            c.Email = item.email;
            c.FirstName = item.firstname;
            c.LastName = item.lastname;
            
            if(account != NULL)
            {
                c.AccountId = account.Id;
            }
            String phone = null;
            for(Integer i = 0; i < item.addresses.size(); i++) {
                CustomerAddresses addresses = item.addresses[i];
                if(item.default_billing == addresses.id || item.default_billing == null) {
                    c.OtherStreet = String.join(addresses.street,',');
                    c.OtherCity = addresses.city;
                    c.OtherState = addresses.region.region;
                    c.OtherPostalCode = addresses.postcode;
                    c.OtherStateCode = addresses.region.region_code;
                    c.OtherCountryCode = addresses.country_id;
                    c.OtherPhone=addresses.telephone;
                    if(phone==null)
                        phone=addresses.telephone;
                } 
                if(item.default_shipping == addresses.id) {
                    c.MailingStreet = String.join(addresses.street,',');
                    c.MailingCity = addresses.city;
                    c.MailingState = addresses.region.region;
                    c.MailingPostalCode = addresses.postcode;
                    c.MailingStateCode = addresses.region.region_code;
                    c.MailingCountryCode = addresses.country_id;
                    c.HomePhone=addresses.telephone;
                    if(phone==null)
                        phone=addresses.telephone;
                }
            }
            
            if(phone!=null)
                c.Phone = phone;
            
            c.MagentoId__c = item.id;
            c.EComStoreID__c = record.EComStoreID__c;
            
            System.debug('*-*-*-*-*-*-*-*'+c);
            insert c;
            
            record.SalesForceID__c = c.Id;
            record.SalesForceExecutionTime__c = DateTime.now();
            record.SalesForceObjectStatus__c = true;
            record.SalesForceResponse__c = 'Success';
        } catch (Exception ex) {
            record.SalesForceExecutionTime__c = DateTime.now();
            record.SalesForceObjectStatus__c = false;
            record.SalesForceResponse__c = ex.getMessage();
            Notifier.sendEmail('EpSFContactSync - createSFContact - Exception error: '+ ex);
        }
    }
}