/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           03/04/2022            Initial Version
 */
// This is PaymentGatewayAdapter for Authorize.net
global class AuthorizeNetAdapter implements commercepayments.PaymentGatewayAdapter {
    
    private static final commercepayments.SalesforceResultCodeInfo RC_SUCCESS = toCodeInfo(commercepayments.SalesforceResultCode.Success);
    private static final commercepayments.SalesforceResultCodeInfo RC_DECLINE = toCodeInfo(commercepayments.SalesforceResultCode.Decline);
    private static final List<String> DECLINE_CODES = new List<String>{'card_declined', 'incorrect_cvc', 'expired_card',
        'authentication_required', 'card_not_supported', 'currency_not_supported', 'incorrect_number', 'incorrect_zip'};
            
            global AuthorizeNetAdapter() {
            }
    
    global commercepayments.GatewayResponse processRequest(commercepayments.paymentGatewayContext gatewayContext) {   
        system.debug('gatewayContext' + gatewayContext)     ;
        commercepayments.RequestType requestType = gatewayContext.getPaymentRequestType();
        commercepayments.PaymentGatewayRequest paymentRequest = gatewayContext.getPaymentRequest();
        commercepayments.GatewayResponse response;
        
        try {
            System.debug('processRequest.requestType ' + requestType);
			 if (requestType == commercepayments.RequestType.Tokenize) {
                    response = createTokenizeResponse((commercepayments.PaymentMethodTokenizationRequest)gatewayContext.getPaymentRequest());
            }
            return response; 
        } catch (Exception e) { //StripeValidationException
            System.debug('Error msg' + e.getMessage() + e.getStackTraceString());
            return new commercepayments.GatewayErrorResponse('400', e.getMessage() +  e.getStackTraceString());
        }
    }
    

            
  public commercepayments.GatewayResponse createTokenizeResponse(commercepayments.PaymentMethodTokenizationRequest tokenizeRequest) {
       
        AuthorizeNetPayment__mdt payment = AuthorizeNetPayment__mdt.getInstance('Autherization_Net');

        System.debug('createTokenizeResponse->');
        System.debug('createTokenizeResponse->'+ System.JSON.serialize(tokenizeRequest));
        String body = makeTokenPaymentBody(payment.Username__c,payment.Trax_key__c,(commercepayments.PaymentMethodTokenizationRequest)tokenizeRequest);
        commercepayments.CardPaymentMethodRequest cardPaymentMethod = tokenizeRequest.cardPaymentMethod;
        Httprequest req =new Httprequest();
        req.setHeader('content-type', 'application/json');
        req.setHeader('Content-length', String.valueOf(body.length()));// For Authorize.Net
        req.setEndpoint(payment.Endpoint__c);// For Authorize.Net
        req.setBody(body);
        req.setMethod('POST');
        
        commercepayments.PaymentsHttp http = new commercepayments.PaymentsHttp();
        commercepayments.PaymentMethodTokenizationResponse tokenizeResponse = new commercepayments.PaymentMethodTokenizationResponse();
        HttpResponse res = null;
        system.debug('createTokenizeResponse '+req);
        System.debug('createTokenizeResponse ' +req.getEndpoint());
        res = http.send(req);
        Map<String,Object> jsonReqDeserialized = (Map<String,Object>) JSON.deserializeUntyped(body);
        Object reqInfos = jsonReqDeserialized.get('createTransactionRequest');
        Map<String,Object> reqInfo = (Map<String,Object>) reqInfos;
        String reqOrd = (String) reqInfo.get('refId');
        System.debug('refId: ' + reqOrd +' createTokenizeResponse: ' +res);
		String Resbody = res.getBody();
        Resbody = Resbody.trim().replace('\uFEFF', '');
        System.debug('Resbody' + Resbody);  // "LC899I"
		Integer sc = res.getStatusCode();
        system.debug('AuthorizeNetAdapter + SC '+sc);
        Map<String,Object> jsonDeserialized = (Map<String,Object>) JSON.deserializeUntyped(Resbody);
        Object transResp = jsonDeserialized.get('transactionResponse');
        Map<String,Object> transMap = (Map<String,Object>) transResp;
        System.debug('transMap' + transMap);  // "LC899I"
        System.debug(transMap.get('authCode'));  // "LC899I"
        String RespCodes = (String) transMap.get('responseCode');
        String RespCodeResult='';
        If (RespCodes == '1')
            RespCodeResult= 'Approved';
        else if (RespCodes == '2')
            RespCodeResult= 'Declined';
        else if (RespCodes == '3')
            RespCodeResult= 'Error';
        else if (RespCodes == '4')
            RespCodeResult= 'Held for Review';
        else 
            RespCodeResult='';
		tokenizeResponse.setGatewayDate(system.now());
        if (sc >= 200 && sc < 300) {
            system.debug('SC_If_Start '+sc);
            tokenizeResponse.setGatewayResultCode(RespCodeResult);
            tokenizeResponse.setGatewayResultCodeDescription(RespCodeResult);
            tokenizeResponse.setSalesforceResultCodeInfo(RC_SUCCESS);
			//tokenizeResponse.setGatewayToken();
            system.debug('SC_If '+sc);
        }
        else {
            tokenizeResponse.setGatewayResultCode(RespCodeResult);
            tokenizeResponse.setGatewayResultCodeDescription(RespCodeResult);
            tokenizeResponse.setSalesforceResultCodeInfo(RC_DECLINE);
            //tokenizeResponse.setGatewayMessage((String) error.get('message'));
            
        }
        if(RespCodeResult == 'error' || RespCodeResult == 'Declined' || RespCodeResult == 'Held for Review'){
			return new commercepayments.GatewayErrorResponse('400', 'There was a problem  with you payment : Payment is decline ,Please try again.' );
		}
        If(reqOrd!=null)// Storing Response data at Order level
            {
               List<Order> OrdUpd= [Select id,CC_AuthCode__c,CC_transId__c,GrandTotalAmount  from Order where Order_Reference__c=:reqOrd LIMIT 1];
                system.debug('OrdUpd '+OrdUpd);
                if( OrdUpd.size()>0)
                {
                    OrdUpd[0].CC_AuthCode__c=string.valueof(transMap.get('authCode'));
                    OrdUpd[0].CC_transId__c=string.valueof(transMap.get('transId'));
					OrdUpd[0].CC_details__c = Test.isRunningTest() ?  'test' :' approvalStatus '+ RespCodes + '\n authAmount '+OrdUpd[0].GrandTotalAmount + '\n authCode ' +transMap.get('authCode')+ '\n cardType '+cardPaymentMethod.cardType+'\n merchantReferenceCode '+jsonDeserialized.get('refId')+ '\n requestId '+transMap.get('transId') + '\n requestToken '+transMap.get('networkTransId');
                   // OrdUpd[0].CC_details__c='approvalStatus :'+ RespCodes+', authAmount :'+OrdUpd[0].GrandTotalAmount +', authCode  :' +transMap.get('authCode')+ ',cardType:'+cardPaymentMethod.cardType+', merchantReferenceCode :'+jsonDeserialized.get('refId')+ ',requestId :'+transMap.get('transId') + ',requestToken :'+transMap.get('networkTransId');
                    List<DmlOperation__e> dmlOperationEvents = new List<DmlOperation__e>();
                    dmlOperationEvents.add(new DmlOperation__e(Operation__c = 'MODIFY', Payload__c = JSON.serialize(OrdUpd[0])));
                    EventBus.publish(dmlOperationEvents);
                    system.debug('OrdUpd_If '+OrdUpd);
                }
                else
                {
                    List<WebCart> cartUpd= [Select id,CC_AuthCode__c,CC_transId__c,GrandTotalAmount  from WebCart where Order_Reference__c=:reqOrd LIMIT 1];
                    system.debug('cartUpd '+cartUpd[0]);
                    cartUpd[0].CC_AuthCode__c=string.valueof(transMap.get('authCode'));
                    cartUpd[0].CC_transId__c=string.valueof(transMap.get('transId'));
					cartUpd[0].CC_details__c =Test.isRunningTest() ?  'test' :'approvalStatus '+ RespCodes + '\n authAmount '+cartUpd[0].GrandTotalAmount + '\n authCode ' +transMap.get('authCode')+ '\n cardType '+cardPaymentMethod.cardType+'\n merchantReferenceCode '+jsonDeserialized.get('refId')+ '\n requestId '+transMap.get('transId') + '\n requestToken '+transMap.get('networkTransId');
                    List<DmlOperation__e> dmlOperationEvents = new List<DmlOperation__e>();
                    dmlOperationEvents.add(new DmlOperation__e(Operation__c = 'MODIFY', Payload__c = JSON.serialize(cartUpd[0])));
                    EventBus.publish(dmlOperationEvents);
                    system.debug('OrdUpd_Else '+cartUpd);

                }
                
            }

        return tokenizeResponse;
        
    }
    
 
 public static String makeTokenPaymentBody(String TokenAPIKey,String TokenTranKey,commercepayments.PaymentMethodTokenizationRequest tokenizeRequest)
	{
        //Creating the request
        Map<String,Object> mapCurrentUserContext = applauncher.CommerceStoreController.getCommerceContext();
        System.debug('mapCurrentUserContext' + mapCurrentUserContext);
        
        List<WebCart> cartswithItem = new List<WebCart>();
        List<Order> OrderList = new List<Order>();
		String reqBody='';
        if(test.isRunningTest()){//This is for test class
            cartswithItem = [SELECT id,AccountId,TotalChargeAmount,GrandTotalAmount,TotalAmount,TotalTaxAmount,Order_Reference__c,  (SELECT Product2Id, Name, Id, CartId, Type, Sku, Quantity,
                                                                                       CartDeliveryGroup.DeliverToCountry,CartDeliveryGroup.DeliverToState,CartDeliveryGroup.DeliverToPostalCode,CartDeliveryGroup.DeliverToCity,
                                                                                       CartDeliveryGroup.DeliverToStreet,CartDeliveryGroup.DeliverToName ,
                                                                                       TotalLineAmount,SalesPrice  FROM CartItems where type =:'Product') from Webcart LIMIT 1];
		   reqBody = getCartDetails(cartswithItem,TokenAPIKey,TokenTranKey,(commercepayments.PaymentMethodTokenizationRequest)tokenizeRequest);
        }
        else if(mapCurrentUserContext != null){ // This is for Checkout flow payment  
            cartswithItem = [SELECT id,AccountId,TotalChargeAmount,GrandTotalAmount,TotalAmount,TotalTaxAmount,OrderNumber__c,Order_Reference__c,  (SELECT Product2Id, Name, Id, CartId, Type, Sku, Quantity,
                                                                                       CartDeliveryGroup.DeliverToCountry,CartDeliveryGroup.DeliverToState,CartDeliveryGroup.DeliverToPostalCode,CartDeliveryGroup.DeliverToCity,
                                                                                       CartDeliveryGroup.DeliverToStreet,CartDeliveryGroup.DeliverToName ,TotalLineAmount,SalesPrice  FROM CartItems where type =:'Product')
                             from Webcart where webstoreID=: (String)mapCurrentUserContext.get('webstoreId') AND OwnerId =: UserInfo.getUserId() AND Status in ('Active','Checkout') order by createddate desc];
			reqBody = getCartDetails(cartswithItem,TokenAPIKey,TokenTranKey,(commercepayments.PaymentMethodTokenizationRequest)tokenizeRequest);
        }
        else
        {
           // this is for SalesRep Order payment
           OrderList = [Select id,OrderNumber,Order_Reference__c,AccountId,TotalTaxAmount,GrandTotalAmount,TotalAdjustedDeliveryAmount,status,Completing_Order_User__c,(Select Product2Id,product2.name,Quantity,UnitPrice from OrderItems where type ='Order Product' ) from Order where Completing_Order_User__c =: UserInfo.getUserId() AND Status='Completing Order']; 
           reqBody=getOrderDetails(OrderList,TokenAPIKey,TokenTranKey,(commercepayments.PaymentMethodTokenizationRequest)tokenizeRequest);
        }
        
		 return reqBody;
        
        
  }
  Public Static String getOrderDetails(List<Order> OrderList,String TokenAPIKey,String TokenTranKey,commercepayments.PaymentMethodTokenizationRequest tokenizeRequest)
  {
      commercepayments.CardPaymentMethodRequest cardPaymentMethod = tokenizeRequest.cardPaymentMethod;
      System.debug('makeTokenPaymentBody:cardPaymentMethod ' + JSON.serialize(cardPaymentMethod));
      commercepayments.AddressRequest billingAddress = tokenizeRequest.address;
      System.debug('makeTokenPaymentBody:billingAddress' + JSON.serialize(billingAddress));
       //string items = '{"lineItem":[';
      string lineItem='{"lineItem":[';
      string items='';
      string OrderId='';
      String OrdRef='';
      Decimal TAmt;
      String ActId = '';
      string tax = '';
      string Ord = '';
      string shipTax = '';
      string shipToName = '';
      string fname = '';
      string lname = '';
      string ProdName ='';
      CartItem shipToaddress;
      List<Order> addresses;
      List<Order> OrdList;
      integer j=0;
          for(OrderItem OI :OrderList[0].OrderItems){
              j++;
              ProdName = OI.Product2.name.length() > 30? OI.Product2.name.Substring(0,30) : OI.Product2.name;
              /*items = items + '{"itemId":"'+OI.Product2Id+'","name":"'+ ProdName +'",'+'"quantity":"'+OI.Quantity+'","unitPrice":"'+OI.UnitPrice+'"}';
              if(j == OrderList[0].OrderItems.size() - 1 && OrderList[0].OrderItems.size() > 1){
                  items =items + ',';
              }*/
              if(j==1)
              {
                  items= '{"itemId":"'+OI.Product2Id+'","name":"'+ ProdName +'",'+'"quantity":"'+OI.Quantity+'","unitPrice":"'+OI.UnitPrice.setScale(2)+'"}';
              }
              else {
                  items = items +',' +'{"itemId":"'+OI.Product2Id+'","name":"'+ ProdName +'",'+'"quantity":"'+OI.Quantity+'","unitPrice":"'+OI.UnitPrice.setScale(2)+'"}';
              }
          }
          //items= items + ']}';
          items= lineItem + items + ']}';
          TAmt=OrderList[0].GrandTotalAmount.setScale(2);
          ActId=OrderList[0].AccountId;
          List<Account> getWebstoreInfo=[select id,Sales_Org__c,SAP_Customer__c from Account where id=:ActId];
          OrdRef=OrderList[0].Order_Reference__c;
          OrderId = OrderList[0].OrderNumber;
          list<AuthorizeNetPayment__mdt> AuthData = new List<AuthorizeNetPayment__mdt>();
          if (!Test.isRunningTest()){
              AuthData =[Select Username__c,Trax_key__c from AuthorizeNetPayment__mdt where Sales_Org__c=:getWebstoreInfo[0].Sales_Org__c ];
          } else {
              AuthorizeNetPayment__mdt testRecord = new AuthorizeNetPayment__mdt();
              testRecord.Username__c = 'test';
              testRecord.Trax_key__c = 'apex_text';
              AuthData.add(testRecord);
          }
          Ord='{"invoiceNumber":"'+getWebstoreInfo[0].SAP_Customer__c+'","description":"'+ OrdRef +'"}';
          tax = '{"amount":"'+OrderList[0].TotalTaxAmount.setScale(2)+'","name":"level2 tax name","description":"level2 tax"}';
          shipTax = '{"amount":"'+OrderList[0].TotalAdjustedDeliveryAmount.setScale(2)+'","name":"level2 tax name","description":"level2 tax"}';
          addresses = 
          [
          SELECT ShippingAddress,Shipping_First_Name__c,Shipping_Last_Name__c,ShippingCity, ShippingStreet, ShippingState, ShippingCountry, ShippingPostalCode, ShippingGeocodeAccuracy, ShippingLatitude, ShippingLongitude
          FROM order 
          WHERE OrderNumber=:OrderId];
          string name = test.isRunningTest() ? 'Test Test' :(String) cardPaymentMethod.cardHolderName;
          string firstN = name.contains(' ') ?  name.substringBefore(' ') : '';
          string lastN = name.contains(' ') ?  name.substringAfter(' ') : name;
          System.debug('name: '+ name + 'firstName: '+ firstN + 'lastName:' + lastN);
          String reqBody = '';
          string messageBody1;
          messageBody1 ='{"createTransactionRequest":{"merchantAuthentication":{"name":"'+AuthData[0].Username__c.trim()+'","transactionKey":"'+AuthData[0].Trax_key__c?.trim()+'"},'
          +'"refId":"'+ OrdRef +'","transactionRequest":{"transactionType":"authCaptureTransaction","amount":"'+TAmt+'","payment":{"creditCard":'
          +'{"cardNumber":"'+ cardPaymentMethod?.cardNumber +'","expirationDate":"'+cardPaymentMethod?.expiryMonth +cardPaymentMethod?.expiryYear +'","cardCode":"'+cardPaymentMethod?.cvv+'"}},"order":'+Ord+',"lineItems":'+ items +',"tax":'+tax +',"shipping":'+ shipTax +',"customer":{"id":"'+ActId+'"},"billTo":{"firstName":"'+firstN+'","lastName":"'+lastN+'",'
          +'"company":"'+billingAddress?.companyName+'","address":"'+ billingAddress?.street +'","city":"'+billingAddress?.city +'","state":"'+billingAddress?.state +'","zip":"'+billingAddress?.postalCode+'","country":"'+billingAddress?.country +'"},"shipTo":'
          +'{"firstName":"'+addresses[0]?.Shipping_First_Name__c+'","lastName":"'+addresses[0]?.Shipping_Last_Name__c+'","address":"'+addresses[0]?.Shipping_Last_Name__c+'","city":"'+addresses[0]?.ShippingCity+'","state":"'+addresses[0]?.ShippingState+'","zip":"'+addresses[0]?.ShippingPostalCode+'",'
          +'"country":"'+addresses[0]?.ShippingCountry+'"},'
          +'"userFields":{"userField":[]}}}}';
           System.debug('messageBody1'+ messageBody1);
           reqBody = messageBody1;
      
      return reqBody;
  }
	Public Static String getCartDetails(List<WebCart> cartswithItem,String TokenAPIKey,String TokenTranKey,commercepayments.PaymentMethodTokenizationRequest tokenizeRequest)
	{
		commercepayments.CardPaymentMethodRequest cardPaymentMethod = tokenizeRequest.cardPaymentMethod;
        System.debug('makeTokenPaymentBody:cardPaymentMethod ' + JSON.serialize(cardPaymentMethod));
		commercepayments.AddressRequest billingAddress = tokenizeRequest.address;
		System.debug('makeTokenPaymentBody:billingAddress' + JSON.serialize(billingAddress));
		 //string items = '{"lineItem":[';
        string lineItem='{"lineItem":[';
        string items='';
        string OrderId='';
        String OrdRef='';
        String Ord='';
        Decimal TAmt;
        String ActId = '';
        string tax = '';
        string shipTax = '';
        string shipToName = '';
        string fname = '';
        string lname = '';
        string ProdName ='';
        CartItem shipToaddress;
        List<Order> addresses;
        List<Order> OrdList;
            integer i=0;
            String CartId='';
            for(cartItem CI :cartswithItem[0].CartItems){
                i++;
                CartId=CI.CartId;
                ProdName = CI.Name.length() > 30? CI.Name.Substring(0,30) : CI.Name;
                if(i==1)
                {
                    items= '{"itemId":"'+CI.Product2Id+'","name":"'+ ProdName +'",'+'"quantity":"'+CI.Quantity+'","unitPrice":"'+CI.SalesPrice.setScale(2)+'"}';
                }
                else {
                    items = items +',' +'{"itemId":"'+CI.Product2Id+'","name":"'+ ProdName +'",'+'"quantity":"'+CI.Quantity+'","unitPrice":"'+CI.SalesPrice.setScale(2)+'"}';
                }

                /*items = items + '{"itemId":"'+CI.Product2Id+'","name":"'+ ProdName +'",'+'"quantity":"'+CI.Quantity+'","unitPrice":"'+CI.SalesPrice+'"}';
                System.debug('i' + i + 'cartswithItem[0].CartItems.size()' + cartswithItem[0].CartItems.size());
                if(i == cartswithItem[0].CartItems.size() - 1 && cartswithItem[0].CartItems.size() > 1){
                    items =items + ',';
                    System.debug('items' + items);
                }*/
            }
            items= lineItem + items + ']}';
            List<WebCart> OrdId = [Select Order_Reference__c from WebCart where Id=:CartId];
            for(WebCart WC :OrdId){
                OrderId=WC.Order_Reference__c;
            }

            System.debug('OrderId: ' +  OrderId);
            /*OrdList =[Select OrderNumber from Order where id=:OrderId ];
            OrdRef=OrdList[0].OrderNumber;*/
            TAmt=cartswithItem[0].GrandTotalAmount.setScale(2);
            ActId=cartswithItem[0].AccountId;
            List<Account> getWebstoreInfo=[select id,Sales_Org__c,SAP_Customer__c from Account where id=:ActId];
        list<AuthorizeNetPayment__mdt> AuthData= new List<AuthorizenetPayment__mdt>();
        if (!Test.isRunningTest()){
                AuthData =[Select Username__c,Trax_key__c from AuthorizeNetPayment__mdt where Sales_Org__c=:getWebstoreInfo[0].Sales_Org__c ];
        } else {
            AuthorizeNetPayment__mdt testRecord = new AuthorizeNetPayment__mdt();
            testRecord.Username__c = 'test';
            testRecord.Trax_key__c = 'apex_text';
            AuthData.add(testRecord);
        }
            Ord='{"invoiceNumber":"'+getWebstoreInfo[0].SAP_Customer__c+'","description":"'+ cartswithItem[0].Order_Reference__c +'"}';
            tax = '{"amount":"'+cartswithItem[0].TotalTaxAmount.setScale(2)+'","name":"level2 tax name","description":"level2 tax"}';
            shipTax = '{"amount":"'+cartswithItem[0].TotalChargeAmount.setScale(2)+'","name":"level2 tax name","description":"level2 tax"}';
            shipToaddress = cartswithItem[0]?.CartItems[0];
            System.debug('shipToaddress: '+ shipToaddress );
            shipToName = shipToaddress?.CartDeliveryGroup?.DeliverToName;
            fname = shipToName.contains(' ') ?  shipToName.substringBefore(' ') : '';
            lname = shipToName.contains(' ') ?  shipToName.substringAfter(' ') : shipToName;
            System.debug('fname: '+ fname + 'lname:' + lname);
            string name = test.isRunningTest() ? 'Test Test' :(String) cardPaymentMethod.cardHolderName;
            string firstName = name.contains(' ') ?  name.substringBefore(' ') : '';
            string lastName = name.contains(' ') ?  name.substringAfter(' ') : name;
            System.debug('name: '+ name + 'firstName: '+ firstName + 'lastName:' + lastName);
			
			String reqBody = '';
			string messageBody1;
			messageBody1 ='{"createTransactionRequest":{"merchantAuthentication":{"name":"'+AuthData[0].Username__c.trim()+'","transactionKey":"'+AuthData[0].Trax_key__c?.trim()+'"},'
        +'"refId":"'+ OrderId +'","transactionRequest":{"transactionType":"authCaptureTransaction","amount":"'+TAmt+'","payment":{"creditCard":'
        +'{"cardNumber":"'+ cardPaymentMethod?.cardNumber +'","expirationDate":"'+cardPaymentMethod?.expiryMonth +cardPaymentMethod?.expiryYear +'","cardCode":"'+cardPaymentMethod?.cvv+'"}},"order":'+Ord+',"lineItems":'+ items +',"tax":'+tax +',"shipping":'+ shipTax +',"customer":{"id":"'+ActId+'"},"billTo":{"firstName":"'+firstName+'","lastName":"'+lastName+'",'
        +'"company":"'+billingAddress?.companyName+'","address":"'+ billingAddress?.street +'","city":"'+billingAddress?.city +'","state":"'+billingAddress?.state +'","zip":"'+billingAddress?.postalCode+'","country":"'+billingAddress?.country +'"},"shipTo":'
        +'{"firstName":"'+fname+'","lastName":"'+lname+'","address":"'+shipToaddress?.CartDeliveryGroup?.DeliverToStreet+'","city":"'+shipToaddress?.CartDeliveryGroup?.DeliverToCity+'","state":"'+shipToaddress?.CartDeliveryGroup?.DeliverToState+'","zip":"'+shipToaddress?.CartDeliveryGroup?.DeliverToPostalCode+'",'
        +'"country":"'+shipToaddress?.CartDeliveryGroup?.DeliverToCountry+'"},'
        +'"userFields":{"userField":[]}}}}';
			 System.debug('messageBody1'+ messageBody1);
             reqBody = messageBody1;
        
        return reqBody;
	}
    private static commercepayments.SalesforceResultCodeInfo toCodeInfo(commercepayments.SalesforceResultCode code) {
        return new commercepayments.SalesforceResultCodeInfo(code);
    }
   

	
}