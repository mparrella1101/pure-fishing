public with sharing class AddOrderController {
    @AuraEnabled
    public static OrderDetailsWrapper getContactPointAddresses(Id accountId){
        Account account = [SELECT Id, Name, SAP_Customer__c, CurrencyIsoCode, Sales_Org__c, Pricebook__c FROM Account WHERE Id =: accountId];
        PriceBook2 priceBook = [SELECT Id, Name, IsStandard FROM Pricebook2 WHERE IsStandard = true];
        List<ContactPointAddress> contactPointAddressList = [SELECT Id, Name, Address, AddressType, SAP_Customer__c, Billing_Shipping_Account__r.SAP_Customer__c FROM ContactPointAddress WHERE AddressType = 'Shipping' AND ParentId = :accountId];

        OrderDetailsWrapper orderDetailsWrapper = new OrderDetailsWrapper();
        orderDetailsWrapper.contactPointAddresses = contactPointAddressList;
        orderDetailsWrapper.account = account;
        orderDetailsWrapper.pricebookId = priceBook.Id;

        return orderDetailsWrapper;
    }

    @AuraEnabled
    public static ProductDetailsWrapper getProductDetails(Id orderId){
        // MParrella | 10-13-2022 | TSK-00232019 | Adding 'Product_Category__c' field to order query
        Order order = [SELECT Id, AccountId, Type, Reason_Code__c, EffectiveDate, CurrencyIsoCode, Distribution_Channel__c, Product_Category__c FROM Order WHERE Id = :orderId LIMIT 1];//Arsee:updated for Plano
        Account account = [SELECT Id, Division_SAP__c, SAP_Customer__c, Distribution_Channel__c, Sales_Org__c, Customer_Group__r.Min_Order__c FROM Account WHERE Id = :order.AccountId LIMIT 1];
        // List<Product2> variationProducts = [SELECT Id, Name, ProductCode, Image__c, Hero_Image__c, Material__c, Case_Quantity__c FROM Product2 WHERE ProductClass = 'Variation'];
        // List<Product2> variationProducts = new List<Product2>();
        // for(Product2 product : [SELECT Id, Name, ProductCode, Image__c, Hero_Image__c, Material__c, Case_Quantity__c FROM Product2 WHERE ProductClass = 'Variation']){
        //     variationProducts.add(product);
        // }
        // PriceBook2 priceBook = [SELECT Id, Name, IsStandard FROM Pricebook2 WHERE IsStandard = true];
        // List<PricebookEntry> pricebookEntries = [SELECT Id, Name, UnitPrice, Product2Id, ProductCode FROM PricebookEntry WHERE Pricebook2Id = :priceBook.Id AND Product2Id IN :variationProducts];
        
        ProductDetailsWrapper productDetailsWrapper = new ProductDetailsWrapper();
        productDetailsWrapper.order = order;
        productDetailsWrapper.account = account;
        // productDetailsWrapper.variationProducts = variationProducts;
        // productDetailsWrapper.pricebookEntries = pricebookEntries;

        return productDetailsWrapper;
    }

    @AuraEnabled
    public static Map<String,Object> getOrderTotal(String orderId) {
        Map<String,Object> result = new Map<String,Object>();
        Order orderRecord = [SELECT Product_Category__c, TotalAmount FROM Order WHERE Id =: orderId LIMIT 1];
        result.put('totalAmount', orderRecord.TotalAmount);
        String distChannel = '';
        switch on orderRecord.Product_Category__c {
            when 'PF Finished Goods' {
                distChannel = '00';
            }
            when 'Plano Legacy' {
                distChannel = '08';
            }
             when 'Spare Parts' {
                distChannel = '00';
             }
             when else {
                // Do nothing, shouldn't land here
             }
        }
        result.put('distChannel', distChannel);

        return result;
    }



    @AuraEnabled
    public static OrderDistChannelResultWrapper OrdDisChannel(String productCategory,String accountId)////Arsee:Created for Plano
    {
        Account account = [SELECT Distribution_Channel__c,Sales_Org__c FROM Account WHERE Id = :accountId LIMIT 1];
        String ordReason;
        String disChannel;
       // String OrdMin;
        if(productCategory=='PF Finished Goods')
        {
            ordReason='100';
            disChannel='00';
            //OrdMin=String.valueOf(account.Customer_Group__r.Min_Order__c);
        }
        else if(productCategory=='Plano Legacy')//Arsee:Added for Canada plano
        {
            if(account.Sales_Org__c=='0020')
            {
                ordReason='100';
                disChannel='08';
            }
            else if (account.Sales_Org__c=='0040')
            {
                ordReason='100';
                disChannel='00';
            }
            else {
                ordReason='100';
                disChannel=account.Distribution_Channel__c;
            }
            //OrdMin='500';
        }
        else if(productCategory=='Spare Parts')
        {
            ordReason='125';
            disChannel='00';
            //OrdMin='0';
        }
        else {
            
        }
        OrderDistChannelResultWrapper OrdDistChannel = new OrderDistChannelResultWrapper();
        OrdDistChannel.ordReason = ordReason;
        OrdDistChannel.disChannel = disChannel;
        //OrdDistChannel.OrdMin=OrdMin;
        return OrdDistChannel;

    }

    @AuraEnabled
    public static ProductQueryWrapper productQuery(String productCode){
        System.debug('productCode: ' + productCode);
        Product2 product;
        Boolean productValid = true;
        String error;
        try{
            product = [
                SELECT Id, Name, ProductCode, Image__c, Hero_Image__c, Material__c, Case_Quantity__c, IsActive
                FROM Product2
                WHERE (ProductCode = :productCode OR Model_Number__c = :productCode)
                // WHERE ProductCode = :productCode
                AND ProductClass != 'Variation Parent'
                AND ProductClass != null
                AND Material__c != null
                AND IsActive = true
                LIMIT 1];
        }
        catch(QueryException ex){
            productValid = false;
            error = 'Product not found';
        }
        
        ProductQueryWrapper productQueryWrapper = new ProductQueryWrapper();
        productQueryWrapper.product = product;
        productQueryWrapper.productValid = productValid;
        productQueryWrapper.error = error;

        return productQueryWrapper;
    }

    @AuraEnabled
    // public static ProductQueryWrapper productValidation(List<String> productCodes){
    public static ProductQueryWrapper productValidation(String dataModel){
        // System.debug('productCodes: ' + productCodes);
        System.debug('dataModel: ' + dataModel);
        String error;        
        CsvHeaders csvHeaders = new CsvHeaders();
        csvHeaders = (CsvHeaders) JSON.deserialize(dataModel, CsvHeaders.class);
        System.debug(csvHeaders);
        List<String> materialList = csvHeaders.materialList;
        List<String> upcList = csvHeaders.upcList;
        List<String> modelList = csvHeaders.modelList;
        System.debug('materialList: ' + materialList);
        System.debug('upcList: ' + upcList);
        System.debug('modelList: ' + modelList);
        List<Product2> validProductList = new List<Product2>();
        try {
            validProductList = [
                SELECT Id, Name, ProductCode, Image__c, Hero_Image__c, Material__c, UPC_Code__c, Model_Number__c, Case_Quantity__c
                FROM Product2
                // WHERE ProductCode IN :productCodes
                WHERE (Material__c IN :materialList OR UPC_Code__c IN :upcList OR Model_Number__c IN :modelList)
                AND IsActive = true
                AND Material__c != null
                AND ProductClass != 'Variation Parent'
                AND ProductClass != null];
        } catch(QueryException ex) {
            error = 'Product not found';
        }
        System.debug('validProductList: ' + validProductList);
        ProductQueryWrapper productQueryWrapper = new ProductQueryWrapper();
        productQueryWrapper.validProductList = validProductList;
        productQueryWrapper.error = error;

        return productQueryWrapper;
    }

    @AuraEnabled
    public static List<Object> getPricing(String productData){
        System.debug('productData: ' + productData);
        List<b2b_pricingWrapper> b2bWrapperList = new List<b2b_pricingWrapper>();
        b2b_pricingWrapper productPricingWrapper = new b2b_pricingWrapper();

        productPricingWrapper = (b2b_pricingWrapper) JSON.deserialize(productData, b2b_pricingWrapper.class);
        System.debug('productPricingWrapper: ' + productPricingWrapper);
        // b2b_pricingWrapper.cls_products product = new b2b_pricingWrapper.cls_products();
        // product.materialNumber = productPricingWrapper.materialNumber;
        // product.quantity = String.valueOf(productPricingWrapper.quantity);
        // productList.add(product);
        // System.debug('productList: ' + productList);
        // for(ProductPricingWrapper productPricingWrapper : productPricingWrapperList){
        List<b2b_pricingWrapper.cls_products> productList = productPricingWrapper.products;
        System.debug('productList: ' + productList);
        for(b2b_pricingWrapper.cls_products product : productList){
            System.debug('product: ' + product);
            // b2b_pricingWrapper.cls_products product = new b2b_pricingWrapper.cls_products();
            // product.materialNumber = productList.materialNumber;
            // product.quantity = String.valueOf(productList.quantity);
            // productList.add(product);
        }
        
        System.debug('productList: ' + productList);

        b2b_pricingWrapper b2b_priceWrapper = new b2b_pricingWrapper();
        // b2b_priceWrapper.division = '00' + productPricingWrapper.division;
        b2b_priceWrapper.division = '0' + productPricingWrapper.division;
        // b2b_priceWrapper.division = productPricingWrapper.division;
        b2b_priceWrapper.customerNumber = productPricingWrapper.customerNumber;
        // b2b_priceWrapper.distributionChannel = '0' + productPricingWrapper.distributionChannel;
        b2b_priceWrapper.distributionChannel = productPricingWrapper.distributionChannel.length() == 2 ? productPricingWrapper.distributionChannel : '0' + productPricingWrapper.distributionChannel; // This HAS to be 2-digits, for some reason SF is only returning 1 and is causing a 500 response
        // b2b_priceWrapper.salesOrganization = '00' + productPricingWrapper.salesOrganization;
        b2b_priceWrapper.salesOrganization = productPricingWrapper.salesOrganization;
        // b2b_priceWrapper.salesOrganization = '0020';
        String OrdType='OR';
        switch on productPricingWrapper.orderReason
        {
            when '400'{OrdType='ZSMP';}
            when '401'{OrdType='ZSMP';}
            when '402'{OrdType='ZSMP';}
            when '403'{OrdType='ZSMP';}
            when '404'{OrdType='ZSMP';}
            when '405'{OrdType='ZSMP';}
            when else{}
        }
        b2b_priceWrapper.orderType = OrdType;
        b2b_priceWrapper.products = productList;

        b2b_priceWrapper.orderReason = productPricingWrapper.orderReason;
        b2b_priceWrapper.shipmentDate = productPricingWrapper.shipmentDate;
    
        //     b2bWrapperList.add(b2b_priceWrapper);
        // }
        
        System.debug('b2b_Integration response: ' + b2b_IntegrationService.priceCal(b2b_priceWrapper));
        return b2b_IntegrationService.priceCal(b2b_priceWrapper);

        // test
        // List<b2b_pricingWrapper.cls_products> testProducts = new List<b2b_pricingWrapper.cls_products>();
        // b2b_pricingWrapper.cls_products testProduct = new b2b_pricingWrapper.cls_products();
        // testProduct.materialNumber = '1004950';
        // testProduct.quantity = '2';
        // testProducts.add(testProduct);
        // b2b_pricingWrapper b2bWrapperTest = new b2b_pricingWrapper();
        // b2bWrapperTest.division = '00';
        // b2bWrapperTest.customerNumber = '39889';
        // b2bWrapperTest.distributionChannel = '00';
        // b2bWrapperTest.salesOrganization = '0020';
        // b2bWrapperTest.orderType = 'OR';
        // b2bWrapperTest.products = testProducts;
        // b2bWrapperTest.orderReason = '100';
        // b2bWrapperTest.shipmentDate = '2022-04-02';

        // System.debug('b2bWrapperTest: ' + b2bWrapperTest);
        // System.debug('b2b_Integration test response: ' + b2b_IntegrationService.priceCalBulk(b2bWrapperTest));

    }

    @AuraEnabled
    public static List<Object> getInventory(String inventoryData){
        InventoryWrapper inventoryWrapper = new InventoryWrapper();
        inventoryWrapper = (InventoryWrapper) JSON.deserialize(inventoryData, InventoryWrapper.class);
        System.debug('inventoryWrapper: ' + inventoryWrapper);
        String accountId;
        string orderId;

        List<String> productIds = new List<String>();
        List<String> productIdWrapper = InventoryWrapper.productIds;
        for(String productId : productIdWrapper){
            System.debug('productId: ' + productId);
            productIds.add(productId);            
        }
        accountId = inventoryWrapper.accountId;
        orderId = inventoryWrapper.orderId;

        System.debug('InventoryCal: ' + b2b_IntegrationService.InventoryCal(accountId, productIds,orderId));
        // List<Object> returnObj = new List<Object>();
        // Map<String, String> responseMap = new Map<String, String>();
        // List<b2b_InventoryWrapper.response> inventoryResponse = b2b_IntegrationService.InventoryCal(accountId, productIds);
        // for(b2b_InventoryWrapper.response response : inventoryResponse){
        //     responseMap.put(response.materialNumber, response.quantity);
        // }
        // System.debug('returnObj: ' + returnObj);
        // System.debug('responseMap: ' + responseMap);
        // List<Object> responseObj = (List<Object>) b2b_IntegrationService.InventoryCal(accountId, productIds);
        return b2b_IntegrationService.InventoryCal(accountId, productIds,orderId);

    }

    @AuraEnabled
    public static List<String> pricebookCheck(String productData){
        System.debug('pricbookCHeck productData value: ' + productData);
        PricebookCheckWrapper pricebookWrapper = new PricebookCheckWrapper();
        pricebookWrapper = (PricebookCheckWrapper) JSON.deserialize(productData, PricebookCheckWrapper.class);
        // MParrella | Coastal Cloud | 03-08-2023 | TSK-00236400 | Changing pricebookId reference to custom mdt: START
        String orderId = pricebookWrapper.orderId;
        // Query for Account's Sales Org
        String salesOrg = [SELECT Account.Sales_Org__c FROM Order WHERE Id =: orderId Limit 1]?.Account?.Sales_Org__c;
//        String pricebookId = System.Label.cc_standard_pricebook_id;
        String pricebookId = getPricebookId(salesOrg);
        System.debug('*** PricebookId: ' + pricebookId + ' found for Sales Org: ' + salesOrg);
        // MParrella | Coastal Cloud | 03-08-2023 | TSK-00236400 | Changing pricebookId reference to custom mdt: STOP
        List<String> validProductList = new List<String>();
        Set<String> productIds = pricebookWrapper.productIds;
        String currencyCode = pricebookWrapper.currencyCode;
        List<PricebookEntry> priceBookEntryList = [
            SELECT Id, Product2Id, ProductCode, UnitPrice
            FROM PricebookEntry
            WHERE Product2Id IN :productIds
            AND Pricebook2Id = :pricebookId
            AND CurrencyIsoCode = :currencyCode];
        for(PricebookEntry pricebookEntry : priceBookEntryList){
            if(productIds.contains(pricebookEntry.Product2Id)){
                validProductList.add(pricebookEntry.ProductCode);
            }
        }
        System.debug('validProductList.size(): ' + validProductList.size());
        System.debug('validProductList: ' + validProductList);

        return validProductList;
    }

    @AuraEnabled
    public static OrderItemInsertResultWrapper submitOrderItems(String dataModel){
        List<OrderItemWrapper> orderItemWrapperList = new List<OrderItemWrapper>();
        orderItemWrapperList = (List<OrderItemWrapper>) JSON.deserialize(dataModel, List<OrderItemWrapper>.class);
        
        System.debug('dataModel: ' + dataModel);
        
        Boolean saveSuccess = true;
        PriceBook2 priceBook = [SELECT Id, Name, IsStandard FROM Pricebook2 WHERE IsStandard = true];
        String testPricebook = '01s4P0000047PQoQAM';
        // MParrella | Coastal Cloud | 03-08-2023 | TSK-00236400 | Changing pricebookId reference to custom mdt: START
//        String pricebookId = System.Label.cc_standard_pricebook_id;

        // Get Sales Org from account (all products passed-in will have the same account, so can hard-code the array access)
        String acctId = orderItemWrapperList[0].accountId;
        String orderId = orderItemWrapperList[0].orderId;
        String salesOrg = [SELECT Account.Sales_Org__c FROM Order WHERE Id =: orderId LIMIT 1]?.Account?.Sales_Org__c;
        String pricebookId;
        if (salesOrg != null) {
            pricebookId = getPricebookId(salesOrg);
        } else {
            pricebookId = '';
        }

        // MParrella | Coastal Cloud | 03-08-2023 | TSK-00236400 | Changing pricebookId reference to custom mdt: STOP
        Set<Id> validProductSet = new Set<Id>();

		// SMaxwell | 7-18-2022 | Fixing  error regarding mismatched CurrencyCodes for multicurrency        
        for(orderItemWrapper orderItemWrapper : orderItemWrapperList){
            validProductSet.add(orderItemWrapper.productId);
            orderId = orderItemWrapper.orderId;
        }
        String currencyCode = [SELECT Id, CurrencyIsoCode FROM Order WHERE Id = :orderId]?.CurrencyIsoCode;
        
        System.debug('pricebookId: ' + pricebookId);
        System.debug('currencyCode: ' + currencyCode);
        List<PricebookEntry> priceBookEntryList = [
            SELECT Id, Product2Id, UnitPrice
            FROM PricebookEntry
            WHERE Product2Id IN :validProductSet
            // AND Pricebook2Id = :testPricebook];
            AND Pricebook2Id = :pricebookId
            AND CurrencyIsoCode = :currencyCode]; // SMaxwell | 7-18-2022 | Fixing  error regarding mismatched CurrencyCodes for multicurrency
        System.debug('priceBookEntryList: ' + priceBookEntryList);
        Map<String, PricebookEntry> priceBookEntryMap = new Map<String, PricebookEntry>();
        for(PricebookEntry priceBookEntry : priceBookEntryList){
            priceBookEntryMap.put(priceBookEntry.Product2Id, priceBookEntry);
        }   
        System.debug('priceBookEntryMap: ' + priceBookEntryMap);     
        // List<OrderItemWrapper> orderItemWrapperList = new List<OrderItemWrapper>();
        // orderItemWrapperList = (List<OrderItemWrapper>) JSON.deserialize(dataModel, List<OrderItemWrapper>.class);
        System.debug('dataModel: ' + dataModel);
        List<OrderItem> orderItemList = new List<OrderItem>();
        List<String> errorItems = new List<String>();
        Map<String, String> orderItemProductCodeMap = new Map<String, String>();
        for(orderItemWrapper orderItemWrapper : orderItemWrapperList){
            System.debug('Order Item Wrapper Line: ' + orderItemWrapper);
            OrderItem orderItem = new OrderItem();
            orderItem.OrderId = orderItemWrapper.orderId;
            orderItem.Product2Id = orderItemWrapper.productId;
            // orderItem.ProductCode = orderItemWrapper.productCode;
            orderItem.Quantity = orderItemWrapper.quantity;
            orderItem.Quantity_Ordered__c = orderItemWrapper.quantity;
            orderItem.Type = 'Order Product';
            // orderItem.ListPrice = priceBookEntryMap.get(orderItemWrapper.productId).UnitPrice;
            orderItem.UnitPrice = orderItemWrapper.unitPrice;
            // orderItem.UnitPrice = priceBookEntryMap.get(orderItemWrapper.productId).UnitPrice;
            // orderItem.TotalPrice = orderItemWrapper.extPrice;
            System.debug('product id: ' + orderItemWrapper.productId);
            System.debug('pricebook entry: ' + priceBookEntryMap.get(orderItemWrapper.productId)?.Id);
            orderItem.PricebookEntryId = priceBookEntryMap.get(orderItemWrapper.productId)?.Id;
            orderItemList.add(orderItem);
            orderItemProductCodeMap.put(orderItem.quantity+'-'+orderItemWrapper.productId, orderItemWrapper.productCode);
        }
        /* for(OrderItem orderItem : orderItemList){
            if(orderItem.PricebookEntryId == null){
                errorItems.add(orderItem.Product2Id);
                orderItemList.remove(orderItemList.indexOf(orderItem));
            }
        } */
        for(Integer i = (orderItemList.size()-1); i >= 0; i--){
            OrderItem orderItem = orderItemList[i];
            if(orderItem.PricebookEntryId == null){
                errorItems.add(orderItem.Quantity+'-'+orderItemProductCodeMap.get(orderItem.Quantity+'-'+orderItem.Product2Id)+'-Product not found in pricebook.');
                orderItemList.remove(i);
            } else {
                orderItemProductCodeMap.remove(orderItem.Quantity+'-'+orderItem.Product2Id);
            }
        }
        System.debug('orderItemList: ' + orderItemList);
        System.debug('errorItems: ' + errorItems);
        System.debug('orderItemProductCodeMap: ' + orderItemProductCodeMap);
        if(!orderItemList.isEmpty()){
            Database.SaveResult[] srList = Database.insert(orderItemList, true);
                for(Database.SaveResult sr : srList){
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted OrderItem record with ID: ' + sr.getId());
                    } else {
                        // Operation failed, so get all errors 
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('OrderItem fields that affected this error: ' + err.getFields());
                        }
                        // return false;
                        return getOrderItemInsertResults(orderItemList, errorItems, orderItemList.size(), errorItems.size());
                    }
                }
                // return true;
                return getOrderItemInsertResults(orderItemList, errorItems, orderItemList.size(), errorItems.size());
        } else {
            // return true;
            return getOrderItemInsertResults(null, null, null, null);
        }
    }

    private static OrderItemInsertResultWrapper getOrderItemInsertResults(List<OrderItem> orderItems, List<String> errors, Integer numOfSuccessfulInserts, Integer numOfErrors){
        OrderItemInsertResultWrapper orderItemInsertWrapper = new OrderItemInsertResultWrapper();
        orderItemInsertWrapper.insertedOrderItems = orderItems;
        orderItemInsertWrapper.errorList = errors;
        orderItemInsertWrapper.successfulInserts = numOfSuccessfulInserts;
        orderItemInsertWrapper.numberOfErrors = numOfErrors;

        return orderItemInsertWrapper;
    }

    @AuraEnabled
    public static Map<Id,Account> getAccountFieldData(String accountId){
        return new Map<Id,Account>([SELECT Id, CurrencyIsoCode, Sales_Org__c FROM Account WHERE Id =: accountId LIMIT 1]);
    }

    // MParrella | Coastal Cloud | 03-07-2023 | TSK-00236400 | Creating new method to retrieve pricebook data from Custom MDT: START
    @AuraEnabled
    public static String getPricebookId(String salesOrgId) {
        // Use Sales Org to access correct Custom MDT
        String salesOrgKey = 'X' + salesOrgId; // Need to prepend 'X' to the sales org, so we can look up the custom mdt record by developer name
        Sales_Org_Configuration__mdt salesOrgConfig = Sales_Org_Configuration__mdt.getInstance(salesOrgKey);

        if (salesOrgConfig == null) {
            return '';
        } else {
            return salesOrgConfig.PricebookId__c;
        }
    }
    // MParrella | Coastal Cloud | 03-07-2023 | TSK-00236400 | Creating new method to retrieve pricebook data from Custom MDT: STOP


    // ------------------ WRAPPER CLASSES ------------------ // 
    public class OrderDetailsWrapper {
        @AuraEnabled
        public List<ContactPointAddress> contactPointAddresses {get;set;}
        @AuraEnabled
        public Account account {get;set;}    
        @AuraEnabled
        public String pricebookId {get;set;}    
    }

    public class ProductDetailsWrapper {
        @AuraEnabled
        public Order order {get;set;}
        @AuraEnabled
        public Account account {get;set;}
        // @AuraEnabled
        // public List<Product2> variationProducts {get;set;}    
        // @AuraEnabled
        // public List<PricebookEntry> pricebookEntries {get;set;}    
    }

    public class ProductQueryWrapper {
        @AuraEnabled
        public List<Product2> validProductList {get;set;}
        @AuraEnabled
        public List<String> invalidProductList {get;set;}
        @AuraEnabled
        public Product2 product {get;set;}
        @AuraEnabled
        public Boolean productValid {get;set;}
        @AuraEnabled
        public String error {get;set;}
    }

    public class PricebookCheckWrapper {
        public String orderId {get;set;}
        public Set<String> productIds {get;set;}
        public String currencyCode {get;set;}
        public String accountId {get;set;}
    }

    public class CsvHeaders {
        public List<String> materialList {get;set;}
        public List<String> modelList {get;set;}
        public List<String> upcList {get;set;}
    }

    public class InventoryWrapper {
        public String accountId {get;set;}
        public String orderId {get;set;}
        public List<String> productIds {get;set;}
    }

    public class OrderItemWrapper {
        public String orderId {get;set;}
        public String productId {get;set;}
        public String productCode {get;set;}
        public String productName {get;set;}
        public Integer quantity {get;set;}
        public Decimal unitPrice {get;set;}
        public Decimal extPrice {get;set;}
        public String accountId {get;set;} // MParrella | TSK-00236400
    }

    public class OrderItemInsertResultWrapper {
        @AuraEnabled
        public List<OrderItem> insertedOrderItems {get;set;}
        @AuraEnabled
        public List<String> errorList {get;set;}
        @AuraEnabled
        public Integer successfulInserts {get;set;}
        @AuraEnabled
        public Integer numberOfErrors {get;set;}
    }
    public class OrderDistChannelResultWrapper{
        @AuraEnabled
        public String ordReason;
        @AuraEnabled
        public String disChannel;
       // @AuraEnabled
        //public String OrdMin;

    }
}