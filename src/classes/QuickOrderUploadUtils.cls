/**
 * @description Utility class to supplement the 'QuickOrderUploadController' class for the 'Quick Order Upload' LWC
 */
/* Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
   1.0         Matt Parrella           Coastal Cloud           March 22, 2022          Initial version
*/

public with sharing class QuickOrderUploadUtils {

    private static final Integer MAX_SEARCH_TERMS = 32;
    private static final Integer MAX_SUBREQUESTS = 25;
    private static final Integer MAX_PAGE_SIZE = 50;
    public static final Integer ADD_TO_CART_MAX = 100;


    /**
     * @description This method will make a callout to the Connect API's "Product Search" endpoint in order to locate products
     * based on the passed in search string. This endpoint will take into consideration any and all Entitlements that may be
     * related to the running user's Account.
     *
     * @param searchTermsInput The list of search terms (derived from Material/Model/UPC) separated by a space
     * @param webstoreId The 18-character Salesforce Id representing the Web Store within the Community
     * @param effectiveAccountId The 18-character Salesforce Id of the current user's Account
     * @param pageSize The number of page
     * @param itemsList The list of ImportItem objects that were derived from the uploaded CSV rows
     *
     * @return (Map<String,String>) A map of all valid search terms (?)
    */
    public static Map<String,String> productSearch(List<String> searchTermsInput, String webstoreId, String effectiveAccountId, Integer pageSize, Map<String,ImportItem> itemsMap ) {
        Map<String,String> validSearchTerms = new Map<String,String>();

        // Get the org domain from the custom settings record
        B2B_Cart_Upload_Settings__c customSetting = B2B_Cart_Upload_Settings__c.getOrgDefaults();

        String domain = customSetting.Domain_URL__c;

        // URL path for the Connect API "Product Search" endpoint
        String connectApiEndpoint = '/v53.0/commerce/webstores/' + webstoreId + '/search/product-search';

        // URL path for the Connect API "Batch" endpoint
        String batchRestAPIURL = domain + '/services/data/v53.0/connect/batch';

        if (effectiveAccountId != null){
            connectApiEndpoint += '?effectiveAccountId=' + effectiveAccountId;
        }

        // Create one long string of search terms, separated by a space
        String searchTerm = '';

        for (String str : searchTermsInput) {
            if (searchTerm != '') {
                searchTerm += ' ';
            }
            searchTerm += str;
        }

        String [] searchTermWords = searchTerm.split(' ');

        // Create a batch request for each set of 32 search term words
        Integer count = 0;

        Set<String> searchTerms = new Set<String>();

        List<Object> batchRequestList = new List<Object>();

        for (String st : searchTermWords){
            count++;
            searchTerms.add(st);

            if (searchTerms.size() >= MAX_SEARCH_TERMS) {
                Map<String,Object> searchRequest = createSearchRequest(searchTerms, 'Post', connectApiEndpoint, MAX_PAGE_SIZE);
                batchRequestList.add(searchRequest);
                searchTerms = new Set<String>();
            }

            if (batchRequestList.size() >= MAX_SUBREQUESTS) {
                String response = submitBatchRequest(batchRestAPIURL, batchRequestList);
                Map<String, String> temp = processBatchResults(response, itemsMap);
                validSearchTerms.putAll(temp);
                batchRequestList = new List<Object>();
            }
        }

        // Process remaining search terms
        if (!searchTerms.isEmpty()){
            Map<String,Object> searchRequest = createSearchRequest(searchTerms, 'Post', connectApiEndpoint, MAX_PAGE_SIZE);
            batchRequestList.add(searchRequest);
        }

        // Process the search requests that remain

        if (!batchRequestList.isEmpty()) {
            String response = submitBatchRequest(batchRestAPIURL, batchRequestList);
            Map<String,String> temp = processBatchResults(response, itemsMap);
            validSearchTerms.putAll(temp);
        }
        return validSearchTerms;
    }

    /**
     * @description Helper method to process each line uploaded by the user in the CSV file. It will create an ImportItem object for each line,
     * and return a list of ImportItem objects
     *
     * @param inputLines A list of 'UploadedItem' objects derived from the rows within the uploaded CSV file
     *
     * @return A list where each object represents a line from within the uploaded CSV file
    */
    public static Map<String,ImportItem> createInputItems(List<QuickOrderUploadController.UploadedItem> inputLines){
        List<ImportItem> itemsList = new List<ImportItem>();

        Map<String,ImportItem> searchTermMap = new Map<String,ImportItem>(); // Will hold the searchTerm => InputItem
        Integer row = 1;
        for (QuickOrderUploadController.UploadedItem item : inputLines){
            ImportItem newItem = new ImportItem();
            row++;
            newItem.row = row;
            String identifier;
            if (item.material != null && item.material != ''){
                newItem.material = item.material;
                newItem.qty = item.quantity;
            }
            if (item.upc != null && item.upc != ''){
                newItem.upc = item.upc;
                newItem.qty = item.quantity;
            }
            if (item.model != null && item.model != ''){
                newItem.model = item.model;
                newItem.qty = item.quantity;
            }
            // Set the identifier to be material (if present), then upc (if present), then model
            identifier = (item.material != null && item.material != '') ? item.material : (item.upc != null && item.upc != '') ? item.upc : (item.model != null && item.model != '') ? item.model : '';
            newItem.uniqueKey = identifier;
            searchTermMap.put(newItem.uniqueKey, newItem);
        }

        return searchTermMap;
    }

    /**
     * @description Helper method to add error messages to the response map, in the event that we encounter an error. This will be used
     * to show the user what the current issues are so they can be resolved
     *
     * @param msg A Message object containing details about the error encountered
     * @param pos Integer representing the index within the list of Message objects (0 => first position in the Message array)
     * @param responseMap The ResponseMap that will be updated with error messages
     */
    public static void addMessage(Message msg, Integer pos, Map<String,Object> responseMap){
        List<Message> retMessages = null;

        if (responseMap.containsKey('messages')){
            retMessages = (List<Message>) responseMap.get('messages');
        } else {
            retMessages = new List<Message>();
        }

        if (pos == null) {
            retMessages.add(msg);
        }
        else if (retMessages.size() <= 0){
            retMessages.add(msg);
        }
        else {
            retMessages.add(pos, msg);
        }
        responseMap.put('messages', retMessages);
    }

    /**
     * @description Helper method to update the 'totErrors' parameter within the ResponseMap. This is used to display this informaiton
     * to the end user on the front-end
     *
     * @param key The name of the parameter we are trying to update
     * @param value The value that will be assigned to the passed-in 'key' parameter
     * @param responseMap The ResponseMap whose parameters will be updated
     */
    public static void updateResponseMapTotalValue(String key, Integer value, Map<String, Object> responseMap) {
        if(responseMap.containsKey(key)) {
            Integer tempTot = (Integer)responseMap.get(key);
            tempTot += value;
            responseMap.put(key, tempTot);
        }
        else {
            responseMap.put(key, value);
        }
    }

    /**
     * @description Helper method that will create a search request when we have > 32 search terms. This request will be added to a list of other
     * requests that will be executed in chunks of 32 search terms.
     *
     * @param searchTerms The list of searchTerms that we are searching for Products against
     * @param method The HTTP Method being issued (POST,PUT,GET,etc.)
     * @param url The full Connect API endpoint url
     * @param pageSize The number of records per page (?)
     *
     * @return A Batch Request Map containing specific information about that search
     */

    private static Map<String, Object> createSearchRequest(Set<String>searchTerms, String method, String url, Integer pageSize) {
        Map<String, Object> batchRequest = new Map<String, Object>();
        batchRequest.put('method', method);
        batchRequest.put('url', url);
        Map<String, Object> searchInput = new Map<String, Object>();
        String searchTerm = '';
        // Separate each search term with a space
        for(String st : searchTerms) {
            if(searchTerm != '') {
                searchTerm += ' ';
            }
            searchTerm += st;
        }

        searchInput.put('searchTerm', searchTerm);
        // Add "Grouping" parameter setting
        String groupingJSON = '{ "groupingOption" : "NoGrouping" }';
        Object groupingObj = (Object) JSON.deserializeUntyped(groupingJSON);
        searchInput.put('grouping', groupingObj);

        // Add fields we'd like to retrieve from the Product record
        List<String> fieldList = new List<String>();
        fieldList.add('UPC_Code__c');
        fieldList.add('Material__c');
        fieldList.add('Model_Number__c');
        searchInput.put('fields', fieldList);

        searchInput.put('page', 0);
        searchInput.put('pageSize', pageSize);

        batchRequest.put('richInput', searchInput);

        return batchRequest;
    }

    /**
     * @description This method will submit batch requests to the Connect API, to be processed when resources are available.
     *
     * @param batchRestAPIURL The full Connect API Batch endpoint url
     * @param batchRequestList The list of all batch requests that need to be processed
     *
     * @return The response from the Connect API callout
     */
    private static String submitBatchRequest(String batchRestAPIURL, List<Object> batchRequestList) {
        String response = '';

        Set<String> validSearchTerms = new Set<String>();

        Map<String,Object> batchInput = new Map<String,Object>();
        batchInput.put('batchRequests', batchRequestList);

        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setMethod('POST');
        httpRequest.setHeader('Content-Type', 'application/json');
//        httpRequest.setHeader('Content-Length', '0'); // testing
        httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
        httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        httpRequest.setTimeout(60000);
        httpRequest.setEndpoint(batchRestAPIURL);
        String requestBody = JSON.serialize(batchInput);
        httpRequest.setBody(requestBody);

        try {
            Http http = new Http();
            HttpResponse resp = http.send(httpRequest);
            System.debug('----------------Submit Batch Callout Info: START -----------------------');
            System.debug('ENDPOINT: ' + httpRequest.getEndpoint());
            System.debug('REQUEST BODY: ' + httpRequest.getBody());
            System.debug('RETURN CODE: ' + resp.getStatusCode());
            System.debug('RESPONSE BODY: ' + resp.getBody());
            System.debug('----------------Submit Batch Callout Info: STOP -----------------------');
            if (resp.getStatusCode() == 200 || resp.getStatusCode() == 201) {
                response = resp.getBody();
            } else {
                throw new CalloutException( resp.getBody() );
            }
        }
        catch (CalloutException ce) { System.debug('CalloutException caught: ' + ce + ' Stack: ' + ce.getStackTraceString() + ' LineNumber: ' + ce.getLineNumber());}
        catch (Exception ex) {
            System.debug('Callout ERROR: ' + ex.getMessage());
        }

        return response;
    }

    /**
     * @description This method will process the results from the submitted Batch jobs.
     *
     * @param response The Response from the Batch processing callout
     * @param itemsMap The Map of ImportItem objects that were derived from the uploaded CSV rows
     *
     * @return A List containing the all the successfully processed ImportItems, with 'productId' attribute populated
     */
    @TestVisible
    private static Map<String,String> processBatchResults(String response, Map<String,ImportItem> itemsMap) {
        Map<String,String> validItems = new Map<String,String>(); // Key: material_upc_model Value: found productId
        Map<String,Object> responseMap = new Map<String,Object>();
        if (response != null && response != ''){
            responseMap = (Map<String,Object>) JSON.deserializeUntyped(response);
        }
        Boolean hasErrors = (Boolean) responseMap.get('hasErrors');
        List<Object> resultsList = (List<Object>) responseMap.get('results');

        // Iterate over results to get the product Id
        integer numProductsFound = 0;
        for (Object obj : resultsList) {
            Map<String,Object> resultsMap = (Map<String,Object>)obj;
            if (!hasErrors) {

                Map<String,Object> result = (Map<String,Object>) resultsMap.get('result');

                Map<String,Object> productsPage = (Map<String,Object>) result.get('productsPage');

                List<Object> products = (List<Object>) productsPage.get('products');
                numProductsFound += products.size();
                for (Object obj2 : products) {
                    Map<String, Object> product = (Map<String,Object>) obj2;
                    String productId = (String)product.get('id');
                    Map<String,Object> fields = (Map<String,Object>) product.get('fields');

                    // Get Material__c value
                    Map<String,Object> materialMap = (Map<String,Object>)fields.get('Material__c');
                    String material = (String) materialMap.get('value');

                    // Get UPC_Code__c value
                    Map<String,Object> upcMap = (Map<String,Object>)fields.get('UPC_Code__c');
                    String upc = (String) upcMap.get('value');

                    // Get Model Number value
                    Map<String,Object> modelMap = (Map<String,Object>) fields.get('Model_Number__c');
                    String model = (String) modelMap.get('value');


                    // Check if there is an entry in the itemsMap
                    if (itemsMap.containsKey(material)){
                        validItems.put(material, productId);
                    }
                    else if (itemsMap.containsKey(upc)){
                        validItems.put(upc, productId);
                    }
                    else if (itemsMap.containsKey(model)){
                        validItems.put(model, productId);
                    }
                }
            } else {
                List<Object> results = (List<Object>) resultsMap.get('result');
                for (Object obj3 : results) {
                    Map<String, Object> result = (Map<String,Object>) obj3;
                    String message = (String) result.get('message');
                    System.debug(message);
                }
            }
        }
        return validItems;
    }

    /**
     * @description Method to add items to cart in batches
     *
     * @param webstoreId The 18-character Salesforce Id representing the Web Store within the Community
     * @param effectiveAccountId The 18-character Salesforce Id of the current user's Account
     * @param activeCartOrId The 18-character Salesforce Id representing the Cart object within the Community
     * @param batchInputList The list of BatchInput objects that need to be processed
     * @param itemsList The list of ImportItems objects that did not have a matching product
     * @param responseMap The ResponseMap whose attributes will be updated
     *
     * @return List of processed ImportItem objects
     */

    public static List<ImportItem> processBatchInputItems(String webstoreId, String effectiveAccountId, String activeCartOrId, List<ConnectApi.BatchInput> batchInputList, List<ImportItem> itemsList, Map<String,Object> responseMap) {
        List<ConnectApi.BatchInput> tempBatchList = new List<ConnectApi.BatchInput>();
        List<ImportItem> tempItemsList = new List<ImportItem>();
        List<ImportItem> processedItemsList = new List<ImportItem>();

        Integer count = 0;
        Integer index = 0;
        Integer totBatches = 0;
        Integer totItems = 0;

        for (ConnectApi.BatchInput bi : batchInputList) {
            tempBatchList.add(bi);
            ImportItem importItem = itemsList.get(index);
            tempItemsList.add(importItem);
            count++;
            index++;
            totItems++;


            if (count == ADD_TO_CART_MAX) {
                totBatches++;
                addBatchToCart(webstoreId, effectiveAccountId, activeCartOrId, tempBatchList, tempItemsList, totBatches, responseMap);
                processedItemsList.addAll(tempItemsList);

                count = 0;

                tempBatchList = new List<ConnectApi.BatchInput>();
                tempItemsList = new List<ImportItem>();
            }
        }

        if (!tempBatchList.isEmpty()){
            totBatches++;
            addBatchToCart(webstoreId, effectiveAccountId, activeCartOrId, tempBatchList, tempItemsList, totBatches, responseMap);
            processedItemsList.addAll(tempItemsList);
            tempItemsList = new List<ImportItem>();
        }
        responseMap.put('batchCount', totBatches);

        if(responseMap.containsKey('totItems')) {
            Integer tempTotItems = (Integer)responseMap.get('totItems');
            tempTotItems += totItems;
            responseMap.put('totItems', tempTotItems);
        }
        else {
            responseMap.put('totItems', totItems);
        }


        return processedItemsList;
    }

    /**
     * @description Method to add items to the cart in batches
     *
     * @param webstoreId The 18-character Salesforce Id representing the Web Store within the Community
     * @param effectiveAccountId The 18-character Salesforce Id of the current user's Account
     * @param activeCartOrId The 18-character Salesforce Id representing the Cart object within the Community
     * @param batchInputList The list of BatchInput objects that need to be processed
     * @param itemsList The list of ImportItems objects that did not have a matching product
     * @param batchCount The number of batches
     * @param responseMap The ResponseMap whose attributes will be updated
     */

    public static void addBatchToCart(String webstoreId, String effectiveAccountId, String activeCartOrId, List<ConnectApi.BatchInput> batchInputList, List<ImportItem> itemsList, Integer batchCount, Map<String, Object> responseMap) {
        ConnectApi.BatchResult[] batchResults = null;

        if(!Test.isRunningTest()) {
            batchResults = addItemsToCart(webstoreId, effectiveAccountId, activeCartOrId, batchInputList);
        }

        processBatchResults(batchResults, itemsList, responseMap, webstoreId, effectiveAccountId, activeCartOrId);
    }

    /**
     * @description Method to add items to the cart
     *
     * @param webstoreId The 18-character Salesforce Id representing the Web Store within the Community
     * @param effectiveAccountId The 18-character Salesforce Id of the current user's Account
     * @param activeCartOrId The 18-character Salesforce Id representing the Cart object within the Community
     * @param batchInputList The list of BatchInput objects that need to be processed
     *
     * @return A list of processed Batch results
     */
    public static ConnectApi.BatchResult[] addItemsToCart(String webstoreId, String effectiveAccountId, String activeCartOrId, List<ConnectApi.BatchInput> batchInputList) {
        ConnectApi.BatchResult[] batchResults = ConnectApi.CommerceCart.addItemsToCart(webstoreId, effectiveAccountId, activeCartOrId, batchInputList);
        return batchResults;
    }

    /**
     * @description
     *
     * @param batchResults The batch results list
     * @param itemsList  The list of ImportItem objects representing the rows within the uploaded CSV file
     * @param responseMap The ResponseMap whose attributes will be updated
     */

    public static void processBatchResults(ConnectApi.BatchResult[] batchResults, List<ImportItem> itemsList, Map<String, Object> responseMap, String webstoreId, String effectiveAccountId, String cartId) {
        Integer i = 0;
        Integer totErrors = 0;
        Integer totSuccess = 0;
        Boolean hasErrors = false; // When true, we will clear the cart items that were successful so the user doesn't have to do mass editing of csv file
        if(batchResults != null) {

            for(ConnectApi.BatchResult batchResult : batchResults) {
                ImportItem importItem = itemsList.get(i);
                ConnectApi.CartItem cartItem;
                if(batchResult.getResult() instanceof ConnectApi.CartItem) {
                    cartItem = (ConnectApi.CartItem)batchResult.getResult();
                }
                if(cartItem != null) {
                    if(!batchResult.isSuccess) {
                        hasErrors = true;
                        importItem.isSuccess = batchResult.isSuccess;
                        importItem.errorMsg = batchResult.getErrorMessage();
                        totErrors++;
                        System.debug('Error adding product ' + importItem + ': ' + batchResult.getErrorMessage());

                    }
                    else {
                        importItem.isSuccess = true;
                        totSuccess++;
                    }
                }
                else {
                    // Handle add to cart error
                    if(batchResult.isSuccess == false) {
                        hasErrors = true;
                        importItem.isSuccess = batchResult.isSuccess;
                        importItem.errorMsg = batchResult.getErrorMessage();
                        totErrors++;
                        System.debug('Error adding product ' + importItem + ': ' + batchResult.getErrorMessage());

                    }
                }
                itemsList.set(i, importItem);
                i++;
            }
        }

        if (hasErrors){
            clearCart(webstoreId, effectiveAccountId, cartId, (List<String>) responseMap.get('addedProducts'));
        }

        // Modify the itemsList to look like the batchInputList was actually sent to the ConnectApi and processed.
        if(Test.isRunningTest()) {
            for(ImportItem importItem : itemsList) {
                importItem.isSuccess = true;
                totSuccess++;
            }
        }
        updateResponseMapTotalValue('totSuccess', totSuccess, responseMap);
        updateResponseMapTotalValue('totErrors', totErrors, responseMap);
    }

    public static void clearCart(String webstoreId, String effectiveAccountId, String activeCartOrId, List<String> cartItemIds) {
        List<CartItem> cartItemsToDelete;

        // If: productIds parameter has values in the list, then only delete those corresponding CartItems from the Cart
        // Else: select all CartItem records related to the passed-in CartId and delete them all

        if (!cartItemIds.isEmpty()) {
            cartItemsToDelete = [SELECT Id FROM CartItem WHERE Id IN :cartItemIds];
        } else {
            cartItemsToDelete = [SELECT Id FROM CartItem WHERE CartId =: activeCartOrId AND Cart.AccountId =: effectiveAccountId AND Cart.WebStoreId =: webstoreId];
        }
        System.debug('Deleting ' + cartItemsToDelete.size() + ' CartItem records!');
        System.debug('cartItemsToDelete: ' + cartItemsToDelete);

        delete cartItemsToDelete;
    }


    /* Wrapper Classes: START */

    public class ImportItem {
        public String material {get; set;}
        public String upc {get; set;}
        public String model {get; set;}
        public Decimal qty {get; set;}
        public String productId {get; set;}
        public Boolean isValid {get; set;}
        public String uniqueKey {get; set;}

        public Integer row {get; set;}

        public Boolean isSuccess {get; set;}
        public String errorMsg {get; set;}

        public ImportItem() {
            isValid = false;
            isSuccess = false;
        }
    }

    public static final String ERROR = 'error';
    public static final String INFO = 'info';
    public static final String SUCCESS = 'success';
    public static final String WARN = 'warn';

    public class Message {

        @AuraEnabled public String severity {get; set;}
        @AuraEnabled public String title {get; set;}
        @AuraEnabled public String message {get; set;}
        @AuraEnabled public Integer row {get; set;}

        public Message() {
        }

        public Message(String title, String message, String severity, Integer row) {
            this();
            this.title = title;
            this.message = message;
            this.severity = severity;
            this.row = row;
        }

        public Message(String title, String message, String severity) {
            this();
            this.title = title;
            this.message = message;
            this.severity = severity;
        }
    }
    /* Wrapper Classes: STOP */





}