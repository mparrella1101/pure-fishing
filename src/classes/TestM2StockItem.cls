@isTest(seeAllData=true)
public class TestM2StockItem {
    static testMethod void testM2StockItemUpdate() {
		Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new EpM2StockItemUpdateMock());
        EpM2StockItemUpdateSync.stockItemUpdate('CAB56066A','100','00000000');
        Test.stopTest();
    }
}