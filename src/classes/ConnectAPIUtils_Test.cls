/**
 * @description Apex Test class to cover the 'ConnectAPIUtils' Apex Class
 */

 /* Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud            06-16-2022      Initial version
    1.1         Matt Parrella           Coastal Cloud            10-12-2022      Adding coverage for new 'removeCharacters()' method
 */
@IsTest
public without sharing class ConnectAPIUtils_Test {
    @TestSetup
    static void setup_data() {
        // Create Test Account
        Account testAccount = CCTestUtils.createAccount('Aperture Labs',  true);

        // Create Buyer Account
        BuyerAccount testBuyerAccount = CCTestUtils.createBuyerAccount('Aperture Labs Buyer Account', testAccount.Id, true);

        // Create Buyer Group
        BuyerGroup testBuyerGroup = CCTestUtils.createBuyerGroup('Test Buyer Group', true);

        // Create Buyer Group Member
        BuyerGroupMember testBuyerGroupMember = CCTestUtils.createBuyerGroupMember(testAccount.Id, testBuyerGroup.Id, true);

        // Create Strikethrough Pricebook
        Pricebook2 testStrikethroughPricebook = CCTestUtils.createPriceBook(true, 'Test Strikethrough Pricebook', false, 'USD', true, true);

        // Create Discounted Pricebook
        Pricebook2 testDiscountedPricebook = CCTestUtils.createPriceBook(true, 'Test Discounted Pricebook', false, 'USD', true, true);

        // Create Buyer Group Pricebook
        BuyerGroupPricebook testBuyerGroupPricebook = CCTestUtils.createBuyerGroupPricebook(testDiscountedPricebook.Id, testBuyerGroup.Id, true);

        // Create EntitlementPolicy
        CommerceEntitlementPolicy testPolicy = CCTestUtils.createEntitlementPolicy('Test Entitlement Policy', true);

        // Create Test Contact
        Contact testContact = CCTestUtils.createContact(testAccount, true);

        // Create Test Community User
        User commUser = CCTestUtils.createTestUser(testContact, true);

        // Create Catalog
        ProductCatalog testProductCatalog = CCTestUtils.createProductCatalog('Apex Test Catalog', true);

        // Create list of product grid fields
        List<String> productGridFields = new List<String>{'Rod_Butt__c', 'Bait_Length__c', 'Reel_Size__c' };

        // Create Product Category
        ProductCategory testProductCategory = CCTestUtils.createProductCategory('Apex Test Category', testProductCatalog, null, productGridFields, true );

        // Create Product2 Master Record and related Variant records
        Product2 masterVariantProduct = CCTestUtils.createMasterVariantProduct('Test Master Product 1', '1101', 'abc123', testPolicy.Id, testProductCategory.Id, true);

        // Create Test Store
        WebStore testStore = CCTestUtils.createWebStore('Apex Test Store', testStrikethroughPricebook.Id, true);

        // Create Test Cart
        WebCart testCart = CCTestUtils.createWebCart('Apex Test Cart', commUser, testStore, testAccount, true);

        // Create Test Cart Delivery Group
        CartDeliveryGroup testDelivGroup = CCTestUtils.createCartDeliveryGroup('Apex Test Cart Delivery Group', testCart, true);

        // Setup test Suggested Order record with Suggested Order Items
        Suggested_Order__c testSuggestedOrder = CCTestUtils.createSuggestedOrder('Apex Test Suggested Order', testAccount, testStore, true);


        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertNotEquals(null, variantProducts, 'Needed records to be returned here.');

        List<Suggested_Order_Item__c> testSuggestedOrderItems = new List<Suggested_Order_Item__c>();
        for(Integer i = 0; i < 2; i++) {
            testSuggestedOrderItems.add(
                    CCTestUtils.createSuggestedOrderItem(variantProducts[i].Product, testSuggestedOrder, false)
            );
        }
        insert testSuggestedOrderItems;
    }

    @IsTest
    static void deleteSuggestedOrder_test() {
        // Get Comm User
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Suggested_Order__c testOrder = [SELECT Id FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];

        System.runAs(commUser) {
            String communityId = [SELECT NetworkId FROM NetworkMember WHERE MemberId =: commUser.Id LIMIT 1].NetworkId;
            Test.startTest();
            ConnectAPIUtils.deleteSuggestedOrder(communityId, testOrder.Id);
            try{
                testOrder = [SELECT Id FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];
            }
            catch(QueryException ex) {
                System.assertEquals('System.QueryException', ex.getTypeName(), 'Should have produced a QueryException due to no rows being selected.');
                System.assertEquals(true, ex.getMessage() == 'List has no rows for assignment to SObject', 'Should have returned an error stating no rows were returned.');
            }
            Test.stopTest();
        }
    }

    @IsTest
    static void getSuggestedOrders_Test() {
        // Get Comm User
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];

        System.runAs(commUser) {
            String communityId = [SELECT NetworkId FROM NetworkMember WHERE MemberId =: UserInfo.getUserId() LIMIT 1].NetworkId;
            Test.startTest();
            List<ConnectAPIUtils.SuggestedOrderOption> result = ConnectAPIUtils.getSuggestedOrders(communityId, testStore.Id, testAccount.Id);
            Test.stopTest();
            System.assertEquals(1, result.size(), 'Should have returned 1 Suggested Order Option record.');
        }
    }

    @IsTest
    static void getAllSuggestedOrders_Test() {
        // Get Comm User
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];

        System.runAs(commUser) {
            String communityId = [SELECT NetworkId FROM NetworkMember Where MemberId = :commUser.Id LIMIT 1].NetworkId;
            Test.startTest();
            List<Suggested_Order__c> result = ConnectAPIUtils.getAllSuggestedOrders(communityId, testAccount.Id);
            System.assertEquals(1, result.size(), 'Should have returned 1 Suggested Order record');
            Test.stopTest();
        }
    }

    @IsTest
    static void createSuggestedOrder_Test() {
        // Get Comm User
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];

        // Create Test Data
        Map<String,Decimal> testDataMap = new Map<String,Decimal>();
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        String stringifiedData = '[';
        for (Integer i = 0; i < 3; i++ ) {
            stringifiedData += '{ "productId":"' + variantProducts.get(i).ProductId + '","quantity":"' + Decimal.valueOf(i) + '"}';
            if (i != 2) {
                stringifiedData += ',';
            }
        }
        stringifiedData += ' ]';

        Test.startTest();
        System.runAs(commUser) {
            Suggested_Order__c result = ConnectAPIUtils.createSuggestedOrder('Matt Test Suggested Order', testStore.Id, testAccount.Id, stringifiedData, Date.today());
            System.assertNotEquals(null, result, 'Should have returned the newly created Suggested Order record.');
        }
        Test.stopTest();
    }

    @IsTest
    static void addToSuggestedOrder_Test() {
        // Get Comm User
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Suggested_Order__c testOrder = [SELECT Id FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];

        // Create Test Data
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        String stringifiedData = '[';
        for (Integer i = 0; i < 3; i++ ) {
            if (i==2){
                stringifiedData += '{ "productId":"' + variantProducts.get(i).ProductId + '","quantity":"' + Decimal.valueOf(i) + '"}';
           }
        }
        stringifiedData += ' ]';

        System.runAs(commUser) {
            Test.startTest();
            Suggested_Order_Item__c result = ConnectAPIUtils.addToSuggestedOrder(testStore.Id, testAccount.Id, testOrder.Id, stringifiedData);
            System.assertNotEquals(null, result, 'Should have returned the new Suggested Order Item record.');
            Test.stopTest();
        }
    }

    @IsTest
    static void addToSuggestedOrder_Test2() {
        // Get Comm User
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Suggested_Order__c testOrder = [SELECT Id FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];

        // Create Test Data
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        String stringifiedData = '[';
        for (Integer i = 0; i < 3; i++ ) {
            if (i == 1) {
                stringifiedData += '{ "productId":"' + variantProducts.get(i).ProductId + '","quantity":"' + Decimal.valueOf(i) + '"},';
            }
            if (i==2){
                stringifiedData += '{ "productId":"' + variantProducts.get(i).ProductId + '","quantity":"' + Decimal.valueOf(i) + '"}';
            }
        }
        stringifiedData += ' ]';

        System.runAs(commUser) {
            Test.startTest();
            Suggested_Order_Item__c result = ConnectAPIUtils.addToSuggestedOrder(testStore.Id, testAccount.Id, testOrder.Id, stringifiedData);
            System.assertNotEquals(null, result, 'Should have returned the new Suggested Order Item record.');
            Test.stopTest();
        }
    }

    @IsTest
    static void getSuggestedOrder_Test() {
        // Get Comm User
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Suggested_Order__c testOrder = [SELECT Id FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];

        String productFields = 'Name,Hero_Image__c,Model_Number__c,Material__c,Case_Quantity__c,Product_Type__c';

        System.runAs(commUser){
            String communityId = [SELECT NetworkId FROM NetworkMember Where MemberId = :commUser.Id LIMIT 1].NetworkId;
            Test.startTest();
            ConnectAPIUtils.SuggestedOrderWrapper result = ConnectApiUtils.getSuggestedOrder(communityId, testAccount.Id, testOrder.Id, productFields);
            System.assertNotEquals(null, result, 'Should have returned a SuggestedOrderWrapper record.');
            Test.stopTest();
        }

    }

    @IsTest
    static void updateCartItem_Test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        CartDeliveryGroup cdg = [SELECT Id FROM CartDeliveryGroup WHERE Name = 'Apex Test Cart Delivery Group' LIMIT 1];

        CartItem testItem = CCTestUtils.createCartItem(variantProducts[0].Product, testCart, cdg, true);

        Test.startTest();
        System.runAs(commUser) {
            String result = ConnectAPIUtils.updateCartItem(testItem.Id, 55.0);
            CartItem updatedRecord = [SELECT Id, Quantity FROM CartItem WHERE Id =: result LIMIT 1];
            System.assertNotEquals(null, result, 'Should have returned the updated CartItemId');
            System.assertEquals(55.0, updatedRecord.Quantity, 'Unexpected Quantity value returned from CartItem');
        }
        Test.stopTest();
    }

    @IsTest
    static void updateOrderItemQuantity_Test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Suggested_Order__c testOrder = [SELECT Id, (SELECT Id, Quantity__c FROM Suggested_Order_Items__r) FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];
        System.assertEquals(2, testOrder.Suggested_Order_Items__r.size(), 'Should have 2 Suggested Order Items related');

        Suggested_Order_Item__c testOrderItem = testOrder.Suggested_Order_Items__r[0];


        System.runAs(commUser) {
            Test.startTest();
            Decimal originalQty = testOrderItem.Quantity__c;
            String result = ConnectAPIUtils.updateOrderItemQuantity(testOrderItem.Id, 55.0);
            Test.stopTest();
        }
    }

    @IsTest
    static void deleteSuggestedOrderItem_Test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        Suggested_Order__c testOrder = [SELECT Id FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');
        Suggested_Order_Item__c testItem = CCTestUtils.createSuggestedOrderItem(variantProducts[0].Product, testOrder, true );


        Test.startTest();
        System.runAs(commUser) {
            String communityId = [SELECT NetworkId FROM NetworkMember Where MemberId = :commUser.Id LIMIT 1].NetworkId;
            String result = ConnectAPIUtils.deleteSuggestedOrderItem(communityId, testAccount.Id, testOrder.Id, testItem.Id);
            try{
                Suggested_Order_Item__c deletedRecord = [SELECT Id FROM Suggested_Order_Item__c WHERE Id =: result LIMIT 1];
            }
            catch(Exception ex){
                System.assertEquals('System.QueryException', ex.getTypeName(), 'Should have thrown a QueryException');
            }
        }
        Test.stopTest();
    }

    @IsTest
    static void getMapMsrpPrices_Test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertNotEquals(null, variantProducts, 'Needed records to be returned here.');
        WebStore testStore = [SELECT Id FROM WebStore WHERE Name = 'Apex Test Store' LIMIT 1];

        // Build Company Product Info records so we can specify specific MAP/MSRP prices and assert that the code is gathering data from the correct source
        List<Company_Product_Info__c> prodInfos = new List<Company_Product_Info__c>();
        Company_Product_Info__c testInfo =  new Company_Product_Info__c (
                MAP__c = 59.99,
                MSRP__c = 49.99,
                SAP_Company__c = 'apex_test',
                Related_Product__c = variantProducts[0].Product.Id
        );
        prodInfos.add(testInfo);

        Company_Product_Info__c testInfo2 = new Company_Product_Info__c(
                MAP__c = 2.99,
                MSRP__c = 3.99,
                SAP_Company__c = 'apex_test',
                Related_Product__c = variantProducts[1].Product.Id
        );
        prodInfos.add(testInfo2);

        insert prodInfos;




        System.runAs(commUser) {
            String communityId = [SELECT NetworkId FROM NetworkMember WHERE MemberId =: commUser.Id LIMIT 1].NetworkId;
            Map<String,ConnectAPIUtils.PriceWrapper> priceMap = new Map<String,ConnectAPIUtils.PriceWrapper>();
            String longId = variantProducts[1].Product.Id;
            String shortId = longId.substring(0,15); // Get 15-character Id, since that is what ConnectAPI deals with
            priceMap.put(shortId, new ConnectAPIUtils.PriceWrapper(shortId, '2.99','3.99','10.99','14.99'));
            Test.startTest();
            ConnectAPIUtils.getMapMsrpPrices(testStore.Id, new List<String>{ variantProducts[0].Product.Id, variantProducts[1].Product.Id }, priceMap);
            System.assertNotEquals(0, priceMap.size(), 'Expected data to be populated in the map sent');


            Test.stopTest();
        }
    }

    @IsTest
    static void resolveSortParam_Test() {
        ConnectApi.CartItemSortOrder result = ConnectAPIUtils.resolveSortParam('test');
    }

    @IsTest
    static void getCartItems_Test() {
        User commUser = [SELECT Id FROM User WHERE Username = 'fullname@domain.com.b2b' LIMIT 1];
        WebCart testCart = [SELECT Id FROM WebCart WHERE Name = 'Apex Test Cart' LIMIT 1];

        Account testAccount = [SELECT Id FROM Account WHERE Name = 'Aperture Labs' LIMIT 1];
        Product2 masterProduct = [SELECT Id FROM Product2 WHERE Name = 'Test Master Product 1' LIMIT 1];
        Suggested_Order__c testOrder = [SELECT Id FROM Suggested_Order__c WHERE Name = 'Apex Test Suggested Order' LIMIT 1];
        List<ProductAttribute> variantProducts = [SELECT Id, Product.Material__c, Product.StockKeepingUnit, Product.Model_Number__c, Product.Name FROM ProductAttribute WHERE VariantParentId =: masterProduct.Id];
        System.assertEquals(3, variantProducts.size(), 'Expected 3 Variation Product records to be returned.');

        System.runAs(commUser) {
            Test.startTest();
            String communityId = [SELECT NetworkId FROM NetworkMember Where MemberId = :commUser.Id LIMIT 1].NetworkId;
            ConnectApi.CartItemCollection result = ConnectAPIUtils.getCartItems(communityId, testAccount.Id, testCart.Id, '', '',0,'');
            System.assertNotEquals(null, result);
            Test.stopTest();
        }
    }

    @IsTest
    static void priceWrapperConst_Test() {
        ConnectAPIUtils.PriceWrapper pw = new ConnectAPIUtils.PriceWrapper('123', '10.00', '15.00', '20.00', '25.00');
        System.assertEquals('123', pw.productId);
    }

    @IsTest
    static void removeCharacters_Test() {
        String testString = 'This has a tab character.\t And a \n character.';
        Test.startTest();
        List<String> removedPatterns = new List<String>{'\t', '\n'};
        String result = ConnectAPIUtils.removeCharacters(removedPatterns, testString);
        System.assertEquals(-1, result.indexOf('\t'), 'Expected Tab character to be removed.');
        System.assertEquals(-1, result.indexOf('\n'), 'Expected New Line character to be removed.');
        Test.stopTest();
    }
}