/**
 * @description Handler class for the CartItem Trigger. All custom logic lives here.
 */
/* Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           5/19/2022       Initial version
    1.1         Matt Parrella           Coastal Cloud           6/01/2022       Adding method to check for spare parts in cart
    1.2         Matt Parrella           Coastal Cloud           9/07/2022       Implementing custom business logic for Plano product line
    1.3         Matt Parrella           Coastal Cloud           10/13/2022      Removing Plano cart minimum
	1.4			Matt Parrella			Coastal Cloud			10/24/2022		TSK-00233576: Fixing issue with 'stuck' CVO records
    1.5         Arsee Khan              Coastal Cloud           03/07/2023      TSK-TSK-00243337:Plano Canada support
 */

// made 'without' sharing due to accessing the Customer_group__r.Min_Order__c field value
public without sharing class CartItemTriggerHandler {

    /* The following class vars represent values within the Product2.Product_Type__c field */
    private static final String SPARE_PARTS_LABEL = 'Spare Part';
    private static final String STANDARD_PRODUCT_LABEL = 'Pure Fishing Legacy';
    private static final String PLANO_PRODUCT_LABEL = 'Plano Legacy';

    private static final Decimal PLANO_MINIMUM_TOTAL = 500.00;

    private static final String ORDER_REASON_CODE_PRODUCTS = '100'; // When cart contains only 'Standard Products' (non spare-part/Plano) product types
    private static final String ORDER_REASON_CODE_SPARE_PARTS = '125'; // When cart contains only 'Spare Parts' product types
    private static final String ORDER_REASON_CODE_PLANO = '100'; // When cart contains only 'Plano' product types (created it's own var, in case this changes in the future)

    /* Trigger Context Logic: START */
    /**
     * @description Method to execute logic on CartItem records AFTER they have been committed to the Database
     *
     * @param newMap A Map of CartItems with the key being the 18-character Salesforce record Id and the value being the CartItem record itself. This map
     *               contains all the latest changes
     */
    public static void handleAfterInsert(Map<Id,CartItem> newMap) {
        checkCasePackQuantity(newMap);
        handleMixedProductsInCart(newMap);
        checkCartTotalMinimum(newMap, null);
        setCartOrderReasonField(newMap);
        setCartDistributionChannel(newMap);
    }
    /**
     * @description Method to execute logic on CartItem records after modifications but before committing the modifications to the Database
     *
     * @param newMap A Map of CartItems with the key being the 18-character Salesforce record Id and the value being the CartItem record itself. This map
     *               contains all the latest changes
     * @param oldMap A Map of CartItems with the key being the 18-character Salesforce record Id and the value being the CartItem record itself. This map
     *               contains the old version of the CartItem record - before any modifications were made
     */
    public static void handleBeforeUpdate(Map<Id,CartItem> newMap, Map<Id,CartItem> oldMap) {
        handleMixedProductsInCart(newMap);
        checkCartTotalMinimum(newMap, oldMap);
    }

    /**
     * @description Method to execute logic on CartItem records after modifications have been committed to the Database
     *
     * @param newMap A Map of CartItems with the key being the 18-character Salesforce record Id and the value being the CartItem record itself. This map
     *               contains all the latest changes
     * @param oldMap A Map of CartItems with the key being the 18-character Salesforce record Id and the value being the CartItem record itself. This map
     *               contains the old version of the CartItem record - after any modifications were made
     */
    public static void handleAfterUpdate(Map<Id,CartItem> newMap, Map<Id, CartItem> oldMap) {
        checkCasePackQuantity(newMap);
    }

    /**
     * @description Method to execute logic on CartItem records after they are fully deleted/removed from the database
     *
     * @param deletedRecords The CartItem records that were deleted
     */
    public static void handleAfterDelete(List<CartItem> deletedRecords) {
        // Build map to send to various methods
        Map<Id,CartItem> oldMap = new Map<Id,CartItem>();
        for (CartItem item : deletedRecords) {
            oldMap.put(item.Id, item);
        }
        checkCasePackQuantity(oldMap); // MParrella | 10-24-2022 | TSK-00233576: making call to check case pack quantity CVOs when deleting an item
        handleMixedProductsInCart(oldMap);
        checkCartTotalMinimum(oldMap, null);
        setCartOrderReasonField(oldMap);
        setCartDistributionChannel(oldMap);
    }
    /* Trigger Context Logic: STOP */

    /* CUSTOM METHOD LOGIC: START */

    /**
     * @description Method to check the that the Quantity field value is a multiple of the 'Case_Pack__c' field. If it is not,
     *              a CartValidationOutput record is created and associated to the CartItem record. This CartValidationOutput is then
     *              displayed within the Cart component
     *
     * @param records (Map<Id,CartItem>) A Map containing CartItem objects (key: Record Id value: CartItem record)
    */
    public static void checkCasePackQuantity(Map<Id,CartItem> records) {
        List<CartValidationOutput> validationRecordsToInsert = new List<CartValidationOutput>();
        List<CartValidationOutput> validationRecordsToDelete = new List<CartValidationOutput>();
        Set<Id> cartItemIds = new Set<Id>();
        Set<Id> cartIds = new Set<Id>();

        // Build Map of Product2Id -> CartItem record
        Map<Id,CartItem> productCartItemMap = new Map<Id,CartItem>();
        for (CartItem currItem : records.values()) {
            if (currItem.Type == 'product'){
                productCartItemMap.put(currItem.Product2Id, currItem);
            }
        }

        String cartId;
        if (!records.isEmpty()){
            cartId = records.values().get(0).CartId;
        }
        // Iterate over related Product2 records and check that the CartItem's Quantity value is a multiple of the Product2's 'Case_Quantity__c' value
        List<Product2> productsList = [SELECT Id, Case_Quantity__c FROM Product2 WHERE Id IN :productCartItemMap.keySet()];
        for (Product2 currProduct : productsList ){
            Integer cartQuantity = (Integer) productCartItemMap.get(currProduct.Id).Quantity;
            Integer casePackValue = currProduct.Case_Quantity__c != null ? (Integer) currProduct.Case_Quantity__c : 1;
            // MParrella | 10-24-2022 | TSK-00233576 | Adding logic to remove CVO records when in a delete context: START
            // If we're in a Delete context, just remove the associated CartValidationOutput records (if any)
            if (Trigger.isDelete) {
                cartItemIds.add(productCartItemMap.get(currProduct.Id)?.Id);
            } else {
                if (Math.mod(cartQuantity, casePackValue) != 0) {
                    validationRecordsToInsert.add(
                            new CartValidationOutput(
                                    CartId = productCartItemMap.get(currProduct.Id).CartId,
                                    Level = 'Error',
                                    Message = Label.cc_cart_error_quantity_error + ' ' + casePackValue,
                                    RelatedEntityId = productCartItemMap.get(currProduct.Id).Id,
                                    Name = 'CARTITEM_QUANTITY_ERROR_ ' + (String) productCartItemMap.get(currProduct.Id).Id,
                                    Type = 'Other'
                            )
                    );
                    // Add Cart-Level error also
                    validationRecordsToInsert.add(
                            new CartValidationOutput(
                                    CartId = cartId,
                                    Level = 'Error',
                                    Message = Label.cc_cart_error_cart_quantity_error,
                                    RelatedEntityId = cartId,
                                    Name = 'CART_QUANTITY_ERROR_ ' + (String) productCartItemMap.get(currProduct.Id).Id,
                                    Type = 'Other'
                            )
                    );
                } else {
                    // Add CartItem Id to a list for querying later (will delete the related CartValidationOutput records)
                    cartItemIds.add(productCartItemMap.get(currProduct.Id).Id);
                    cartIds.add(cartId);
                }
            }
            // MParrella | 10-24-2022 | TSK-00233576 | Adding logic to remove CVO records when in a delete context: STOP
        }

        // Attempt to retrieve CartValidationOutput records whose Id exists in the 'cartItemIds' Set
        if (!cartItemIds.isEmpty() || !cartIds.isEmpty()) {
            List<String> cvoNamesToDelete = new List<String>();
            for (String cartItemId : cartItemIds) {
                cvoNamesToDelete.add(
                        'CART_QUANTITY_ERROR_ ' + cartItemId
                );
            }
            validationRecordsToDelete = [SELECT Id, Name FROM CartValidationOutput WHERE RelatedEntityId IN :cartItemIds OR Name IN :cvoNamesToDelete];
        }

        try {
            insert validationRecordsToInsert;
            delete validationRecordsToDelete;
        }
        catch(DmlException ex) {
            System.debug('*** CartItemTriggerHandler.checkCasePackQuantity() ERROR: ' + ex.getMessage());
        }
    }

    /**
     * @description This method will check the current cart total value to ensure that it is greater or equal to the
     *              value specified on the 'Min_Order__c' field on the Customer Group record that is associated with
     *              the running user's Effective Account. If the current total is < the Min_Order__c field value, a CartValidationOutput
     *              record will be created and assigned to the cart for displaying within the community on the Cart page
     *
     *  @param records (Map<Id,CartItem>) A Map of CartItem records that have the latest modifications
     *  @param oldRecords (Map<Id,CartItem>) A Map of CartItem records have do not have the latest modifications
     */
    public static void checkCartTotalMinimum(Map<Id,CartItem> records, Map<Id,CartItem> oldRecords) {
        List<CartValidationOutput> validationOutputsToInsert = new List<CartValidationOutput>();
        List<CartValidationOutput> validationOutputsToDelete = new List<CartValidationOutput>();

        String cartId;
        if (!records.isEmpty()){
            cartId = records.values().get(0).CartId;
        }

        // Get Min_Order__c field value from the Account.Customer_Group__r record (updated SM 7/8/22)
        Account effectiveAccount = [SELECT Customer_Group__r.Min_Order__c, CurrencyIsoCode FROM Account WHERE Id IN (SELECT AccountId FROM WebCart WHERE Id =: cartId) LIMIT 1];
        Decimal minimumOrderAmt = effectiveAccount.Customer_Group__r.Min_Order__c;

        // Get the TotalAmount change for the CartItem
        Decimal priceDelta = 0.00;
        if (oldRecords != null){
            priceDelta = records.values()[0].TotalAmount - oldRecords.get(records.values()[0].Id).TotalAmount;
        }

        // Calculate the new Cart Grand Total (since we're in a before update, we won't have the latest data)
        Decimal cartGrandTotal = 0.00;
        List<CartItem> cartItems = [SELECT Id, TotalAmount FROM CartItem WHERE CartId =: cartId];
        for (CartItem ci : cartItems) {
            cartGrandTotal += ci.TotalAmount;
        }

        Decimal newTotal = 0.00;
        if (cartGrandTotal != null) {
            newTotal = cartGrandTotal + priceDelta;
        } else {
            cartGrandTotal = 0.00;
        }

        Set<String> productTypesInCart = getCartProductTypes(records);

        // Determine what types of products the cart is comprised of
        Boolean hasSpareParts = productTypesInCart.contains(SPARE_PARTS_LABEL);
        Boolean hasProducts = productTypesInCart.contains(STANDARD_PRODUCT_LABEL);
        Boolean hasPlano = productTypesInCart.contains(PLANO_PRODUCT_LABEL);

        // Scenario: Total is less than minimum required; No spare parts in cart; Standard products in cart; no Plano products in cart
        if ((newTotal < minimumOrderAmt) && !hasSpareParts && hasProducts && !hasPlano) {
            validationOutputsToInsert.add(
                    new CartValidationOutput(
                            CartId = cartId,
                            Level = 'Error',
                            Message = Label.cc_cart_error_below_minimum + ' ' + minimumOrderAmt + ' ' + effectiveAccount.CurrencyIsoCode,
                            RelatedEntityId = cartId,
                            Name = 'AMOUNT_ERROR ' + (String) cartId + Math.random(),
                            Type = 'Other'
                    )
            );
        }
        // Scenario: Total is less than minimum required; No standard products in cart; No spare parts in cart; Only Plano products in cart
            // MParrella | 10-13-2022 | TSK-00231931 | Removing Plano products minimum (using the Account-based minimum now)
        else if(!hasProducts && !hasSpareParts && (newTotal < minimumOrderAmt) && hasPlano) {
            validationOutputsToInsert.add(
                    new CartValidationOutput(
                            CartId = cartId,
                            Level = 'Error',
                            Message = Label.cc_cart_error_below_minimum + ' ' + minimumOrderAmt + ' ' + effectiveAccount.CurrencyIsoCode,
                            RelatedEntityId = cartId,
                            Name = 'PLANO_AMOUNT_ERROR ' + (String) cartId + Math.random(),
                            Type = 'Other'
                    )
            );
        }
        // Scenario: Total is >= the minimum required; No standard products; No spare parts in cart; ONly Plano products in cart
        else if (!hasProducts && !hasSpareParts && hasPlano && (newTotal >= minimumOrderAmt)) {
            validationOutputsToDelete = [SELECT Id FROM CartValidationOutput WHERE RelatedEntityId =: cartId AND Name LIKE 'PLANO_AMOUNT_ERROR %'];
        }
        // Scenario: Total is greater than or equal to the minimum required; no spare parts in cart; standard products in cart; no Plano products in cart
        else if ((newTotal >= minimumOrderAmt) && !hasSpareParts && hasProducts && !hasPlano) {
            validationOutputsToDelete = [SELECT Id FROM CartValidationOutput WHERE RelatedEntityId =: cartId AND Name LIKE 'AMOUNT_ERROR %'];
        }
        // If no longer have Plano products, clear all Plano-related CVOs
        if (!hasPlano) {
            validationOutputsToDelete.addAll([SELECT Id FROM CartValidationOutput WHERE RelatedEntityId =: cartId AND Name LIKE 'PLANO_AMOUNT_ERROR %']);
        }
        if (!hasProducts) {
            validationOutputsToDelete.addAll([SELECT Id FROM CartValidationOutput WHERE RelatedEntityId =: cartId AND Name LIKE 'AMOUNT_ERROR %']);
        }

        try {
            insert validationOutputsToInsert;
            delete validationOutputsToDelete;
        }
        catch(DmlException ex) {
            System.debug('*** CartItemTriggerHandler.checkCartTotalMinimum() ERROR: ' + ex.getMessage());
        }
    }

    public static Set<String> getCartProductTypes(Map<Id,CartItem> newRecords) {
        String cartId;
        Set<String> productTypesInCart = new Set<String>();
        if (!newRecords.isEmpty()){
            cartId = newRecords.values().get(0).CartId;
        }
        // Iterate over CartItem objects and see if we have both products and spare parts
        List<AggregateResult> cartItems = [SELECT Product2.Spare_Part__c, Product2.Plano_legacy__c FROM CartItem WHERE CartId =: cartId AND Type = 'Product' GROUP BY Product2.Spare_Part__c, Product2.Plano_legacy__c];
        if (!cartItems.isEmpty()) {
            for (AggregateResult res : cartItems) {
                if (res.get('Spare_Part__c') == true) {
                    productTypesInCart.add(SPARE_PARTS_LABEL);
                } else if (res.get('Plano_Legacy__c') == true) {
                    productTypesInCart.add(PLANO_PRODUCT_LABEL);
                } else {
                    productTypesInCart.add(STANDARD_PRODUCT_LABEL);
                }
            }
        }
        return productTypesInCart;
    }

    public static void handleMixedProductsInCart(Map<Id,CartItem> records) {
        List<CartValidationOutput> cvoToInsert = new List<CartValidationOutput>();
        List<CartValidationOutput> cvoToDelete = new List<CartValidationOutput>();

        String cartId;
        if (!records.isEmpty()){
            cartId = records.values().get(0).CartId;
        }
        //Arsee:1.5
        WebCart actId=[Select AccountId from WebCart where Id = : cartId];//Arsee:1.5
        Account ActInfos = [Select Distribution_Channel__c,Sales_Org__c  from Account where Id = : actId.AccountId];//Arsee:1.5
        String ActDistChannel=ActInfos.Sales_Org__c;//Arsee:1.5

        Set<String> productTypesInCart = getCartProductTypes(records);

        Boolean hasProducts = productTypesInCart.contains(STANDARD_PRODUCT_LABEL);
        Boolean hasSpareParts = productTypesInCart.contains(SPARE_PARTS_LABEL);
        Boolean hasPlanoParts = productTypesInCart.contains(PLANO_PRODUCT_LABEL);


        // Scenario:  Cart contains: Standard Products, Spare Parts, Plano Products
        if (hasProducts && hasSpareParts && hasPlanoParts) {
            // Add a Cart Validation Output record to let the user know the Cart cannot have mixed product types (parts + products + plano parts)
            cvoToInsert.add(
                    new CartValidationOutput(
                            CartId = cartId,
                            Level = 'Error',
                            Message = Label.cc_cart_error_mixed_products,
                            RelatedEntityId = cartId,
                            Name = 'MIXED_PRODUCT_ERROR ' + (String)cartId,
                            Type = 'Other'
                    )
            );
        // Scenario: Cart Contains: Standard Products, Spare Parts
        } else if (hasProducts && hasSpareParts && !hasPlanoParts) {
            // Add a Cart Validation Output record to let the user know the Cart cannot have mixed product types (parts + products)
            cvoToInsert.add(
                    new CartValidationOutput(
                            CartId = cartId,
                            Level = 'Error',
                            Message = Label.cc_cart_error_mixed_products2,
                            RelatedEntityId = cartId,
                            Name = 'MIXED_PRODUCT_ERROR ' + (String)cartId,
                            Type = 'Other'
                    )
            );
        // Scenario: Cart Contains: Standard Products, Plano Products
        } else if (hasProducts && hasPlanoParts && !hasSpareParts && ActDistChannel!='0040') {//Arsee:1.5
            cvoToInsert.add(
                    new CartValidationOutput(
                            CartId = cartId,
                            Level = 'Error',
                            Message = Label.cc_cart_error_mixed_products3,
                            RelatedEntityId = cartId,
                            Name = 'MIXED_PRODUCT_ERROR ' + (String)cartId,
                            Type = 'Other'
                    )
            );
        // Scenario: Cart Contains: Plano Products, Spare Parts
        } else if (!hasProducts && hasPlanoParts && hasSpareParts){
            cvoToInsert.add(
                    new CartValidationOutput(
                            CartId = cartId,
                            Level = 'Error',
                            Message = Label.cc_cart_error_mixed_products4,
                            RelatedEntityId = cartId,
                            Name = 'MIXED_PRODUCT_ERROR ' + (String)cartId,
                            Type = 'Other'
                    )
            );
        } else {
            cvoToDelete = [SELECT Id FROM CartValidationOutput WHERE RelatedEntityId =: cartId AND Name LIKE 'MIXED_PRODUCT_ERROR %'];
        }

        try {
            insert cvoToInsert;
            delete cvoToDelete;
        }
        Catch(DmlException ex) {
            System.debug('*** CartItemTriggerHandler.handleMixedProductsInCart() ERROR: ' + ex.getMessage());
        }
    }

    /**
     * @description This method will check all CartItem records within the Cart to see if the Cart contains only
     *              Product items, or only Spare Part items. Based on this, it will populate the 'Cart_Order_Reason__c'
     *              field as follows:
     *
     *              '100' => Cart contains only Products
     *              '125' => Cart contains only Spare Parts
     *              '999' => [ERROR] Cart contains both Products and Spare Parts (this value should never be present when Cart makes it to Checkout process)
     *
     * @param records A map of new CartItem records
     */
    public static void setCartOrderReasonField(Map<Id,CartItem> records) {
        List<WebCart> cartsToUpdate = new List<WebCart>();

        // Get all CartItem records related to the cart
        String cartId;
        if (!records.isEmpty()){
            cartId = records.values().get(0).CartId;
        }

        Boolean hasProducts = false;
        Boolean hasParts = false;
        Boolean hasPlano = false;
        String orderReasonCode = '999';

        if (cartId != null) {
            Set<String> productTypesInCart = getCartProductTypes(records);
            hasProducts = productTypesInCart.contains(STANDARD_PRODUCT_LABEL);
            hasParts = productTypesInCart.contains(SPARE_PARTS_LABEL);
            hasPlano = productTypesInCart.contains(PLANO_PRODUCT_LABEL);

            // Scenario: Only standard products in cart
            if (hasProducts && !hasParts && !hasPlano) {
                orderReasonCode = ORDER_REASON_CODE_PRODUCTS;
            }
            // Scenario: Only spare parts products in cart
            else if (!hasProducts && hasParts && !hasPlano) {
                orderReasonCode = ORDER_REASON_CODE_SPARE_PARTS;
            }
            // Scenario: Only Plano products in cart
            else if (!hasProducts && !hasParts && hasPlano) {
                orderReasonCode = ORDER_REASON_CODE_PLANO;
            }

            WebCart newRecord = new WebCart(
                    Id = cartId,
                    Cart_Order_Reason__c = orderReasonCode
            );

            try {
                update newRecord;
            }
            catch(DmlException ex) {
                System.debug('*** CartItemTriggerHandler.setCartOrderReasonField() ERROR: ' + ex.getMessage());
            }
        }
    }

    /**
     * @description This method will populate the 'Distribution_Channel__c' field on the WebCart record, depending on the type of products contained within
     *              the cart.
     *
     *              Cart contains All Standard Products OR All Spare Parts => Distribution_Channel__c = '00'
     *              Cart contains All 'Plano' Products => Distribution_Channel__c = '08'
     *
     * @param records A map of new CartItem records
     */
    public static void setCartDistributionChannel(Map<Id,CartItem> records) {
        String cartId;
        if (!records.isEmpty()) {
            cartId = records.values().get(0).cartId;
        }
        WebCart actId=[Select AccountId from WebCart where Id = : cartId];//Arsee:1.5
        Account ActInfos = [Select Distribution_Channel__c,Sales_Org__c  from Account where Id = : actId.AccountId];//Arsee:1.5
        String distributionValue = '00';
        String ActDistChannel=ActInfos.Sales_Org__c;//Arsee:1.5

        // Get a list of all product types present within the cart currently
        Set<String> cartProductTypes = getCartProductTypes(records);
        Boolean hasProducts = cartProductTypes.contains(STANDARD_PRODUCT_LABEL);
        Boolean hasSpareParts = cartProductTypes.contains(SPARE_PARTS_LABEL);
        Boolean hasPlanoParts = cartProductTypes.contains(PLANO_PRODUCT_LABEL);

        // Scenario: Cart only contains Spare Part Products
        if (!hasProducts && hasSpareParts && !hasPlanoParts) {
            distributionValue = '00';
        }
        // Scenario: Cart only contains Plano Products
        else if (!hasProducts && !hasSpareParts && hasPlanoParts && ActDistChannel=='0020') //Arsee:1.5
        {
            distributionValue = '08';
        }
        else if (!hasProducts && !hasSpareParts && hasPlanoParts ) {//Arsee:1.5
            distributionValue = ActDistChannel;
        }

        WebCart tempCart = new WebCart(
                Id = cartId,
                Distribution_Channel__c = distributionValue
        );

        try {
            update tempCart;
        }
        catch(DmlException ex) {
            System.debug('*** CartItemTriggerHandler.setCartDistributionChannel() ERROR: ' + ex.getMessage());
        }



    }

    /* CUSTOM METHOD LOGIC: STOP */
}