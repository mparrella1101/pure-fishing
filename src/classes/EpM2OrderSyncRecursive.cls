public class EpM2OrderSyncRecursive implements Queueable {
    private String lastSalesOrderSyncTime{get;Set;}
    private String now_sync_time{get;Set;}
    private Double pageSize{get;Set;}
    private Double currentPage{get;Set;}
    private String token{get;Set;}
    private String eComStoreID{get;Set;}
    private String baseURL{get;Set;}
    private String supportEmail{get;Set;}
    /*
     * Constructor
     */
    public EpM2OrderSyncRecursive(String now_sync_time, Double pageSize, Double currentPage, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail){
        this.now_sync_time = now_sync_time;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.token = token;
        this.eComStoreID = eComStoreID;
        this.lastSalesOrderSyncTime = lastSalesOrderSyncTime;
        this.baseURL = baseURL;
        this.supportEmail = supportEmail;
    }
    
    public void execute(QueueableContext context) {
		EpM2Sync.getSalesOrdersCreatedAfter(now_sync_time, pageSize, currentPage, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
    } 
}