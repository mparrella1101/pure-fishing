public class ShipmentTrack
{
    public string track_number { get; set; }
    public string title { get; set; }
    public string carrier_code { get; set; }
}