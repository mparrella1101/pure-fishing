public class B2BAuthorizeTokenizedPayment {

    public class B2BAuthorizeTokenizedPaymentRequest {
        @InvocableVariable(required=true)
        public ID cartId;
    }
    
    /**
     * @description Authorizes payment for credit information that was previously tokenized. 
     * @param request The cart id.
     * @return Any errors that occurred.
     */
    @InvocableMethod(callout=true label='Authorize Tokenized Payment' description='Authorizes payment for credit information that was previously tokenized' category='B2B Commerce')
    public static List<String> authorizePaymentInfo(List<B2BAuthorizeTokenizedPaymentRequest> request) {
        String cartId = request[0].cartId;
        List<String> errors = new List<String>();
		System.debug('B2BAuthorizeTokenizedPayment.authorizePaymentInfo');
        try {
            WebCart cart = [SELECT WebStoreId, GrandTotalAmount, AccountId, PaymentMethodId
                            FROM WebCart WHERE Id=:cartId];

            ConnectApi.AuthorizationRequest authRequest = new ConnectApi.AuthorizationRequest();
            
            // Set the tokenized ID
            ConnectApi.AuthApiPaymentMethodRequest authApiPaymentMethodRequest = new ConnectApi.AuthApiPaymentMethodRequest();
            authApiPaymentMethodRequest.Id = cart.PaymentMethodId;
            String orderId =  getOrderId(cartId);
            if (orderId != null && !orderId.equals(''))
            {
                Order Ord=[Select Id,Payment_Method__c,CC_Details__c from Order  where Id=:orderId];
                Ord.Payment_Method__c=cart.PaymentMethodId;
                //Ord.CC_Details__c=String.valueof(authApiPaymentMethodRequest);
                update Ord;
            }
            authRequest.accountId = cart.AccountId;
            authRequest.amount = cart.GrandTotalAmount;
            authRequest.comments = 'Authorizing $' + cart.GrandTotalAmount;
            authRequest.effectiveDate = Datetime.now();
            authRequest.currencyIsoCode = UserInfo.getDefaultCurrency();
            
            // Cannot proceed if paymentGatewayId does not exist
            String paymentGatewayId = getPaymentGatewayId(cart.WebStoreId);
            if (paymentGatewayId == null || paymentGatewayId.equals('')) {
               throw new AuraHandledException('This store is not authorized to process payments.');
            }

            authRequest.paymentGatewayId = paymentGatewayId;
            authRequest.paymentMethod = authApiPaymentMethodRequest;
            authRequest.paymentGroup = getPaymentGroup(cartId);

            // Authorize Payment with Payments API
            ConnectApi.AuthorizationResponse authResponse = ConnectApi.Payments.authorize(authRequest);
            System.debug('authResponse ' + authResponse);
            if (authResponse.error != null || Test.isRunningTest()) {
                String errorMessage = 'AuthResponseError: ' + (Test.isRunningTest() ? '' : authResponse.error.message);
                System.debug(errorMessage);
                errors.add(errorMessage);
            }
        } catch (ConnectApi.ConnectApiException e) {
            System.debug('A Connect Api exception occurred authorizing tokenized payment: ' + e.getMessage());
            errors.add(e.getMessage());
        } catch (Exception e) {
            System.debug('An error occurred authorizing tokenized payment: ' + e.getMessage());
            errors.add(e.getMessage());
        }

        return errors;
    }

    
	@testVisible
	private static ConnectApi.PaymentGroupRequest getPaymentGroup(String cartId) {
            ConnectApi.PaymentGroupRequest paymentGroup = new ConnectApi.PaymentGroupRequest();
        
            paymentGroup.createPaymentGroup = true;
            paymentGroup.currencyIsoCode = UserInfo.getDefaultCurrency();
        
            String orderId = getOrderId(cartId);
            if (orderId != null && !orderId.equals('')) {
                paymentGroup.sourceObjectId = getOrderId(cartId);            
            } else {
                NoDataFoundException e = new NoDataFoundException();
                e.setMessage('No order is associated with this cart');
                throw e;
            }
            return paymentGroup;
    }
    
    private static String getOrderId(String cartId) {
            return (Test.isRunningTest() )? [select id from order LIMIT 1].Id :[SELECT OrderId FROM CartCheckoutSession WHERE WebCartId=:cartId AND IsArchived=false].OrderId;
    }
    
    private static String getPaymentGatewayId(String webStoreId) {
        String paymentGatewayId = null;
        try {
            paymentGatewayId = [SELECT Integration FROM StoreIntegratedService WHERE ServiceProviderType='Payment' AND StoreId=:webStoreId].Integration;
        } catch (Exception e) {
            // For debug logs
            System.debug('Error querying the payment integration from StoreIntegratedService: ' + e.getMessage());
        }
        return paymentGatewayId;
    }
}