/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan               Coastal Cloud           06/02/2022            Initial Version
    1.1         Matt Parrella            Coastal Cloud           10/12/2022            TSK-00231338 : Adding logic to remove tab characters ('\t') from Order_Notes__c field before sending to SAP
    1.2         Matt Parrella            Coastal Cloud           10/17/2022            TSK-00230936 : Applying fix for intermittent 'QueryLocator' error
 */
// This Class is used to sendOrder to SAP.
 global class SendOrdersToSAP implements Database.Batchable<SObject>,   Database.AllowsCallouts,Database.Stateful {
    
    List<OrderItem> UpdOrdItemLst = new List<OrderItem>();
    List<OrderItem> DelOrdItemLst = new List<OrderItem>();
    Set<Id> orderItemsToDeleteIds = new Set<Id>();
    List<Order> Ordup=new List<Order>();
    map<id,Order> ChkOrdup = new map<id,Order>();
    String OrdType='OR';
    List<OrderItemSummary> orderitemSummaryList = new List<OrderItemSummary>();
    String OrdStatus = 'Created';
    String orderProduct = 'Order Product';
    public String query;
    //String OrdId='801R0000001RvImIAK';   // 801R0000001RSUjIAO
    //public String query = 'Select Reason_Code__c,Distribution_Channel__c,Sales_Org__c,EffectiveDate,Account.SAP_Customer__c,Type,PO_Type__c,ShippingFutureDate__c,Placed_By__c,Order_Reference__c,BillingStreet,BillingCity,BillingPostalCode,BillingCountryCode,BillingPhoneNumber,ShippingStreet,ShippingCity,ShippingPostalCode,ShippingCountryCode,ShippingPhone__c,Ship_Cancel_Flag__c,PoNumber,Order_Notes__c,CC_Details__c,account.Division_SAP__c,account.Distribution_Channel__c,account.name,account.BillingStreet,account.BillingCity,account.BillingPostalCode,account.BillingCountryCode,account.phone,Bill_To_Account__c,Bill_To_Name__c,Ship_To_Account__c,Ship_To_Name__c,(SELECT UnitPrice,Product2.Material__c,Quantity,SF_External_ID__c, CurrencyIsoCode,Supplied_Line__c,Type FROM OrderItems) from Order where Id=:OrdId LIMIT 1';
    //public String query = 'Select Reason_Code__c,Distribution_Channel__c,Sales_Org__c,EffectiveDate,Account.SAP_Customer__c,Type,PO_Type__c,ShippingFutureDate__c,Placed_By__c,Order_Reference__c,BillingStreet,BillingCity,BillingPostalCode,BillingCountryCode,BillingPhoneNumber,ShippingStreet,ShippingCity,ShippingPostalCode,ShippingCountryCode,ShippingPhone__c,Ship_Cancel_Flag__c,PoNumber,Order_Notes__c,CC_Details__c,account.Division_SAP__c,account.Distribution_Channel__c,account.name,account.BillingStreet,account.BillingCity,account.BillingPostalCode,account.BillingCountryCode,account.phone,Bill_To_Account__c,Bill_To_Name__c,Ship_To_Account__c,Ship_To_Name__c,(SELECT UnitPrice,Product2.Material__c,Quantity,SF_External_ID__c, CurrencyIsoCode,Supplied_Line__c,Type FROM OrderItems) from Order where Status=:OrdStatus and Order_Reference__c<>null Limit 1';
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Order> records) {
        // MParrella | Coastal Cloud | 10-17-2022 | TSK-00230936 | Separating the OrderItem query out and building a Map of OrderId -> List<OrderItem>: START
        // Get OrderItems in separate query
        List<OrderItem> orderItemList = [SELECT UnitPrice,
                                                 Product2.Material__c,
                                                 Quantity,
                                                 OrderId,
                                                 SF_External_ID__c,
                                                 CurrencyIsoCode,
                                                 Supplied_Line__c,
                                                 Type
                                          FROM   OrderItem
                                          WHERE  OrderId
                                          IN     :records];

        // Build Map of OrderId -> List<OrderItem>
        Map<Id,List<OrderItem>> orderItemMap = new Map<Id,List<OrderItem>>();
        for (OrderItem currLoopVar : orderItemList) {
            if (orderItemMap.containsKey(currLoopVar.OrderId)) {
                orderItemMap.get(currLoopVar.OrderId).add(currLoopVar);
            } else {
                orderItemMap.put(currLoopVar.OrderId, new List<OrderItem>{ currLoopVar });
            }
        }

        // MParrella | Coastal Cloud | 10-17-2022 | TSK-00230936 | Separating the OrderItem query out and building a Map of OrderId -> List<OrderItem>: STOP

        System.debug('*** SendOrdersToSAP Execution Start ***');
        System.debug('### Number of Records: ' + records.size());
        map<String,String> OrderUpdate= new map<String,String>();
        String endpoint;  
        integer i = 0 ;
        List<Order> UpdateOrderList = New List<Order>();
        for (order ordInfo : records){
            i++;
            try {               
                // Order ordInfo=[Select Reason_Code__c,Sales_Org__c,Type,PO_Type__c,ShippingFutureDate__c,Placed_By__c,Order_Reference__c,BillingStreet,BillingCity,BillingPostalCode,BillingCountryCode,BillingPhoneNumber,ShippingStreet,ShippingCity,ShippingPostalCode,ShippingCountryCode,ShippingPhone__c,Ship_Cancel_Flag__c,PoNumber,Order_Notes__c,CC_Details__c,account.Division_SAP__c,account.Distribution_Channel__c,account.name,account.BillingStreet,account.BillingCity,account.BillingPostalCode,account.BillingCountryCode,account.phone,Bill_To__r.SAP_Customer__c,Bill_To__r.Name,Ship_To__r.SAP_Customer__c,Ship_To__r.Name from Order where Id=: records[i].Id];
                 b2b_OrderWrapper Req= new b2b_OrderWrapper();//Payload creation
                 List<b2b_OrderWrapper> ReqList= new List<b2b_OrderWrapper>();
                 Order ordUpd=New order();
                 Req.division=ordInfo.Account.Division_SAP__c;
                 //Req.divisionChannel=ordInfo.Account.Distribution_Channel__c;
                 Req.divisionChannel=ordInfo.Distribution_Channel__c;//Arsee:added for Plano
                 Req.salesOrg=ordInfo.Sales_Org__c;
                 Req.shippingFutureDate=string.valueOf(ordInfo.ShippingFutureDate__c);
                 Req.poType=ordInfo.PO_Type__c;
                 Req.poDate=string.valueOf(ordInfo.EffectiveDate);
                 Req.placedBy=ordInfo.Placed_By__c;
                 //Req.orderType=ordInfo.Type;
                 Req.reasonCode=ordInfo.Reason_Code__c;
                //System.debug(' Req.reasonCode' +  Req.reasonCode);
                 switch on ordInfo.Reason_Code__c
                 {
                     when '100'{OrdType='OR';}
                     when '101'{OrdType='OR';}
                     when '102'{OrdType='OR';}
                     when '110'{OrdType='OR';}
                     when '115'{OrdType='OR';}
                     when '116'{OrdType='OR';}
                     when '118'{OrdType='OR';}
                     when '124'{OrdType='OR';}
                     when '125'{OrdType='OR';}
                     when '131'{OrdType='OR';}
                     when '132'{OrdType='OR';}
                     when '133'{OrdType='OR';}
                     when '134'{OrdType='OR';}
                     when '400'{OrdType='ZSMP';}
                     when '401'{OrdType='ZSMP';}
                     when '402'{OrdType='ZSMP';}
                     when '403'{OrdType='ZSMP';}
                     when '404'{OrdType='ZSMP';}
                     when '405'{OrdType='ZSMP';}
                     when else{}
                 }
                 Req.orderType=OrdType;
                 ordUpd.Type=OrdType;
                 ordUpd.id=ordInfo.Id;
                 //UpdateOrderList.add(ordUpd);
                 //System.debug(' Req.orderType' +  Req.orderType);
                 Req.accountSAPCustomer=ordInfo.Account.SAP_Customer__c;
                 Req.accountBillingStreet=ordInfo.Account.Name;
                 Req.accountBillingStreet=ordInfo.Account.BillingStreet;
                 Req.accountBillingCity=ordInfo.Account.BillingCity;
                 Req.accountBillingPostalCode=ordInfo.Account.BillingPostalCode?.replace('-', '');
                 Req.accountBillingCountryCode=ordInfo.Account.BillingCountryCode;
                 Req.accountPhone=ordInfo.Account.Phone;
                 Req.orderReference=ordInfo.Order_Reference__c;
                 Req.poNumber=ordInfo.PoNumber == null ? ordInfo.Order_Reference__c : ordInfo.PoNumber;
                // M. Parrella | 10-12-2022 | TSK-00231338 | Making call to new method to strip out Tab characters from 'Order_Notes__c' field: START
                List<String> removePatterns = new List<String>{'\t'};
                Req.orderNotes = ConnectAPIUtils.removeCharacters(removePatterns, ordInfo.Order_Notes__c);
//                Req.orderNotes = ordInfo.Order_Notes__c;
                // M. Parrella | 10-12-2022 | TSK-00231338 | Making call to new method to strip out Tab characters from 'Order_Notes__c' field: STOP
                 Req.ccDetails=ordInfo.CC_Details__c;
                 Req.shipCancelFlag=ordInfo.Ship_Cancel_Flag__c;
                 Req.billingStreet=ordInfo.BillingStreet;
                 Req.billingCity=ordInfo.BillingCity;
                 Req.billingPostalCode=ordInfo.BillingPostalCode?.replace('-', '');
                 Req.billingCountryCode=ordInfo.BillingCountryCode;
                 Req.shippingStreet=ordInfo.ShippingStreet;
                 Req.shippingCity=ordInfo.shippingCity;
                 Req.shippingPostalCode=ordInfo.ShippingPostalCode?.replace('-', '');
                 Req.shippingPhone=ordInfo.ShippingPhone__c;
                 Req.billToSAPCustomer=Test.isRunningTest() ?  'test':ordInfo.Bill_To_Account__c;
                 Req.billToName=Test.isRunningTest() ?  'test':ordInfo.Bill_To_Name__c;
                 Req.shipToSAPCustomer=Test.isRunningTest() ?  'test':ordInfo.Ship_To_Account__c;
                 Req.shipToName=Test.isRunningTest() ?  'test':ordInfo.Ship_To_Name__c;
                 Integer Quant;
                 list<b2b_OrderWrapper.cls_orderitem> proList = new list<b2b_OrderWrapper.cls_orderitem>();
                 Integer j=0;
                 String AutoString;
                 String OrdRef=ordInfo.Order_Reference__c;
                 // System.debug('ordInfo.OrderItems: ' + ordInfo.OrderItems);
                 for (OrderItem orderItem : orderItemMap.get(ordInfo.Id))  // MParrella | Coastal Cloud | 10-17-2022 | TSK-00230936 | Changing loop to iterate over newly created 'orderItemMap' instead of the joined query results
                 {
                    //System.debug('orderItem.type' + orderItem.type);
                    if(orderItem.type==orderProduct)
                    {
                        j++;
                        /*System.debug('j: ' + j);
                        System.debug('material: ' + orderItem.Product2.Material__c);*/
                    
                        b2b_OrderWrapper.cls_orderitem product = new b2b_OrderWrapper.cls_orderitem();
                        product.materialNumber = orderItem.Product2.Material__c;
                        Quant = Integer.valueOf(orderItem.Quantity);
                        product.quantity = string.valueOf(Quant);
                        product.price=string.valueof(orderItem.UnitPrice.setScale(2));
                        product.currencyType = orderItem.CurrencyIsoCode;
                        AutoString = String.valueOf(j).leftPad(6, '0');
                        //System.debug('orderItem.id' + orderItem.id);
                        product.suppliedLineNumber=orderItem.Supplied_Line__c;
                        //orderItem.Supplied_Line__c=AutoString;
                        //orderItem.SF_External_ID__c=OrdRef+'~'+AutoString;
                        proList.add(product);
                        //UpdOrdItemLst.add(orderItem);
                    }
                    else
                    { 
                        OrderItem DelOrdline= new OrderItem();
                        DelOrdline.id=orderItem.Id;
                        DelOrdItemLst.add(DelOrdline);
                        orderItemsToDeleteIds.add(orderITem.Id);
                    }
                }
                // update UpdOrdItemLst;    
                 //System.debug('prolist: ' + proList);
                 Req.OrderItem=proList;
                 //System.debug('Req.OrderItem: ' + Req.OrderItem);
                 ReqList.add(Req);
                 System.debug('json' + JSON.serialize(ReqList));
                 b2b_IntegrationService.SendOrder(ReqList);
                List<Order> UpdOrderItem = New List<Order>();
                //System.debug('ordInfo.id' + ordInfo.id);
                 ordUpd.Status='Submitted to SAP';
                 UpdateOrderList.add(ordUpd);
                //OrderUpdate.put(ordInfo.id,'Submitted to SAP');                 
            }
            catch (Exception e) {
                System.debug('Error Transmitting' + e.getMessage() + 'LN:' + e.getLineNumber() );
                Order ordUpd1=New order();
                ordUpd1.Id=ordInfo.id;
                ordUpd1.Status='Error Transmitting';
                UpdateOrderList.add(ordUpd1);
                //OrderUpdate.put(ordInfo.id,'Error Transmitting');                       
            }
        }
        try{
            Database.update(UpdateOrderList,false);
            System.debug('Updated Order Data'+ UpdateOrderList);
            //Database.update(UpdOrdItemLst,false);
            /* for(string Key:OrderUpdate.keySet())
                    {
                        Order ord=New order();
                        ord.Id=Key;

                        ord.Status=OrderUpdate.get(Key);
                        //ord.Type=OrdType;
                        Ordup.add(ord);
                    }
             //Database.update(Ordup,false);
             ChkOrdup.putall(Ordup);//Arsee:fixed duplicate list error
             if(ChkOrdup.size()>0)
             {
                update ChkOrdup.values();
                System.debug('Updated Order Data'+ ChkOrdup);
             }*/
             orderitemSummaryList = [select id from OrderItemSummary where OriginalOrderItemId IN :orderItemsToDeleteIds];
             Database.delete(orderitemSummaryList,false);
             Database.delete(DelOrdItemLst, false);
        }
        catch (Exception e) {
                System.debug('SendOrdersToSAP:Error is :' + e.getMessage() + 'LN:' + e.getLineNumber() );
                //OrderUpdate.put(ordInfo.id,'Error Transmitting');                     
        }
    } 
    global void finish(Database.BatchableContext BC){
        /*List<Order> Ordup=new List<Order>();
        try{
            System.debug('UpdOrdItemLst :' + UpdOrdItemLst);
            System.debug('OrderUpdate(map): ' + OrderUpdate);
            System.debug('DelOrdItemLst: ' + DelOrdItemLst);
            update UpdOrdItemLst;
            for(string Key:OrderUpdate.keySet())
                {
                    Order ord=New order();
                    ord.Id=Key;
                    ord.Status=OrderUpdate.get(Key);
                    //ord.Type=OrdType;
                    Ordup.add(ord);
                }
            System.debug('Ordup: ' + Ordup);
            Update Ordup;
            orderitemSummaryList = [select id from OrderItemSummary where OriginalOrderItemId IN :orderItemsToDeleteIds];
             Database.delete(orderitemSummaryList);
            //delete DelOrdItemLst;
            Database.delete(DelOrdItemLst, true);
        }
        catch (Exception e) {
                System.debug('Error is :' + e.getMessage() + 'LN:' + e.getLineNumber() );
                //OrderUpdate.put(ordInfo.id,'Error Transmitting');                     
        }*/
        // chained separate batch class to handle pricebook logic, we can move methods from it into this class if needed.
        /*SetSAPOrdersPricebookBatch orderPricebookUpdateBatch = new SetSAPOrdersPricebookBatch(Ordup);
        Database.executeBatch(orderPricebookUpdateBatch);*/
    }
}