/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           07/14/2022            Initial Version
 */
//This Class used to create contact point address
public class B2B_CreateContactPointAddr{
    @AuraEnabled
    public static string CreateCPA(string recordId,string country,string city)
    {
        ContactPointAddress CPA = new ContactPointAddress();
        try{
               System.debug('recordId:'+recordId+' country: ' + country + ' city' + city);
                CPA.name='Test';
                CPA.Country=country;
                CPA.city=city;
                Insert CPA;
                
            }
        catch (Exception e)
            {
                System.debug('Error in creating ContactPointAddress ' + e.getMessage());
            }
            return CPA.id;
    }
    @AuraEnabled
    public static List<OptionsWrapper> getCountryList()
    {
        List<OptionsWrapper> lstOptions = new List<OptionsWrapper>();
        try{
                Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                System.debug('Picklist::'+ple);
                for( Schema.PicklistEntry f : ple){
                    OptionsWrapper options = new OptionsWrapper();
                    options.label = f.getLabel();
                    options.value = f.getValue();
                    System.debug(options);
                    lstOptions.add(options);
                }
                System.debug(lstOptions);
            }
        catch (Exception e)
            {
                System.debug('Error in getCountryList ' + e.getMessage());
            }
            return lstOptions;
    }
    public class OptionsWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
    }

}