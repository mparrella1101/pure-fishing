/**
 * Created by Matt on 3/18/2022.
 */
/**
 * Trigger helper class for the ContentDocumentLinkTrigger class
 */
 /* Version      Author                  Company                 Date              Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           Mar 18, 2022       Initial version
 */

public with sharing class ContentDocLinkTriggerHelper {

   public static void handleBeforeInsert(List<ContentDocumentLink> newRecords){
        setDocLinkVisibility(newRecords);
   }


 public static void setDocLinkVisibility(List<ContentDocumentLink> newRecords){
      for (ContentDocumentLink docLink : newRecords){
         System.debug('ConDocLink: ' + docLink);
         docLink.Visibility = 'AllUsers';
      }
 }

}