global class ScheduledJobsInitiator implements Schedulable {
	global void execute(SchedulableContext SC) {
        System.schedule('SyncOrders#1', '0 5 * * * ?', new scheduledSync());
        System.schedule('SyncOrders#2', '0 15 * * * ?', new scheduledSync());
        System.schedule('SyncOrders#3', '0 25 * * * ?', new scheduledSync());
        System.schedule('SyncOrders#4', '0 35 * * * ?', new scheduledSync());
        System.schedule('SyncOrders#5', '0 45 * * * ?', new scheduledSync());
        System.schedule('SyncOrders#6', '0 55 * * * ?', new scheduledSync());

        Integer PRODUCTS_PER_REQUEST = 10;
        Integer PRODUCTS_PER_QUERY = 1000;
        Integer PRODUCTS_OFFSET = 0;
        Integer MINUTES_OFFSET = 1;
        Integer ITERATION = -1;
        System.schedule('SyncItemsFirst1000', '0 15/60 0/1 * * ?', new ItemAvailabilityScheduledSync(ITERATION, PRODUCTS_PER_REQUEST,PRODUCTS_PER_QUERY,PRODUCTS_OFFSET,MINUTES_OFFSET));
        
        PRODUCTS_PER_REQUEST = 10;
        PRODUCTS_PER_QUERY = 1000;
        PRODUCTS_OFFSET = 1000;
        MINUTES_OFFSET = 1;
        ITERATION = -1;
        System.schedule('SyncItemsNext1000', '0 45/60 0/1 * * ?', new ItemAvailabilityScheduledSync(ITERATION, PRODUCTS_PER_REQUEST,PRODUCTS_PER_QUERY,PRODUCTS_OFFSET,MINUTES_OFFSET));

        //System.abortJob(SC.getTriggerId());
    }
}