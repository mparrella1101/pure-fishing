public class EpM2OrderSyncRecursiveSchedulable implements Schedulable {
    private String now_sync_time{get;Set;}
    private Double pageSize{get;Set;}
    private Double currentPage{get;Set;}
    private String token{get;Set;}
    private String eComStoreID{get;Set;}
    private String lastSalesOrderSyncTime{get;Set;}
    private String baseURL{get;Set;}
    private String supportEmail{get;Set;}
    /*
     * Constructor
     */
    public EpM2OrderSyncRecursiveSchedulable(String now_sync_time, Double pageSize, Double currentPage, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail){
        this.now_sync_time = now_sync_time;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.token = token;
        this.eComStoreID = eComStoreID;
        this.lastSalesOrderSyncTime = lastSalesOrderSyncTime;
        this.baseURL = baseURL;
        this.supportEmail = supportEmail;
    }
    
    public void execute(SchedulableContext sc) {
		System.enqueueJob(new EpM2OrderSyncRecursive(now_sync_time, pageSize, currentPage, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail));
        // Abort the job once the job is queued
        System.abortJob(sc.getTriggerId());
    }
}