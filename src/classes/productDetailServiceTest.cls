@IsTest
public with sharing class productDetailServiceTest {
    private static Id userId = null;
    private static Account accountId = null;
    private static Id storeId;
    private static Id cartId;
    private static User storeUser;
    private static Id productId;
    private static List<Product2> productList;

   @testSetup static void setup_data() {
        Account account = new Account(Name='TestAccount',Sales_Org__c='0020');
        insert account;
        Contact contact = new Contact(LastName='TestContact', AccountId=account.Id);
        insert contact;
        Contact contact1 = new Contact(LastName='TestContact1', AccountId=account.Id);
        insert contact1;
        Contact contact2 = new Contact(LastName='TestContact2', AccountId=account.Id);
        insert contact2;
        Profile pro = [SELECT Id FROM Profile WHERE Name='B2B Customer Community Plus Login Delegated External User' LIMIT 1];
        User user1 = new User(Alias='buyer', Email='buyer@example.com', LastName='Buyer',
                TimeZoneSidKey='GMT', LocaleSidKey='fr', Username='buyer@example.com',
                EmailEncodingKey='UTF-8', ProfileId=pro.Id, LanguageLocaleKey='fr',User_Role__c='Owner',
                ContactId=contact.Id);
        insert user1;
        User user2 = new User(Alias='buyer', Email='buyer1@example.com', LastName='Buyer1',
                TimeZoneSidKey='GMT', LocaleSidKey='da', Username='buyer1@example.com',
                EmailEncodingKey='UTF-8', ProfileId=pro.Id, LanguageLocaleKey='da',User_Role__c='Owner',
                ContactId=contact1.Id);
        insert user2;
       User user3 = new User(Alias='buyer', Email='buyer2@example.com', LastName='Buyer2',
                TimeZoneSidKey='GMT', LocaleSidKey='sv', Username='buyer2@example.com',
                EmailEncodingKey='UTF-8', ProfileId=pro.Id, LanguageLocaleKey='sv',User_Role__c='Owner',
                ContactId=contact2.Id);
        insert user3;
        Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test');
        insert Prod;
        Product2DataTranslation ProdTrans= new Product2DataTranslation(ParentId=Prod.Id,Language='fr',Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test');
        insert ProdTrans;
        Product2DataTranslation ProdTrans1= new Product2DataTranslation(ParentId=Prod.Id,Language='da',Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test');
        insert ProdTrans1;
       Product2DataTranslation ProdTrans2= new Product2DataTranslation(ParentId=Prod.Id,Language='sv',Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test');
        insert ProdTrans2;
		PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'B2B_Buyer'];
        System.runAs(user1) {
            PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = user1.id, PermissionSetId = ps.Id);
		    insert psa;
        }
       System.runAs(user2) {
            PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = user2.id, PermissionSetId = ps.Id);
		    insert psa;
        }
       System.runAs(user3) {
            PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = user3.id, PermissionSetId = ps.Id);
		    insert psa;
        }
        
    }
    @isTest static void getProductDetailsTest()
    {
        User buyer = [SELECT Id,ContactId from User WHERE Username='buyer@example.com' LIMIT 1];
        User buyer1 = [SELECT Id,ContactId from User WHERE Username='buyer1@example.com' LIMIT 1];
        User buyer2 = [SELECT Id,ContactId from User WHERE Username='buyer2@example.com' LIMIT 1];
        Test.startTest();
        Product2 Prod = [Select Id from Product2 where Name='Test1' LIMIT 1];
        string ProdId = Prod.Id;
        productDetailService.getProductDetails(ProdId);
        System.runAs(buyer){
        productDetailService.getProductDetails(ProdId);
        }
        System.runAs(buyer1){
        productDetailService.getProductDetails(ProdId);
        }
        System.runAs(buyer2){
        productDetailService.getProductDetails(ProdId);
        }
        Test.stopTest();
    }
}