@isTest
private class AddOrderControllerTest {
    @TestSetup
    private static void makeData(){        
        Account testAccount = CCTestUtils.createAccount('Coastal Cloud', false);
        testAccount.Division_SAP__c = '12';
        testAccount.SAP_Customer__c = '1234';
        testAccount.Distribution_Channel__c = '12';
        testAccount.Sales_Org__c = '0020';
        insert testAccount;
        ContactPointAddress testShippingAddress1 = CCTestUtils.createContactPointAddress('Test Shipping address 1', testAccount, 'Shipping', '555 E University Ave', true, false);
        ContactPointAddress testShippingAddress2 = CCTestUtils.createContactPointAddress('Test Shipping address 2', testAccount, 'Shipping', '123 W University Ave', false, false);
        ContactPointAddress testBillingAddress = CCTestUtils.createContactPointAddress('Test Billing address', testAccount, 'Billing', '111 Archer Rd', false, false);
        insert new List<ContactPointAddress>{testShippingAddress1, testShippingAddress2, testBillingAddress};
        PriceBook2 testStandardPricebook = CCTestUtils.createPriceBook(true, 'test pricebook', true, 'USD', true, false);
        Pricebook2 testPricebook = CCTestUtils.createPriceBook(true, 'test coastal pricebook', false, 'USD', true, false);
        // insert new List<Pricebook2>{testStandardPricebook, testPricebook};
        update testStandardPricebook;
        insert testPricebook;
        Order testOrder = CCTestUtils.createOrder(testAccount, testPricebook, true);
//        testOrder.Pricebook2Id = System.Label.cc_standard_pricebook_id;
        testOrder.Pricebook2Id = Sales_Org_Configuration__mdt.getInstance('X' + testAccount.Sales_Org__c).PricebookId__c;
        update testOrder;
        Product2 testVariationProduct = CCTestUtils.createProduct('test variation product', '123VAR', '1234567', false, false);
        testVariationProduct.Material__c = '123';
        testVariationProduct.UPC_Code__c = '456';
        testVariationProduct.Model_Number__c = '789';
        Product2 testSimpleProduct = CCTestUtils.createProduct('test simple product', '123SIMP', '3456789',  false, false);
        testSimpleProduct.Material__c = '1234';
        testSimpleProduct.UPC_Code__c = '5678';
        testSimpleProduct.Model_Number__c = '9101';
        insert new List<Product2>{testVariationProduct, testSimpleProduct};
        ProductCatalog testProductCatalog = CCTestUtils.createProductCatalog('test product catalog', true);
        ProductCategory testVariationProductCategory = CCTestUtils.createProductCategory('Variation', testProductCatalog, null, null, false);
        ProductCategory testSimpleProductCategory = CCTestUtils.createProductCategory('Simple', testProductCatalog, null, null, false);
        insert new List<ProductCategory>{testVariationProductCategory, testSimpleProductCategory};
        ProductCategoryProduct testVariationPCP = CCTestUtils.createProductCategoryProduct(testVariationProduct, testVariationProductCategory, false);
        ProductCategoryProduct testSimplePCP = CCTestUtils.createProductCategoryProduct(testSimpleProduct, testSimpleProductCategory, false);
        insert new List<ProductCategoryProduct>{testVariationPCP, testSimplePCP};
        PricebookEntry standardPricebookEntry = CCTestUtils.createPriceBookEntry(testVariationProduct, testStandardPricebook, true, false);
        PricebookEntry testPricebookEntry = CCTestUtils.createPriceBookEntry(testVariationProduct, testPricebook, true, false);
//        testPricebookEntry.Pricebook2Id = System.Label.cc_standard_pricebook_id;
        testPricebookEntry.Pricebook2Id = Sales_Org_Configuration__mdt.getInstance('X' + testAccount.Sales_Org__c).PricebookId__c;

//        insert new List<PricebookEntry>{standardPricebookEntry, testPricebookEntry};
    }

    @isTest
    private static void getContactPointAddressesTest(){
        Account queriedAccount = [SELECT Id FROM Account LIMIT 1];
        Id testPricebookId = Test.getStandardPricebookId();
        List<ContactPointAddress> queriedContactPointAddresses = [SELECT Id, Address, AddressType FROM ContactPointAddress WHERE ParentId = :queriedAccount.Id];
        List<Id> contactPointAddressIds = new List<Id>();
        for(ContactPointAddress contactPointAddress : queriedContactPointAddresses){
            contactPointAddressIds.add(contactPointAddress.Id);
        }
        Pricebook2 testPricebook = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        AddOrderController.getContactPointAddresses(queriedAccount.Id);
        System.assert(contactPointAddressIds.contains(AddOrderController.getContactPointAddresses(queriedAccount.Id).contactPointAddresses[0].Id), 'We expect our contact point addresses to be found.');
        System.assert(contactPointAddressIds.contains(AddOrderController.getContactPointAddresses(queriedAccount.Id).contactPointAddresses[1].Id), 'We expect our contact point addresses to be found.');
    }
    
    @isTest
    private static void getProductDetailsTest(){
        Account queriedAccount = [SELECT Id, Division_SAP__c, SAP_Customer__c, Distribution_Channel__c FROM Account LIMIT 1];
        Pricebook2 queriedPricebook = [SELECT Id FROM Pricebook2 LIMIT 1];
        Order queriedOrder = [SELECT Id, AccountId, TotalAmount FROM Order LIMIT 1];
        AddOrderController.getProductDetails(queriedOrder.Id);
        System.assertEquals(queriedAccount.Id, AddOrderController.getProductDetails(queriedOrder.Id).account.Id, 'We want to make sure our order is associated to the correct account.');

        Map<String,Object> orderTotal = AddOrderController.getOrderTotal(queriedOrder.Id);
        System.assertEquals(0.00, orderTotal.get('totalAmount'), 'We expect our Order records TotalAmount field to be correct.');
        
    }
    
    @isTest
    private static void productQueryTest(){
    	List<Product2> testProducts = [SELECT Id, ProductCode, Material__c, UPC_Code__c, Model_Number__c, ProductClass FROM Product2];
        System.debug('testProducts: ' + testProducts);
        // List<String> testProductCodes = new List<String>();
        List<String> modelList = new List<String>();
        List<String> upcList = new List<String>();
        List<String> materialList = new List<String>();
        for(Product2 product : testProducts){
            // testProductCodes.add(product.ProductCode);
            modelList.add(product.Model_Number__c);
            upcList.add(product.UPC_Code__c);
            materialList.add(product.Material__c);
        }
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();        
        gen.writeObjectField('modelList', modelList);
        gen.writeObjectField('upcList', upcList);
        gen.writeObjectField('materialList', materialList);
        gen.writeEndObject();
        String jsonString = gen.getAsString();
        List<String> invalidProductCodes = new List<String>{'INVALID'};
        // testProductCodes.add('invalid product code');
        AddOrderController.productQuery(testProducts[0].ProductCode); 
        AddOrderController.productValidation(jsonString);
        System.assertEquals(testProducts[0].ProductCode, AddOrderController.productQuery(testProducts[0].Productcode).product.ProductCode, 'We expect to have successfully queried for our product.');
        System.assert(modelList.contains(AddOrderController.productValidation(jsonString).validProductList[0].Model_Number__c), 'We expect our test product\'s Model_Number__c field to have returned as a valid product');
        System.assert(upcList.contains(AddOrderController.productValidation(jsonString).validProductList[0].UPC_Code__c), 'We expect our test product\'s UPC_Code__c field to have returned as a valid product');
        System.assert(materialList.contains(AddOrderController.productValidation(jsonString).validProductList[0].Material__c), 'We expect our test product\'s Material__c field to have returned as a valid product');
    }
    
    @isTest
    private static void getPricingTest(){
        String quantity = '10';
        Account queriedAccount = [SELECT Id, Division_SAP__c, SAP_Customer__c, Distribution_Channel__c, Sales_Org__c FROM Account LIMIT 1];
        Product2 queriedProduct = [SELECT Id, Material__c FROM Product2 WHERE Name = 'test variation product' LIMIT 1];
        Order queriedOrder = [SELECT Id, Reason_Code__c, Type, EffectiveDate FROM Order LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new pricingMock());
        Test.startTest();
        String productData = '{"division":"'+queriedAccount.Division_SAP__c+'","customerNumber":"'+queriedAccount.SAP_Customer__c+'","distributionChannel":"'+queriedAccount.Distribution_Channel__c+'","salesOrganization":"'+queriedAccount.Sales_Org__c+'","products":[{"materialNumber":"'+queriedProduct.Material__c+'","quantity":"'+quantity+'"}],"orderType":"'+queriedOrder.Type+'","orderReason":"'+queriedOrder.Reason_Code__c+'","shipmentDate":"'+queriedOrder.EffectiveDate+'"}';
        AddOrderController.getPricing(productData);
        Test.stopTest();
        List<Object> l = (List<Object>) AddOrderController.getPricing(productdata);
		Map<String,Object> m = (Map<String,Object>) l[0];
		String materialNumber = (String) m.get('materialNumber');
        String price = (String) m.get('price');
        System.assertEquals('1265710', materialNumber, 'We expect to have retrieved the correct materialNumber');
        System.assertEquals('47.6900', price, 'We expect to have successfully retreived a price.');
    }
    
    @isTest
    private static void getInventoryTest(){
        Account queriedAccount = [SELECT Id, Division_SAP__c, SAP_Customer__c, Distribution_Channel__c, Sales_Org__c FROM Account LIMIT 1];
        Product2 queriedProduct = [SELECT Id, Material__c FROM Product2 WHERE Name = 'test variation product' LIMIT 1];
        String inventoryData = '{"accountId":"'+queriedAccount.Id+'","productIds":["'+queriedProduct.Id+'"]}';
        Test.setMock(HttpCalloutMock.class, new inventoryMock());
        Test.startTest();
        AddOrderController.getInventory(inventoryData);
        Test.stopTest();
        b2b_InventoryWrapper.response response = (b2b_InventoryWrapper.response)AddOrderController.getInventory(inventoryData)[0];
        System.assertEquals('12345', response.materialNumber, 'We expect to have retrieved the correct materialNumber');
        System.assertEquals('47', response.quantity, 'We expect to have successfully retierved an inventory count for our submitted product.');
    }
    
    @isTest
    private static void submitOrderItemsTest(){
        String quantity = '10';
        String unitPrice = '15.50';
        Product2 queriedProduct = [SELECT Id, Material__c, ProductCode, Name FROM Product2 WHERE Name = 'test variation product' LIMIT 1];
//        Id testPricebookId = System.Label.cc_standard_pricebook_id;
//        Id testPricebookId = Sales_Org_Configuration__mdt.getInstance('X0020').PricebookId__c;
//        PricebookEntry testPricebookEntry = [SELECT Id FROM PricebookEntry WHERE product2Id = :queriedProduct.Id AND pricebook2Id = :testPricebookId];
        Order queriedOrder = [SELECT Id, Reason_Code__c, Type, EffectiveDate FROM Order LIMIT 1];
        String dataModel = '[{"orderId":"'+queriedOrder.Id+'","productId":"'+queriedProduct.Id+'","productCode":"'+queriedProduct.ProductCode+'","productName":"'+queriedProduct.Name+'","quantity":"'+quantity+'","unitPrice":"'+unitPrice+'"}]';
        AddOrderController.submitOrderItems(dataModel);
        System.debug('submitOrderItems: ' + AddOrderController.submitOrderItems(dataModel));
//        System.assert(AddOrderController.submitOrderItems(dataModel).successfulInserts > 0, 'We want to verify our order lines were created successfully.');
    }
    
    public class pricingMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[{"materialNumber":"1265710","price":"47.6900"}]');
            res.setStatusCode(200);
            return res;
        }
    }
    
    public class inventoryMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('[{"materialNumber":"12345","quantity":"47"}]');
            res.setStatusCode(200);
            return res;
        }
    }    
}