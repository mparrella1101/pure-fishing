@isTest
global class EpJDECreateJDESalesOrderMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        response.setBody('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ns2:createSalesOrderResponse xmlns:ns2="http://oracle.e1.bssv.JP574201/"><e1MessageList/><customerId>238826</customerId><items><documentLineNumber>1.000</documentLineNumber><itemId>54653</itemId><itemProduct>CAB56260M</itemProduct><quantityOrdered>1</quantityOrdered></items><items><documentLineNumber>2</documentLineNumber><itemId>60336</itemId><itemProduct>BILLABLE FREIGHT TAXABLE</itemProduct><quantityOrdered>1</quantityOrdered></items><SOId>1210463</SOId><status>SUCCESS</status></ns2:createSalesOrderResponse></soapenv:Body></soapenv:Envelope>');
        response.setStatusCode(200);
        response.setStatus('Success');
        return response;
    }
}