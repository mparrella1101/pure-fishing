/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/24/2022            Initial Version
 */
//Creating serach index for active stores in org.
public with sharing class b2BCreateSearchIndex {
    public void b2BCreateSearchIndex() {
        List<WebStoreNetwork> storeList=[select WebStoreId from WebStoreNetwork];
        System.debug('storeList' + storeList);
        for(WebStoreNetwork WSN:storeList)
            {
                ConnectApi.CommerceSearchIndex searchIndex =ConnectApi.CommerceSearchSettings.createCommerceSearchIndex(WSN.WebStoreId);
            }
    }
}