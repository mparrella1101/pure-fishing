/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Company_Product_InfoTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Company_Product_InfoTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Company_Product_Info__c());
    }
}