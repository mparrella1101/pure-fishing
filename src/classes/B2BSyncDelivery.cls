/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           04/15/2022            Initial Version
 */
// This class determines if we can ship to the buyer's shipping address and creates
// CartDeliveryGroupMethods for the different options and prices the buyer may choose from
public class B2BSyncDelivery {
    // This invocable method only expects one ID
    @InvocableMethod(callout=true label='Prepare the Delivery Method Options' description='Runs a synchronous version of delivery method preparation' category='B2B Commerce')
    public static void syncDelivery(List<ID> cartIds) {
        // Validate the input
        if (cartIds == null || cartIds.size() != 1) {
            String errorMessage = 'A cart id must be included to B2BSyncDelivery';
            // Sync non-user errors skip saveCartValidationOutputError
            throw new CalloutException (errorMessage);
        }
        
        // Extract cart id and start processing
        Id cartId = cartIds[0];
        startCartProcessSync(cartId);
    }

    private static void startCartProcessSync(Id cartId) {
        try {
            // We need to get the ID of the cart delivery group in order to create the order delivery groups.
            String ActId;
            Decimal Amt;
            String OrdReason;
            String DC;
            for( WebCart ActInfo:[Select AccountId,TotalProductAmount,Cart_Order_Reason__c,Distribution_Channel__c from WebCart where Id = : cartId])
            {
                ActId=ActInfo.AccountId;
                Amt= ActInfo.TotalProductAmount.setScale(2);
                OrdReason = ActInfo.Cart_Order_Reason__c;
                DC=ActInfo.Distribution_Channel__c;
            }
            Account ActInfos = [Select Division_SAP__c,SAP_Customer__c,Distribution_Channel__c,Sales_Org__c,Currency__c  from Account where Id = : ActId][0];
            Id cartDeliveryGroupId = [SELECT Id FROM CartDeliveryGroup WHERE CartId = :cartId WITH SECURITY_ENFORCED][0].Id;

            // Used to increase the cost by a multiple of the number of items in the cart (useful for testing but should not be done in the final code)
            Integer numberOfUniqueItems = [SELECT count() from cartItem WHERE CartId = :cartId WITH SECURITY_ENFORCED];
            b2b_ShippingWrapper req = new b2b_ShippingWrapper();
           req.division = '00' + ActInfos.Division_SAP__c;
            req.customerNumber = ActInfos.SAP_Customer__c;
            req.salesOrganization = ActInfos.Sales_Org__c;
            req.orderValue = String.valueOf(amt);
            req.orderReason = OrdReason;
            req.currencyType=ActInfos.Currency__c;
            //req.distributionChannel = '0' + ActInfos.Distribution_Channel__c;
            req.distributionChannel=DC;
            /*req.division = '00';
            req.customerNumber = '20641';
            req.salesOrganization ='0180';
            req.orderValue =String.valueOf(amt);
            req.orderReason = '100';
            req.currencyType='EUR';
            req.distributionChannel = '00';*/
            System.debug('json' + JSON.serialize(req));
             b2b_ShippingWrapper.response ShippingFromExternalService = b2b_IntegrationService.Shippingcal(req);  
             System.debug('ShippingFromExternalService' + ShippingFromExternalService);
            // Get shipping options, including aspects like rates and carriers, from the external service. 
            //ShippingOptionsAndRatesFromExternalService[] shippingOptionsAndRatesFromExternalService = getShippingOptionsAndRatesFromExternalService(cartId, numberOfUniqueItems);

            // On re-entry of the checkout flow delete all previous CartDeliveryGroupMethods for the given cartDeliveryGroupId
            delete [SELECT Id FROM CartDeliveryGroupMethod WHERE CartDeliveryGroupId = :cartDeliveryGroupId];

			OrderDeliveryMethod defaultOrderDeliveryMethod = new OrderDeliveryMethod(
                    Name = 'Shipping',
                    Carrier = 'ground',
                    isActive = true,
                    ProductId = getDefaultShippingChargeProduct2Id(),
                    ClassOfService = 'A'
                );
			insert defaultOrderDeliveryMethod;
            CartDeliveryGroupMethod cartDeliveryGroupMethod = new CartDeliveryGroupMethod(
            CartDeliveryGroupId = cartDeliveryGroupId,
            DeliveryMethodId = defaultOrderDeliveryMethod.Id,
            ExternalProvider = 'Delivery Method',
            Name = 'Delivery Method10',
            CurrencyIsoCode=ActInfos.Currency__c,
            ShippingFee =Double.valueOf(ShippingFromExternalService.price),
            WebCartId = cartId
        );
        insert(cartDeliveryGroupMethod);
        } catch (DmlException de) {
            // To aid debugging catch any exceptions thrown when trying to insert the shipping charge to the CartItems
            // In production you might want to hide these details from the buyer user.
            Integer numErrors = de.getNumDml();
            String errorMessage = 'There were ' + numErrors + ' errors when trying to insert the charge in the CartItem: ';
            for(Integer errorIdx = 0; errorIdx < numErrors; errorIdx++) {
                errorMessage += 'Field Names = ' + de.getDmlFieldNames(errorIdx);
                errorMessage += 'Message = ' + de.getDmlMessage(errorIdx);
                errorMessage += ' , ';
            }
			system.debug('de.getNumDml()' + de.getNumDml() + de);
            saveCartValidationOutputError(errorMessage, cartId);
            throw new CalloutException (errorMessage);
        }
    }
@TestVisible
     private static void saveCartValidationOutputError(String errorMessage, Id cartId) {
            // In order for the error to be propagated to the user, we need to add a new CartValidationOutput record.
            // The following fields must be populated:
            // BackgroundOperationId: Foreign Key to the BackgroundOperation
            // CartId: Foreign key to the WebCart that this validation line is for
            // Level (required): One of the following - Info, Error, or Warning
            // Message (optional): Message displayed to the user
            // Name (required): The name of this CartValidationOutput record. For example CartId
            // RelatedEntityId (required): Foreign key to WebCart, CartItem, CartDeliveryGroup
            // Type (required): One of the following - SystemError, Inventory, Taxes, Pricing, Shipping, Entitlement, Other
            CartValidationOutput cartValidationError = new CartValidationOutput(
                CartId = cartId,
                Level = 'Error',
                Message = errorMessage.left(255),
                Name = (String)cartId,
                RelatedEntityId = cartId,
                Type = 'Shipping'
            );
            insert(cartValidationError);
    }

  
    private static Id getDefaultShippingChargeProduct2Id() {
        // In this example we will name the product representing shipping charges 'Shipping Charge for this delivery method'.
        // Check to see if a Product2 with that name already exists.
        // If it doesn't exist, create one.
        String shippingChargeProduct2Name = 'Shipping Charge';
        List<Product2> shippingChargeProducts = [SELECT Id FROM Product2 WHERE Name = :shippingChargeProduct2Name];
        if (shippingChargeProducts.isEmpty()) {
            Product2 shippingChargeProduct = new Product2(
                isActive = true,
                Name = shippingChargeProduct2Name,
                ShortItemNumber__c='Charge'
            );
            insert(shippingChargeProduct);
            return shippingChargeProduct.Id;
        }
        else {
            return shippingChargeProducts[0].Id;
        }
    }
}