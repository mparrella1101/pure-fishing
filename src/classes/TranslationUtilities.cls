/**
 * @description     Utility class to host any and all custom logic dealing with Product translations in the B2B Store
 * @author          Coastal Cloud (Matt Parrella)
 * @date            January 5, 2023
 */

/* Author                  Company                 TaskRay                  Date            Description
-------------------------------------------------------------------------------------------------------------------------
    Matt Parrella           Coastal Cloud          TSK-00243994             1-05-2023       Initial version
 */

public with sharing class TranslationUtilities {

    /**
     * @description   Helper method to retrieve translated field values for a passed-in Product2Id. It will return translated values for any fields that
     *                had translated values available. For the fields where there were no translated values available, the method will simply take the value
     *                from the Product2 record (not translated), to prevent returning null field values. NOT BULKIFIED -- accepts only 1 Product Id at a time.
     *
     * @param         product2Id The 18-character Salesforce Record Id for the Product2 record we wish to get translated data for
     * @param         fields A list of fields that we'd like to get translated values for
     *
     * @return        (Map<String,Object>) A Map with field api names as the key and their translated (or non-translated) value as the value
   */
    @AuraEnabled
    public static Map<String,Object> getTranslatedProductFields(String product2Id, List<String> fields) {
        Map<String,Object> returnData = new Map<String,Object>();

        // Build Dynamic queries based on passed-in fields
        String productQueryStr = 'SELECT Id, '; // Query for Product2
        String translateQueryStr = 'SELECT Id, '; // Query for Product2DataTranslation (to look for any existing translations of the record)

        // Add the passed-in list of fields to the query
        for (String currField : fields) {
            translateQueryStr += currField + ', ';
            productQueryStr += currField + ', ';
        }

        // Remove the last ',' from the fields list in the query
        translateQueryStr = translateQueryStr.substring(0, translateQueryStr.length()-2);
        productQueryStr = productQueryStr.substring(0, productQueryStr.length()-2);

        translateQueryStr += ' FROM Product2DataTranslation WHERE ParentId = \'' + product2Id + '\'';
        productQueryStr += ' FROM Product2 WHERE Id = \'' + product2Id + '\'';

        // Each of these queries should only return 1 record (since we're filtering on Product Ids), hence the hardcoding of index values from this point forward
        List<Product2DataTranslation> translations = (List<Product2DataTranslation>) Database.query(translateQueryStr);
        List<Product2> products = (List<Product2>) Database.query(productQueryStr);

        // Check if we were able to locate a Product2DataTranslation record for the passed-in Product2Id
        if (!translations.isEmpty()) {
            // Check each field to see if there is a corresponding value on the translated record
            for (String currStringVar : fields) {
                if (translations[0].get(currStringVar) != null) {
                    // If a translated value is found, return that
                    returnData.put(currStringVar, translations[0].get(currStringVar));
                } else {
                    // Else if no translated value is found, return the original value from the Product2 record
                    returnData.put(currStringVar, products[0].get(currStringVar));
                }
            }
        } else {
            // If no translations records are found, return the original product2 record values
            for (String currStringVar : fields) {
                returnData.put(currStringVar, products[0].get(currStringVar));
            }
        }

        return returnData;
    }

    /**
     * @description     Helper method that will accept a Product2Id and a Product2 Field API Name. It will return the translated field label and field value (from Product2TranslationData), if available.
     *                  If no translation is available, it will return the field label/value from the original Product2 record.
     *
     * @param           product2Id      The Salesforce 18-character record Id for the Product2 record we wish to retrieve field information from
     * @param           fieldApiName    The Field API Name of the Product2 field we wish to retrieve data for
     *
     * @return          (Map<String,Object>) A Map with the field Label as the key and the field value as the value
     */
    @AuraEnabled
    public static Map<String,Object> getSingleProductFieldData(String product2Id, String fieldApiName) {
        String fieldLabel;
        Map<String,Object> returnData = new Map<String,Object>();
        try {
            // Get the Label in the user's language
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType productSchema = schemaMap.get('Product2');
            Map<String,Schema.SObjectField> productFieldMap = productSchema.getDescribe().fields.getMap();
            fieldLabel = productFieldMap.get(fieldApiName).getDescribe().getLabel();

            String translatedQueryStr = 'SELECT ' + fieldApiName + ' FROM Product2DataTranslation WHERE ParentId = \'' + product2Id + '\'';
            List<Product2DataTranslation> translationData = Database.query(translatedQueryStr);

            String productQueryStr = 'SELECT ' + fieldApiName + ' FROM Product2 WHERE Id =\'' + product2Id + '\'';
            List<Product2> productData = Database.query(productQueryStr);

            if (!translationData.isEmpty()) {
                if (translationData[0].get(fieldApiName) != null){
                    returnData.put(fieldLabel, translationData[0].get(fieldApiName));
                } else {
                    returnData.put(fieldLabel, productData[0].get(fieldApiName));
                }
            } else {
                returnData.put(fieldLabel, productData[0].get(fieldApiName));
            }
        }
        catch(Exception ex) {
            returnData.put('error', ex.getMessage());
        }

        System.debug('returnData: ' + returnData);

        return returnData;
    }

}