public with sharing class OrderLineWithShipment {
    @AuraEnabled(cacheable=true)
    public static List<OrderWrapper> getOrderLineWithShipment(Id OrdId, Integer retrieveLimit) {
        System.debug('getOrderLineWithShipment::OrdId:' + OrdId + '   retrieveLimit:' + retrieveLimit);

        String Ord=string.valueOf(OrdId);
        Ord=Ord.mid(0, 15);
        List<Order> OrdProd=[Select id,(select id,UnitPrice,Quantity_Ordered__c,Quantity_Shipped__c,Product2.Model_Number__c,Product2.Name,TotalLineAmount from OrderItems) from order where Id =:OrdId];
        /*List<shipment_package_item__c> shipLineItemList=[select id,Order_Line__c,Shipment_Package__r.Tracking_Number__c, Shipment_Package__r.Quantity,Shipment_Package__r.ShipmentItemNumber from shipment_package_item__c where Order_Id__c =:Ord];*/

        List<Schema.ShipmentItem> shipLineItemList=[select id,Order_Line__c,TrackingNumber, Quantity,ShipmentItemNumber from ShipmentItem where Order_Id__c =:Ord];

        System.debug('getOrderLineWithShipment::shipLineItemList:' + shipLineItemList +'Ord:' + Ord + ' OrdProd:' + OrdProd);
       // Map<Id,List< Schema.ShipmentItem>> orderlineIdShipmentMap = new Map<Id,List<Schema.ShipmentItem>>();
        List<OrderWrapper> ords = new List<OrderWrapper> ();
        /*for(Schema.ShipmentItem item : shipLineItemList){
            if(!orderlineIdShipmentMap.containsKey(item.Order_Line__c)){
                orderlineIdShipmentMap.put(item.Order_Line__c, new List<Schema.ShipmentItem>{item});
                System.debug('Not Contain orderlineIdShipmentMap'+ orderlineIdShipmentMap);
            }
            else{
                orderlineIdShipmentMap.get(item.Order_Line__c).add(item);
                System.debug('Contain orderlineIdShipmentMap'+ orderlineIdShipmentMap);
            }
        }*/
        String OrdItemId='';
        for(OrderItem OI : OrdProd[0].OrderItems){
            OrderWrapper OrdLines = new OrderWrapper ();
            OrdLines.Id = OI.id;
            OrdItemId=OI.id;
            OrdItemId=OrdItemId.mid(0, 15);
            OrdLines.Name = OI.Product2.name;
            OrdLines.UP = OI.UnitPrice;
            OrdLines.Shiped = OI.Quantity_Shipped__c;
            OrdLines.Ordered = OI.Quantity_Ordered__c;
            OrdLines.Mat = OI.Product2.Model_Number__c;
            OrdLines.Ext = OI.TotalLineAmount;
            //OrdLines.shipments =Shipments;
            OrdLines.OrderLineDet = 'Order Lines';
            List<ShipWrapper> shpWrapList = new List<ShipWrapper>();
            for(Schema.ShipmentItem shpRecord : shipLineItemList){
                ShipWrapper shpWrap = new ShipWrapper();
                if(OI.id==shpRecord.Order_Line__c){
                    System.debug('shipment Present');
                shpWrap.Id = shpRecord.Id;
                shpWrap.Qty = shpRecord.Quantity;
                shpWrap.No = shpRecord.ShipmentItemNumber;
                shpWrap.TrackingNo = shpRecord.TrackingNumber;
                shpWrapList.add(shpWrap);
                }
            }
            OrdLines.shipments = shpWrapList;
            OrdLines.ShipmentDet = ' Shipment (' + shpWrapList.size() + ')';
            OrdLines.OrdLineHasShipments = (shpWrapList.size() == 0 ? false : true);
            ords.add(OrdLines);
        }





        // for (OrderItem ordItem : OrdWithOrdLine) {
        //     OrderWrapper OrdLines = new OrderWrapper ();
        //     List<ShipWrapper> Shipments = new List<ShipWrapper> ();
        //     Integer numOfShip = 0;
        //     for (Shipment_Package_Item__c Shipment : ordItem.Shipment_Package_Item__c) {
        //         ShipWrapper ShipWrpr = new ShipWrapper ();
        //         ShipWrpr.Id = Shipment.Id;
        //         ShipWrpr.Qty = Shipment.Quantity__c;
        //         ShipWrpr.No = Shipment.ShipmentItem.ShipmentItemNumber;
        //         ShipWrpr.TrackingNo = Shipment.Tracking_Number__c;
        //         Shipments.add(ShipWrpr);
        //         numOfShip++;
        //     }
        //     OrdLines.Id = ordItem.Id;
        //     OrdLines.Name = ordItem.Product2.name;
        //     OrdLines.UP = ordItem.UnitPrice;
        //     OrdLines.Shiped = ordItem.Quantity_Shipped__c;
        //     OrdLines.Ordered = ordItem.Quantity_Ordered__c;
        //     OrdLines.Mat = ordItem.Material_Number__c;
        //     OrdLines.shipments =Shipments;
        //     OrdLines.OrderLineDet = 'Order Lines';
        //     OrdLines.ShipmentDet = ' Shipment (' + numOfShip + ')';
        //     OrdLines.OrdLineHasShipments = (numOfShip == 0 ? false : true);
        //     ords.add(OrdLines);
        // }
        System.debug('getOrderLineWithShipment::Ord:' + ords);

        return ords;
}
public class OrderWrapper {
    @AuraEnabled public String Id;
    @AuraEnabled public String Name;
    @AuraEnabled public Decimal UP;
    @AuraEnabled public Decimal Shiped;
    @AuraEnabled public Decimal Ordered;
    @AuraEnabled public String Mat;
    @AuraEnabled public Decimal Ext;
    @AuraEnabled public String ShipmentDet;
    @AuraEnabled public String OrderLineDet;
    @AuraEnabled public Boolean OrdLineHasShipments;
    @AuraEnabled public List<ShipWrapper> shipments;

}
public class ShipWrapper {
    @AuraEnabled public String Id;
    @AuraEnabled public String No;
    @AuraEnabled public Decimal Qty;
    @AuraEnabled public String TrackingNo;
   
}

}