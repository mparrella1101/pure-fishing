@isTest(seeAllData=true)
public class TestEpM2Customer {
	static testMethod void testGetM2CustomerById() {
		Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, new EpM2CustomerMock());
        String item = '{"items":[{"increment_id":"CAB1031","store_id":1035,"store_name":"Caboodles","customer_id":"5569273299104","customer_is_guest":0,"created_at":"2021-11-09 14:17:05","base_grand_total":"20.00","base_discount_amount":"0.00","tax_amount":"0.00","base_shipping_amount":0,"base_subtotal":"20.00","base_subtotal_incl_tax":20,"entity_id":4322640658592,"id":4322640658592,"shipping_description":"","payment":{"cc_type":"","cc_owner":"","cc_last4":"","cc_exp_month":"","cc_exp_year":"","last_trans_id":"","base_currency_code":"","method":"!"},"coupon_code":"","billing_address":{"city":"Elizabethtown","country_id":"US","postcode":"17022","region":"Pennsylvania","region_code":"PA","street":["133 Rouen Place",""],"telephone":"+17176395591","firstname":"william 2","lastname":"leisey 2"},"extension_attributes":{"shipping_assignments":[{"shipping":{"address":{"city":"Elizabethtown","country_id":"US","postcode":"17022","region":"Pennsylvania","street":["133 Rouen Place",""],"telephone":"+17176395591","firstname":"william 2","lastname":"leisey 2"}}}]},"items":[{"qty_ordered":1,"price":"20.00","tax_amount":0,"item_id":7119690104992,"sku":"CAB707006","discount_percent":0}]}]}';
        EpM2Sync.getCustomerById(item, '5468703293600', 'orx161y845xry84i4b219fuy6ig70fjr', '00000000', '2/24/2019 11:20 AM', 'https://pfbridge.simple105.com/salesForceEndpoint.php', 'logsdebug@gmail.com');
        Test.stopTest();   
    }
}