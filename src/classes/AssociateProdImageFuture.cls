/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           06/03/2022            Initial Version
 */
// This Class is used for Product image association
global class AssociateProdImageFuture {
   //public static HttpCalloutMock mock = null;
  @future(callout=true)
    public static void AssociateProdImage(String conDoc){
        AuthorizeNetPayment__mdt AuthInfo = AuthorizeNetPayment__mdt.getInstance('Autherization_Net');
        String webStoreId=AuthInfo.WebstoreId__c;
		String cmsWorkspaceId=AuthInfo.CMSWorkSpaceId__c;
		HttpRequest httpRequest = new HttpRequest();
        Http http = new Http();
        HttpResponse res = new HttpResponse();
		httpRequest.setMethod('POST');
        httpRequest.setHeader(
            'Content-Type',
            'application/json;  charset=utf-8'
        );
        httpRequest.setEndpoint(
            'callout:CallMeBack/services/data/v54.0/commerce/management/webstores/'+ webstoreId +
            '/product-import'
        );
		String Req='{ "importConfiguration":{"importSource":{"contentVersionId":"'+conDoc+'"},"importSettings":{"media":{"cmsWorkspaceId":"'+cmsWorkspaceId+'"}}}}';

        httpRequest.setBody(Req);
        if (!Test.isRunningTest()){
            httpRequest.setTimeout(120000);
        	res = http.send(httpRequest);
        }
        boolean redirect = false;
        if((res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306) || Test.isRunningTest()) {
            do {
                system.debug('AssociateProdImage Redirected');
                redirect = false; // reset the value each time
                String loc = '';
                if (Test.isRunningTest()) {
                    loc = 'callout:CallMeBack/services/data/v54.0/commerce/management/webstores/'+ webstoreId + '/product-import';
                } else {
                    loc = res.getHeader('Location'); // get location of the redirect
                }
                if(loc == null) {
                	continue;
                }
                // httpRequest = new HttpRequest();
                httpRequest.setEndpoint(loc);
                httpRequest.setMethod('POST');
                if (!Test.isRunningTest()){
                	res = http.send(httpRequest);
                } 
                if(res.getStatusCode() != 500) { // 500 = fail
                    if((res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306)) {
                        redirect= true;
                    }
                }
            } while (redirect && Limits.getCallouts() != Limits.getLimitCallouts());
        }
system.debug(res.getBody());
        Integer sc = Test.isRunningTest()? 201 :  res.getStatusCode();
        String Resbody =Test.isRunningTest()? req :  res.getBody();
        System.debug('sc' + sc + 'Resbody'+ Resbody + 'Req' + Req);
        /*if (Test.isRunningTest() && (mock!=null)) {
           mock.respond(httpRequest);
        } else {
            Http h = new Http();
           h.send(httpRequest);
        }*/
      
    }
}