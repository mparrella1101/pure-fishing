public class B2B_CreatePriceList {
    public Static void CreatePriceList(List<Id> ProdId)
    {
        //ProdId=01t8B000002kVjpQAE;
        List<CurrencyType> ISOCurrCode = new List<CurrencyType>();
        List<PricebookEntry> PBEList= new List<PricebookEntry>();
        Id StdPricebukid;
        If (ProdId!=null)
        {
            if(test.isRunningTest())
			StdPricebukid=Test.getStandardPricebookId();
			else
            StdPricebukid=[Select id from Pricebook2 where IsStandard =true ].Id;
            ISOCurrCode = [Select IsoCode from CurrencyType];
            list<Product2> ProdDetails=[Select Material__c,IsActive from Product2 where Id IN :ProdId];
            for(Product2 prod :ProdDetails)
            {
                  for(CurrencyType CT: ISOCurrCode) 
                  {
                    PricebookEntry PBE = new PricebookEntry();
                    PBE.CurrencyIsoCode=CT.IsoCode;
                    PBE.Pricebook2Id=StdPricebukid;
                    PBE.Product2Id=prod.Id;
                    PBE.UnitPrice=0.00;
                    PBE.IsActive=prod.IsActive;
                    PBE.SAP_ExternalID__c='STD'+'~'+CT.IsoCode+'~'+prod.Material__c;
                    PBEList.add(PBE);
                  }
            }
            if(PBEList.size()>0)
            Insert PBEList;

        }
    }
}