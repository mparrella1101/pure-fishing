public class CurrentProductRecordIdController{
public String currentRecordId {get;set;}
public Product2 prod{get;set;}
 
    public CurrentProductRecordIdController(ApexPages.StandardController controller) {
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        prod = [select DAM_Set__c from Product2 where id =: currentRecordId ];
        system.debug('currentRecordId ' + currentRecordId + 'prod' + prod.DAM_Set__c);
    }
}