global class EpM2StoreViewsSync {
    
    @future(callout=true)
    public static void getStoreViewsFuture(String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {
        getStoreViews(token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
    }
    
    public static void getStoreViews(String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail) {

        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setEndpoint(baseURL+'store/storeViews');
        request.setHeader('authorization', 'Bearer ' + token);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('GET');
        
        try {
            response = http.send(request);

            String jsonString = response.getBody();
            System.debug('EpM2StoreViewsSync - Response: ' + jsonString); 
            
            if(response.getStatusCode() == 200) {
                List<StoreView> storeViews = (List<StoreView>) System.JSON.deserialize(jsonString, List<StoreView>.class);
                for (StoreView storeView : storeViews) {
                    System.debug(storeView.id+'||'+storeView.name);
                    String store_name = storeView.name.substringBefore('Store View').trim();
                    AccountHandler.insertAccountIfNotExists(storeView.id, store_name, eComStoreID);
                }
            } else {
                Notifier.sendEmail('EpM2StoreViewsSync - getStoreViews - ' + response.getStatus()+'|'+response.toString(), supportEmail);
            }
        } catch(System.Exception e) {
            System.debug('EpM2StoreViewsSync - getStoreViews - Exception error: '+ e);
            System.debug(response.toString());
            Notifier.sendEmail('EpM2StoreViewsSync - getStoreViews - Exception error: '+ e, supportEmail);
        }
    }
}