@isTest(seeAllData=true)
public class TestECRecordManager {
    static testMethod void testECRecordManager() {
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EpM2UpdateShipmentMock());
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        request.requestUri ='/services/apexrest/ECRecordsManager';
        request.httpMethod = 'POST';
        RestContext.request = request;
        RestContext.response = response;
        
		//Test.startTest();
        //String invId = ECRecordsManager.createECRecord('[{"items":[{"invoice_no":"456876","item_id":"CAB53020F","line_item_id":1.0,"invoice_qty":1.0}],"order_id":1418261}]','Invoice Update');
        String shpId = ECRecordsManager.createECRecord('[{"items":[{"tracking_no":"1ZEVR4455HHHSAAA","item_id":"CAB53020F","line_item_id":1.0,"ship_qty":1.0}],"order_id":1418261}]','Shipment Update');
        //String strId = ECRecordsManager.createECRecord('[{"items":[{"tracking_no":"1ZEVR4455HHHSAAA","item_id":"CAB56260M","line_item_id":1.0,"ship_qty":1.0}],"order_id":1210462}]','Shipment Update');
        Test.stopTest();        
    }
}