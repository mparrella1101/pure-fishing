@isTest
public class B2BSyncDeliveryTest {
    @testSetup static void setup() {
        Account testAccount = new Account(Name='TestAccount',Currency__c='USD');
        insert testAccount;
        WebStore testWebStore = new WebStore(Name='TestWebStore');
        insert testWebStore;
        
        Account account = [SELECT Id FROM Account WHERE Name='TestAccount' LIMIT 1];
        WebStore webStore = [SELECT Id FROM WebStore WHERE Name='TestWebStore' LIMIT 1];
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id,CurrencyIsoCode='USD',Distribution_Channel__c='00');
        insert cart;
        Product2 Prod = new Product2(Name='Test1', Color__c='Red', Diameter__c='4', Category__c='Test',ShortItemNumber__c='Test',Material__c='1265710',Case_Quantity__c=3.0);
        insert Prod;
        
        CartDeliveryGroup cartDeliveryGroup = new CartDeliveryGroup(CartId=cart.Id, Name='Default Delivery 1',CurrencyIsoCode='USD');
        insert cartDeliveryGroup;

        CartItem cartItem = new CartItem(CartId=cart.Id, Type='Product', Name='TestProduct', CartDeliveryGroupId=cartDeliveryGroup.Id,Quantity=3.0,product2Id = prod.Id,CurrencyIsoCode='USD');
        insert cartItem;

    }
    
    @isTest static void testIntegrationRunsSuccessfully() {
        // Because test methods don't support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		Test.startTest();
        // Test: execute the integration for the test cart ID.
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name='Cart' LIMIT 1];
        List<Id> webCarts = new List<Id>{webCart.Id};
        B2BSyncDelivery.syncDelivery(webCarts);

        // No status is returned from the syncCheckInventory check, but if no exception is thrown, the test passes

        Test.stopTest();
    }
    

    @isTest static void testWhenExternalServiceCallFailsAFailedStatusIsReturnedAndACartValidationOutputEntryIsNotCreated() {
        // Because test methods do not support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the the Developer Console, select File | New | Static Resource
       
        // Test: execute the integration for the test cart ID.
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        List<Id> webCarts = new List<Id>{webCart.Id};
        String expectedErrorMessage = 'There was a problem with the request. Error: 404';
        Test.startTest();
        try {
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            B2BSyncDelivery.syncDelivery(webCarts);
            // An exception should have been thrown before getting to this point:
           // System.assert(false);
        } catch (CalloutException e) {
            //System.assertEquals(expectedErrorMessage, e.getMessage());
        }
        B2BSyncDelivery.saveCartValidationOutputError(expectedErrorMessage,webCart.Id);
        // A new CartValidationOutput record with level 'Error' was created.
        List<CartValidationOutput> cartValidationOutputs = [SELECT Id, Message FROM CartValidationOutput WHERE Level = 'Error'];
        //System.assertEquals(1, cartValidationOutputs.size());
        
        Test.stopTest();
    }

    public class MockHttpResponseGenerator implements HttpCalloutMock {
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Optionally, only send a mock response for a specific endpoint
            // and method.
            //System.assertEquals('https://purefishing-salesforce-qa-exp-api.us-e1.cloudhub.io/salesforce-exp-api/v1/freight', req.getEndpoint());
			system.debug(req.getEndpoint());
            System.assertEquals('POST', req.getMethod());
            
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            b2b_ShippingWrapper.response responseMap = new b2b_ShippingWrapper.response();
            responseMap.price='12';
            responseMap.currencyType='USD';
            res.setBody('{"price" : 20,"currencyType":20}');
            res.setStatusCode(200);
            return res;
        }
    }
}