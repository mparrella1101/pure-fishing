@isTest
private class SetSAPOrdersPricebookBatchTest {
    @isTest
    static void makeData(){
        Account testAccount = CCTestUtils.createAccount('Coastal Cloud', false);
        testAccount.Division_SAP__c = '12';
        testAccount.SAP_Customer__c = '1234';
        testAccount.Distribution_Channel__c = '12';
        testAccount.Sales_Org__c = '1234';
        insert testAccount;
        PriceBook2 testStandardPricebook = CCTestUtils.createPriceBook(true, 'test pricebook', true, 'USD', true, false);
        update testStandardPricebook;
        Order testOrder = CCTestUtils.createOrder(testAccount, testStandardPricebook, false);
        testOrder.Status = 'Draft';
        List<Order> testOrderList = new List<Order>();
        testOrderList.add(testOrder);
        insert testOrderList;
        Product2 testSimpleProduct = CCTestUtils.createProduct('test simple product', '123SIMP', '3456789', true, false);
        ProductCatalog testProductCatalog = CCTestUtils.createProductCatalog('test product catalog', true);
        ProductCategory testVariationProductCategory = CCTestUtils.createProductCategory('Variation', testProductCatalog, null, null, false);
        ProductCategory testSimpleProductCategory = CCTestUtils.createProductCategory('Simple', testProductCatalog, null, null, false);
        insert new List<ProductCategory>{testVariationProductCategory, testSimpleProductCategory};
        ProductCategoryProduct testSimplePCP = CCTestUtils.createProductCategoryProduct(testSimpleProduct, testSimpleProductCategory, true);
        PricebookEntry testPricebookEntry = CCTestUtils.createPriceBookEntry(testSimpleProduct, testStandardPricebook, true, true);
        OrderItem testOrderItem = CCTestUtils.createOrderItem(testOrder, testSimpleProduct, testPricebookEntry, true);
        for(Order order : testOrderList){
            order.Status = 'Submitted to SAP';
        }
        update testOrderList;
        
		Test.startTest();
        SetSAPOrdersPricebookBatch testBatch = new SetSAPOrdersPricebookBatch(testOrderList);
        // SetSAPOrdersPricebookBatch testBatch = new SetSAPOrdersPricebookBatch(testOrder.Id);
        Database.executeBatch(testBatch);
        Test.stopTest();
        
        Order queriedOrder = [SELECT Id, Pricebook2Id FROM Order LIMIT 1];
        List<OrderItem> queriedOrderItems = [SELECT Id, OrderId FROM OrderItem WHERE OrderId = :queriedOrder.Id];
        System.assertEquals(null, queriedOrder.Pricebook2Id, 'We expect our pricebook to have been set to null on our order header.');
        System.assertEquals(1, queriedOrderItems.size(), 'We expect to still have the same number of order lines associated to this order after the batch has completed.');

    }
}