/**
 * Apex Controller for the 'QuickorderUpload' LWC
 *
 *
 * Version      Author                  Company                 Date                Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           Mar 16, 2022        Initial version
    1.1         Matt Parrella           Coastal Cloud           Aug 24, 2022        Adding functionality to only delete CartItems that were added via Upload when an error occurs
 */

public class QuickOrderUploadController {

    private static final Integer MAX_UPLOAD_ROWS = 500;

    @AuraEnabled(Cacheable=true)
    public static String getTemplateId(String communityId){
        String usrLang = UserInfo.getLanguage();
        String webstoreId = ConnectAPIUtils.getWebstoreId(communityId);
        String baseURL = URL.getOrgDomainUrl().toExternalForm(); // Need this or the file is not found (my.site.com vs my.salesforce.com in the domain)
        switch on usrLang {
            when 'fr' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_Fr__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_Fr__c;
            }
            when 'fi' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_FI__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_FI__c;
            }
            when 'da' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_DA__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_DA__c;
            }
            when 'de' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_DE__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_DE__c;
            }
            when 'sv' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_SV__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_SV__c;
            }
            when 'it' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_IT__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_IT__c;
            }
            when 'nl_NL' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_NL__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_NL__c;
            }
            when 'no' {
                return  baseURL + [SELECT Quick_Order_Upload_Template_NO__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template_NO__c;
            }
            when else {
                return baseURL + [SELECT Quick_Order_Upload_Template__c FROM WebStore WHERE Id =: webstoreId LIMIT 1]?.Quick_Order_Upload_Template__c;
            }
        }
    }

    /**
     * @description This method will be used to get the 'cartId', 'webstoreId', and 'maxUploadRows' attribute data, using the ConnectAPI.CommerceCart
     * API.
     *
     * @param userId The current user's 18-character Salesforce User Id
     * @param effectiveAccountId The current user's 18-character Salesforce Account record Id
     * @param communityId The 18-character Salesforce Id that identifies the current Experience (community)
     * @param webstoreId The 18-character Salesforce WebStore Id
     *
     * @return responseMap (Map<String,Object>) A map containing the attribute name as keys and their corresponding values (as the values within the map)
     */
    @AuraEnabled(Cacheable=false)
    public static Map<String,Object> getInfo(String userId, String effectiveAccountId, String communityId, String webstoreId){
        Map<String,Object> responseMap = new Map<String,Object>();
        if (webstoreId == null) {
            webstoreId = ConnectAPIUtils.getWebstoreId(communityId);
        }

        // If AccountId is NULL, retrieve it from User record
        if (effectiveAccountId == null || effectiveAccountId == ''){
            effectiveAccountId = [SELECT Effective_Account_ID__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1].Effective_Account_Id__c;
        }

        System.debug('webstoreId: ' + webstoreId);
        System.debug('effectiveAccountId: ' + effectiveAccountId);



        // Get CurrencyISOCode from effectiveAccountId, so currencies will be displayed properly
        String currencyCode = [SELECT CurrencyISOCode FROM Account WHERE Id =: effectiveAccountId LIMIT 1]?.CurrencyIsoCode;
        String cartId = null;


        System.debug('currency code: ' + currencyCode);

        // Attempt to get the Cart Id
        if (!Test.isRunningTest()){
            if (effectiveAccountId != null && effectiveAccountId != '000000000000000000'){
                ConnectApi.CartSummary cart = ConnectApi.CommerceCart.getOrCreateActiveCartSummary(webstoreId, effectiveAccountId, 'active', currencyCode);
                cartId = cart.cartId;
                System.debug('cart: ' + cart);
            }
        }

        // Populate the Respone Map with this data
        responseMap.put('cartId', cartId);
        responseMap.put('webstoreId', webstoreId);
        responseMap.put('maxUploadRows', MAX_UPLOAD_ROWS);
        responseMap.put('effectiveAccountId', effectiveAccountId);
        responseMap.put('currencyCode', currencyCode);

        return responseMap;
    }

    /**
     * @description This method is used to process the parsed data from within the uploaded CSV file. It will attempt to retrieve products based
     * on the following pieces of information (in order): Material #, UPC Code, or Model #
     *
     * @param uploadData The data parsed from the uploaded CSV file
     * @param userId The Current User's 18-character Salesforce User record Id
     * @param webstoreId The 18-character Salesforce Id representing the Web Store within the Community
     * @param effectiveAccountId The 18-character Salesforce Id of the current user's Account
     * @param cartId The 18-character Salesforce Id representing the Cart object within the Community
     *
     * @return (Map<String,Object>) A Map containing information about the total successful additions and unsuccessful additions
     */
    @AuraEnabled
    public static Map<String,Object> processData(String uploadData, String userId, String webstoreId, String effectiveAccountId, String cartId) {
        Map<String, Object> responseMap = new Map<String, Object>();
        List<String> searchTerms = new List<String>(); // holds search terms pulled from Material # / Model # / UPC fields of uploaded CSV file
        Map<String,QuickOrderUploadUtils.ImportItem> itemsMap = new Map<String, QuickOrderUploadUtils.ImportItem>();
        List<QuickOrderUploadUtils.ImportItem> invalidItems = new List<QuickOrderUploadUtils.ImportItem>(); // holds all search terms that did not produce a result from the product search endpoint

        try {
            // Deserialize data into wrapper objects
            List<UploadedItem> uploadedItems = (List<UploadedItem>) JSON.deserialize(uploadData, List<QuickOrderUploadController.UploadedItem>.class);

            // Iterate over parsed data and build list of search terms with a max length of 32 words

            String activeCartOrId = cartId;

            List<ConnectApi.CartItemInput> linesToAdd = new List<ConnectApi.CartItemInput>(); // Holds all valid cart items
            List<ConnectApi.BatchInput> batchInputList = new List<ConnectApi.BatchInput>(); // When we have > 32 search terms, we will split it in to batches and store them here for processing

            ConnectApi.BatchResult[] batchResults = null;

            // Create items list
            itemsMap = QuickOrderUploadUtils.createInputItems(uploadedItems);

            // Build search terms list
            for (QuickOrderUploadUtils.ImportItem item : itemsMap.values()){
                if (item.material != null && item.material != ''){
                    searchTerms.add(item.material);
                }
                if (item.upc != null && item.upc != '') {
                    searchTerms.add(item.upc);
                }
                if (item.model != null && item.model != '') {
                    searchTerms.add(item.model);
                }
            }

            // Check if the size of the list is more than the allotted number of uploaded rows
            if (itemsMap.size() > MAX_UPLOAD_ROWS){
                QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message(
                        'Too many rows', 'There can be a max of ' + MAX_UPLOAD_ROWS + ' uploaded rows.', QuickOrderUploadUtils.ERROR, 0
                ), 0, responseMap);

                QuickOrderUploadUtils.updateResponseMapTotalValue('totErrors', 1, responseMap);

                throw new CustomException('Too many rows');
            }

            Map<String,String> validSearchTerms = QuickOrderUploadUtils.productSearch(searchTerms, webstoreId, effectiveAccountId, 50, itemsMap);
            invalidItems = verifySearchTerms(itemsMap.values(), validSearchTerms, responseMap);

            if (!invalidItems.isEmpty()) {
                QuickOrderUploadUtils.updateResponseMapTotalValue('totErrors', invalidItems.size(), responseMap);
                processInvalidItems(invalidItems, responseMap);

                QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message('Row(s) contain incorrect product data', 'Please double check' +
                        ' the values entered into the spreadsheet', QuickOrderUploadUtils.ERROR, 0), 0, responseMap);
                throw new CustomException('Invalid data');
            }

            linesToAdd = createCartItems(itemsMap.values(), responseMap);

            // MParrella | 08-24-2022 | Adding Product2Ids that have been added to the Cart to responseMap: START
            List<String> cartProductIds = new List<String>();
            for (ConnectApi.CartItemInput ci : linesToAdd) {
                cartProductIds.add(ci.productId);
            }
            responseMap.put('addedProducts', cartProductIds);
            // MParrella | 08-24-2022 | Adding Product2Ids that have been added to the Cart to responseMap: STOP

            batchInputList = createBatchInputLines(linesToAdd, responseMap);

            if (batchInputList.size() <= QuickOrderUploadUtils.ADD_TO_CART_MAX) {
                if (!Test.isRunningTest()) {
                    batchResults = QuickOrderUploadUtils.addItemsToCart(webstoreId, effectiveAccountId, activeCartOrId, batchInputList);
                    QuickOrderUploadUtils.processBatchResults(batchResults, itemsMap.values(), responseMap, webstoreId, effectiveAccountId, cartId);
                    responseMap.put('totItems', batchInputList.size());
                } else {
                    QuickOrderUploadUtils.processBatchResults(batchResults, itemsMap.values(), responseMap, webstoreId, effectiveAccountId, cartId);
                    responseMap.put('totItems', linesToAdd.size());
                }
            } else {
                List<QuickOrderUploadUtils.ImportItem> processedItemsList = addItemsToCartInBatches(webstoreId, effectiveAccountId, activeCartOrId, batchInputList, itemsMap.values(), responseMap);

                // Update the itemsMap
                for (QuickOrderUploadUtils.ImportItem item : processedItemsList){
                    String identifier = (item.material != null && item.material != '') ? item.material : (item.upc != null && item.upc != '') ? item.upc : (item.model != null && item.model != '') ? item.model : '';

                    itemsMap.put(identifier, item);
                }
            }
            Integer totErrors = (Integer) responseMap.get('totErrors');
            if (totErrors > 0) {
                processAddToCartErrors(itemsMap.values(), responseMap);
            }
        }
        catch(CustomException ce) {}
        catch(Exception ex){
            QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message('Processing Exception', ex.getMessage(), QuickOrderUploadUtils.ERROR, 0), 0, responseMap);
            QuickOrderUploadUtils.updateResponseMapTotalValue('totErrors', 1, responseMap);
        }
        finally {
            Integer totErrors = (Integer) responseMap.get('totErrors');
            Integer totSuccess = (Integer) responseMap.get('totSuccess');

            if (totErrors > 0) {
//                QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message('Processing Exception', totErrors.format(), QuickOrderUploadUtils.ERROR, 0), 0, responseMap);
            }

            if (totSuccess > 0 && totErrors == 0) {
                QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message('Processing Complete', totSuccess.format(), QuickOrderUploadUtils.SUCCESS, 0), 0, responseMap);
            }

            List<QuickOrderUploadUtils.Message> retMessages = (List<QuickOrderUploadUtils.Message>)responseMap.get('messages');

            String messagesJson = JSON.serialize(retMessages);
            responseMap.put('messagesJson', messagesJson);
        }
        return responseMap;
    }

    /**
     * @description Method to add the items to the cart synchronously, in bundles of 100 items
     *
     * @param webstoreId The 18-character Salesforce Id representing the Web Store within the Community
     * @param effectiveAccountId The 18-character Salesforce Id of the current user's Account
     * @param activeCartOrId The 18-character Salesforce Id representing the Cart object within the Community
     * @param batchInputList The list of BatchInput objects that need to be processed
     * @param itemsList The list of ImportItems objects that did not have a matching product
     * @param responseMap The ResponseMap whose attributes will be updated
     *
     * @return A list of ImportItem objects
     */
    @TestVisible
    private static List<QuickOrderUploadUtils.ImportItem> addItemsToCartInBatches(String webstoreId,
            String effectiveAccountId,
            String activeCartOrId,
            List<ConnectApi.BatchInput> batchInputList,
            List<QuickOrderUploadUtils.ImportItem> itemsList,
            Map<String, Object> responseMap) {

        List<QuickOrderUploadUtils.ImportItem> processedItemsList = QuickOrderUploadUtils.processBatchInputItems(webstoreId, effectiveAccountId, activeCartOrId, batchInputList, itemsList, responseMap);
        Integer totBatches = (Integer)responseMap.get('totBatches');

        responseMap.put('batchCount', totBatches);

        return processedItemsList;

    }


    /**
     * @description This method will iterate over the items that did not have a matching product and will add error messages to the
     * response map so we can display to the user.
     *
     * @param itemsList The list of ImportItems objects that did not have a matching product
     * @param responseMap The ResponseMap whose attributes will be updated
     */
    @TestVisible
    private static void processInvalidItems(List<QuickOrderUploadUtils.ImportItem> itemsList, Map<String,Object> responseMap) {
        for (QuickOrderUploadUtils.ImportItem item : itemsList) {
            if (!item.isValid) {
                String errorMsg = 'ROW ' + item.row + ': Could not locate the product with the following information: <br/>Material: ' + item.material + ' <br/>UPC: ' + item.upc + ' <br/>Model: ' + item.model;
                QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message('Product Not Found', errorMsg, QuickOrderUploadUtils.ERROR, item.row), 0, responseMap);
            }
        }
    }

    /**
     * @description Helper method that will verify that there is a corresponding product for the provided Material #, Model #, or UPC #. If an item does
     * not have a corresponding product, it will be returned within a list.
     *
     * @param itemsList A list of ImportItem objects representing the rows within the uploaded CSV file
     * @param validSearchTerms A Map of all Items that have a corresponding Product Id
     * @param responseMap The Response Map that will have it's attributes updated
     *
     * @return A list of search terms that did not have a corresponding product
     */
    public static List<QuickOrderUploadUtils.ImportItem> verifySearchTerms(List<QuickOrderUploadUtils.ImportItem> itemsList, Map<String,String> validSearchTerms, Map<String,Object> responseMap){
        List<QuickOrderUploadUtils.ImportItem> invalidItems = new List<QuickOrderUploadUtils.ImportItem>();
        for (QuickOrderUploadUtils.ImportItem item : itemsList){
            if (validSearchTerms.containsKey(item.uniqueKey)){
                item.productId = validSearchTerms.get(item.uniqueKey);
                item.isValid = true;
            } else {
                item.isSuccess = false;
                item.errorMsg = 'Could not locate a Product based on the provided information.';
                invalidItems.add(item);
            }
        }
        responseMap.put('totValidItems', validSearchTerms.size());
        responseMap.put('totInvalidItems', invalidItems.size());
        return invalidItems;
    }

    /**
     * @description Method to add the valid items to the cart
     *
     * @param itemsList The list of ImportItem objects representing the rows within the uploaded CSV file
     * @param responseMap The ResponseMap that is to be updated with any error information
     *
     * @return A list of ConnectApi.CartItemInput objects
     */
    public static List<ConnectApi.CartItemInput> createCartItems(List<QuickOrderUploadUtils.ImportItem> itemsList, Map<String,Object> responseMap){
        List<ConnectApi.CartItemInput> linesToAdd = new List<ConnectApi.CartItemInput>();

        // Iterate over passed-in list and add those that have a 'isValid' parameter value of TRUE


        for (QuickOrderUploadUtils.ImportItem item : itemsList) {
            if (item.isValid){
                ConnectApi.CartItemInput lineToAdd = new ConnectApi.CartItemInput();

                lineToAdd.productId = item.productId;
                lineToAdd.quantity = String.valueOf(item.qty);
                lineToAdd.type = ConnectApi.CartItemType.Product;

                linesToAdd.add(lineToAdd);
            }
        }
        return linesToAdd;
    }

    /**
     * @description Method to create a list of BatchInput objects for adding items to the cart
     *
     * @param linesToAdd The list of ImportItem objects representing the rows within the uploaded CSV file
     * @param responseMap The responseMap to be updated with any errors
     *
     * @return A list of BatchInput objects
     */
    public static List<ConnectApi.BatchInput> createBatchInputLines(List<ConnectApi.CartItemInput> linesToAdd, Map<String,Object> responseMap) {
        List<ConnectApi.BatchInput> batchInputList = new List<ConnectApi.BatchInput>();

        for (ConnectApi.CartItemInput item : linesToAdd) {
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(item);
            batchInputList.add(batchInput);
        }
        return batchInputList;
    }

    /**
     * @description Method to extract error messages encountered when adding products to the cart
     *
     * @param itemsList The list of ImportItem objects that represent rows from the uploaded CSV file
     * @param responseMap The responseMap whose attributes will be updated w/ error information
     */
    @TestVisible
    private static void processAddToCartErrors(List<QuickOrderUploadUtils.ImportItem> itemsList,
            Map<String, Object> responseMap) {
        Boolean errorPresent = false;
        Boolean runOnce = false;
        for(QuickOrderUploadUtils.ImportItem item : itemsList) {
            if(item.isValid && !item.isSuccess) {
                // only want to add this generic message once, to be used as the high-level error text

                if (!runOnce){
                    QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message(
                            'Error Adding the following Product(s) to Cart',
                            'Please refer to error details below.',
                            QuickOrderUploadUtils.WARN,
                            0),
                            0, responseMap);
                    runOnce = true;
                }
                if (item.errorMsg != null){
                    String errorMsg = 'ROW ' + item.row + ': ' + (item.material != null ? item.material : item.upc != null ? item.upc : item.model != null ? item.model : '') + ': ' + item.errorMsg;
                    QuickOrderUploadUtils.addMessage(new QuickOrderUploadUtils.Message(
                            '',
                            errorMsg,
                            QuickOrderUploadUtils.WARN,
                            item.row),
                            null, responseMap);
                    errorPresent = true;
                }
            }
        }
    }





    /*  Wrapper Classes: START  */
    public class UploadedItem {
        @AuraEnabled public String material;
        @AuraEnabled public String model;
        @AuraEnabled public Integer quantity;
        @AuraEnabled public String upc;
        public UploadedItem() {
            material = '';
            model = '';
            quantity = 0;
            upc = '';
        }
    }

    public class CustomException extends Exception {}

    /*  Wrapper Classes: STOP  */

}