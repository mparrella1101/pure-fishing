/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
1.0         Arsee Khan           Coastal Cloud           07/11/2022            Initial Version
*/
// This Class is used for order Update
global with sharing class B2B_UpdateOrders 
{
    
 public Static void UpdateOrders(List<Id> ordId)
    {
        Integer j=0;
        String AutoString;
        String OrdRef;
        List<OrderItem> updOrdItems=new List<OrderItem>();

        System.debug('B2B_UpdateOrders:UpdateOrders' );
    try{
            If (ordId!=null)
            {
                list<Order> OrdList =[Select Order_Reference__c, Status,(Select id,SF_External_ID__c,type from OrderItems) from Order where id IN :ordId];
                //list<Order> orderList=[SELECT Id, Pricebook2Id , (SELECT PricebookEntryId, Product2Id, OrderId, ListPrice, UnitPrice, Product2.Material__c, Quantity, Quantity_Ordered__c, SF_External_ID__c, CurrencyIsoCode, Supplied_Line__c, Type FROM OrderItems) FROM Order WHERE Id IN :ordId AND Pricebook2Id != null];
                for(Order ord:OrdList)
                {
                    OrdRef=ord.Order_Reference__c;
                     
                        for(OrderItem OrdItem :ord.OrderItems)
                        {
                            if(OrdItem.SF_External_ID__c==null && OrdItem.type=='Order Product' )
                            {       j++;
                                    AutoString = String.valueOf(j).leftPad(6, '0');
                                    OrdItem.Supplied_Line__c=AutoString;
                                    OrdItem.SF_External_ID__c=OrdRef+'~'+AutoString;
                                    updOrdItems.add(OrdItem);
                            }
                            else if(OrdItem.SF_External_ID__c==null && OrdItem.type !='Order Product')
                            {
                                    AutoString='00000';
                                    OrdItem.Supplied_Line__c=AutoString;
                                    OrdItem.SF_External_ID__c=OrdRef+'~'+AutoString;
                                    updOrdItems.add(OrdItem);
                            }
                            else {
                                
                            }
                        }
                }
                update updOrdItems;
                System.debug('B2B_UpdateOrders:UpdateOrders updOrdItems :'+ updOrdItems );
            }
        }
        catch (Exception e) 
        {
            System.debug('B2B_UpdateOrders Error is :' + e.getMessage() + 'LN:' + e.getLineNumber() );
        }
    }
    
}