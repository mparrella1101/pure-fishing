@isTest
global class EpM2CustomerMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json; charset=utf-8');
        response.setBody('{"id":"5468703293600","email":"william@simple101.com","firstname":"William","lastname":"Leisey","phone":null,"addresses":[{"region":{"region_code":"PA","region":"Pennsylvania","region_id":0},"region_id":0,"country_id":"US","street":["133 Rouen Place",""],"telephone":"+17176395591","postcode":"17022","city":"Elizabethtown","firstname":"William","lastname":"Leisey","default_shipping":0}]}}');
        response.setStatusCode(200);
        response.setStatus('Success');
        return response;
    }
}