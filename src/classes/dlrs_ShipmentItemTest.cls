/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 * * Modified by SM to add "Schema" due to issue with ShipmentItem object
 **/
@IsTest
private class dlrs_ShipmentItemTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_ShipmentItemTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Schema.ShipmentItem());
    }
}