@isTest(seeAllData=true)
public class TestECRecords {
	static testMethod void testCustomerCreateRecord() {
        ECRecord__c record = new ECRecord__c();
        record.Dump__c = '{"id":79,"group_id":1,"default_shipping":"93","created_at":"2018-07-18 14:26:21","updated_at":"2018-07-18 14:29:09","created_in":"Caboodles Store View","email":"ben.pierce@publicishawkeye.com","firstname":"Ben","lastname":"Pierce","store_id":4,"website_id":4,"addresses":[{"id":93,"customer_id":79,"region":{"region_code":"TX","region":"Texas","region_id":57},"region_id":57,"country_id":"US","street":["2828 Routh Street, Suite 300"],"telephone":"7049951175","postcode":"75201","city":"Dallas","firstname":"Ben","lastname":"Pierce","default_shipping":true}],"disable_auto_group_change":0}';
        record.MagentoAPIStatus__c = true;
        record.MagentoExecutionTime__c = DateTime.now();
        record.MagentoResponse__c = 'Success';
        record.Type__c = 'Customer Create';
        insert record;
		Test.startTest();

        Test.stopTest();        
    }
}