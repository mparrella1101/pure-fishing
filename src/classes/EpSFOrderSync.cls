public class EpSFOrderSync {
    public static void createSFOrder(ECRecord__c record) {
        ApexGovernorLimits.printMaxLimits();
        
            System.debug('Dump:'+record.Dump__c);
            SalesOrderItem item = (SalesOrderItem) System.JSON.deserialize(record.Dump__c, SalesOrderItem.class);
            
            Account account = AccountHandler.getAccount(item.store_id, record.EComStoreID__c);
            
            Order order = new Order();

            if(account != NULL)
            {
                order.AccountId = account.Id;
            }
            order.MagentoCustomerId__c = (String)item.customer_id;
            order.EComStoreID__c = record.EComStoreID__c;
            order.Customer_Is_Guest__c = item.customer_is_guest;
            
            order.ECRecordName__c = record.Name;
            order.EffectiveDate = Date.valueOf(item.created_at);
            order.Grand_Total__c = item.base_grand_total;
            order.Discount_Amount__c = item.base_discount_amount;
            order.Tax_Amount__c = item.tax_amount;
            order.Shipping_Amount__c = item.base_shipping_amount;
            order.Sub_Total__c = item.base_subtotal;
            order.Sub_Total_Including_Tax__c = item.base_subtotal_incl_tax;
            
            order.Magento_Order_Number__c = item.increment_id;
            order.MagentoId__c = item.entity_id;
            //order.AccountId = '0012F000008Tk7fQAC';
            order.Status = 'Draft';
            
            order.Shipping_Method__c = item.shipping_description;
            
            order.CC_TYPE__c = item.payment.cc_type;
            order.CC_OWNER__c = item.payment.cc_owner;
            order.CC_LAST4__c = item.payment.cc_last4;
            order.CC_EXP_MONTH__c = item.payment.cc_exp_month;
            order.CC_EXP_YEAR__c = item.payment.cc_exp_year;
            order.LAST_TRANSACTION_ID__c = item.payment.last_trans_id;
            order.Currency_code__c = item.base_currency_code;
            order.Payment_Method__c = item.payment.method;
                
            if(item.payment.additional_information !=null && item.payment.additional_information.size() > 0)
            {
                for(Integer size=0; size<item.payment.additional_information.size(); size++) {
                    String auth_code = item.payment.additional_information.get(size);
                    if(auth_code!=null && auth_code.length()>4 && auth_code.length()<7 ) {
                        order.Auth_Code__c = item.payment.additional_information.get(size);
                        break;
                    } 
                }
                if(item.payment.method == 'payflow_express') {
                    order.PNREF01__c = item.payment.additional_information.get(8);
                    order.PNREF02__c = item.payment.additional_information.get(7);
                    order.PROVID__c = item.payment.additional_information.get(6);
                }
            }
            
            if(item.coupon_code != null)
            {
                order.Coupon_Code__c = item.coupon_code;
            }
            
            order.BillingCity = item.billing_address.city;
            order.BillingCountryCode = item.billing_address.country_id;
            order.BillingPostalCode = item.billing_address.postcode;
            order.BillingState = item.billing_address.region;
            //order.BillingStateCode = 'NY';//item.billing_address.region_code;
            order.BillingStreet = String.join(item.billing_address.street,',');
            order.BillingPhone__c = item.billing_address.telephone;
            order.Billing_First_Name__c = item.billing_address.firstname;
            order.Billing_Last_Name__c = item.billing_address.lastname;
            System.debug('item.billing_address.region:'+item.billing_address.region);
            System.debug('item.billing_address.region_code:'+item.billing_address.region_code);
            
            for(Integer i = 0; i < item.extension_attributes.shipping_assignments.size(); i++) {
                SalesOrderShippingAssignment shipping_assignment = item.extension_attributes.shipping_assignments[i];
                order.ShippingCity = shipping_assignment.shipping.address.city;
                order.ShippingCountryCode = shipping_assignment.shipping.address.country_id;
                order.ShippingPostalCode = shipping_assignment.shipping.address.postcode;
                order.ShippingState = shipping_assignment.shipping.address.region;
                //order.ShippingStateCode = shipping_assignment.shipping.address.region_code;
                order.ShippingStreet = String.join(shipping_assignment.shipping.address.street,',');
                order.ShippingPhone__c = shipping_assignment.shipping.address.telephone;
                order.Shipping_First_Name__c = shipping_assignment.shipping.address.firstname;
            	order.Shipping_Last_Name__c = shipping_assignment.shipping.address.lastname;
            }
			order.Pricebook2Id = '01sU0000000DkcGIAS';
            
            if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                System.debug('Need to stop processing to avoid hitting a governor limit.');
                record.SalesForceObjectStatus__c = false;
                record.SalesForceResponse__c = 'Need to stop processing to avoid hitting a governor limit.';
            } else {
                System.debug('Continue processing. Not going to hit DML governor limits');
                insert order;
                
                ApexGovernorLimits.printLimitsUsed();
                System.debug('EpSFOrderSync:'+order);
    
                if(item.items.size()==0) {
                    Notifier.sendEmail('EpSFOrderSync - createSFOrder - Order created without products, OrderID: ' + order.Id);
                } else {
                    List<OrderItem> orderItems = new List<OrderItem>();
                    System.debug('Order Item Count: ' + item.items.size());
                    for(Integer i = 0; i < item.items.size(); i++) {
                        SalesOrderProductItem productItem = item.items[i];
                        if(productItem.parent_item_id == null) {
                            System.debug('productItem.sku:'+productItem.sku.trim());
                            //System.debug('PriceBookEntry:'+[SELECT Id, Name FROM PriceBookEntry WHERE PRODUCTCODE = :productItem.sku.trim()]);
                            if (Limits.getQueries() > Limits.getLimitQueries()) {
                                System.debug('Need to stop processing to avoid hitting a governor limit.');
                                record.SalesForceObjectStatus__c = false;
                                record.SalesForceResponse__c = 'Need to stop processing to avoid hitting a governor limit.';
                            } else {
                                System.debug('Continue processing. Not going to hit Queries governor limits');
								PriceBookEntry pe = [SELECT Id, Name FROM PriceBookEntry WHERE PRODUCTCODE = :productItem.sku.trim()];
                                System.debug('pe:'+pe);
                                ApexGovernorLimits.printLimitsUsed();
                                
                                //System.debug('Product2:'+[SELECT Id, Name, EComStoreID__c FROM Product2 WHERE PRODUCTCODE = :productItem.sku.trim()]);
                                if (Limits.getQueries() > Limits.getLimitQueries()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                    record.SalesForceObjectStatus__c = false;
                                    record.SalesForceResponse__c = 'Need to stop processing to avoid hitting a governor limit.';
                                } else {
                                    System.debug('Continue processing. Not going to hit Queries governor limits');
                                    Product2 prod = [SELECT Id, Name, EComStoreID__c FROM Product2 WHERE PRODUCTCODE = :productItem.sku.trim()];
                                    System.debug('prod:'+prod);
                                    ApexGovernorLimits.printLimitsUsed();
                                    
                                    OrderItem orderItem = new OrderItem();
                                    orderItem.OrderId = order.Id;
                                    orderItem.Quantity = productItem.qty_ordered;
                                    orderItem.Quantity_Ordered__c = productItem.qty_ordered;
                                    orderItem.Product2Id = prod.Id;
                                    orderItem.PricebookEntryId = pe.Id;
                                    orderItem.UnitPrice = productItem.price;
                                    orderItem.Discount_Amount__c = productItem.price*productItem.discount_percent/100.0;
                                    orderItem.Tax_Amount__c = productItem.tax_amount;
                                    orderItem.MagentoId__c = productItem.item_id;
                                    orderItem.EComStoreID__c = prod.EComStoreID__c;
                                    orderItems.add(orderItem);
                                }
                            }
                        }
                    }
                    System.debug('Is it Empty: ' + orderItems.isEmpty());
                    if(!orderItems.isEmpty())
                    {
                        if (orderItems.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                            record.SalesForceObjectStatus__c = false;
                            record.SalesForceResponse__c = 'Need to stop processing to avoid hitting a governor limit.';
                        } else {
                            System.debug('Continue processing. Not going to hit DML governor limits');
                            System.debug('Jantz');
                            System.debug(orderItems);
                            insert orderItems;
                            record.SalesForceObjectStatus__c = true;
                            record.SalesForceResponse__c = 'Success';
                            ApexGovernorLimits.printLimitsUsed();
                        }
                    }
                }
            }
            
            record.SalesForceID__c = order.Id;
            record.SalesForceExecutionTime__c = DateTime.now();

        ApexGovernorLimits.printLimitsLeft();
    }
}