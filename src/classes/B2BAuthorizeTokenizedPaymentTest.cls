// This is a minimal test for this class that verifies some of the handling of B2BAuthorizeTokenizedPayment
// without completing the authorization
@isTest
public class B2BAuthorizeTokenizedPaymentTest {
    @testSetup static void setup() {
        // Account and Contact are related
        Account account = new Account(Name='TestAccount');
        insert account;
        Contact contact = new Contact(LastName='TestContact', AccountId=account.Id);
        insert contact;

        WebStore webStore = new WebStore(Name='TestWebStore');
        insert webStore;

        CardPaymentMethod cardPaymentMethod = new CardPaymentMethod(AccountId=account.Id, ProcessingMode='External', Status='Active');
        insert cardPaymentMethod;

        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id, PaymentMethodId=cardPaymentMethod.Id);
        insert cart;

        CartDeliveryGroup cartDeliveryGroup = new CartDeliveryGroup(CartId=cart.Id, Name='Default Delivery');
        insert cartDeliveryGroup;
        
        //insertCartItem(cart.Id, cartDeliveryGroup.Id);

		 Order order = new Order(
            AccountId = account.Id,
            BillingCity = 'Florida',
            BillingCountry = 'United States',
            BillingPostalCode = '71923',
            BillingState = 'Florida',
            BillingStreet = '231 S Adams Rd, Sand Springs, OK, 74063',
            EffectiveDate = Date.today(),
            Status = 'Draft'
        );
        insert order;
        
        addStoreIntegratedService(webStore.id);
        
        
    }
    
    // Setup code that creates a non-functional store integrated service
    static void addStoreIntegratedService(Id StoreId) {
        // Any named credential should do for the purposes of tests that don't 100% complete
        List<NamedCredential> namedCredential = [SELECT Id FROM NamedCredential LIMIT 1];

        // This provider should be installed while running the SFDX installation that this test is associated with
        List<PaymentGatewayProvider> paymentGatewayProvider = [SELECT Id FROM PaymentGatewayProvider Limit 1];
        
        PaymentGateway paymentGateway = new PaymentGateway(PaymentGatewayName='PGN', MerchantCredentialId=namedCredential[0].Id,
                                                           PaymentGatewayProviderId=paymentGatewayProvider[0].Id, Status='Active');
        //List<PaymentGateway> paymentGateway=[SELECT Id FROM paymentGateway Limit 1]
        insert paymentGateway;
        System.debug('paymentGateway'+ paymentGateway);

        StoreIntegratedService newStoreIntegratedService = new StoreIntegratedService(Integration=paymentGateway.Id,
                                                                                      ServiceProviderType='Payment', StoreId=StoreId);
        insert newStoreIntegratedService;
    }

    // Starter test. Doesn't bother setting up an order, so errors are expected
    // To create more tests, consider setting up an order associated with a session and a cart to get past the failure
    // this test verifies.
    @isTest static void testNoOrderYetCreated() {
        // Get the data we'll need to use for running the test or for verification
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name='Cart' LIMIT 1];
		order order = [SELECT Id FROM order LIMIT 1];

        Test.startTest();
        List<B2BAuthorizeTokenizedPayment.B2BAuthorizeTokenizedPaymentRequest> requests = new List<B2BAuthorizeTokenizedPayment.B2BAuthorizeTokenizedPaymentRequest>();
        B2BAuthorizeTokenizedPayment.B2BAuthorizeTokenizedPaymentRequest request = new B2BAuthorizeTokenizedPayment.B2BAuthorizeTokenizedPaymentRequest();
        request.cartId = webCart.Id;
        requests.add(request);

        List<String> errors = B2BAuthorizeTokenizedPayment.authorizePaymentInfo(requests);

        // No order was specified, so an error should be returned
        System.assertEquals(1, errors.size());
        //System.assertEquals('No order is associated with this cart', errors[0]);
        
        Test.stopTest();
    }

	@isTest static void testNoOrderYetCreatedq() {
        // Get the data we'll need to use for running the test or for verification

        Test.startTest();
        B2BAuthorizeTokenizedPayment.getPaymentGroup('123');
        
        Test.stopTest();
    }


    // Inserts a cart item when we only know the cart id
   /* static void insertCartItem(String cartId) {
        List<CartDeliveryGroup> cartDeliveryGroups = [SELECT Id FROM CartDeliveryGroup WHERE CartId = :cartId LIMIT 1];

        insertCartItem(cartId, cartDeliveryGroups.get(0).Id);
    }
    
    // Inserts a cart item that matches the cart and cart delivery group
    static void insertCartItem(String cartId, String cartDeliveryGroupId) {
        CartItem cartItem = new CartItem(
            CartId=cartId, 
            Sku='SKU_Test1', 
            Quantity=3.0, 
            Type='Product', 
            Name='TestProduct', 
            CartDeliveryGroupId=cartDeliveryGroupId
        );
        insert cartItem;
    }*/
}