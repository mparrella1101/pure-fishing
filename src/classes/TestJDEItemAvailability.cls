@isTest(seeAllData=true)
public class TestJDEItemAvailability {
	static testMethod void testItemAvailability() {
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EpJDEGetItemAvailabilityMock());
        String[] items = new String[]{'CAB56260M'};
        EpJDESync.getItemAvailability(items);
        Test.stopTest();        
    }
}