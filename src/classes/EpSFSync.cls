global class EpSFSync {

    public static void updateSFOrder(ECRecord__c record) {
        record.SalesForceExecutionTime__c = DateTime.now();
        record.SalesForceObjectStatus__c = false;
        record.SalesForceResponse__c = 'Failed';
        try {
            List<JDEShipmentOrder> shipementOrders = (List<JDEShipmentOrder>) System.JSON.deserialize(record.Dump__c, List<JDEShipmentOrder>.class);
            string orderIDs = '';
            string failedOrderIDs = '';
            for(JDEShipmentOrder shipementOrder : shipementOrders) {
                Order order = null;
                
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    List<Order> orders = [SELECT Id,OrderNumber,EComStoreID__c FROM Order WHERE JDE_Order_Number__c = :shipementOrder.order_id];
                    ApexGovernorLimits.printLimitsUsed();
                    
                    System.debug(orders);
                    
                    if(orders !=null && orders.size()>0)
                    {
                        order = (Order) orders.get(0);
                        System.debug(order);
                        if (Limits.getQueries() > Limits.getLimitQueries()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit Queries governor limits');
                            List<OrderItem> orderItems = [SELECT Id, OrderId, Quantity_Shipped__c, Tracking_Pro_Number__c, Product2Id, Carrier_Name__c, MagentoId__c, JDEId__c, EComStoreID__c FROM OrderItem WHERE OrderId = :order.Id];
                            ApexGovernorLimits.printLimitsUsed();
                            
                            for(OrderItem orderItem: orderItems) {
                                for(Integer index = 0; index<shipementOrder.items.size(); index++) {
                                    JDEShipmentOrderItem shipmentItem = shipementOrder.items.get(index);
                                    String line_item_id = String.valueOf(Integer.valueOf(shipmentItem.line_item_id));
                                    if(orderItem.JDEId__c == line_item_id) {
                                        System.debug(orderItem.JDEId__c + '=-=-=-='+ line_item_id);
                                        System.debug('record=-=-=-='+ record);
                                        orderIDs += orderItem.Id+',';
                                        orderItem.Quantity_Shipped__c = shipmentItem.ship_qty;
                                        orderItem.Tracking_Pro_Number__c = shipmentItem.tracking_no;
                                    }
                                }
                            }
    
                            if(!orderItems.isEmpty())
                            {
                                if (orderItems.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                } else {
                                    System.debug('Continue processing. Not going to hit DML governor limits');
                                    update orderItems;
                                    ApexGovernorLimits.printLimitsUsed();
                                }	
                            }
                        }
                    } else {
                        failedOrderIDs += shipementOrder.order_id+',';
                    }
                }
            }
            if(orderIDs.endsWith(',') && orderIDs.length()>1)
                orderIDs = orderIDs.substring(0,orderIDs.length()-1);
                
            if(failedOrderIDs.endsWith(',') && failedOrderIDs.length()>1)
                failedOrderIDs = failedOrderIDs.substring(0,failedOrderIDs.length()-1);
            
            record.SalesForceID__c = orderIDs;
            if(failedOrderIDs == '') {
                record.SalesForceResponse__c = 'Success';
                record.SalesForceObjectStatus__c = true;
            }
            else {
                record.SalesForceResponse__c = 'Failed: '+failedOrderIDs;
                //Notifier.sendEmail('EpSFSync - updateSFOrder - There are some others that failed in ECRecord: ' + record.Id + ' >>> ' + failedOrderIDs);
            }

        } catch (Exception ex) {
            record.SalesForceResponse__c = ex.getMessage();
            Notifier.sendEmail('EpSFSync - updateSFOrder - Exception error: '+ ex);
        }
    }
    
    public class JDEShipmentOrderItem
    {
        public Decimal ship_qty { get; set; }
        public String tracking_no { get; set; }
        public Decimal line_item_id { get; set; }
    }
    
    public class JDEShipmentOrder
    {
        public String order_id { get; set; }
        public List<JDEShipmentOrderItem> items { get; set; }
    }
}