global class EpJDESync {
    private static Integer TIMEOUT = 120000;
    private static String METHOD = 'POST';
    private static String CONTENT_TYPE = 'text/xml;charset=UTF-8';
    
    public static void defensiveItemAvailability(String[] items) {
        if(System.isFuture() || System.isBatch()){
            getItemAvailability(items);
        } else if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()) {
            getItemAvailabilityFuture(items);
        } else {
            Notifier.sendEmail('EpJDESync - defensiveItemAvailability - @future call was denied for ' + items.size() + ' items:' + items);
        }
    }
    
    @future(callout=true)
    public static void getItemAvailabilityFuture(String[] items) {
        getItemAvailability(items);
    }
    
    public static void getItemAvailability(String[] items) {
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        req.setEndpoint('callout:Plano_JDE_Credentials/EcomInventoryAvailabilityProcessor');
        req.setMethod(METHOD);
        req.setHeader('Content-Type', CONTENT_TYPE);
        req.setHeader('SOAPAction', 'http://oracle.e1.bssv.JP570101/EcomInventoryAvailabilityProcessor/getItemAvailability');
        req.setTimeout(TIMEOUT);
       
        req.setBody(createItemAvailabilityEnvelope(items));
        
        System.debug('req: ' + req.getBody());
        
        Long mStartTime = DateTime.Now().getTime();
        System.debug('getItemAvailabilityReq Milliseconds Start: '+ mStartTime);
        try {
            res = http.send(req);

            System.debug('req res.getStatusCode(): ' + res.getStatusCode());
            if(res.getStatusCode() == 200)
            {
                String doc = res.getBody();
                System.debug('JDEComm: ' + doc);
                
                Dom.Document docx = new Dom.Document();
                docx.load(doc);
                
                dom.XmlNode xroot = docx.getrootelement() ;
        
                dom.XmlNode body = xroot.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
                system.debug(body);
                dom.XmlNode getItemAvailabilityResponse = body.getChildElement('getItemAvailabilityResponse', 'http://oracle.e1.bssv.JP574101/');
                system.debug(getItemAvailabilityResponse);
            
                Boolean status = false;
                String errors = '';
                List<Product2> products = new List<Product2>();
                List<Product2> dupProducts = new List<Product2>();
                for(dom.XmlNode child: getItemAvailabilityResponse.getChildElements())
                {
                    if(child.getName() == 'items') {
                        dom.XmlNode identifier2ndItem = child.getChildElement('identifier2ndItem', null);
                        String identifier = identifier2ndItem.getText();
                        system.debug(identifier2ndItem.getText());
                        
                        dom.XmlNode identifierShortItem = child.getChildElement('identifierShortItem', null);
                        system.debug(identifierShortItem.getText());
                        
                        dom.XmlNode itemQuantity = child.getChildElement('itemQuantity', null);
                        system.debug(itemQuantity.getText());
                        
                        if (Limits.getQueries() > Limits.getLimitQueries()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit Queries governor limits');
                            List<Product2> prods = [SELECT Id,JdeId__c,MagentoId__c,Qty_Available__c,EComStoreID__c FROM Product2 WHERE JdeId__c = :identifier];
                            if(prods.size()>1)
                            {
                                dupProducts.addAll(prods);
                            } else {
                                Product2 product = prods.get(0);
                                
                                ApexGovernorLimits.printLimitsUsed();
                                
                                if(product.Qty_Available__c != Decimal.valueOf(itemQuantity.getText()))
                                {
                                    system.debug('EpJDESync - getItemAvailability - updating: ' + product.JdeId__c + ' | ' + product.Qty_Available__c + ' != ' + Decimal.valueOf(itemQuantity.getText()));
                                    product.Qty_Available__c = Decimal.valueOf(itemQuantity.getText());
                                    products.add(product);
                                } else {
                                    system.debug('EpJDESync - getItemAvailability - not updating: ' + product.JdeId__c + ' | ' + product.Qty_Available__c + ' == ' + Decimal.valueOf(itemQuantity.getText()));
                                }
                            }
                            
                        }
                    } else if(child.getName() == 'e1MessageList') {
                        for(dom.XmlNode e1Message: child.getChildElements()) {
                            system.debug(e1Message.getChildElement('message', null).getText());
                            errors += e1Message.getChildElement('message', null).getText();
                        }
                    } else if(child.getName() == 'status') {
                        system.debug(child.getText());
                        status = true;
                    }
                }
                if(!products.isEmpty())
                {
                    if (products.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit DML governor limits');
                        update products;
                        ApexGovernorLimits.printLimitsUsed();
                    }
                }
                if(!dupProducts.isEmpty())
                {
                    String log = '';
                    for(Product2 p : dupProducts) {
                        log += '{SalesforceID:'+p.ID+' JdeId:'+p.JdeId__c+'}';
                    }
                    Notifier.sendEmail('EpJDESync - getItemAvailability: Duplicate products: '+ log);
                }
                /*if(errors == null || errors.length()>0) {
                    Notifier.sendEmail('EpJDESync - getItemAvailability: Receiving some errors: '+ errors);
                }*/
            } else {
                Notifier.sendEmail('EpJDESync - getItemAvailability: '+ res.getStatusCode() + '|' + res.getStatus() + ' items:'+items);
            }
        } catch(System.Exception e) {
            Long mEndTime = DateTime.Now().getTime();
            System.debug('getItemAvailabilityReq Milliseconds Start: '+ mEndTime);
            System.debug('getItemAvailabilityReq CallOut time: '+ (mEndTime-mStartTime));
            System.debug('Exception error: '+ e);
            Notifier.sendEmail('EpJDESync - getItemAvailability - Exception error: '+ e + '  CallOut time: '+ (mEndTime-mStartTime));
        } finally {
            System.debug(res.toString());
        }
    }

    private static String createItemAvailabilityEnvelope(String[] itemList) {
        Dom.Document doc = new Dom.Document();
        
        dom.XmlNode envelope = doc.createRootElement('Envelope', 'http://schemas.xmlsoap.org/soap/envelope/', 'soapenv');
        envelope.setNamespace('orac', 'http://oracle.e1.bssv.JP574101/');
        
        dom.XmlNode header = envelope.addChildElement('Header', 'http://schemas.xmlsoap.org/soap/envelope/', 'soapenv');
        
        dom.XmlNode security = header.addChildElement('Security', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        security.setAttributeNS('mustUnderstand', '1', 'http://schemas.xmlsoap.org/soap/envelope/', null);
        
        dom.XmlNode usernameToken = security.addChildElement('UsernameToken', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        
        dom.XmlNode username = usernameToken.addChildElement('Username', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        username.addTextNode('{!$Credential.Username}');
        
        dom.XmlNode password = usernameToken.addChildElement('Password', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse');
        password.setAttribute('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
        password.addTextNode('{!$Credential.Password}');
        
        dom.XmlNode body = envelope.addChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/', 'soapenv');
        
        dom.XmlNode getItemAvailability = body.addChildElement('getItemAvailability', 'http://oracle.e1.bssv.JP574101/', 'orac');
        
        for(String item : itemList)
        {
            dom.XmlNode items = getItemAvailability.addChildElement('items', null, null);
            
            dom.XmlNode identifier2ndItem = items.addChildElement('identifier2ndItem', null, null);
            identifier2ndItem.addTextNode(item);
            
            dom.XmlNode identifierShortItem = items.addChildElement('identifierShortItem', null, null);
        }
        System.debug(doc.toXmlString());
        return doc.toXmlString();
    }
    
    @future(callout=true)
    public static void stockItemUpdateSynchronously(String jsonString, Integer PRODUCTS_PER_REQUEST) {
        List<Product2> products = (List<Product2>) JSON.deserialize(jsonString, List<Product2>.class);
        System.debug('EpJDESync > stockItemUpdateSynchronously > products:'+products);
        jdeItemAvailabilitySyncProcess(products, PRODUCTS_PER_REQUEST);
    }
    
    public static void jdeItemAvailabilitySyncProcess(List<Product2> products, Integer PRODUCTS_PER_REQUEST) {
        System.debug('EpJDESync > jdeItemAvailabilitySyncProcess > products.size():'+products.size());
        if(products!=null && products.size() != 0) {
            String[] items = new String[] {};
            for(Integer index=0; index<products.size() && index<PRODUCTS_PER_REQUEST && products.size()!=0; index++)
            {
                Product2 prod = products.get(index);
                items.add(prod.JdeId__c);
                products.remove(index);
            }
            System.debug('EpJDESync > jdeItemAvailabilitySyncProcess > items.size():'+items.size());
            if(items.size()>0) {
                EpJDESync.defensiveItemAvailability(products,PRODUCTS_PER_REQUEST,items);
            } 
        }
    }
    
    public static void defensiveItemAvailability(List<Product2> products, Integer PRODUCTS_PER_REQUEST, String[] items) {
        if(System.isFuture() || System.isBatch()){
            getItemAvailability(JSON.serialize(products), PRODUCTS_PER_REQUEST, items);
        } else if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()) {
            getItemAvailabilityFuture(JSON.serialize(products), PRODUCTS_PER_REQUEST, items);
        } else {
            Notifier.sendEmail('EpJDESync - defensiveItemAvailability - @future call was denied for ' + items.size() + ' items:' + items);
        }
    }
    
    @future(callout=true)
    public static void getItemAvailabilityFuture(String products, Integer PRODUCTS_PER_REQUEST, String[] items) {
        getItemAvailability(products, PRODUCTS_PER_REQUEST, items);
    }
    
    public static void getItemAvailability(String productList, Integer PRODUCTS_PER_REQUEST, String[] items) {
        List<Product2> productz = (List<Product2>)JSON.deserialize(productList, List<Product2>.class);
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        req.setEndpoint('callout:Plano_JDE_Credentials/EcomInventoryAvailabilityProcessor');
        req.setMethod(METHOD);
        req.setHeader('Content-Type', CONTENT_TYPE);
        req.setHeader('SOAPAction', 'http://oracle.e1.bssv.JP570101/EcomInventoryAvailabilityProcessor/getItemAvailability');
        req.setTimeout(TIMEOUT);
       
        req.setBody(createItemAvailabilityEnvelope(items));
        
        System.debug('req: ' + req.getBody());
        
        Long mStartTime = DateTime.Now().getTime();
        System.debug('getItemAvailabilityReq Milliseconds Start: '+ mStartTime);
        try {
            res = http.send(req);
            System.debug('EpJDESync > getItemAvailability > productz.size():'+ productz.size());
            System.debug('EpJDESync > getItemAvailability > productz.size()!=0:'+ (productz.size()!=0));
            if(productz.size()!=0) {
                jdeItemAvailabilitySyncProcess(productz,PRODUCTS_PER_REQUEST);
            }
            System.debug('req res.getStatusCode(): ' + res.getStatusCode());
            if(res.getStatusCode() == 200)
            {
                String doc = res.getBody();
                System.debug('JDEComm: ' + doc);
                
                Dom.Document docx = new Dom.Document();
                docx.load(doc);
                
                dom.XmlNode xroot = docx.getrootelement() ;
        
                dom.XmlNode body = xroot.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
                system.debug(body);
                dom.XmlNode getItemAvailabilityResponse = body.getChildElement('getItemAvailabilityResponse', 'http://oracle.e1.bssv.JP574101/');
                system.debug(getItemAvailabilityResponse);
            
                Boolean status = false;
                String errors = '';
                List<Product2> products = new List<Product2>();
                List<Product2> dupProducts = new List<Product2>();
                for(dom.XmlNode child: getItemAvailabilityResponse.getChildElements())
                {
                    if(child.getName() == 'items') {
                        dom.XmlNode identifier2ndItem = child.getChildElement('identifier2ndItem', null);
                        String identifier = identifier2ndItem.getText();
                        system.debug(identifier2ndItem.getText());
                        
                        dom.XmlNode identifierShortItem = child.getChildElement('identifierShortItem', null);
                        system.debug(identifierShortItem.getText());
                        
                        dom.XmlNode itemQuantity = child.getChildElement('itemQuantity', null);
                        system.debug(itemQuantity.getText());
                        
                        if (Limits.getQueries() > Limits.getLimitQueries()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit Queries governor limits');
                            List<Product2> prods = [SELECT Id,JdeId__c,MagentoId__c,Qty_Available__c,EComStoreID__c FROM Product2 WHERE JdeId__c = :identifier];
                            if(prods.size()>1)
                            {
                                dupProducts.addAll(prods);
                            } else {
                                Product2 product = prods.get(0);
                                
                                ApexGovernorLimits.printLimitsUsed();
                                
                                if(product.Qty_Available__c != Decimal.valueOf(itemQuantity.getText()))
                                {
                                    system.debug('EpJDESync - getItemAvailability - updating: ' + product.JdeId__c + ' | ' + product.Qty_Available__c + ' != ' + Decimal.valueOf(itemQuantity.getText()));
                                    product.Qty_Available__c = Decimal.valueOf(itemQuantity.getText());
                                    products.add(product);
                                } else {
                                    system.debug('EpJDESync - getItemAvailability - not updating: ' + product.JdeId__c + ' | ' + product.Qty_Available__c + ' == ' + Decimal.valueOf(itemQuantity.getText()));
                                }
                            }
                            
                        }
                    } else if(child.getName() == 'e1MessageList') {
                        for(dom.XmlNode e1Message: child.getChildElements()) {
                            system.debug(e1Message.getChildElement('message', null).getText());
                            errors += e1Message.getChildElement('message', null).getText();
                        }
                    } else if(child.getName() == 'status') {
                        system.debug(child.getText());
                        status = true;
                    }
                }
                if(!products.isEmpty())
                {
                    if (products.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit DML governor limits');
                        update products;
                        ApexGovernorLimits.printLimitsUsed();
                    }
                }
                if(!dupProducts.isEmpty())
                {
                    String log = '';
                    for(Product2 p : dupProducts) {
                        log += '{SalesforceID:'+p.ID+' JdeId:'+p.JdeId__c+'}';
                    }
                    Notifier.sendEmail('EpJDESync - getItemAvailability: Duplicate products: '+ log);
                }
                if(errors == null || errors.length()>0) {
                    System.debug('JDEError: EpJDESync - getItemAvailability: Receiving some errors: '+ errors);
                }
            } else {
                System.debug('JDEError:'+ res.getStatusCode() + ' items:'+items);
                Notifier.sendEmail('EpJDESync - getItemAvailability: '+ res.getStatusCode() + '|' + res.getStatus() + ' items:'+items);
            }
        } catch(System.Exception e) {
            Long mEndTime = DateTime.Now().getTime();
            System.debug('getItemAvailabilityReq Milliseconds Start: '+ mEndTime);
            System.debug('getItemAvailabilityReq CallOut time: '+ (mEndTime-mStartTime));
            System.debug('Exception error: '+ e);
            Notifier.sendEmail('EpJDESync - getItemAvailability - Exception error: '+ e + '  CallOut time: '+ (mEndTime-mStartTime)  + ' items:'+items);
        } finally {
            System.debug(res.toString());
        }
    }

}