/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           11/09/2022            Initial Version
 */
// This class call from flow when order status updated to Synched. In this class we hit the Capture authorize payment.
// B2BCCCapturePaymentTest is test class for this class.
public class B2BCCCapturePayment {
    @InvocableMethod(callout=true label='CCCapture call' description='CCCapture call' category='B2B Commerce')
    public static void CCCapturePay(List<ID> Ords) {
        if(Ords != null)
        {
            Id ordId = Ords[0];
            startOrdProcessSync(ordId);
        }
        
    }

    private static void startOrdProcessSync(Id ordId) 
    {
        try{
            If (ordId!=null)
                {
                    list<Order> OrdList =[Select Order_Reference__c, Status,GrandTotalAmount,CC_transId__c,AccountId,OrderReferenceNumber  from Order where id =: ordId];
                    Http h = new Http();
                    Httprequest req =new Httprequest();
                    HttpResponse res = null;
                    List<Account> getWebstoreInfo=[select id,Sales_Org__c,SAP_Customer__c from Account where id=: OrdList[0].AccountId];
                    list<AuthorizeNetPayment__mdt> AuthData = new List<AuthorizeNetPayment__mdt>();
                        if (!Test.isRunningTest()){
                            AuthData =[Select Username__c,Trax_key__c,FullEndpoint__c from AuthorizeNetPayment__mdt where Sales_Org__c=:getWebstoreInfo[0].Sales_Org__c ];
                        } else {
                            AuthorizeNetPayment__mdt testRecord = new AuthorizeNetPayment__mdt();
                            testRecord.Username__c = 'test';
                            testRecord.Trax_key__c = 'apex_text';
                            testRecord.FullEndpoint__c = 'https://apitest.authorize.net/xml/v1/request.api';
                            AuthData.add(testRecord);
                        }
                        
                        string body= '{"createTransactionRequest": {"merchantAuthentication": {"name":"'+AuthData[0].Username__c.trim()+'","transactionKey":"'+AuthData[0].Trax_key__c?.trim()+'"},'+'"refId": "'+ OrdList[0].OrderReferenceNumber  +'","transactionRequest": {"transactionType": "priorAuthCaptureTransaction","amount":"'+ OrdList[0].GrandTotalAmount+'","refTransId": "'+ OrdList[0].CC_transId__c+'"}}}';
                        System.debug('body' + body );
                        req.setHeader('content-type', 'application/json');
                        req.setHeader('Content-length', String.valueOf(body.length()));// For Authorize.Net
                        req.setEndpoint(AuthData[0].FullEndpoint__c);// For Authorize.Net
                        req.setBody(body);
                        req.setMethod('POST');
                        res = h.send(req);
                        String Resbody = res.getBody();
                        Resbody = Resbody.trim().replace('\uFEFF', '');
                        System.debug('Resbody' + Resbody);  // "LC899I"
                        Map<String,Object> result = (Map<String,Object>) JSON.deserializeUntyped(Resbody);
                        Map<String,Object> firstDeserializeResult = (Map<String,Object>)result.get('transactionResponse');
                        System.debug('First deserialize: ' + firstDeserializeResult);
                        Object secondDeserializeResult = (Object) firstDeserializeResult.get('errors');
                        String serialized = JSON.serialize(secondDeserializeResult);
                        serialized = serialized.replace('[','');
                        serialized = serialized.replace(']','');
                        Map<String,Object> thirdDeserialize = (Map<String,Object>) JSON.deserializeUntyped(serialized);
                        //System.debug('Final Deserialization: ' + thirdDeserialize.get('errorText'));
                        if(thirdDeserialize != null)
                        {   
                            String RespErr = (String)thirdDeserialize.get('errorText');
                            Order ord = new Order();
                            if(RespErr !='')
                            {
                                ord.CC_capture_msg__c=RespErr;
                                ord.Status='Synced - Capture Error';
                                ord.id=ordId;
                                Update ord;
                            }
                        } 
                    
                            
                }
    
         }
         catch (Exception e) 
            {
                System.debug('B2BCCCapturePayment:startOrdProcessSync Error is :' + e.getMessage() + 'LN:' + e.getLineNumber() );
            }
    }
}