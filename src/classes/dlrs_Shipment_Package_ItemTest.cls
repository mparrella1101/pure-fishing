/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Shipment_Package_ItemTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Shipment_Package_ItemTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Shipment_Package_Item__c());
    }
}