global with sharing class SetSAPOrdersPricebookBatch implements Database.Batchable<sObject>, Database.Stateful {
    public String query;
    public String orderId;
    public String orderFields = 'Id, Pricebook2Id';
    public String orderProduct = 'Order Product';
    public String orderItemFields = 'PricebookEntryId, Product2Id, OrderId, ListPrice, UnitPrice, Product2.Material__c, Quantity, Quantity_Ordered__c, SF_External_ID__c, CurrencyIsoCode, Supplied_Line__c, Type';
    public List<Order> orderList = new List<Order>();
    public List<OrderItem> orderItemList = new List<OrderItem>();
    public List<OrderItem> masterOrderItemInsertList = new List<OrderItem>();
    public Map<String, String> orderPricebookMap = new Map<String, String>();
    public Map<String, String> orderItemPricebookEntryMap = new Map<String, String>();
    public Map<String, String> orderItemToOrderMap = new Map<String, String>(); 

    public SetSAPOrdersPricebookBatch(List<Order> orderList) {
        this.orderList = orderList;
        this.query = 'SELECT ' + orderFields + ', (SELECT ' + orderItemFields + ' FROM OrderItems) FROM Order WHERE Id IN :orderList AND Pricebook2Id != null';
    }
    public SetSAPOrdersPricebookBatch(String orderId) { // testing constructor
        this.orderId = orderId;
        this.query = 'SELECT ' + orderFields + ', (SELECT ' + orderItemFields + ' FROM OrderItems) FROM Order WHERE Id = :orderId AND Pricebook2Id != null';
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('SetSAPOrdersPricebookBatch start');
        System.debug('orderList: ' + orderList);
        System.debug('orderId: ' + orderId);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Order> orderList) {
        System.debug('orderList: ' + orderList);
        buildMaps(orderList); // set up maps for reference.
        // orphanOrderItems(orderItemList); //  orphan all order items from parent OrderId. not going to work, PricebookEntryId and OrderID fields are not writeable.
        removeOrderLines(orderItemList); // store all order lines from order in map, then delete and reinsert later.
        clearOrderPricebook(orderList); // remove pricebook2Id from all orders.
        // reparentOrderLines(orderItemList); //  reparent orderLines to original parent order.
        reinsertOrderLines(masterOrderItemInsertList); // reinsert order lines.        
        // setOrderPricebooks(orderList); // set original pricebooks back to orders, this step not needed.
    }

    global void finish(Database.BatchableContext BC){
        System.debug('SetSAPOrdersPricebookBatch finished');
    }

    private void buildMaps(List<Order> orderList){
        for(Order order : orderList){
            System.debug('buildMaps');
            orderPricebookMap.put(order.Id, order.Pricebook2Id);
            for(OrderItem orderItem : order.OrderItems){ // assumes all orderItems have pricebook entries since the order would have a pricebook.
                orderItemPricebookEntryMap.put(orderItem.Id, orderItem.PricebookEntryId);
                orderItemToOrderMap.put(orderItem.Id, orderItem.OrderId);
                orderItemList.add(orderItem);
            }
            System.debug('orderItemToOrderMap: ' + orderItemToOrderMap);
            System.debug('orderItemList: ' + orderItemList);
            System.debug('orderItemPricebookEntryMap: ' + orderItemPricebookEntryMap);
        }
    }

    // private void orphanOrderItems(List<OrderItem> orphanedOrderItems){ // process not possible.
    //     System.debug('orphanOrderItems');
    //     System.debug('orphanedOrderItems before: ' + orphanedOrderItems);
    //     for(OrderItem orphanedOrderItem : orphanedOrderItems){
    //         orphanedOrderItem.PricebookEntryId = null; // can't be done, field not writeable, going to need to store all order line information, then delete, then reinsert.
    //         orphanedOrderItem.OrderId = null;
    //     }
    //     System.debug('orphanedOrderItems after: ' + orphanedOrderItems);
    //     runUpdate(orphanedOrderItems, 'OrderItem', true);        
    // }

    private void removeOrderLines(List<OrderItem> orderItemList){
        for(OrderItem orderItem : orderItemList){
            if(orderItem.Type == orderProduct){
                OrderItem newOrderItem = new OrderItem(
                    OrderId = orderItem.OrderId,
                    Product2Id = orderItem.Product2Id,
                    ListPrice = orderItem.ListPrice,
                    UnitPrice = orderItem.UnitPrice,
                    Quantity = orderItem.Quantity,
                    Quantity_Ordered__c = orderItem.Quantity_Ordered__c,
                    SF_External_ID__c = orderItem.SF_External_ID__c,
                    Supplied_Line__c = orderItem.Supplied_Line__c,
                    Type = orderItem.Type
                );
                masterOrderItemInsertList.add(newOrderItem);
            }            
        }
        System.debug('masterOrderItemInsertList: ' + masterOrderItemInsertList);
        runDelete(orderItemList, 'OrderItem', true);
    }

    private void clearOrderPricebook(List<Order> orderList){
        System.debug('clearOrderPricebook');
        System.debug('orderList before: ' + orderList);
        for(Order order : orderList){
            order.Pricebook2Id = null;
        }
        System.debug('orderList after: ' + orderList);
        runUpdate(orderList, 'Order', true);
    }

    // private void reparentOrderLines(List<OrderItem> orderItemList){ // process not possible.
    //     System.debug('reparentOrderLines');
    //     System.debug('orderItemList before: ' + orderItemList);
    //     for(OrderItem orderItem : orderItemList){
    //         orderItem.OrderId = orderItemToOrderMap.get(orderItem.Id);
    //     }
    //     System.debug('orderItemList after: ' + orderItemList);
    //     runUpdate(orderItemList, 'OrderItem', true);
    // }

    private void reinsertOrderLines(List<OrderItem> orderItemsToInsertList){
        System.debug('reinsertOrderLines');
        System.debug('orderItemsToInsertList: ' + orderItemsToInsertList);
        runInsert(orderItemsToInsertList, 'OrderItem', true);
    }

    // private void setOrderPricebooks(List<Order> orderList){ // process not needed.
    //     System.debug('setOrderPricebooks');
    //     System.debug('orderList before: ' + orderList);
    //     for(Order order : orderList){
    //         order.Pricebook2Id = orderPricebookMap.get(order.Id);
    //     }
    //     System.debug('orderList before: ' + orderList);
    //     runUpdate(orderList, 'Order', true);
    // }    

 /** UTILITY METHODS START **/
    private void runInsert(List<sObject> listToInsert, String objectType, Boolean continueOnError){
        System.debug('runInsert');
        System.debug(objectType + ' listToInsert: ' + listToInsert);
        Database.SaveResult[] srList = Database.insert(listToInsert, continueOnError);
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted ' + objectType + ', ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    private void runUpdate(List<sObject> listToUpdate, String objectType, Boolean continueOnError){
        System.debug('runUpdate');
        System.debug(objectType + ' listToUpdate: ' + listToUpdate);
        Database.SaveResult[] srList = Database.update(listToUpdate, continueOnError);
        for(Database.SaveResult sr : srList){
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated '+ objectType + ' record with ID: ' + sr.getId());
            } else {
                // Operation failed, so get all errors 
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(objectType + ' fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    private void runDelete(List<sObject> listToDelete, String objectType, Boolean continueOnError){
        System.debug('runDelete');
        System.debug(objectType + ' listToDelete: ' + listToDelete);
        Database.DeleteResult[] drList = Database.delete(listToDelete, continueOnError);
        for(Database.DeleteResult dr : drList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted ' + objectType + ' with ID: ' + dr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(objectType + ' fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
}