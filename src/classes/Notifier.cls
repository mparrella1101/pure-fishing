public class Notifier {
    public static void sendEmail(String body) {
        sendEmail(body, 'logsdebug@gmail.com');
    }
    
    public static void sendEmail(String body, String supportEmail) {
        if(Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()) {
            try {
                Messaging.reserveSingleEmailCapacity(1);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                String[] toAddressesSandbox = new String[] {supportEmail/*, 'jhorvath@planosynergy.com'*/};
                String[] toAddressesProduction = new String[] {supportEmail, 'jhorvath@planosynergy.com', 'web@planosynergy.com'};
                
                if(isSandbox()) {
                    mail.setSubject('Ecom Error');
                    mail.setToAddresses(toAddressesSandbox);
                } else {
                    mail.setSubject('Production: Ecom Error');
                    mail.setToAddresses(toAddressesProduction);
                }
                
                mail.setSenderDisplayName('ECom');
                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setPlainTextBody(body);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            } catch (Exception ex) {
                System.debug('Notifier.sendEmail():'+ex.getMessage());
            }
        } else { // schedule email to be sent later
            System.debug('Could not send email: ' + body);
        }
    }
    
    public static Boolean isSandbox() {
        Organization org = [SELECT IsSandbox FROM Organization LIMIT 1];
        return org.IsSandbox;
    }
}