public class CustomerItems {
    public String id {get;set;}
    public String default_billing {get;set;} 
    public String default_shipping {get;set;} 
    public String email {get;set;} 
    public String firstname {get;set;} 
    public String lastname {get;set;} 
    public Integer store_id {get;set;}
    public List<CustomerAddresses> addresses {get;set;} 
}