/**
 * @author Matt Parrella
 * @date August 25, 2022
 * @description This class hosts an Invocable Method that will make a callout to Mulesoft/SAP to retrieve inventory amounts for a specific product
 */

 /* Version      Author                  Company                 Date            description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud            08-25-2022      Initial version
 */

public with sharing class InventoryCallout {

    /**
     * @description Sub-class to hold the information that is passed-in from the flow
     */
    public class InventoryInput {
        /** @description String var to hold the 18-character Salesforce record Id for the Product we are retrieving inventory for */
        @InvocableVariable(label='Product2Id' description='The 18-character Salesforce Product2 RecordId value associated with the Product that we are retrieving inventory information for.' required=true)
        public String productId;
        /** @description String var to hold the 18-character Salesforce record Id for the Account we wish to use SAP credential from */
        @InvocableVariable(label='AccountId' description='The 18-character Account recordId that we will be using SAP information from to make the callout' required=true)
        public String accountId;
    }

    /**
     * @description Sub-class to hold the information returned from the callout (will be passed back to the flow)
     */
    public class InventoryOutput {
        /** @description Boolean var to indicate if the callout was successful or not */
        @InvocableVariable(label='Callout successful?' description='Boolean variable that indicates if there was an error encountered during callout (false) or callout was successful (true)')
        public Boolean isSuccess;
        /** @description Decimal var to hold the returned inventory level from Mulesoft/SAP */
        @InvocableVariable(label='Current Inventory' description='The returned inventory level from the callout to Mulesoft/SAP')
        public Decimal inventoryResult;
        /** @description String var to hold any error messages returned from the callout */
        @InvocableVariable(label='Error Message' description='Error message associated with an unsuccessful callout (if applicable)')
        public String errorMsg;
    }

    /**
     * @description This Invocable Method will make a callout to Mulesoft/SAP, passing along an AccountId and Product2Id. Using these values,
     *              the callout will be made and response will be stored in the 'InventoryOutput' wrapper object, which is ultimately
     *              passed back to the flow after execution
     *
     * @param inputParams List<InventoryInput> A list containing InventoryInput objects (list size should never be > 1 in this execution context)
     *
     * @return List<InventoryOutput> A list containing the information returned from Mulesoft/SAP
     */
    @InvocableMethod(iconName='slds:standard:product_transfer' label='[CALLOUT] Mulesoft/SAP Inventory' callout=true description='Will make a callout to Mulesoft/SAP passing along a Product\'s 18-character RecordId value. The method will return the current inventory level for that Product in a list, in Decimal format' )
    public static List<InventoryOutput> getInventoryRealTime(List<InventoryInput> inputParams) {
        List<b2b_InventoryWrapper.response> calloutResp = new List<b2b_InventoryWrapper.response>();
        InventoryOutput response = new InventoryOutput();
        List<InventoryOutput> result = new List<InventoryOutput>();
        String accountId;
        String productId;

        try {
            accountId = inputParams[0].accountId;
            productId = inputParams[0].productId;
            calloutResp = b2b_IntegrationService.InventoryCal(accountId, new List<String>{ productId }, '');
            response.isSuccess = true;
            response.inventoryResult = Decimal.valueOf(calloutResp[0].quantity);
            response.errorMsg = '';
        }
        catch(Exception ex) {
            response.isSuccess = false;
            response.inventoryResult = null;
            response.errorMsg = ex.getMessage();
        }

        result.add(response);

        return result;
    }

}