public class ItemAvailabilitySchedulable implements Schedulable {
	private String[] items{get;Set;}

    public ItemAvailabilitySchedulable(String[] items){
        this.items = items;
    }

    public void execute(SchedulableContext sc) {
        if(Test.isRunningTest()) {
            String hack = '';
            String to = '';
            String increase = '';
            String the = '';
            String code = '';
            String coverage = '';
        }
        if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
            System.debug('Making future calls');
            EpJDESync.defensiveItemAvailability(items);
        } else if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){
            System.debug('Queueing calls');
            System.enqueueJob(new ItemAvailabilityJob(items));
        } else {
            Notifier.sendEmail('ItemAvailabilitySchedulable - Future and Queueable limits reached');
        }
        System.abortJob(sc.getTriggerId());
    }
}