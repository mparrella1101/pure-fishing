/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           05/11/2022            Initial Version
 */
// This class determines if we can ship to the buyer's shipping address and it will upadte the price at orderitem
public class B2BSyncOrderDelivery {
    // This invocable method only expects one ID
    @InvocableMethod(callout=true label='Prepare the Order Delivery Method Options' description='Runs a synchronous version of delivery method preparation' category='B2B Commerce')
    public static void syncDelivery(List<ID> orderIds) {
        // Validate the input
        if (orderIds == null || orderIds.size() != 1) {
            String errorMessage = 'A Order id must be included to B2BSyncDelivery';
            // Sync non-user errors skip saveCartValidationOutputError
            throw new CalloutException (errorMessage);
        }
        
        // Extract cart id and start processing
        Id orderId = orderIds[0];
        startOrderProcessSync(orderId);
    }

    private static void startOrderProcessSync(Id orderId) {
        try {
            // We need to get the ID of the cart delivery group in order to create the order delivery groups.
            String ActId;
            Decimal Amt;
            String PriBkId;
            String reasonCode;
            string CIC;
            String DS;
            for( Order ActInfo:[Select AccountId,TotalAmount,Pricebook2Id,Reason_Code__c,CurrencyIsoCode,Distribution_Channel__c from Order where Id = : orderId])
            {
                ActId=ActInfo.AccountId;
                Amt= ActInfo.TotalAmount.setScale(2);
                PriBkId=ActInfo.Pricebook2Id;
                reasonCode=ActInfo.Reason_Code__c;
                CIC=ActInfo.CurrencyIsoCode;
                DS=ActInfo.Distribution_Channel__c;

            }
            Account ActInfos = [Select Division_SAP__c,SAP_Customer__c,Distribution_Channel__c,Sales_Org__c,Currency__c  from Account where Id = : ActId][0];
            b2b_ShippingWrapper req = new b2b_ShippingWrapper();
            req.division = '00' + ActInfos.Division_SAP__c;
            req.customerNumber = ActInfos.SAP_Customer__c;
            req.salesOrganization =ActInfos.Sales_Org__c;
            req.orderValue = String.valueOf(amt);
            req.orderReason =reasonCode;
            req.currencyType=CIC;
           // req.distributionChannel = '0' + ActInfos.Distribution_Channel__c;
            req.distributionChannel=DS;//Arsee:Plano update
            /*req.division = '00';
            req.customerNumber = '20641';
            req.salesOrganization ='0180';
            req.orderValue =String.valueOf(amt);
            req.orderReason = '100';
            req.currencyType='EUR';
            req.distributionChannel = '00';*/
            System.debug('json' + JSON.serialize(req));
             b2b_ShippingWrapper.response ShippingFromExternalService = b2b_IntegrationService.Shippingcal(req);  
             System.debug('ShippingFromExternalService' + ShippingFromExternalService);
            String Chg='Delivery Charge';
            delete [SELECT Id FROM OrderItem WHERE OrderId =:orderId AND Type ='Delivery Charge'];
            PricebookEntry prcBuKInt = new PricebookEntry();
            String ProductId=getDefaultShippingChargeProduct2Id();
            if(test.isRunningTest())
				 prcBuKInt=[Select Id from PricebookEntry LIMIT 1];
			else
				 prcBuKInt=[Select Id from PricebookEntry where Product2Id=:ProductId and Pricebook2Id=:PriBkId and CurrencyIsoCode=:CIC];
            //String prcBuKIntId= prcBuKInt.Id;
            system.debug('ProductId' + ProductId);
            OrderItem chrgProd = new OrderItem(
                OrderId=orderId,
                Product2Id=ProductId,
                Quantity=1,
                Type ='Delivery Charge',
                //CurrencyIsoCode=ActInfos.Currency__c,
                PricebookEntryId=prcBuKInt.Id,
                UnitPrice=Double.valueOf(ShippingFromExternalService.price)
                //TotalLineAmount=Double.valueOf(ShippingFromExternalService.price)
            );
         insert(chrgProd);
        } catch (DmlException de) {
            // To aid debugging catch any exceptions thrown when trying to insert the shipping charge to the CartItems
            // In production you might want to hide these details from the buyer user.
            Integer numErrors = de.getNumDml();
            String errorMessage = 'There were ' + numErrors + ' errors when trying to insert the charge in the OrderItem: ';
            for(Integer errorIdx = 0; errorIdx < numErrors; errorIdx++) {
                errorMessage += 'Field Names = ' + de.getDmlFieldNames(errorIdx);
                errorMessage += 'Message = ' + de.getDmlMessage(errorIdx);
                errorMessage += ' , ';
            }
			system.debug('de.getNumDml()' + de.getNumDml() + de);
           // saveCartValidationOutputError(errorMessage, orderId);
            throw new CalloutException (errorMessage);
        }
    }
   private static Id getDefaultShippingChargeProduct2Id() {
        // In this example we will name the product representing shipping charges 'Shipping Charge for this delivery method'.
        // Check to see if a Product2 with that name already exists.
        // If it doesn't exist, create one.
        String shippingChargeProduct2Name = 'Shipping Charge';
        List<Product2> shippingChargeProducts = [SELECT Id FROM Product2 WHERE Name = :shippingChargeProduct2Name];
        if (shippingChargeProducts.isEmpty()) {
            Product2 shippingChargeProduct = new Product2(
                isActive = true,
                Name = shippingChargeProduct2Name,
                ShortItemNumber__c='Charge'
            );
            insert(shippingChargeProduct);
            return shippingChargeProduct.Id;
        }
        else {
            return shippingChargeProducts[0].Id;
        }
    }
}