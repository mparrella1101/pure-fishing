@istest
public class orderStatusUpdateBatchTest {
    static testmethod void OrderTestMethod()
    {
        Account testAccount = new Account(Name='TestAccount');
        insert testAccount;
        Account account = [SELECT Id FROM Account WHERE Name='TestAccount' LIMIT 1];
        
        Order Ord = new Order(AccountId=account.Id,EffectiveDate=Date.valueOf('2022-04-02'),status='Completing Order',Pricebook2Id=Test.getStandardPricebookId());
        insert Ord;
        
        //Ord.Description='Test';
        //update Ord;
        system.debug('the last mod date is '+Ord.LastModifiedDate);
        system.debug('the system now '+ system.now());
        String CRON_EXP = '0 0 0 15 3 ? *';
       
        // Create your test data
      	Test.startTest();

            String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new orderStatusUpdateSchedular());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
   }
    
}