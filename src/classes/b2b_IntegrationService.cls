/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Arsee Khan           Coastal Cloud           04/04/2022            Initial Version
 */
// created to have all integration mulesoft api call here.
public class b2b_IntegrationService {

    /* The following class vars represent values within the Product2.Product_Type__c field */
    private static final String SPARE_PARTS_LABEL = 'Spare Part';
    private static final String STANDARD_PRODUCT_LABEL = 'Pure Fishing Legacy';
    private static final String PLANO_PRODUCT_LABEL = 'Plano Legacy';

    public static List<b2b_TaxWrapper.response> Taxcal(b2b_TaxWrapper request){
        HttpResponse res = new HttpResponse();
        Integer SuccessfulHttpRequest = 200;
        B2BIntegrationHelper.IntegrationHelperWrapper integrationWrapper = B2BIntegrationHelper.getIntegrationData();
        if (!integrationWrapper.hasData) {
            throw new CalloutException('Unable to retrieve Integration data from Custom Metadata!');
        }
        String endpointURL = integrationWrapper.baseUrl + integrationWrapper.tax_endpoint;

        // Store Endpoint Creds
        String clientKey = integrationWrapper.client_key;
        String clientSecret = integrationWrapper.client_secret;

        try {
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setEndpoint(endpointURL);
            req.setMethod('POST');

            //these parts of the POST you may want to customize
            req.setBody(JSON.serialize(request));
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('client_id', clientKey);
            req.setHeader('client_secret', clientSecret);
            req.setTimeout(120000);
            res = http.send(req);

        } catch(exception e) {
            System.debug('Callout error: '+ e);
        }
        System.debug(res.getBody());
        if (res.getStatusCode() == SuccessfulHttpRequest)
        {
            List<b2b_TaxWrapper.response> responseMap = (List<b2b_TaxWrapper.response>)JSON.deserialize(res.getBody(), List<b2b_TaxWrapper.response>.class);
            //List<b2b_TaxWrapper.response> responseMap = (List<b2b_TaxWrapper.response>)JSON.deserialize(res, List<b2b_TaxWrapper.response>.class);
            System.debug('responseMap' + responseMap);
            return responseMap;
        }
        else
        {
            Map<String,Object> jsonBody = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            String errorMessage = 'CALLOUT:'+jsonBody.get('errorMessage');
            throw new CalloutException (errorMessage);
        }

    }
    public static List<Object> priceCal(b2b_pricingWrapper request){
        HttpResponse res = new HttpResponse();
        Integer SuccessfulHttpRequest = 200;
        B2BIntegrationHelper.IntegrationHelperWrapper integrationWrapper = B2BIntegrationHelper.getIntegrationData();
        if (!integrationWrapper.hasData) {
            throw new CalloutException('Unable to retrieve Integration data from Custom Metadata!');
        }
        String endpointURL = integrationWrapper.baseUrl + integrationWrapper.pricing_endpoint;

        // Store Endpoint Creds
        String clientKey = integrationWrapper.client_key;
        String clientSecret = integrationWrapper.client_secret;

        try {
            HttpRequest req = new HttpRequest();
            Http http = new Http();

            req.setEndpoint(endpointURL);
            req.setMethod('POST');

            //these parts of the POST you may want to customize
            System.debug('request: ' + JSON.serialize(request));
            req.setBody(JSON.serialize(request));
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('client_id', clientKey);
            req.setHeader('client_secret',clientSecret);
            req.setTimeout(120000);

            res = http.send(req);
        } catch(exception e) {
            System.debug('Callout error: '+ e);
        }
        System.debug(res.getBody());
        if (res.getStatusCode() == SuccessfulHttpRequest)
        {
            List<object> responseMap = (List<object>) JSON.deserializeUntyped(res.getBody());
            System.debug('responseMap' + responseMap);
            return responseMap;
        }
        else
        {
            Map<String,Object> jsonBody = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            String errorMessage = 'CALLOUT:'+jsonBody.get('errorMessage');
            throw new CalloutException (errorMessage);
        }

    }
    public static List<b2b_InventoryWrapper.response> InventoryCal(String accountId, List<String> productIds,String orderId){
        /* Record Data Config: START */
        Account acct;
        Order ord;
        List<Product2> products;
        try {
            /* Account Info: START */
            acct = [SELECT Id, Division_SAP__c, SAP_Customer__c, Distribution_Channel__c, Sales_Org__c FROM Account WHERE Id =: accountId LIMIT 1];
            /* Account Info: STOP */

            /* Product Info: START */
            products = [SELECT Id, Material__c, Product_Type__c FROM Product2 WHERE Id IN :productIds];

            /* Product Info: STOP */
            if(orderId !='' && orderId != null)
                ord=[SELECT Distribution_Channel__c FROM order WHERE Id =: orderId LIMIT 1];

        }
        catch(DmlException ex) {
            throw new CustomException('Error retrieving record data. Details: ' + ex.getMessage());
        }

        // Build list of Material Numbers and populate the product type booleans so we know what type of products are sending out in the callout
        Map<String,Boolean> productTypePresentMap = new Map<String,Boolean>();
        // Initialize the map
        productTypePresentMap.put(STANDARD_PRODUCT_LABEL, false);
        productTypePresentMap.put(SPARE_PARTS_LABEL, false);
        productTypePresentMap.put(PLANO_PRODUCT_LABEL, false);

        List<b2b_InventoryWrapper.cls_products> productsList = new List<b2b_InventoryWrapper.cls_products>();
        for (Product2 prod : products) {
            if (prod.Material__c != null && prod.Material__c != ''){
                productTypePresentMap.put(prod.Product_Type__c, true);
                b2b_InventoryWrapper.cls_products newProd = new b2b_InventoryWrapper.cls_products();
                newProd.materialNumber = prod.Material__c;
                productsList.add(newProd);
            }
        }

        System.debug('Product types within the callout: ' + productTypePresentMap);

        // Build wrapper object for callout request
        b2b_InventoryWrapper invWrap = new b2b_InventoryWrapper();
        if (acct.Division_SAP__c == null || acct.Division_SAP__c == '') { throw new CustomException('Missing Account.Division__c field value'); }

        invWrap.division = '00' + acct.Division_SAP__c;
        if (acct.SAP_Customer__c == null || acct.SAP_Customer__c == '') { throw new CustomException('Missing Account.SAP_Customer__c field value'); }
        invWrap.customerNumber = '0' + acct.SAP_Customer__c;

        // Adjust the Distribution channel value sent to Mulesoft/SAP, based on if we only have Plano products in the callout
        // If only Plano products, Dist. Channel = '08'; Else, Dist. Channel = '00';
        // If an order Id was passed-in, use the distribution channel value from that record, else check the product types contained in the request
        if(orderId != '' && orderId != null){
            invWrap.distributionChannel = ord.Distribution_Channel__c;
        }
        else if (productTypePresentMap.get(PLANO_PRODUCT_LABEL) == true && acct.Sales_Org__c=='0020') {
            invWrap.distributionChannel = '08';
        }
        else if (productTypePresentMap.get(PLANO_PRODUCT_LABEL) == true && acct.Sales_Org__c=='0040') {
            invWrap.distributionChannel = '00';
        }
        else {
            invWrap.distributionChannel = acct.Distribution_Channel__c; // MParrella | Coastal Cloud | 03-01-2023 | Swapping to get value from Account if non-Plano order
            // MParrella | Coastal Cloud | 03-08-2023 | Fixing issue in callout where a single digit in the distribution channel parameter would cause a 'customer not found' error response from mulesoft/SAP
            if (invWrap.distributionChannel.length() == 1) {
                invWrap.distributionChannel = '0' + invWrap.distributionChannel;
            }
        }
        if (acct.Sales_Org__c == null || acct.Sales_Org__c == '') { throw new CustomException('Missing Account.Sales_Org__c field value'); }
        invWrap.salesOrganization = acct.Sales_Org__c != null ? acct.Sales_Org__c : '';

        if (productsList.isEmpty()) { throw new CustomException('No Material Numbers found!'); }
        invWrap.products = productsList;

        /* Record Data Config: STOP */

        /* API Endpoint Config: START */

        // Use B2BIntegrationHelper class to get the appropriate integration information
        B2BIntegrationHelper.IntegrationHelperWrapper integrationWrapper = B2BIntegrationHelper.getIntegrationData();

        // Check if there was an exception encountered during the retrieval of integration data
        if (!integrationWrapper.hasData) {
            throw new CustomException('Unable to retrieve Integration data from Custom Metadata!');
        }

        // Construct the full endpoint url using the data returned from the B2BIntegrationHelper class
        String endpointURL = integrationWrapper.baseUrl + integrationWrapper.inventory_endpoint;

        // Store Endpoint Creds
        String clientKey = integrationWrapper.client_key;
        String clientSecret = integrationWrapper.client_secret;

        /* API Endpoint Config: STOP */

        HttpResponse res = new HttpResponse();
        try {
            HttpRequest req = new HttpRequest();
            Http http = new Http();

            req.setEndpoint(endpointURL);
            req.setMethod('POST');

            //these parts of the POST you may want to customize
            req.setBody(JSON.serialize(invWrap));
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('client_id', clientKey);
            req.setHeader('client_secret', clientSecret);
            req.setTimeout(120000);
            System.debug('**Inventory Request: ' + req);
            res = http.send(req);
        } catch(Exception e) {
            System.debug('Callout error: '+ e.getMessage());
            throw new CustomException(e.getMessage());
        }
        System.debug('### Inventory Callout Logging: START ###');
        System.debug('Endpoint: ' + endpointURL);
        System.debug('Request Body: ' + JSON.serialize(invWrap));
        System.debug('Response: ' + res);
        System.debug('Response Code: ' + res.getStatusCode());
        System.debug('Response Body: ' + res.getBody());
        System.debug('### Inventory Callout Logging: STOP ###');

        if (res.getStatusCode() == 200)
        {
            List<b2b_InventoryWrapper.response> responseList = (List<b2b_InventoryWrapper.response>) JSON.deserializeStrict(res.getBody(), List<b2b_InventoryWrapper.response>.class);
            return responseList;
        }
        else
        {
            String errorMessage = 'CALLOUT;'+ res.getStatusCode() + ';' + res.getBody();
            System.debug('**errorMessage: ' + errorMessage);
            throw new CalloutException (errorMessage);
        }
    }
    public static b2b_ShippingWrapper.response Shippingcal(b2b_ShippingWrapper request){
        HttpResponse res = new HttpResponse();
        Integer SuccessfulHttpRequest = 200;
        B2BIntegrationHelper.IntegrationHelperWrapper integrationWrapper = B2BIntegrationHelper.getIntegrationData();
        if (!integrationWrapper.hasData) {
            throw new CalloutException('Unable to retrieve Integration data from Custom Metadata!');
        }
        String endpointURL = integrationWrapper.baseUrl + integrationWrapper.freight_endpoint;

        // Store Endpoint Creds
        String clientKey = integrationWrapper.client_key;
        String clientSecret = integrationWrapper.client_secret;

        try {
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setEndpoint(endpointURL);
            req.setMethod('POST');

            //these parts of the POST you may want to customize
            req.setBody(JSON.serialize(request));
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('client_id', clientKey);
            req.setHeader('client_secret', clientSecret);
            req.setTimeout(120000);
            res = http.send(req);
        } catch(exception e) {
            System.debug('Callout error: '+ e);
        }
        System.debug(res.getBody());
        if (res.getStatusCode() == SuccessfulHttpRequest)
        {
            b2b_ShippingWrapper.response responseMap = (b2b_ShippingWrapper.response)JSON.deserialize(res.getBody(),b2b_ShippingWrapper.response.class);
            //List<b2b_TaxWrapper.response> responseMap = (List<b2b_TaxWrapper.response>)JSON.deserialize(res, List<b2b_TaxWrapper.response>.class);
            System.debug('responseMap' + responseMap);
            return responseMap;
        }
        else
        {
            Map<String,Object> jsonBody = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            String errorMessage = 'CALLOUT:'+jsonBody.get('errorMessage');
            throw new CalloutException (errorMessage);
        }

    }
    public static void SendOrder(List<b2b_OrderWrapper> request){
        HttpResponse res = new HttpResponse();
        Integer SuccessfulHttpRequest = 200;
        B2BIntegrationHelper.IntegrationHelperWrapper integrationWrapper = B2BIntegrationHelper.getIntegrationData();
        if (!integrationWrapper.hasData) {
            throw new CalloutException('Unable to retrieve Integration data from Custom Metadata!');
        }
        String endpointURL = integrationWrapper.baseUrl + integrationWrapper.order_endpoint;

        // Store Endpoint Creds
        String clientKey = integrationWrapper.client_key;
        String clientSecret = integrationWrapper.client_secret;

        try {
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            req.setEndpoint(endpointURL);
            req.setMethod('POST');

            //these parts of the POST you may want to customize
            req.setBody(JSON.serialize(request));
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('client_id', clientKey);
            req.setHeader('client_secret', clientSecret);
            req.setTimeout(120000);
            res = http.send(req);
        } catch(exception e) {
            System.debug('Callout error: '+ e);
        }
        System.debug(res.getBody());
        if (res.getStatusCode() == SuccessfulHttpRequest)
        {
            /*b2b_OrderWrapper.response responseMap = (b2b_OrderWrapper.response)JSON.deserialize(res.getBody(),b2b_OrderWrapper.response.class);
            //List<b2b_TaxWrapper.response> responseMap = (List<b2b_TaxWrapper.response>)JSON.deserialize(res, List<b2b_TaxWrapper.response>.class);
            System.debug('responseMap' + responseMap);
            return responseMap; */
        }
        else
        {
            Map<String,Object> jsonBody = (Map<String,Object>) JSON.deserializeUntyped(res.getBody());
            String errorMessage = 'CALLOUT:'+jsonBody.get('errorMessage');
            throw new CalloutException (errorMessage);
        }

    }

    /* Exception Sub-class: START */
    public class CustomException extends Exception {}
    /* Exception Sub-class: STOP */
}