global class ScheduledJobsTerminator implements Schedulable {
	global void execute(SchedulableContext SC) {
        List<CronJobDetail> CronJobDetailIDs = [SELECT Id FROM CronJobDetail WHERE Name LIKE 'SyncOrders%' OR Name LIKE 'SyncItems%'];
        for(CronTrigger cronTrigger : [SELECT CronJobDetailId,Id FROM CronTrigger WHERE CronJobDetailId IN :CronJobDetailIDs]) {
            System.abortJob(cronTrigger.Id);
        }
    }
}