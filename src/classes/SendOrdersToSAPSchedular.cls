global class SendOrdersToSAPSchedular implements Schedulable {
    global void execute(SchedulableContext SC)  {
        SendOrdersToSAP sqrb = new SendOrdersToSAP();
           sqrb.query=  'SELECT Reason_Code__c,' +
                   '            Distribution_Channel__c,' +
                   '            Sales_Org__c,EffectiveDate,' +
                   '            Account.SAP_Customer__c,' +
                   '            Type,' +
                   '            PO_Type__c,' +
                   '            ShippingFutureDate__c,' +
                   '            Placed_By__c,' +
                   '            Order_Reference__c,' +
                   '            BillingStreet,' +
                   '            BillingCity,' +
                   '            BillingPostalCode,' +
                   '            BillingCountryCode,' +
                   '            BillingPhoneNumber,' +
                   '            ShippingStreet,' +
                   '            ShippingCity,' +
                   '            ShippingPostalCode,' +
                   '            ShippingCountryCode,' +
                   '            ShippingPhone__c,' +
                   '            Ship_Cancel_Flag__c,' +
                   '            PoNumber,' +
                   '            Id, ' +
                   '            Name, ' +
                   '            Order_Notes__c,' +
                   '            CC_Details__c,' +
                   '            Account.Division_SAP__c,' +
                   '            Account.Distribution_Channel__c,' +
                   '            Account.name,' +
                   '            Account.BillingStreet,' +
                   '            Account.BillingCity,' +
                   '            Account.BillingPostalCode,' +
                   '            Account.BillingCountryCode,' +
                   '            Account.phone,Bill_To_Account__c,' +
                   '            Bill_To_Name__c,' +
                   '            Ship_To_Account__c,' +
                   '            Ship_To_Name__c' +
                   '     FROM   Order ' +
                   '     WHERE  Status =: OrdStatus ' +
                   '     AND    Order_Reference__c <> NULL';
			Database.executeBatch(sqrb,1);
         }
}