public class ShipmentSalesOrderItem
{
    public List<ShipmentItem> items { get; set; }
    public Boolean notify { get; set; }
    public Boolean appendComment { get; set; }
    public List<ShipmentTrack> tracks { get; set; }
}