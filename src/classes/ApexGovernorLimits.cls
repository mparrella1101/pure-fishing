global class ApexGovernorLimits {
    public static void printMaxLimits() {
        System.debug(' 1. Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
        System.debug(' 2. Total Number of records that can be queried  in this Apex code context: ' +  Limits.getLimitDmlRows());
        System.debug(' 3. Total Number of DML Statements allowed in this Apex code context: ' +  Limits.getLimitDmlStatements() );
        System.debug(' 4. Total Number of CPU usage time (in ms) allowed in this Apex code context: ' +  Limits.getLimitCpuTime());
        System.debug(' 5. Max Heap Size allowed in this Apex code context: ' +  Limits.getLimitHeapSize());
        System.debug(' 6. Total Number of Aggregate Queries allowed in this Apex code context: ' +  Limits.getLimitAggregateQueries());
        System.debug(' 7. Total Number of Async Calls allowed in this Apex code context: ' +  Limits.getLimitAsyncCalls());
        System.debug(' 8. Total Number of Callouts allowed in this Apex code context: ' +  Limits.getLimitCallouts());
        System.debug(' 9. Total Number of Email Invocations allowed in this Apex code context: ' +  Limits.getLimitEmailInvocations());
        System.debug('10. Total Number of Future Calls allowed in this Apex code context: ' +  Limits.getLimitFutureCalls());
        System.debug('11. Total Number of Queueable Jobs allowed in this Apex code context: ' +  Limits.getLimitQueueableJobs());
    }
    
    public static void printLimitsUsed() {
        System.debug(' 1. Number of Queries used in this Apex code so far: ' + Limits.getQueries());
        System.debug(' 2. Number of Rows queried in this Apex code so far: ' + Limits.getDmlRows());
        System.debug(' 3. Number of DML statements used so far: ' +  Limits.getDmlStatements());    
        System.debug(' 4. Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
        System.debug(' 5. Heap Size used so far: ' + Limits.getHeapSize());
        System.debug(' 6. Number of Aggregate Queries used so far: ' + Limits.getAggregateQueries());
        System.debug(' 7. Number of Async Calls used so far: ' + Limits.getAsyncCalls());
        System.debug(' 8. Number of Callouts used so far: ' + Limits.getCallouts());
        System.debug(' 9. Number of Email Invocations used so far: ' + Limits.getEmailInvocations());
        System.debug('10. Number of Future Calls used so far: ' + Limits.getFutureCalls());
        System.debug('11. Number of Queueable Jobs used so far: ' + Limits.getQueueableJobs());
    }
    
    public static void printLimitsLeft() {
        System.debug(' 1. Final number of SOQL Queries used so far: ' +  Limits.getQueries());
        System.debug(' 2. Final number of DML Rows used so far: ' +  Limits.getDmlRows());
        System.debug(' 3. Final number of DML Statements used so far: ' +  Limits.getDmlStatements());
        System.debug(' 4. Final number of CPU time (in ms) used so far: ' +  Limits.getCpuTime());
        System.debug(' 5. Final heap size used so far: ' +  Limits.getHeapSize());
        System.debug(' 6. Final number of Aggregate Queries used so far: ' +  Limits.getAggregateQueries());
        System.debug(' 7. Final number of Aync Calls used so far: ' +  Limits.getAsyncCalls());
        System.debug(' 8. Final number of Callouts used so far: ' +  Limits.getCallouts());
        System.debug(' 9. Final number of Email Invocations used so far: ' +  Limits.getEmailInvocations());
        System.debug(' 10. Final number of Future Calls used so far: ' +  Limits.getFutureCalls());
        System.debug(' 11. Final number of Queueable Jobs used so far: ' +  Limits.getQueueableJobs());
    }
}