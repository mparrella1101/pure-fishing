public class SalesOrderProductItem
{
    public Double base_discount_amount { get; set; }
    public String item_id { get; set; }
    public Double price { get; set; }
    public Double price_incl_tax { get; set; }
    public Integer qty_ordered { get; set; }
    public String sku { get; set; }
    public Double tax_amount { get; set; }
    public Double parent_item_id { get; set; }
    public Double discount_percent { get; set; }
}