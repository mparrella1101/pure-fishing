public class EpM2SyncJob implements Queueable {
    private Object item{get;Set;}
    private String token{get;Set;}
    private String eComStoreID{get;Set;}
    private String lastSalesOrderSyncTime{get;Set;}
    private String baseURL{get;Set;}
    private String supportEmail{get;Set;}
    /*
     * Constructor
     */
    public EpM2SyncJob(Object item, String token, String eComStoreID, String lastSalesOrderSyncTime, String baseURL, String supportEmail){
        System.debug('The item object:' + item);
        this.item = item;
        this.token = token;
        this.eComStoreID = eComStoreID;
        this.lastSalesOrderSyncTime = lastSalesOrderSyncTime;
        this.baseURL = baseURL;
        this.supportEmail = supportEmail;
    }
    
    public void execute(QueueableContext context) {
		EpM2Sync.syncOrders(item, token, eComStoreID, lastSalesOrderSyncTime, baseURL, supportEmail);
    }
}