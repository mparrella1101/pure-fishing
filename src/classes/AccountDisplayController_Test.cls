/**
 * @description Test class to cover the 'AccountDisplayController' Apex Class
 */

 /* Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud            July 7, 2022    Initial version
 */

@IsTest
public with sharing class AccountDisplayController_Test {
    @TestSetup
    static void setup_data() {
        // Create Test Account
        Account testAccount = CCTestUtils.createAccount('Apex Test Account', true);
    }

    @IsTest
    static void getAccountName_test() {
        // Get Account Id
        String testAccountId = [SELECT Id FROM Account WHERE Name = 'Apex Test Account' LIMIT 1].Id;

        Test.startTest();
        String result = AccountDisplayController.getAccountName(testAccountId);
        System.assertEquals('Apex Test Account', result);
        Test.stopTest();
    }
}