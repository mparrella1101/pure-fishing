/**
 * Created by Matt on 6/24/2022.
 */
/**
 * [CLASS DESCRIPTION GOES HERE]
 *
 *
 * Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           [DATE]          [DESCRIPTION]
 */

@IsTest
public with sharing class DmlOperationEventTrigger_Test {
    @TestSetup
    static void setup_data() {
        Account testAccount = CCTestUtils.createAccount('Apex test', true);
        Pricebook2 testPricebook2 = CCTestUtils.createPriceBook(true, 'Apex Pricebook', true, 'USD', true, true);
        Order testOrder = CCTestUtils.createOrder(testAccount, testPricebook2, true);
    }


    @IsTest
    static void test_trigger() {
        Order testOrder = [SELeCT Id FROM Order WHERE BillingCity = 'Gainesville' LIMIT 1];
        DmlOperation__e e = new DmlOperation__e();
        e.Payload__c = JSON.serialize(testOrder);
        e.Operation__c = 'MODIFY';
        TEst.startTest();
        Database.SaveResult sr = Eventbus.publish(e);
        Test.stopTest();
    }
}