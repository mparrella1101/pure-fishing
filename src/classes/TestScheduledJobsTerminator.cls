@isTest(seeAllData=true)
public class TestScheduledJobsTerminator {
	static testMethod void testScheduling() {
		System.Test.startTest();

        System.schedule('ScheduledJobsTerminator#1', '0 30 19 ? * * *', new ScheduledJobsTerminator());
        System.schedule('ScheduledJobsInitiator#1', '0 30 21 ? * * *', new ScheduledJobsInitiator());

        System.Test.stopTest();        
    }
}