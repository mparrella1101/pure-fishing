public class SalesOrderBillingAddress
{
    public String city { get; set; }
    public String country_id { get; set; }
    public String firstname { get; set; }
    public String lastname { get; set; }
    public String postcode { get; set; }
    public String region { get; set; }
    public String region_code { get; set; }
    public List<string> street { get; set; }
    public String telephone { get; set; }
}