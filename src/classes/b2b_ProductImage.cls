/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
1.0         Arsee Khan           Coastal Cloud           06/03/2022            Initial Version
*/
// This Class is used for Product image association
public class b2b_ProductImage {
    
    public Static void ProductImage(List<Id> ProdId)
    {
        //"ProductCode","Media Listing URL","Media Listing AltText 1""1373559","https://media.purefishing.com/s/purefishing/1373559_MS/1","Abu Garcia Revo® STX Spinning Reel";
        //string ProdId='01t56000009v98qAAA';
        If (ProdId!=null)
        {
            list<Product2> ProdDetails=[Select DAM_Set__c,Name,Material__c from Product2 where Id IN :ProdId];
            String header = 'ProductCode,Media Listing URL,Media Listing AltText 1 \n';
            list <String> Cont = new list <String>();
            for(Product2 prod :ProdDetails){
                 Cont.add(prod.Material__c + ',https://media.purefishing.com/s/purefishing/'+prod.DAM_Set__c+'/1 ,' + prod.Name);    
            }
            String yourFilesContent =header + String.join(Cont, '\n');
            //String yourFilesContent =header + Cont;
            system.debug('yourFilesContent' + yourFilesContent);
            //String header = 'ProductCode,Media Listing URL,Media Listing AltText 1 \n';
            //String Cont = ProdDetails.Material__c + ',https://media.purefishing.com/s/purefishing/'+ProdDetails.DAM_Set__c+'/1 ,' + ProdDetails.Name;
            //String yourFilesContent =header+Cont;
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
            conVer.PathOnClient = 'File.csv'; // The files name, extension is very important here which will help the file in preview.
            conVer.Title = 'ProductImageFile'; // Display name of the files
            //String base64Content = ( String ) JSON.deserialize(yourFilesContent, String.class );
            //conVer.VersionData = EncodingUtil.base64Decode(yourFilesContent); // converting your binary string to Blog
            conVer.VersionData = Blob.valueof(yourFilesContent);
            insert conVer;
            // First get the Content Document Id from ContentVersion Object
            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
            if(System.IsBatch() == false && System.isFuture() == false)
            {
                if(Limits.getFutureCalls() < Limits.getLimitFutureCalls())
                {
                    AssociateProdImageFuture.AssociateProdImage(conDoc);//Calling the future method
                }
                else {
                    Notifier.sendEmail('b2b_ProductImage - ProductImage - @future call was denied','purefishing@coastalcloud.us'); 
                }
            }
            
        }
        
        
    }
}