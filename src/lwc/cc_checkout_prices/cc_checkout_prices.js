/**
 * Created by Matt on 6/1/2022.
 */

import { LightningElement, api } from 'lwc';
import {
    subscribe,
    unsubscribe,
    onError,
    setDebugFlag,
    isEmpEnabled
} from 'lightning/empApi';


export default class cc_checkout_prices extends LightningElement {
    channelName = '/event/CheckoutPricesUpdate__e';
    @api subTotal = 0.00;
    shippingTotal = 0.00;
    taxTotal = 0.00;
    grandTotal = 0.00;

    subscription = {};


    connectedCallback() {
        this.registerErrorListener();
        this.handleSubscribe();
    }

    disconnectedCallback() {
        this.handleUnsubscribe();
    }

    handleChannelName(event) {
        this.channelName = event.target.value;
    }

    registerErrorListener() {
        // Invoke onError empApi method
        onError((error) => {
            console.log('Received error from server: ', JSON.stringify(error));
            // Error contains the server-side error
        });
    }


    handleSubscribe() {
        let self = this;
        const messageCallback = (response) => {
            let responseObj = JSON.parse(JSON.stringify(response));
            self.updatePriceValues(responseObj);
        };

        subscribe(this.channelName, -1, messageCallback).then(response => {
           this.subscription = response;
        });
    }

    handleUnsubscribe() {
        // Invoke unsubscribe method of empApi
        unsubscribe(this.subscription, response => {
            console.log('unsubscribe() response: ', JSON.stringify(response));
        });
    }

    updatePriceValues(input){
        this.subTotal = input.data.payload.Grand_Total__c;
        this.shippingTotal = input.data.payload.Shipping_Total__c;
        this.taxTotal = input.data.payload.Tax_Total__c;
        this.grandTotal = (this.subTotal + this.shippingTotal + this.taxTotal);
    }
}