/**
 * Created by Matt on 5/24/2022.
 */

import{ LightningElement, api } from 'lwc';
import { ReduceError } from 'c/ldsUtils';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import COMMUNITY_ID from '@salesforce/community/Id';

import getSuggestedOrder from '@salesforce/apex/ConnectAPIUtils.getSuggestedOrder';
import doInventoryCallout from '@salesforce/apex/MasterGridController.doInventoryCallout';
import updateOrderItemQuantity from '@salesforce/apex/ConnectAPIUtils.updateOrderItemQuantity';
import deleteSuggestedOrderItem from '@salesforce/apex/ConnectAPIUtils.deleteSuggestedOrderItem';
import getInfo from '@salesforce/apex/QuickOrderUploadController.getInfo';
import addProductsToCart from '@salesforce/apex/MasterGridController.addProductsToCart';


import LABEL_ADD_CART_SUCCESS from '@salesforce/label/c.cc_wishlist_add_cart_success_msg';
import LABEL_LOADING_TEXT from '@salesforce/label/c.cc_mg_inventory_loading';
import LABEL_ADD_ALL_BTN_TEXT from '@salesforce/label/c.cc_wishlist_add_all_items_btn_text';
import LABEL_CREATED_DATE from '@salesforce/label/c.cc_wishlist_column_header_created_date';
import LABEL_EXPIRATION_TEXT from '@salesforce/label/c.cc_wishlist_column_header_exp_date';
import LABEL_MODIFIED_TEXT from '@salesforce/label/c.cc_wishlist_last_modified_text';
import LABEL_ORDER_INFO_TEXT from '@salesforce/label/c.cc_wishlist_order_info_label';
import LABEL_LOADING_ORDER_ITEMS from '@salesforce/label/c.cc_wishlist_loading_order_items';
import LABEL_EMPTY_ORDER from '@salesforce/label/c.No_Order_items';
import LABEL_EMPTY_ORDER_BODY from '@salesforce/label/c.No_Order_items_body';
import LABEL_CLOSED_ORDER from '@salesforce/label/c.cc_wishlist_order_unavailable';
import LABEL_ADD_CART_ERROR from '@salesforce/label/c.cc_wishlist_add_item_cart_error';
import LABEL_STORE_DATA_ERROR from '@salesforce/label/c.cc_wishlist_store_data_error';
import LABEL_PROCESSING_TEXT from '@salesforce/label/c.cc_wishlist_processing_text';

/* Custom Label Import: START */

/* Custom Label Import: STOP */



export default class cc_wishlist_contents extends NavigationMixin(LightningElement) {
    @api recordId;
    @api effectiveAccountId;
    suggestedOrder; // Holds the suggested object
    suggestedOrderItems = []; // Holds the suggested order items
    orderHeader;
    inventoryData;
    isLoading = false;

    addAllButtonText = LABEL_ADD_ALL_BTN_TEXT;
    createdDateText = LABEL_CREATED_DATE;
    expirationDateText = LABEL_EXPIRATION_TEXT;
    lastModifiedText = LABEL_MODIFIED_TEXT;
    orderInfoText = LABEL_ORDER_INFO_TEXT;

    cartId;
    webstoreId;
    hasOrderData = false;
    currencyCode;

    addAllBtnDisabled = false;

    // Plano related stuff
    hasPlanoProducts = false;
    planoProductIds = [];



    hasOrderError = false;
    orderErrorMsg;
    isOrderEmpty = false;

    sortParam = 'CreatedDateDesc';
    sortOptions = [
        { value: 'CreatedDateDesc', label: this.labels.CreatedDateDesc },
        { value: 'CreatedDateAsc', label: this.labels.CreatedDateAsc }
    ];


    resetAddAllBtnText() {
        this.addAllButtonText = LABEL_ADD_ALL_BTN_TEXT;
    }

    get labels() {
        return {
            loadingCartItems: LABEL_LOADING_ORDER_ITEMS,
            addAllToCart: LABEL_ADD_ALL_BTN_TEXT,
            orderHeader: '',
            emptyWishlistHeaderLabel: LABEL_EMPTY_ORDER,
            emptyWishlistBodyLabel: LABEL_EMPTY_ORDER_BODY,
            closedWishlistLabel: LABEL_CLOSED_ORDER
        };
    }

    connectedCallback(){
        // Retrieve store info (async)
        this.retrieveInfo();


         // Get Wishlist data
        this.getOrderData().then(result => {
            if (result.length > 0) {
                this.getInventoryStatus(false);

                // If we have plano products, make another callout
                if (this.hasPlanoProducts) {
                    this.getInventoryStatus(true);
                }
            }
        });
    }


    async retrieveInfo() {
        return new Promise((resolve,reject) => {
            getInfo({
                userId : null,
                effectiveAccountId : this.effectiveAccountId,
                communityId : COMMUNITY_ID,
                webstoreId : null
            })
            .then(result => {
                    let data = {
                        cartId : result.cartId,
                        webstoreId : result.webstoreId
                        // MParrella | 07-07-2022 | Commenting this out as we are passing in effectiveAccountId parameter via experience builder attribute
//                        effectiveAccountId : result.effectiveAccountId
                    };
                    this.currencyCode = result.currencyCode;
                    this.webstoreId = result.webstoreId;
                    this.cartId = result.cartId;
                    resolve(data);
            })
            .catch(error => {
                let errorMsgs = ReduceError(error);
                console.log('Error in retrieveInfo() function: ', errorMsgs[0]);
                this.dispatchEvent(
                    new ShowToastEvent(
                        {
                            title:'',
                            message: LABEL_STORE_DATA_ERROR + ' ' + errorMsgs[0],
                            variant: 'error'
                        }
                    )
                );
                reject(errorMsgs[0]);
            });
        });
    }

    async getInventoryStatus(planoPresent) {
        return new Promise((resolve,reject) => {
            // Get all productIds from CartItems to pass to 'doInventoryCallout' method
            let productIdList = [];
            // If plano products are present in the request, we only want to send those products in their own callout
            if (planoPresent) {
                productIdList = this.planoProductIds;
            } else {
                this.suggestedOrderItems.forEach(item => {
                    if (!item.isPlano) {
                        productIdList.push(item.productId);
                    }
                });
            }
            if (productIdList.length > 0) {
                doInventoryCallout({
                    accountId : this.effectiveAccountId,
                    productIds : JSON.stringify(productIdList)
                })
                .then(result => {
                    this.inventoryData = result;
                    this.setInventory(result);
                    resolve(result);
                })
                .catch(error => {
                    let errorMsg = ReduceError(error);
                    console.log('*** Inventory callout error: ', error);
                    reject(errorMsg[0]);
                });
            }
        });
    }

    setInventory(input) {
        if (input.recordData) {
            let objMap = new Map(Object.entries(input.recordData));
            let tempArray = [...this.suggestedOrderItems];
            tempArray = JSON.parse(JSON.stringify(tempArray));
            tempArray.forEach(item => {
                if (objMap.has(item.productId)){
                    item.inventoryStatus = objMap.get(item.productId);
                }
            });
            this.suggestedOrderItems = JSON.parse(JSON.stringify(tempArray));
            console.log('suggestedItems: ', this.suggestedOrderItems);
            console.log(this.hasOrderData);
            console.log(this.isOrderEmpty);

        }
    }


    getOrderData() {
            return new Promise((resolve,reject) => {
                this.planoProductIds = [];
                this.hasPlanoProducts = false;
                 getSuggestedOrder({
                     communityId : COMMUNITY_ID,
                     effectiveAccountId : this.effectiveAccountId,
                     orderId : this.recordId,
                     productFields : 'Material__c,Model_Number__c,Case_Quantity__c,Hero_Image__c,Name,Product_Type__c'
                 })
                 .then(result => {
                     this.suggestedOrder = result;
                     if (result.orderItems && result.orderItems.length > 0) {
                         this.hasOrderData = true;
                         result.orderItems.forEach(item => {
                             if (item.isPlano) {
                                 this.planoProductIds.push(item.productId);
                             }
                             item.inventoryStatus = LABEL_LOADING_TEXT;
                         });

                         if (this.planoProductIds.length > 0) {
                             this.hasPlanoProducts = true;
                         }
                         this.suggestedOrderItems = result.orderItems;
                         this.generateProductDetailUrl();
                         if (this.inventoryData){
                             this.setInventory(this.inventoryData); // When reloading the data, we need to persist the inventory statuses
                         }
                     } else {
                         this.isOrderEmpty = true;
                         this.hasOrderData = false;
                         this.suggestedOrderItems = [];
                     }
                     this.orderHeader = this.suggestedOrder.name + ' (' + this.suggestedOrder.orderProductCount + ')';
                     resolve(this.suggestedOrderItems);
                 })
                 .catch(error => {
                    let errorMsgs = ReduceError(error);
                    console.log('getOrderData() ERROR: ', errorMsgs[0]);
                    reject(errorMsgs[0]);
                 });
            });
        }

     async generateProductDetailUrl() {
        return new Promise((resolve, reject) => {
            let tempItemList = [];
            tempItemList = [...this.suggestedOrderItems];
            tempItemList.forEach((loopItem) => {
                loopItem.productUrl = '';
                loopItem.productImageUrl = loopItem.photoURL;
                loopItem.productImageAltText = loopItem.imageAltText;

                // Build url for navigating to PDP
                this[NavigationMixin.GenerateUrl]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId : loopItem.productId,
                        objectApiName: 'Product2',
                        actionName: 'view'
                    }
                })
                .then((url) => {
                    console.log('url: ', url);
                    loopItem.productUrl = url;
                })
                .catch((error) => {
                    let errorMsgs = ReduceError(error);
                    console.log('Generate Product Url EXCEPTION: ', errorMsgs[0]);
                    reject(errorMsgs[0]);
                })
            });
//            this.suggestedOrderItems = JSON.parse(JSON.stringify(tempItemList));
            this.suggestedOrderItems = tempItemList;
            resolve(this.suggestedOrderItems);
        });
    }

    handleQuantityChanged(event) {
        let orderItemId = event.detail.orderItemId;
        let quantityVal = Number(event.detail.quantity);
        // Calc new 'totalPrice' parameter
        updateOrderItemQuantity({
            orderItemId,
            quantity: quantityVal
        })
        .then(result => {
            if (result) {
                // Result returns the Id of the update Suggested Order Item, so if successful we can go update our data in the front end accordingly
                let tempArray = [...this.suggestedOrderItems];
                tempArray.forEach(item => {
                    if (item.orderItemId === result) {
                        item.quantity = quantityVal;
                        item.totalPrice = item.quantity * item.salesPrice;
                    }
                });
                this.suggestedOrderItems = JSON.parse(JSON.stringify(tempArray));
            }
           this.updateOrderItemInformation(result);
        })
        .catch(error => {
            this.template.querySelectorAll('c-cc_wishlist_item').forEach(cmp => {
                if (cmp.item.orderItemId === orderItemId) {
                    cmp.setIsLoading(false);
                }
            });
            let errorMsgs = ReduceError(error);
            console.log('Update Quantity ERROR: ', errorMsgs[0]);
        });
    }

    updateOrderItemInformation(orderItemId) {
        this.template.querySelectorAll('c-cc_wishlist_item').forEach(cmp => {
            if (cmp.item.orderItemId === orderItemId) {
                cmp.setIsLoading(false);
            }
        });
    }

    handleRemoveItem(event) {
        const { orderItemId } = event.detail;
        deleteSuggestedOrderItem({
            communityId : COMMUNITY_ID,
            effectiveAccountId : this.effectiveAccountId,
            orderId : this.recordId,
            orderItemId : orderItemId
        })
        .then(result => {
            this.removeOrderItem(orderItemId);
        })
        .catch(error => {
            let errorMsgs = ReduceError(error);
            console.log('remove item ERROR: ', errorMsgs[0]);
        });
    }

    removeOrderItem(orderItemId) {
        const removedItem = (this.suggestedOrderItems || []).filter(item => {
            item.orderItemId === orderItemId
        })[0];

        const updatedWishlistItem = (this.suggestedOrderItems || []).filter(
            (item) => item.orderItemId !== orderItemId
        );

        this.suggestedOrderItems = updatedWishlistItem;
        if (this.suggestedOrderItems.length > 0){
            this.getOrderData();
        } else {
            this.isOrderEmpty = true;
        }
    }


    handleAddAllToCart() {
        this.addAllButtonText = LABEL_PROCESSING_TEXT;
        this.isLoading = true;
        this.addAllBtnDisabled = true;
        // build list of JSON items to send to back-end
        let itemsList = [];
        this.suggestedOrderItems.forEach(item => {
                let productObj = {
                    productId : item.productId,
                    qty : item.quantity,
                    modelNumber: item.modelNumber,
                    type: '',
                    productName: item.productName
                };
                itemsList.push(productObj);
        });
        addProductsToCart({
            JSONitemsList: JSON.stringify(itemsList),
            webstoreId: this.webstoreId,
            effectiveAccountId: this.effectiveAccountId,
            activeCartOrId: this.cartId
        })
        .then(result => {
            this.resetAddAllBtnText();
            this.isLoading = false;
            this.addAllBtnDisabled = false;
            // Refresh the cart icon
           this.dispatchEvent(new CustomEvent("cartchanged", {
                 bubbles: true,
                 composed: true
             }));
            if (result.messages) {
                if (result.messages[0].severity === 'success') {

                    // redirect to cart page
                    this[NavigationMixin.Navigate]({
                       type: 'standard__webPage',
                       attributes : {
                           url: '/cart/' + this.cartId
                       }
                    });
                } else {
                    let messages = JSON.parse(JSON.stringify(result.messages));
                    let highLvlErrorMsg = messages[0].title;
                    let errorMsg = messages[0].message
                    this.dispatchEvent(new ShowToastEvent({
                        title: highLvlErrorMsg,
                        message: errorMsg,
                        variant: 'error'
                    }));
                    }
                }
        })
        .catch(error => {
            this.resetAddAllBtnText();
            console.log(error);
            this.isLoading = false;
            this.addAllBtnDisabled = false;
            let errorMsgs = ReduceError(error);
            this.dispatchEvent(
                new ShowToastEvent(
                    {
                        title: '',
                        message: LABEL_ADD_CART_ERROR + ' ' + errorMsgs[0],
                        variant: 'error'
                    }
                )
            );
        });
    }

    handleSingleAddToCart(event) {
        let itemArray = [];
        let payloadData = JSON.parse(JSON.stringify(event.detail));
        itemArray.push(payloadData);
        addProductsToCart({
            JSONitemsList: JSON.stringify(itemArray),
            webstoreId: this.webstoreId,
            effectiveAccountId: this.effectiveAccountId,
            activeCartOrId: this.cartId
        })
        .then(result => {
            this.isLoading = false;
            if (result.messages) {
                if (result.messages[0].severity === 'success') {
                    this.dispatchEvent(new CustomEvent("cartchanged", {
                         bubbles: true,
                         composed: true
                     }));

                     this.dispatchEvent(new ShowToastEvent({
                         title: '',
                         message: LABEL_ADD_CART_SUCCESS,
                         variant: 'success'
                     }));
                } else {
                    let messages = JSON.parse(JSON.stringify(result.messages));
                    let highLvlErrorMsg = messages[0].title;
                    let errorMsg = messages[0].message
                    this.dispatchEvent(new ShowToastEvent({
                        title: highLvlErrorMsg,
                        message: errorMsg,
                        variant: 'error'
                    }));
                }
                this.updateOrderItemInformation(payloadData.orderItemId);
            } else {
                 let messages = JSON.parse(JSON.stringify(result.messages));
                 console.log('error: ', messages);
                 let highLvlErrorMsg = messages[0].title;
                 let errorMsg = messages[0].message
                 this.dispatchEvent(new ShowToastEvent({
                     title: highLvlErrorMsg,
                     message: errorMsg,
                     variant: 'error'
                 }));
            }
            this.updateOrderItemInformation(payloadData);
        })
        .catch(error => {
            this.isLoading = false;
           let messages = ReduceError(error);
           console.log('error: ', messages[0]);
           this.dispatchEvent(new ShowToastEvent({
               title: '',
               message: messages[0],
               variant: 'error'
           }));
           this.updateOrderItemInformation(payloadData);
        });
    }
}