import {LightningElement, wire, api, track} from 'lwc';
import getOrdLineShipData from '@salesforce/apex/OrderLineWithShipment.getOrderLineWithShipment';
import materialModelNum from '@salesforce/label/c.Material_Model_no';
import Prod from '@salesforce/label/c.Product';
import Ordered from '@salesforce/label/c.Ordered';
import Shipped from '@salesforce/label/c.Shipped';
import unitPrice from '@salesforce/label/c.Unit_Price';
import extPrice from '@salesforce/label/c.Ext_Price';
import lShip from '@salesforce/label/c.Shipment';
import lQty from '@salesforce/label/c.Quantity';
import lTrk from '@salesforce/label/c.Tracking';
import lShips from '@salesforce/label/c.Shipments';

import {getPagesOrDefault, handlePagerChanged} from 'c/paginatorUtils';
const ShipmentColumns = [
    // { label: 'Id', fieldName: 'Id' },
    // { label: 'Name', fieldName: 'Name', type: 'string' },
    
    { label: lShip, fieldName: 'No',fixedWidth:150,
        cellAttributes: {class: {fieldName: 'format'},alignment: 'left'}},
    { label: lQty, fieldName: 'Qty',fixedWidth:150,
        cellAttributes: {class: {fieldName: 'format'},alignment: 'left'}},
    { label:lTrk, fieldName: 'TrackingNo',fixedWidth:200,
        cellAttributes: {class: {fieldName: 'format'},alignment: 'left'}},
];

export default class OrderLineWithShipmentPaginated extends LightningElement 
{
    label = {
        lShips
        
    };

    @api pageTitle;
    @api RetrieveLimit;
    ShipmentColumns = ShipmentColumns;

    @api EntriesPerPage ;
    @api MaxPageButtons ;
    @api hideOrDisableUnnecessaryButtons ;

    @api recordId;

    @track multiple = true;
    @track OrderLineWithShipment;

    label = {
        Prod,
        materialModelNum,
        Ordered,
        Shipped,
        unitPrice,
        extPrice
    };

    OrdLineTableHeadings = [
        this.label.materialModelNum,
        this.label.Prod,
        this.label.Ordered,
        this.label.Shipped,
        this.label.unitPrice,
        this.label.extPrice
    ];


    @wire(getOrdLineShipData, {OrdId: '$recordId', retrieveLimit: '$RetrieveLimit'})
    wiredOrderLineWithShipment({error, data}) {
        if (data) {
            console.log('response ',data);
            console.log(JSON.stringify(data, null, '\t'));
            this.OrderLineWithShipment = data;

        } else if (error) {
            console.log('inside error ',error);
            this.error = error;
            console.log(error);
            debugger;
        }
    }

    _currentlyVisible = [];

    getPagesOrDefault = getPagesOrDefault.bind(this);
    handlePagerChanged = handlePagerChanged.bind(this);
    connectedCallback()
    {
        console.log('Welcome',this.recordId);  
    }
    @api
    get currentlyVisible() {
        if (this.OrderLineWithShipment.length == 0) {
            this.OrderLineWithShipment = undefined;  //'';
        }

        const pages = this.getPagesOrDefault();
        return pages.length === 0 ? this._currentlyVisible : pages;
    }

    set currentlyVisible(value) {
        this._currentlyVisible = value;
    }
}