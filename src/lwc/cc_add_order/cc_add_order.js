import { LightningElement, api, wire } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { getObjectInfo, getPicklistValues } from "lightning/uiObjectInfoApi";
import getContactPointAddresses from '@salesforce/apex/AddOrderController.getContactPointAddresses';
import getOrdDisChannelandOrdreason from '@salesforce/apex/AddOrderController.OrdDisChannel';
import ORDER_OBJECT from "@salesforce/schema/Order";
import ORDER_REASON_CODE from "@salesforce/schema/Order.Reason_Code__c";
import ACCOUNT from "@salesforce/schema/Order.AccountId";
import STATUS from "@salesforce/schema/Order.Status";
import PRICEBOOK_ID from "@salesforce/schema/Order.Pricebook2Id";
import ORDER_DATE from "@salesforce/schema/Order.EffectiveDate";
import BILL_TO from "@salesforce/schema/Order.Bill_To__c";
import PRODUCT_CATEGORY from "@salesforce/schema/Order.Product_Category__c";
import BILL_TO_ACCOUNT from "@salesforce/schema/Order.Bill_To_Account__c";
import BILL_TO_NAME from "@salesforce/schema/Order.Bill_To_Name__c";
import SHIP_TO from "@salesforce/schema/Order.Ship_To__c";
import SHIP_TO_ACCOUNT from "@salesforce/schema/Order.Ship_To_Account__c";
import SHIP_TO_NAME from "@salesforce/schema/Order.Ship_To_Name__c";
import SHIPPING_STREET from "@salesforce/schema/Order.ShippingStreet";
import SHIPPING_CITY from "@salesforce/schema/Order.ShippingCity";
import SHIPPING_STATE from "@salesforce/schema/Order.ShippingState";
import SHIPPING_STATE_CODE from "@salesforce/schema/Order.ShippingStateCode";
import SHIPPING_POSTAL_CODE from "@salesforce/schema/Order.ShippingPostalCode";
import SHIPPING_COUNTRY from "@salesforce/schema/Order.ShippingCountry";
import SHIPPING_COUNTRY_CODE from "@salesforce/schema/Order.ShippingCountryCode";
import PO_NUMBER_FIELD from '@salesforce/schema/Order.PoNumber';
import CURRENCY_ISO_CODE_FIELD from '@salesforce/schema/Order.CurrencyIsoCode';
import SALES_ORG_FIELD from '@salesforce/schema/Order.Sales_Org__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import createOrder from '@salesforce/label/c.Create_Order';
import selectShippingAddress from '@salesforce/label/c.Select_shipping_address';
import address from '@salesforce/label/c.Address';
import cancel from '@salesforce/label/c.Cancel';
//import STND_PRICEBOOK_LABEL from '@salesforce/label/c.cc_standard_pricebook_id';
import noShippingAddresses from '@salesforce/label/c.No_Shipping_Addresses';
import orderReasonCode from '@salesforce/label/c.Order_Reason_Code';
import DIST_CHANNEL from "@salesforce/schema/Order.Distribution_Channel__c";
import POSpeChrError from '@salesforce/label/c.POSpecialCharError';



export default class Cc_add_order extends NavigationMixin(LightningElement) {

    label = {
        createOrder,
        selectShippingAddress,
        address,
        cancel,
        noShippingAddresses,
        orderReasonCode
    };

    @api recordId;
    loading = false;
    reasonCodeOptions = [];
    productCategoryOptions =[];
    pricebookId;
    addressString;
    addressesExist;
    addresses = [];
    selectedShippingAddress = '';
    shippingAddress = {};
    addressMap = new Map();
    addressSelected = false;
    accountFields;
    billTo;
    billToName;
    billToAccount;
    shipTo;
    shipToName;
    shipToAccount;
    customerNumberSAP;
    poNumber;
    orderReasonCode = '100';
    productCategory='PF Finished Goods';
    disChannel='00';
    OrdMin='0';

    @wire(getObjectInfo, { objectApiName: ORDER_OBJECT })
    orderObj;

    // @wire(getPicklistValues, {
    //     recordTypeId: "$accountMetadata.data.defaultRecordTypeId",
    //     fieldApiName: PRODUCT_NAME_FIELD,
    // })
    // productCatalogNames;

    @wire(getPicklistValues, {
        recordTypeId: "$orderObj.data.defaultRecordTypeId",
        fieldApiName: ORDER_REASON_CODE,
    })
    getReasonCodes({ error, data }){
        if(error){
            this.error = error;
            this.showToast('error getting order reason codes', error.message, 'error');
        } else if(data){
            this.reasonCodeOptions = data.values.map(item => {
                return {
                    label: item.label,
                    value: item.value
                };
            });
            
        }
    }
    @wire(getPicklistValues, {
        recordTypeId: "$orderObj.data.defaultRecordTypeId",
        fieldApiName: PRODUCT_CATEGORY,
    })
    getProductCategory({ error, data }){
        if(error){
            this.error = error;
            this.showToast('error getting order reason codes', error.message, 'error');
        } else if(data){
            this.productCategoryOptions = data.values.map(item => {
                return {
                    label: item.label,
                    value: item.value
                };
            });
            
        }
    }

    connectedCallback(){
        // this.loading = true;
        this.getContactPointAddresses();
        
    }
    
    
    getContactPointAddresses(){
        getContactPointAddresses({ accountId : this.recordId })
        .then(result => {
            // this.loading = false;
            this.pricebookId = result.account.Pricebook__c != null ? result.account.Pricebook__c : result.pricebookId;
            this.billToName = result.account.Name;
            this.billToAccount = result.account.SAP_Customer__c;
            this.shipToName = result.account.Name;
            this.accountFields = result.account;
            // this.shipToAccount = result.account.SAP_Customer__c;
            this.addressesExist = result.contactPointAddresses.length > 0 ? true : false;
            this.addresses = result.contactPointAddresses.map(item => {
                this.addressMap.set(item.Id, item);
                return {
                    label: item.SAP_Customer__c + ' - ' + item.Address.street + ' ' + item.Address.city + ' ' + item.Address.stateCode + ', ' + item.Address.postalCode, // MParrella | 10-19-2022 | TSK-00232806: Adding SAP Customer number to address choices
                    value: item.Id
                };
            });
        })
        .catch(error => {
            console.log(error);
            this.showToast('error','error getting account details', 'error');
        });
    }

    handleChange(event){
        this.addressSelected = true;
        this.shippingAddress = this.addressMap.get(event.detail.value.toString()).Address;
        this.billTo = this.addressMap.get(event.detail.value.toString()).Billing_Shipping_Account__c != undefined ? this.addressMap.get(event.detail.value.toString()).Billing_Shipping_Account__c : '';
        this.shipTo = this.addressMap.get(event.detail.value.toString()).Billing_Shipping_Account__c != undefined ? this.addressMap.get(event.detail.value.toString()).Billing_Shipping_Account__c : '';
        this.shipToName = this.addressMap.get(event.detail.value.toString()).Name;
        this.shipToAccount = this.addressMap.get(event.detail.value.toString()).SAP_Customer__c != undefined ? this.addressMap.get(event.detail.value.toString()).SAP_Customer__c : '';
        // this.customerNumberSAP = this.addressMap.get(event.detail.value.toString()).Billing_Shipping_Account__r.SAP_Customer__c;
        this.selectedShippingAddress = event.detail.value;        
    }

    handleReasonCodeChange(event){
        this.orderReasonCode = event.detail.value;
        // this.reasonCodeValue = this.orderReasonCode;
    }
    handleProductCategoryChange(event){
        this.productCategory = event.detail.value;
        getOrdDisChannelandOrdreason({productCategory:this.productCategory,accountId : this.recordId})
        .then(result => {
            // this.loading = false;
            console.log('getOrdDisChannelandOrdreason' + JSON.stringify(result));
            this.orderReasonCode = result.ordReason;
            this.disChannel = result.disChannel;
           // this.OrdMin=result.OrdMin;
        })
        .catch(error => {
            this.showToast('error','error getting Distribute Channel', 'error');
        });
    }

    handlePOChange(event) {
        this.poNumber = event.target.value;
        
            if(!this.poNumber.match(/^[a-zA-Z0-9_]*$/))
                        {
                            console.log('Inside special char check');
                            this.showToast('error',POSpeChrError, 'error');
                        }
        
    }
    createOrder(){
        this.loading = true;
        const fields = {};
        fields[ACCOUNT.fieldApiName] = this.recordId;
        fields[STATUS.fieldApiName] = 'Draft';
//        fields[PRICEBOOK_ID.fieldApiName] = this.pricebookId;
        fields[PRICEBOOK_ID.fieldApiName] = STND_PRICEBOOK_LABEL;
        fields[ORDER_DATE.fieldApiName] = new Date();
        fields[BILL_TO.fieldApiName] = this.billTo;
        fields[BILL_TO_NAME.fieldApiName] = this.billToName;
        fields[BILL_TO_ACCOUNT.fieldApiName] = this.billToAccount;
        fields[SHIP_TO.fieldApiName] = this.shipTo;
        fields[SHIP_TO_NAME.fieldApiName] = this.shipToName;
        fields[SHIP_TO_ACCOUNT.fieldApiName] = this.shipToAccount;
        fields[SHIPPING_STREET.fieldApiName] = this.shippingAddress.street;
        fields[SHIPPING_CITY.fieldApiName] = this.shippingAddress.city;
        fields[SHIPPING_STATE.fieldApiName] = this.shippingAddress.state;
        fields[SHIPPING_STATE_CODE.fieldApiName] = this.shippingAddress.stateCode;
        fields[SHIPPING_POSTAL_CODE.fieldApiName] = this.shippingAddress.postalCode;
        fields[SHIPPING_COUNTRY.fieldApiName] = this.shippingAddress.country;
        fields[SHIPPING_COUNTRY_CODE.fieldApiName] = this.shippingAddress.countryCode;
        fields[ORDER_REASON_CODE.fieldApiName] = this.orderReasonCode; 
        fields[PRODUCT_CATEGORY.fieldApiName] = this.productCategory;
        fields[PO_NUMBER_FIELD.fieldApiName] = this.poNumber;
        fields[CURRENCY_ISO_CODE_FIELD.fieldApiName] = this.accountFields.CurrencyIsoCode;
        fields[SALES_ORG_FIELD.fieldApiName] = this.accountFields.Sales_Org__c;
        fields[DIST_CHANNEL.fieldApiName] = this.disChannel;
        //fields[MIN_ORD.fieldApiName] = this.OrdMin;
        // fields[MAGENTOID.fieldApiName] = '123';

        const recordInput = {
            apiName: ORDER_OBJECT.objectApiName,
            fields: fields
        };



        createRecord(recordInput)
        .then(record => {
            this.showToast('success', 'Order created successfully!', 'success');
            // this.closeQuickAction();
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: record.id,
                    objectApiName: ORDER_OBJECT.objectApiName,
                    actionName: 'view'
                }
            });
        })
        .catch(error => {
            console.log(error);
            this.showToast('error', 'error creating order', 'error');
            this.loading = false;
        });        
    }

    closeQuickAction(){
        this.dispatchEvent(new CustomEvent('closeQuickAction'));
    }

    showToast(title, message, variant){
        this.dispatchEvent(new ShowToastEvent({
            title, message, variant
        }));
    }

}