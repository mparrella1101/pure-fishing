/**
 * Created by Matt on 1/5/2023.
 */

import { LightningElement, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import getFieldData from '@salesforce/apex/TranslationUtilities.getSingleProductFieldData';
import FIELD_VARIANT_PARENT from '@salesforce/schema/Product2.Variation_Parent__c';
import FIELD_ID from '@salesforce/schema/Product2.Id';
import LABEL_ERROR_TEXT from '@salesforce/label/c.cc_pdp_error_message';


export default class cc_pdp_product_field extends LightningElement {
    @api recordId;
    @api fieldApiNameInput;
    @api labelTextSize;
    @api valueTextSize;
    @api showLabel;
    fieldLabel;
    fieldValue;

    errorText = LABEL_ERROR_TEXT;

    valueTextSizeClass;
    labelTextSizeClass;

    hasError = false;

    @wire(getRecord, { recordId: '$recordId', fields: [FIELD_ID, FIELD_VARIANT_PARENT]})
        wiredRecord({ error, data }) {
            let productId = '';
            if (data) {
                if (data.fields.Variation_Parent__c.value) {
                    productId = data.fields.Variation_Parent__c.value;
                } else {
                    productId = this.recordId;
                }
                getFieldData({
                    product2Id : productId,
                    fieldApiName : this.fieldApiNameInput
                })
                .then(result => {
                    if (result['error']) {
                        console.log('error present: ', result['error']);
                        this.hasError = true;
                    } else {
                        this.hasError = false;
                        this.fieldLabel = Object.keys(result)[0];
                        this.fieldValue = result[Object.keys(result)[0]];
                    }
                })
                .catch(error => {
                    console.log('Error retrieving field data for ' + this.fieldApiNameInput + '. Details: ' + error);
                });
            } else if (error) {
                console.log('wired error: ', error);
            }
        }

        connectedCallback() {
                // Set the style based on the 'Label Text Size' selected in builder
                switch (this.labelTextSize) {
                    case 'Small':
                        this.labelTextSizeClass = 'small-text';
                        break;

                    case 'Medium':
                        this.labelTextSizeClass = 'medium-text';
                        break;

                    case 'Large':
                        this.labelTextSizeClass = 'large-text';
                        break;

                    case 'X-Large':
                        this.labelTextSizeClass = 'x-large-text';
                        break;

                    default:
                        this.labelTextSizeClass = 'medium-text';
                        break;
                }
                switch (this.valueTextSize) {
                        case 'Small':
                            this.valueTextSizeClass = 'small-text';
                            break;

                        case 'Medium':
                            this.valueTextSizeClass = 'medium-text';
                            break;

                        case 'Large':
                            this.valueTextSizeClass = 'large-text';
                            break;

                        case 'X-Large':
                            this.valueTextSizeClass = 'x-large-text';
                            break;

                        default:
                            this.valueTextSizeClass = 'medium-text';
                            break;
                    }
                }
            }