/**
 * Created by Matt on 5/10/2022.
 */

import { LightningElement, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ReduceError } from 'c/ldsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getInfo from '@salesforce/apex/QuickOrderUploadController.getInfo';
import getCartItems from '@salesforce/apex/ConnectAPIUtils.getCartItems';
import updateCartItem from '@salesforce/apex/ConnectAPIUtils.updateCartItem';
import deleteCartItem from '@salesforce/apex/ConnectAPIUtils.deleteCartItem';
import deleteCart from '@salesforce/apex/ConnectAPIUtils.deleteCart';
import createCart from '@salesforce/apex/ConnectAPIUtils.createCart';
import getCartSummary from '@salesforce/apex/ConnectAPIUtils.getCartSummary';
import generateCartData from '@salesforce/apex/MasterGridController.generateCartData';
import doInventoryCallout from '@salesforce/apex/MasterGridController.doInventoryCallout';

/* Custom Label Import: START */
import LABEL_CART_LOADING_LABEL from '@salesforce/label/c.cc_cart_loading_label';
import LABEL_CART_CLEAR_CART_LABEL from '@salesforce/label/c.cc_cart_clear_cart_label';
import LABEL_CART_ADD_SUGGESTED_ORDER_LABEL from '@salesforce/label/c.cc_cart_add_suggested_order_label';
import LABEL_CART_SORT_BY_LABEL from '@salesforce/label/c.cc_cart_sort_by_label';
import LABEL_CART_HEADER_LABEL from '@salesforce/label/c.cc_cart_header_label';
import LABEL_CART_EMPTY_HEADER_LABEL from '@salesforce/label/c.cc_cart_empty_header_label';
import LABEL_CART_EMPTY_BODY_LABEL from '@salesforce/label/c.cc_cart_empty_body_label';
import LABEL_CART_CLOSED_CART_LABEL from '@salesforce/label/c.cc_cart_closed_cart_label';
import LABEL_CART_CREATED_DATE_DESC_LABEL from '@salesforce/label/c.cc_cart_created_date_desc_label';
import LABEL_CART_CREATED_DATE_ASC_LABEL from '@salesforce/label/c.cc_cart_created_date_asc_label';
import LABEL_CART_SORT_NAME_ASC_LABEL from '@salesforce/label/c.cc_cart_sort_name_asc_label';
import LABEL_CART_SORT_NAME_DESC_LABEL from '@salesforce/label/c.cc_cart_sort_name_desc_label';
import LABEL_CART_DOWNLOAD_CSV_LABEL from '@salesforce/label/c.cc_cart_download_csv_label';
import LABEL_INVENTORY_LOADING_LABEL from '@salesforce/label/c.cc_mg_inventory_loading';
import LABEL_CART_DOWNLOAD_FILENAME from '@salesforce/label/c.cc_quickorder_cart_download_filename';
/* Custom Label Import: STOP */

import {
    subscribe,
    publish,
    createMessageContext,
    unsubscribe,
    APPLICATION_SCOPE,
    MessageContext
} from 'lightning/messageService';

import CART_MC from '@salesforce/messageChannel/CartMessageChannel__c';

const CLOSED_CART_MESSAGE_REGEXP = new RegExp(/Cart Id: '\S+' is Deleted/, 'i');

import COMMUNITY_ID from '@salesforce/community/Id';

const CART_CHANGED_EVT = 'cartchanged';
const CART_ITEMS_UPDATED_EVT = 'cartitemsupdated';

export default class cc_cart_contents extends NavigationMixin(LightningElement) {
    @api effectiveAccountId;
    @api recordId; // Cart Id
    csvDownloadText = LABEL_CART_DOWNLOAD_CSV_LABEL;
    productIds;
    showWishlistCmp = false;
    webstoreId;
    isCartClosed = false;
    pageToken = null;
    nextPageToken = null;
    prevPageToken = null;
    cartItems = []; // holds all cart item records
    _cartItemCount = 0;
    currencyCode;
    cart;
    isLoading = false;

    cartDownloadLabel = LABEL_CART_DOWNLOAD_FILENAME;


    hasCartItemError = false;
    hasCartError = false;
    cartErrorMsg = '';

    // Plano-related stuff
    hasPlanoProducts = false;
    planoProductIds = []; // holds all current plano products in cart
    planoInventoryMap = new Map(); // holds plano-product-id -> quantity level returned


    wishlistData = [];

    inventoryData = new Map();

    sortParam = 'CreatedDateDesc';
    sortOptions = [
        { value: 'CreatedDateDesc', label: this.labels.CreatedDateDesc },
        { value: 'CreatedDateAsc', label: this.labels.CreatedDateAsc },
        { value: 'NameAsc', label: this.labels.NameAsc },
        { value: 'NameDesc', label: this.labels.NameDesc }
    ];

    get cartHeader() {
        return `${this.labels.cartHeader} (${this._cartItemCount})`;
    }

    get isCartEmpty() {
        // If the items are an empty array (not undefined or null), we know we're empty.
        return Array.isArray(this.cartItems) && this.cartItems.length === 0;
    }

    get labels() {
        return {
            loadingCartItems: LABEL_CART_LOADING_LABEL,
            clearCartButton: LABEL_CART_CLEAR_CART_LABEL,
            addAllToWishlist: LABEL_CART_ADD_SUGGESTED_ORDER_LABEL,
            sortBy: LABEL_CART_SORT_BY_LABEL,
            cartHeader: LABEL_CART_HEADER_LABEL,
            emptyCartHeaderLabel: LABEL_CART_EMPTY_HEADER_LABEL,
            emptyCartBodyLabel: LABEL_CART_EMPTY_BODY_LABEL,
            closedCartLabel: LABEL_CART_CLOSED_CART_LABEL,
            CreatedDateDesc: LABEL_CART_CREATED_DATE_DESC_LABEL,
            CreatedDateAsc: LABEL_CART_CREATED_DATE_ASC_LABEL,
            NameAsc: LABEL_CART_SORT_NAME_ASC_LABEL,
            NameDesc: LABEL_CART_SORT_NAME_DESC_LABEL
        };
    }

    connectedCallback() {
        this.retrieveInfo();
        this.subscribeMC();
        this.getCartItemData().then(result => {
            if (result.length > 0){
                this.getInventoryStatus(false);

                // If we have plano products, make another callout
                if (this.hasPlanoProducts) {
                    this.getInventoryStatus(true);
                }
            }
        });
    }

    @wire(MessageContext)
    messageContext;

    subscribeMC() {
        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                CART_MC,
                (message) => {
                    this.handleMCmessage(message);
                },
                { scope : APPLICATION_SCOPE }
            );
        }
    }

    sendMessageService(value) {
        let payload = {
            buttonDisabled : value
        };
        publish(this.messageContext, CART_MC, payload);
    }

    handleMCmessage(input) {
        // we shouldn't be RECEIVING messages for this cmp..
    }

    async retrieveInfo() {
        return new Promise((resolve,reject) => {
           getInfo({
               userId: null,
               effectiveAccountId: this.effectiveAccountId,
               communityId : COMMUNITY_ID,
               webstoreId : null
           })
           .then(result => {
               let cartData = {
                   cartId : this.recordId,
                   webstoreId : result.webstoreId
               };
               this.currencyCode = result.currencyCode;
               this.webstoreId = cartData.webstoreId;

               resolve(cartData);
           })
           .catch(error => {
               let errorMsgs = ReduceError(error);
               reject(errorMsgs[0]);
           })
        });
    }

    getCartItemData() {
        this.isLoading = true;
        this.hasCartError = false;
        this.planoProductIds = [];
        this.hasCartItemError = false;
        return new Promise((resolve,reject) => {
            getCartItems({
                communityId : COMMUNITY_ID,
                effectiveAccountId : this.effectiveAccountId,
                activeCartOrId : this.recordId,
                productFields : 'Material__c, Model_Number__c, Case_Quantity__c, UPC__c, Product_Type__c',
                pageParam : this.pageToken,
                pageSize : 25,
                sortParam : this.sortParam
            })
            .then(result => {
                this._cartItemCount = result.cartSummary.uniqueProductCount;
                if (result.cartItems.length > 0){
                    // Check if we have any Cart-level errors
                    if (result.cartSummary.messagesSummary.hasErrors) {
                        this.hasCartError = true;
                        this.cartErrorMsg = result.cartSummary.messagesSummary.limitedMessages[0].message;
                    } else {
                        this.cartErrorMsg = '';
                    }

                    // Check if there are more pages
                    if (result.nextPageToken) {
                        // Show next page button on ui
                        this.showNextPageIcon = true;
                        this.nextPageToken = result.nextPageToken;
                    } else {
                        this.showNextPageIcon = false;
                        this.nextPageToken = null;
                    }
                    if (result.previousPageToken) {
                        this.showPrevPageIcon = true;
                        this.prevPageToken = result.previousPageToken;
                    } else {
                        this.showPrevPageIcon = false;
                        this.prevPageToken = null;
                    }

                    // Iterate over list and push the 'cartItem' attribute from the returned data into the list for displaying
                    let tempList = [...result.cartItems];
                    tempList.forEach(tempItem => {
                        // check if Plano product is present
                        if (tempItem.cartItem.productDetails.fields.Product_Type__c != 'Spare Part' &&
                            tempItem.cartItem.productDetails.fields.Product_Type__c != 'Pure Fishing Legacy') {
                                this.planoProductIds.push(tempItem.cartItem.productDetails.productId);
                        }


                        if (tempItem.cartItem.messagesSummary.hasErrors){
                            this.hasCartItemError = true;
                            tempItem.cartItem.hasErrors = true;
                            tempItem.cartItem.errorMsg = tempItem.cartItem.messagesSummary.limitedMessages[0].message;
                        }
                        else {
                            tempItem.cartItem.hasErrors = false;
                            tempItem.cartItem.errorMsg = '';
                        }
                    });


                    // Enable or disable the checkout btn
                    if (!this.hasCartItemError && !this.hasCartError) {
                        this.sendMessageService(false);
                    } else {
                        this.sendMessageService(true);
                    }

                    this.cartItems = tempList;
                    if (this.inventoryData){
                        this.setInventory(this.inventoryData); // When reloading the data, we need to persist the inventory statuses
                    } else {
                        tempItem.cartItem.inventoryStatus = LABEL_INVENTORY_LOADING_LABEL;
                    }
                    this.generateProductDetailUrl();
                } else {
                    this.cartItems = [];
                    this.sendMessageService(true);
                }

                if (this.planoProductIds.length > 0) {
                    this.hasPlanoProducts = true;
                } else {
                    this.hasPlanoProducts = false;
                }
                resolve(this.cartItems);
                this.isLoading = false;
            })
            .catch(error => {
                this.isLoading = false;
                let errors = ReduceError(error);
                this.isCartClosed = CLOSED_CART_MESSAGE_REGEXP.test(errors[0]);
                reject(errors[0]);
            });
        });
    }

    async generateProductDetailUrl() {
        return new Promise((resolve, reject) => {
            let tempItemList = [];
            tempItemList = [...this.cartItems];
            tempItemList.forEach((loopItem) => {
                loopItem.cartItem.productUrl = '';
                loopItem.cartItem.productImageUrl = loopItem.cartItem.productDetails.thumbnailImage.url;
                loopItem.cartItem.productImageAltText = loopItem.cartItem.productDetails.thumbnailImage.alternateText;

                // Build url for navigating to PDP
                this[NavigationMixin.GenerateUrl]({
                    type: 'standard__recordPage',
                    attributes: {
                        recordId : loopItem.productId,
                        objectApiName: 'Product2',
                        actionName: 'view'
                    }
                })
                .then((url) => {
                    loopItem.cartItem.productUrl = url;
                })
                .catch((error) => {
                    let errorMsgs = ReduceError(error);
                    reject(errorMsgs[0]);
                })
            });
            this.cartItems = tempItemList;
            resolve(this.cartItems);
        });
    }


    handleQuantityChanged(event) {
        const { cartItemId, quantity } = event.detail;
        let quantityVal = Number(event.detail.quantity);
        updateCartItem({
            communityId : COMMUNITY_ID,
            effectiveAccountId : this.effectiveAccountId,
            activeCartOrId : this.recordId,
            cartItemId,
            quantity: quantityVal
        })
        .then(result => {
            this.updateCartItemInformation(result);
        })
        .catch(error => {
            this.template.querySelectorAll('c-cc_cart_wishlist_item').forEach(cmp => {
                if (cmp.item.cartItemId == cartItemId) {
                    cmp.setIsLoading(false);
                };
            });
            let errorMsgs = ReduceError(error);
            this.showToastMessage('',errorMsgs[0],'error');
        });
    }

    updateCartItemInformation(cartItemId) {
        this.getCartItemData();
        this.handleCartUpdate();
        this.template.querySelectorAll('c-cc_cart_wishlist_item').forEach(cmp => {
            if (cmp.item.cartItemId == cartItemId) {
                cmp.setIsLoading(false);
            };
        });
    }

    handleCartUpdate() {
        // Update Cart Badge
        this.dispatchEvent(
            new CustomEvent(CART_CHANGED_EVT, {
                bubbles: true,
                composed: true
            })
        );
    }

    handleRemoveCartItem(event) {
        this.isLoading = true;
        const { cartItemId } = event.detail;
        deleteCartItem({
            communityId :COMMUNITY_ID,
            effectiveAccountId : this.effectiveAccountId,
            activeCartOrId : this.recordId,
            cartItemId : cartItemId
        })
        .then(() => {
            this.removeCartItem(cartItemId);
        })
        .catch((error) => {
            let errorMsgs = ReduceError(error);
            this.isCartClosed = CLOSED_CART_MESSAGE_REGEXP.test(errorMsgs[0]);
            this.showToastMessage('',errorMsgs[0],'error');
        });
    }

    removeCartItem(cartItemId) {
        // remove item from list of CartItems
        const removedItem = (this.cartItems || []).filter(item =>
            item.cartItem.cartItemId === cartItemId
        )[0];

        const updatedCartItems = (this.cartItems || []).filter(
            (item) => item.cartItem.cartItemId !== cartItemId
        );

        this.cartItems = updatedCartItems;
        this.getCartItemData();
//        this._cartItemCount -= Number(removedItem.cartItem.quantity);
        this.handleCartUpdate();
    }

    handleAddAllWishlist() {
        this.cartItems.forEach(item => {
            let productObj = {
                productId : item.cartItem.productDetails.productId,
                quantity : item.cartItem.quantity
            };
            this.wishlistData.push(productObj);
        });
        this.showWishlistCmp = true;
    }

    async getInventoryStatus(planoPresent) {
        return new Promise((resolve,reject) => {
            let productIdList = [];
            // If plano products are present in the request, we only want to send those products in their own callout
            if (planoPresent) {
                productIdList = this.planoProductIds;
            } else {
                // Get all productIds from CartItems to pass to 'doInventoryCallout' method
                this.cartItems.forEach(item => {
                    // filter out Plano products from product Ids list
                    let productType = item.cartItem.productDetails.fields.Product_Type__c;
                    if (productType == 'Pure Fishing Legacy' || productType == 'Spare Part') {
                        productIdList.push(item.cartItem.productId);
                    }
                });
            }
            if (productIdList.length > 0) {
                doInventoryCallout({
                    accountId : this.effectiveAccountId,
                    productIds : JSON.stringify(productIdList),

                })
                .then(result => {
                    if (result.isSuccess){
                        this.inventoryData = result;
                        this.setInventory(result);
                        resolve(result);
                    } else {
                        this.showToastMessage('', result.errorMsg, 'error');
                        reject(result.errorMsg);
                    }
                })
                .catch(error => {
                    let errorMsg = ReduceError(error);
                    this.showToastMessage('', errorMsg[0], 'error');
                    reject(errorMsg[0]);
                });
            }
        });
    }

    setInventory(input) {
        if (input.recordData) {
            let objMap = new Map(Object.entries(input.recordData));
            let tempArray = [...this.cartItems];
            tempArray = JSON.parse(JSON.stringify(tempArray));
            tempArray.forEach(item => {
                if (objMap.has(item.cartItem.productId)){
                    item.cartItem.inventoryStatus = objMap.get(item.cartItem.productId);
                }
            });
            this.cartItems = tempArray;
        }
    }

    showToastMessage(title,msg,variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: msg,
                variant: variant
            })
        );
    }

    handleClearCartButtonClicked() {
        this.isLoading = true;
        // Step 1: Delete the current cart
        deleteCart({
            communityId : COMMUNITY_ID,
            effectiveAccountId: this.effectiveAccountId,
            activeCartOrId: this.recordId
        })
        .then(() => {
            // Step 2: If the delete operation was successful,
            // set cartItems to undefined and update the cart header
            this.cartItems = undefined;
            this._cartItemCount = 0;
        })
        .then(() => {
            // Step 3: Create a new cart
            return createCart({
                communityId: COMMUNITY_ID,
                effectiveAccountId: this.effectiveAccountId
            });
        })
        .then((result) => {
            this.isLoading = false;
            // Step 4: If create cart was successful, navigate to the new cart
            this.navigateToCart(result.cartId);

            this.handleCartUpdate();
        })
        .catch((e) => {
            this.isLoading = false;
            // Handle quantity any errors properly
            // For this sample, we can just log the error
            let errorMsgs = ReduceError(e);
            this.isCartClosed = CLOSED_CART_MESSAGE_REGEXP.test(errorMsgs[0]);
        });
    }

    navigateToCart(cartId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: cartId,
                objectApiName: 'WebCart',
                actionName: 'view' 
            }
        });
    }

    handleChangeSortSelection(event) {
        this.sortParam = event.target.value;
        // After the sort order has changed, we get a refreshed list
        this.getCartItemData();
    }


    closeWishlistModal() {
        this.showWishlistCmp = false;
    }

    handleCartDownload() {
        this.isLoading = true;
        generateCartData({
            cartId : this.recordId,
            webstoreId : this.webstoreId
        })
        .then(result => {
            this.isLoading = false;
            let doc = result;
            let element = 'data:application/vnd.ms-excel,' + encodeURIComponent(doc);
            let downloadElement = document.createElement('a');
            downloadElement.href = element;
            downloadElement.target = '_self';

            // Get current date for file name
            let today = new Date();
            let dateValue = today.toISOString().split('T')[0]; // YYYY-MM-DD
            let timeValue = today.toLocaleTimeString();
            let splitTime = timeValue.split(':');
            let splitDate = dateValue.split('-');
            let tempDate = new Date(`${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`);
            let filenameDate = new Date(tempDate.setDate(tempDate.getDate())).toISOString().split('T')[0];

            let currentDate = new Date();
            console.log(this.cartDownloadLabel);
            downloadElement.download = `${LABEL_CART_DOWNLOAD_FILENAME}_${filenameDate}_${splitTime[0]}_${splitTime[1]}_${splitTime[2].slice(0,2)}.csv`;
            document.body.appendChild(downloadElement);
            downloadElement.click();
        })
        .catch(error => {
            this.isLoading = false;
            let errors = ReduceError(error);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: '',
                    message : errors[0],
                    variant: 'error'
                })
            );
        })

    }

    handleNextPage() {
        this.pageToken = this.nextPageToken;
        this.getCartItemData();
    }

    handlePrevPage() {
        this.pageToken = this.prevPageToken;
        this.getCartItemData();
    }

}