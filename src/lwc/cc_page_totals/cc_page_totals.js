/**
 * Created by Matt on 4/5/2022.
 */

import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import LABEL_PAGE_TOTALS from '@salesforce/label/c.cc_mg_page_totals_page_totals_label';
import LABEL_PRICE from '@salesforce/label/c.cc_mg_master_grid_price_header';
import LABEL_QUANTITY from '@salesforce/label/c.cc_mg_master_grid_quantity_header';
import LABEL_ADD_TO_CART from '@salesforce/label/c.cc_mg_page_totals_add_to_cart';
import LABEL_ADD_TO_WISHLIST from '@salesforce/label/c.cc_mg_page_totals_add_to_wishlist';
import MASTER_GRID_MC from '@salesforce/messageChannel/MasterGRidMessageChannel__c';
import addProductsToCart from '@salesforce/apex/MasterGridController.addProductsToCart';
import showMasterGrid from '@salesforce/apex/MasterGridController.showMasterGrid';
import addBulkItemsToWishList from '@salesforce/apex/MasterGridController.addBulkItemsToWishList';
import COMMUNITYID from '@salesforce/community/Id';
import USERID from '@salesforce/user/Id';
import {
    subscribe,
    createMessageContext,
    publish,
    unsubscribe,
    APPLICATION_SCOPE,
    MessageContext
    } from 'lightning/messageService';

export default class cc_page_totals extends NavigationMixin(LightningElement) {
    @api recordId;
    pageTotalsText = LABEL_PAGE_TOTALS;
    quantityText = LABEL_QUANTITY + ':';
    priceText = LABEL_PRICE + ':';
    productIds = [];
    totQuantity = 0;
    subscription = null;
    btnDisabled = true;
    btnText = LABEL_ADD_TO_CART;
    wishListBtnTxt = LABEL_ADD_TO_WISHLIST;
    totPrice = 0.00;
    @api effectiveAccountId;
    webstoreId;
    cartId;
    highLvlErrorMsg;
    showPriceTotals;
    maxUploadRows;
    wishlistData = [];
    showWishlistModal = false;
    @api currencyCode;

    responseMap = new Map();
    productQuantityMap = new Map();

    @wire(MessageContext)
    messageContext;

    connectedCallback() {
        // Subscribe to the MC
        this.subscribeMC();
    }

    @wire(showMasterGrid, { productId : '$recordId'})
        wiredResult({ data, error }){
            if (data) {
                this.showPriceTotals = data;
            } else if (error) {
                console.log('showMasterGrid() error: ', error);
            }
        }

    subscribeMC() {
        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                MASTER_GRID_MC,
                (message) => {
                    this.handleMCmessage(message);
                },
                { scope : APPLICATION_SCOPE }
            );
        }
    }

    handleMCmessage(input) {
        if (input.productId){
            // Update the productQuantityMap
            if (input.productQuantity === 0){
                if (this.productQuantityMap.has(input.productId)){
                    this.productQuantityMap.delete(input.productId);
                }
            } else {
                this.productQuantityMap.set(input.productId, input);
            }

            // Build list of current ProductIds for Wishlist use
            this.buildProductsList();

            // Recalculate totals
            this.calculateTotals();
        } else {
            // Update cart information
            this.cartId = input.cartId != null ? input.cartId : this.cartId;
            this.currencyCode = input.currencyIsoCode;
            // MParrella | 07-07-2022 | Commenting out line below -- the effectiveAccountId parameter will be passed in via Experience Builder Attribute
//            this.effectiveAccountId = input.effectiveAccountId != null ? input.effectiveAccountId : this.effectiveAccountId;
            this.webstoreId = input.webstoreId != null ? input.webstoreId : this.webstoreId;
            this.maxUploadRows = input.maxUploadRows != null ? input.maxUploadRows : this.maxUploadRows;
        }
    }

    sendMessageService(payloadData){
            publish(this.messageContext, MASTER_GRID_MC, payloadData);
    }

    buildProductsList() {
        let tempArray = [];
        for (let [key,value] of this.productQuantityMap) {
            tempArray.push(key);
        }
        this.productIds = tempArray;
    }

    calculateTotals() {
        // Run through the map values and sum up quantity and total
        let totAmount = 0;
        let quantity = 0;
        for (let [key,value] of this.productQuantityMap) {
            if (key){
                totAmount += parseFloat(value.productTotalPrice);
                quantity += parseInt(value.productQuantity);
            }
        }
        // Update the vars for the front-end
        this.totQuantity = quantity;
        this.totPrice = parseFloat(totAmount).toFixed(2);

        if (this.totQuantity === 0){
            this.btnDisabled = true;
        } else {
            this.btnDisabled = false;
        }
    }

    handleAddToCart() {
        // Set 'isLoading' attribute on master grid lwc
        let payload = { isLoading: true };
        this.sendMessageService(payload);



        // build list of JSON items to send to back-end
        let itemsList = [];
        for (let [key,value] of this.productQuantityMap) {
            let productObj = {
                productId : key,
                qty: value.productQuantity,
                modelNumber: value.modelNumber,
                type: '',
                productName: value.productName
            };
            itemsList.push(productObj);
        }
        addProductsToCart({
            JSONitemsList: JSON.stringify(itemsList),
            webstoreId: this.webstoreId,
            effectiveAccountId: this.effectiveAccountId,
            activeCartOrId: this.cartId
        })
        .then(result => {
            let payload = { isLoading: false };
            this.sendMessageService(payload);
            if (result.messages) {
                if (result.messages[0].severity === 'success'){
                    // Refresh the cart icon in the event we have partial success (for example: 2 out of 3 products were found and added to the cart, but 1 had a cart failure)
                       this.dispatchEvent(new CustomEvent("cartchanged", {
                             bubbles: true,
                             composed: true
                         }));
                    // redirect to cart page
                    this[NavigationMixin.Navigate]({
                       type: 'standard__webPage',
                       attributes : {
                           url: '/cart/' + this.cartId
                       }
                    });
                } else {
                    let messages = JSON.parse(JSON.stringify(result.messages));
                    this.highLvlErrorMsg = messages[0].title;
                    let errorMsg = messages[0].message
                    this.dispatchEvent(new ShowToastEvent({
                        title: this.highLvlErrorMsg,
                        message: errorMsg,
                        variant: 'error'
                    }));
                }
            }
        })
        .catch(error => {
            let payload = { isLoading: false };
            this.sendMessageService(payload);

           console.log('***Add products to cart error: ', error);
        });
    }

    handleAddToWishlist() {
        // Build 'wishlistQuantityMap'
        for (let [key,value] of this.productQuantityMap) {
            let newObj = {
                productId : key,
                quantity : value.productQuantity
            };
            this.wishlistData.push(newObj);
        }
        this.showWishlistModal = true;
    }

    closeModal(){
        this.showWishlistModal = false;
    }
}