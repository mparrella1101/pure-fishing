/**
 * Created by Matt on 5/26/2022.
 */

import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { ReduceError } from 'c/ldsUtils';
import getAllSuggestedOrders from '@salesforce/apex/ConnectAPIUtils.getAllSuggestedOrders';
import deleteSuggestedOrder from '@salesforce/apex/ConnectAPIUtils.deleteSuggestedOrder';

import LABEL_NAME_HEADER from '@salesforce/label/c.cc_wishlist_column_header_name';
import LABEL_CREATED_DATE_HEADER from '@salesforce/label/c.cc_wishlist_column_header_created_date';
import LABEL_EXP_DATE_HEADER from '@salesforce/label/c.cc_wishlist_column_header_exp_date';
import LABEL_ITEMS_HEADER from '@salesforce/label/c.cc_wishlist_column_header_items';
import LABEL_NO_ORDERS from '@salesforce/label/c.cc_wishlist_no_orders_text';
import LABEL_NO_ORDERS_DETAILS from '@salesforce/label/c.cc_wishlist_no_orders_detail_text';
import LABEL_DELETE_SUCCESS from '@salesforce/label/c.cc_wishlist_delete_success';
import LABEL_SUCCESS_MESSAGE_PART_2 from '@salesforce/label/c.cc_add_wishlist_success_part_2';
import LABEL_ERROR_DELETING_MSG_PART_1 from '@salesforce/label/c.cc_wishlist_error_deleting_part_1';
import LABEL_ERROR_DELETING_MSG_PART_2 from '@salesforce/label/c.cc_wishlist_error_deleting_part_2';
import LABEL_DELETE_CONFIRM_TEXT from '@salesforce/label/c.cc_wishlist_confirm_delete_text';
import LABEL_SUGGESTED_ORDER_QUESTION from '@salesforce/label/c.cc_wishlist_suggested_order_question';
import LABEL_CANCEL_BTN_TEXT from '@salesforce/label/c.cc_add_wishlist_cancel_label';
import LABEL_DELETE_BTN_TEXT from '@salesforce/label/c.cc_wishlist_delete_btn_text';
import LABEL_CONFIRM_DELETE_HEADER from '@salesforce/label/c.cc_wishlist_confirm_delete_header';
import LABEL_EDIT_LABEL from '@salesforce/label/c.cc_wishlist_edit_label';

import COMMUNITY_ID from '@salesforce/community/Id';

const actions = [
    { label: LABEL_DELETE_BTN_TEXT, name: 'delete' },
    { label: LABEL_EDIT_LABEL, name: 'edit' }
];

const columns = [
        { label: LABEL_NAME_HEADER, fieldName: 'linkName', type: 'url',
         typeAttributes: { label: {fieldName: 'Name'}, target: '_self'}},
        { label: LABEL_CREATED_DATE_HEADER, fieldName: 'CreatedDate', type: 'date-local', typeAttributes: { day: "2-digit", month: "2-digit"}},
        { label: LABEL_EXP_DATE_HEADER, fieldName: 'Expiration_Date__c', type: 'date-local', typeAttributes: { day: "2-digit", month: "2-digit"}},
        { label: LABEL_ITEMS_HEADER, fieldName: 'SuggestedOrderItemCount__c', type: 'number', cellAttributes: { alignment: 'center'} },
        { type: 'action',
          typeAttributes:
                { rowActions: actions }
        }
];


export default class cc_wishlist_listview extends NavigationMixin(LightningElement) {
    orderData;
    showDataTable = false;
    showDeleteConfirm = false;
    noRecords = false;
    selectedListName;
    selectedListId;
    columns = columns;
    cancelBtnText = LABEL_CANCEL_BTN_TEXT;
    deleteBtnText = LABEL_DELETE_BTN_TEXT;
    confirmDeleteHeader = LABEL_CONFIRM_DELETE_HEADER;
    @api effectiveAccountId;

    confirmDeleteText = LABEL_DELETE_CONFIRM_TEXT + ' \'' + this.selectedListName + '\' ' + LABEL_SUGGESTED_ORDER_QUESTION;

    get labels() {
        return {
            noOrders: '',
            noOrders: LABEL_NO_ORDERS,
            emptyOrderBodyLabel: LABEL_NO_ORDERS_DETAILS
        };
    }

    connectedCallback() {
        this.getOrderData();
    }

    getOrderData() {
        getAllSuggestedOrders({
          communityId : COMMUNITY_ID,
          effectiveAccountId : this.effectiveAccountId
        })
        .then(result => {
            if (result.length > 0) {
               // Build record urls
               let tempArray = result.map((item) =>
                Object.assign({}, item, { linkName: '/suggested-order/' + item.Id + '/detail'})
               )
               this.orderData = JSON.parse(JSON.stringify(tempArray));
               this.showDataTable = true;
            } else {
                this.showDataTable = false;
                this.noRecords = true;
            }
        })
        .catch(error => {
            console.log(error);
        });
    }

    handleRowAction(event) {
        let eventType = event.detail.action.name;
        this.selectedListId = event.detail.row.Id;
        if (eventType === 'edit') {
            // Redirect to detail page
            this[NavigationMixin.Navigate]({
                type: 'standard__webPage',
                attributes: {
                    url: '/suggested-order/' + this.selectedListId + '/detail'
                }
            });
        }
        else if (eventType === 'delete') {
            let listName = event.detail.row.Name;
            this.selectedListName = listName;
            this.confirmDeleteText = LABEL_DELETE_CONFIRM_TEXT + ' \'' + this.selectedListName + '\' ' + LABEL_SUGGESTED_ORDER_QUESTION;
            this.showDeleteConfirm = true;
        }
    }

    handleCloseModal() {
        this.showDeleteConfirm = false;
    }

    handleListDelete() {
        this.handleCloseModal();
        this.isLoading = true;
        deleteSuggestedOrder({
            communityId : COMMUNITY_ID,
            suggestedOrderId : this.selectedListId
        })
        .then(result => {
            this.isLoading = false;
            this.dispatchEvent(
                new ShowToastEvent({
                    title : '',
                    message : LABEL_DELETE_SUCCESS + ' \'' + this.selectedListName + ' \'' + LABEL_SUCCESS_MESSAGE_PART_2,
                    variant: 'success'
                })
            );
            this.getOrderData();
        })
        .catch(error => {
           let errorMsgs = ReduceError(error);
           console.log('DELETE ERROR: ', errorMsgs[0]);
           this.isLoading = false;
           this.dispatchEvent(
               new ShowToastEvent({
                   title: '',
                   message: LABEL_ERROR_DELETING_MSG_PART_1 + ' \'' + this.selectedListName + ' \' ' + + LABEL_ERROR_DELETING_MSG_PART_2 + errorMsgs[0],
                   variant: 'error'
               })
           );
           this.handleCloseModal();
        });
    }


}