/**
 * Created by Matt on 3/29/2022.
 */

import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

/* LWC Utility/Style Import(s): START */
import { ReduceError } from 'c/ldsUtils';
/* LWC Utility/Style Import(s): START */



/* APEX METHOD IMPORTS: START */
import getProductVariantData from '@salesforce/apex/MasterGridController.getProductVariantData';
import showMasterGrid from '@salesforce/apex/MasterGridController.showMasterGrid';
import getInfo from '@salesforce/apex/QuickOrderUploadController.getInfo';
import doInventoryCallout from '@salesforce/apex/MasterGridController.doInventoryCallout';
/* APEX METHOD IMPORTS: STOP */

/* CUSTOM LABEL IMPORTS: START */
import LABEL_PRODUCT_INFO from '@salesforce/label/c.cc_mg_master_grid_product_info_header';
import LABEL_PRICE_HEADER from '@salesforce/label/c.cc_mg_master_grid_price_header';
import LABEL_QTY_HEADER from '@salesforce/label/c.cc_mg_master_grid_quantity_header';
import LABEL_INV_LOADING from '@salesforce/label/c.cc_mg_inventory_loading';
/* CUSTOM LABEL IMPORTS: STOP */

import COMMUNITY_ID from '@salesforce/community/Id';

/* MESSAGE CHANNEL IMPORTS: START */
import MASTER_GRID_MC from '@salesforce/messageChannel/MasterGRidMessageChannel__c';
import {
            subscribe,
            publish,
            createMessageContext,
            unsubscribe,
            APPLICATION_SCOPE,
            MessageContext
        } from 'lightning/messageService';
/* MESSAGE CHANNEL IMPORTS: STOP */

export default class CcMgMasterGrid extends NavigationMixin(LightningElement) {
    @api recordId;
    productInfoLabel = LABEL_PRODUCT_INFO;
    priceLabel = LABEL_PRICE_HEADER;
    qtyLabel = LABEL_QTY_HEADER;
    invLoading = LABEL_INV_LOADING;
    isLoading = false;
    showMasterGrid;
    cartId;
    webstoreId;
    @api effectiveAccountId;
    maxUploadRows;
    showWishlistModal = false;
    productIds = [];
    inventoryData;
    currencyCode;

    inputTimer;
    waitInterval = 400; // Used to control the debouncing of input field

    productQuantityMap = new Map();

    subscription = null;
    productData = [];
    columnsData = [];

    @api customFields; // used to store which additional fields/columns should be displayed

    @wire(MessageContext)
    messageContext;

    @wire(showMasterGrid, { productId : '$recordId' })
    wiredResult({ data, error }){
        if (data) {
            this.showMasterGrid = data;
        } else if (error) {
        }
    }


    connectedCallback() {
        // Get Variant data (needs to wait for 'retrieveInfo()' to finish)
        this.getProductVariants();
        this.subscribeMC();
    }



    setInventory(input) {
        if (input.recordData){
            let objMap = new Map(Object.entries(input.recordData));
            // Iterate over the productData array and update the inventory status
            let tempArray = [...this.productData];
            tempArray.forEach(item => {
                if (objMap.has(item.ProductId)){
                    item.Product.Inventory_Status__c = objMap.get(item.ProductId);
                }
            });
            this.productData = tempArray;
        }
    }

    async getInventoryStatus(){
        return new Promise((resolve, reject) => {
            // Make callout
            doInventoryCallout({
                accountId : this.effectiveAccountId,
                productIds : this.productIds
            })
            .then(result => {
                resolve(result);
            })
            .catch(error =>{
                let errorMsg = ReduceError(error);
                reject(errorMsg[0]);
            });
        });
    }

    subscribeMC() {
            if (!this.subscription) {
                this.subscription = subscribe(
                    this.messageContext,
                    MASTER_GRID_MC,
                    (message) => {
                        this.handleMCmessage(message);
                    },
                    { scope : APPLICATION_SCOPE }
                );
            }
    }

    retrieveInfo(){
        this.isLoading = true;
        return new Promise((resolve,reject) => {
            getInfo({
                userId : null,
                effectiveAccountId : this.effectiveAccountId,
                communityId : COMMUNITY_ID,
                webstoreId : null
            })
            .then(result => {
                let data = {
                    cartId : result.cartId,
                    webstoreId : result.webstoreId,
                    effectiveAccountId : result.effectiveAccountId
                };
                this.cartId = result.cartId;
                this.webstoreId = result.webstoreId;
                this.maxUploadRows = result.maxUploadRows;
                this.effectiveAccountId = result.effectiveAccountId;
                this.currencyCode = result.currencyCode;
                let payload = {
                    webstoreId : this.webstoreId,
                    cartId : this.cartId,
                    effectiveAccountId : this.effectiveAccountId,
                    maxUploadRows : this.maxUploadRows,
                    currencyIsoCode : this.currencyCode
                };
                this.sendMessageService(payload);
                this.isLoading = false;
                resolve(data);
            })
            .catch(error => {
                this.isLoading = false;
                const toastEvt = new ShowToastEvent({
                   title: '',
                   message: error.message,
                   variant: 'error'
                });
                this.dispatchEvent(toastEvt);
                this.isLoading = false;
                reject(error);
            });
        })
    }

    handleQuantityChange(event) {
        clearTimeout(this.inputTimer);
        // Get the productId of the product who's quantity change
        let productId = event.target.dataset.id;
        let productName = event.target.dataset.name;
        let inputValue = event.target.value;
        let price = event.target.dataset.price;
        let modelNumber = event.target.dataset.model;

        let quantityChangeObj = {};

        // Get Quantity that was entered, after waiting a second or two for the user to finish entering the number
        this.inputTimer = setTimeout(() => {
            if (inputValue) {
                quantityChangeObj.quantity = inputValue;
                quantityChangeObj.productId = productId;
                quantityChangeObj.unitPrice = price;
                quantityChangeObj.productName = productName;
                quantityChangeObj.modelNumber = modelNumber;
                quantityChangeObj.totalPrice = (price * inputValue).toFixed(2);
                this.addToQuantityMap(quantityChangeObj);
            } else {
                // Quantity input is cleared
                quantityChangeObj.quantity = 0;
                quantityChangeObj.productId = productId;
                quantityChangeObj.unitPrice = 0;
                quantityChangeObj.productName = productName;
                quantityChangeObj.totalPrice = 0.00;
                quantityChangeObj.modelNumber = modelNumber;
                this.addToQuantityMap(quantityChangeObj);
            }
        }, this.waitInterval);

    }
    sendMessageService(payloadData){
        publish(this.messageContext, MASTER_GRID_MC, payloadData);
    }

    addToQuantityMap(input){
        // Build message channel payload to send to the 'cc_page_totals' cmp
        let payload = {
            productTotalPrice : input.totalPrice,
            changeAmt : input.changeAmt,
            productQuantity : input.quantity,
            productCost : input.unitPrice,
            productId : input.productId,
            productName : input.productName,
            modelNumber : input.modelNumber,
            isLoading : false
        };
        this.sendMessageService(payload);
    }


    async getProductVariants(){
        let communityData = await this.retrieveInfo();
        return new Promise((resolve,reject) => {
            getProductVariantData({
                productId: this.recordId,
                webstoreId : communityData.webstoreId,
                effectiveAccountId: communityData.effectiveAccountId,
                communityId: COMMUNITY_ID
            })
            .then(result => {
                if (result.isSuccess){
                    // Build list of products from the returned map
                    let prodData = JSON.parse(JSON.stringify(result.recordData));
                    let placeholderArray = [];
                    for (const [key,value] of Object.entries(prodData)){
                        this.productIds.push(key);
                        placeholderArray.push(value);
                    }
                    this.getInventoryStatus().then(result => {
                        this.setInventory(result)
                    }); // Make callout to get inventory status

                    this.columnsData = result.addedColumns;
                    let newList = this.generateNewList(placeholderArray);
                    this.productData = newList;
                    resolve(this.productData);
                } else {
                    this.dispatchEvent(new ShowToastEvent({
                        title : '',
                        message: result.errorMsg,
                        variant: 'error'
                    }));
                    reject(result.errorMsg);
                }
            })
            .catch(error => {
                let errorMsg = ReduceError(error);
                reject(errorMsg[0]);
            });
        });

    }

    /* Method will gather the new data that will populate the new columns (if any) */
    generateNewList(array) {
        let columns = this.columnsData;
        let rows = array;
        let dynamicList = [];
        let counter = 0;
        rows.forEach( (row) => {
            let newRow = {...row}; // Copy the row of data
            let rowData = [];
            let rowDataObj = {
                value: '',
                key: counter
            };
            columns.forEach( column => {
                    const fieldApi = column.fieldApi;
                    // In the back-end, we are only populating column fieldApi parameter for ADDED (dynamic) columns, so if one is found we know that we have additional data
                    if (!!fieldApi) {
                        let productObj = row['Product'];
                        let rowDataObj = {
                            value: productObj[fieldApi],
                            key: counter
                        };
                        rowData.push(rowDataObj);
                        counter++;
                    }

            });
            newRow.addedCols = rowData; // Add the new data back to the copied row data
            dynamicList.push( newRow ); // Add the updated row data back to a list
        });
        return dynamicList;
    }

    handleMCmessage(input){
        this.isLoading = input.isLoading;
    }

    handleViewDetails(event) {
        let productId = event.target.dataset.id;
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/product/' + productId
            }
        });
    }







}