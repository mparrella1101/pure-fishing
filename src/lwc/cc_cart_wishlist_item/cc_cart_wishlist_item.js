/**
 * Created by Matt on 5/5/2022.
 */

import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ReduceError } from 'c/ldsUtils';

/* Field Data Imports: START */
import COMMUNITY_ID from '@salesforce/community/Id';
/* Field Data Imports: STOP */

/* Custom Label Imports: START */
import LABEL_CART_ITEM_MATERIAL_LABEL from '@salesforce/label/c.cc_cart_item_material_label';
import LABEL_CART_ITEM_MODEL_LABEL from '@salesforce/label/c.cc_cart_item_model_label';
import LABEL_CART_ITEM_CASEPACK_LABEL from '@salesforce/label/c.cc_cart_item_casepack_label';
import LABEL_INVENTORY_STATUS_LABEL from '@salesforce/label/c.cc_mg_product_info_inventory_status_label';
/* Custom Label Imports: STOP */


const QUANTITY_CHANGED_EVT = 'quantitychanged';
const CART_ITEM_REMOVE_EVT = 'removecartitem';

export default class cc_cart_wishlist_item extends NavigationMixin(LightningElement) {
    @api item;
    materialText = LABEL_CART_ITEM_MATERIAL_LABEL;
    modelText = LABEL_CART_ITEM_MODEL_LABEL;
    casePackText = LABEL_CART_ITEM_CASEPACK_LABEL;
    inventoryStatusText = LABEL_INVENTORY_STATUS_LABEL;
    showQtyInfo = false;
    isLoading = false;
    inputsDisabled = false;
    inputTimer;
    waitInterval = 400; // Used to control the debouncing of input field
    @api currencyCode;

    connectedCallback() {
        this.item = JSON.parse(JSON.stringify(this.item));
    }

    @api setIsLoading(value) {
        this.isLoading = value;
        this.inputsDisabled = value;
    }

    handleProductDetailNavigation(event) {
        event.preventDefault();
        const productId = event.target.dataset.productid;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: productId,
                actionName: 'view'
            }
        });
    }

    handleQuantityChange(event) {
        // Debounce the quantity change
        let val = event.target.value;
        clearTimeout(this.inputTimer);
        this.inputTimer = setTimeout(() => {
            this.setIsLoading(true);
           this.updateQuantity(this.item.cartItemId, val);
        }, this.waitInterval);
    }

    handleDeleteCartItem(event) {
        this.setIsLoading = true;
        let removedCartItemId = event.target.dataset.cartitemid;
        this.dispatchEvent(
            new CustomEvent(CART_ITEM_REMOVE_EVT,
            {
                detail: {
                    cartItemId : removedCartItemId
                }
            })
        );
    }

    handleQuantityIncrease(event) {
        this.item = JSON.parse(JSON.stringify(this.item));
        let selectorInput = this.template.querySelector('.selector-input');
        let newQuantity = +this.item.quantity + 1;
        this.item.quantity = newQuantity;
        selectorInput.value = newQuantity;
        selectorInput.dispatchEvent(new Event('change'));
    }

    handleQuantityDecrease(event) {
        if (this.item.quantity != 0){
          this.item = JSON.parse(JSON.stringify(this.item));
          let selectorInput = this.template.querySelector('.selector-input');
          let newQuantity = +this.item.quantity - 1;
          this.item.quantity = newQuantity;
          selectorInput.value = newQuantity;
          selectorInput.dispatchEvent(new Event('change'));
        }

    } 

    showQuantityRuleInfo(event) {
        this.showQtyInfo = !this.showQtyInfo;
    }

    closeQtyInfo(){
        this.showQtyInfo = false;
    }

    updateQuantity(cartItemId, quantity) {
        // fire event to handle on cart items lwc
        this.dispatchEvent(
            new CustomEvent(QUANTITY_CHANGED_EVT, {
                detail: {
                    cartItemId,
                    quantity
                }
            })
        );
    }
}