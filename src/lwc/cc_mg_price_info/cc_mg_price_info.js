/**
 * Created by Matt on 3/30/2022.
 */

import { LightningElement, api } from 'lwc';

/* Custom Label Imports: START */
import LABEL_YOUR_PRICE from '@salesforce/label/c.cc_mg_price_info_your_price_label';
import LABEL_ORIG_PRICE from '@salesforce/label/c.cc_mg_price_info_original_price_label';
import LABEL_MAP from '@salesforce/label/c.cc_mg_price_info_map_label';
import LABEL_MSRP from '@salesforce/label/c.cc_mg_price_info_msrp_label';
import getProductPriceData from '@salesforce/apex/MasterGridController.getProductPriceData';
import COMM_ID from '@salesforce/community/Id';
/* Custom Label Imports: STOP */

export default class cc_mg_price_info extends LightningElement {
    @api yourPrice;
    @api effectiveAccountId;
    @api origPrice;
    @api map;
    @api msrp;
    @api recordId;
    @api isRecordDetailPage;
    @api currencyCode;
    fontClass = 'price-info-container product-table-display';

    connectedCallback() {
        if (this.isRecordDetailPage) {
            this.fontClass = 'price-info-container product-detail-display';
            this.getRecordData();
        }
    }

    // Display Text vars
    yourPriceLabel = LABEL_YOUR_PRICE + ':';
    origPriceLabel = LABEL_ORIG_PRICE + ':';
    mapLabel = LABEL_MAP + ':';
    msrpLabel = LABEL_MSRP + ':';

    getRecordData() {
        getProductPriceData({
            communityId: COMM_ID,
            productId: this.recordId,
            effectiveAccountId: this.effectiveAccountId
        })
        .then(result => {
            console.log('result: ', result);
            this.msrp = result.recordData.msrp;
            this.map = result.recordData.map;
            this.yourPrice = result.recordData.your_price;
            this.origPrice = result.recordData.orig_price;
            this.currencyCode = result.recordData.isoCode;
        })
        .catch(error => {
            console.log('ERROR: ', error);
        })
    }
}