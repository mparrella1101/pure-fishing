/**
 * Created by Matt on 5/23/2022.
 */

import { LightningElement, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import CART_MC from '@salesforce/messageChannel/CartMessageChannel__c';

import {
    subscribe,
    publish,
    createMessageContext,
    unsubscribe,
    APPLICATION_SCOPE,
    MessageContext
} from 'lightning/messageService';

export default class cc_checkout_btn extends NavigationMixin(LightningElement) {

    @api recordId;
    @api buttonText;

    subscription = null;

    checkoutBtnDisabled = false;

    @wire(MessageContext)
    messageContext;

    connectedCallback() {
        this.subscribeMC();
    }

    subscribeMC() {
        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                CART_MC,
                (message) => {
                    this.handleMCmessage(message);
                },
                { scope : APPLICATION_SCOPE }
            );
        }
    }

    handleMCmessage(input) {
        this.checkoutBtnDisabled = input.buttonDisabled;
    }

    handleCheckout() {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/checkout/' + this.recordId
            }
        });
    }
}