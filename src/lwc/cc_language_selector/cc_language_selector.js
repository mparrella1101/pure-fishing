/**
 * Created by Matt on 2/14/2023.
 */

import { LightningElement, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';

import COMMUNITY_ID from '@salesforce/community/Id';
import LANGUAGE_SELECTOR_TEXT from '@salesforce/label/c.cc_lang_lwc_label';
import USER_ID from '@salesforce/user/Id';
import getLanguageSelections from '@salesforce/apex/LanguageSelectorController.getLanguageSelections';
import updateLanguageSetting from '@salesforce/apex/LanguageSelectorController.updateLanguageSetting';

export default class cc_language_selector extends LightningElement {
    langOptions = [];   // Holds the language picklist values
    defaultLang;
    pageInfo;
    languageText = LANGUAGE_SELECTOR_TEXT;

    userIdVar = USER_ID;
    commIdVar = COMMUNITY_ID;


    connectedCallback() {
        this.getLanguageData();
    }

    async getLanguageData() {
        return new Promise((resolve,reject) => {
             getLanguageSelections({
                userId : USER_ID,
                communityId : COMMUNITY_ID
            })
            .then((result) => {
                this.langOptions = result.recordData.langOptions;
                this.defaultLang = result.recordData.defaultLang;
                resolve(result);
            })
            .catch((error) => {
                reject(error);
            });
       });
    }

    handleLangSelection(event) {
        // Get Selected language key
        let selectedLang = event.target.value;

        // Call to apex to update user record
        updateLanguageSetting({
            userId : USER_ID,
            selectedLanguage : selectedLang
        })
        .then(result => {
            if (result) {
                // Call refresh function
                this.refreshPage();
            } else {
                //TODO: Show toast msg here
                console.log('error updating language: ', error);
            }
        })
        .catch(error => {
            // TODO: Show toast msg here
            console.log('error caught updating language: ', error);
        });
    }

    refreshPage() {
        window.location.reload();
        // Note: NavigationMixin would not refresh the page :(
    }

}