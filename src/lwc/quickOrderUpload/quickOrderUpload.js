/**
 * Created by Matt on 3/16/2022.
 */

import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

// Standard Imports
import LOCALE from '@salesforce/i18n/locale';
import USERID from '@salesforce/user/Id';
import COMMUNITYID from '@salesforce/community/Id';

// Apex Method Imports
import getTemplateId from '@salesforce/apex/QuickOrderUploadController.getTemplateId';
import getInfo from '@salesforce/apex/QuickOrderUploadController.getInfo';
import processData from '@salesforce/apex/QuickOrderUploadController.processData';

import LABEL_HEADER_TEXT from '@salesforce/label/c.cc_quickorder_header';
import LABEL_DOWNLOAD_TEMPLATE_TEXT from '@salesforce/label/c.cc_quickorder_template_text';
import LABEL_UPLOAD_TEXT from '@salesforce/label/c.cc_quickorder_upload_text';
import LABEL_ERROR_ENCOUNTERED_TEXT from '@salesforce/label/c.cc_quickorder_error_encountered_text';
import LABEL_EMPTY_FILE_TEXT from '@salesforce/label/c.cc_quickorder_empty_file_text';
import LABEL_INVALID_FILE_TYPE from '@salesforce/label/c.cc_quickorder_invalid_file_type';
import LABEL_INVALID_FILE_TYPE_DETAIL from '@salesforce/label/c.cc_quickorder_invalid_file_detail_text';
import LABEL_NO_FILE_SELECTED from '@salesforce/label/c.cc_quickorder_no_file_selected';

export default class QuickOrderUpload extends NavigationMixin(LightningElement) {
    templateURL;
    @api effectiveAccountId;
    file; // References the file that was uploaded
    fileContents; // Contains the file contents
    fileReader; // Used for parsing the csv
    fileUploaded; // Stores the file that was uploaded
    fileName; // Stores the name of the file uploaded (if needed)
    isLoading = false;
    parsedData; // The lines within the CSV file, parsed
    objectList = []; // Parsed data turned into objects for reference

    headerText = LABEL_HEADER_TEXT;
    templateText = LABEL_DOWNLOAD_TEMPLATE_TEXT;
    uploadText = LABEL_UPLOAD_TEXT;

    hasContent = false; // Used to conditionally display the success/failure log section

    locale = LOCALE;
    userId = USERID;

    cartId;
    webstoreId;
    maxUploadRows;

    processLog;
    showProcessLog = false;
    highLvlErrorMsg;
    isSuccess = false;

    selectedProcessingValues = [];

    hasContent= false; // temporary for testing

    connectedCallback() {
        // Get the cartId, webstoreId, maxRows attribute values
        this.retrieveInfo();
    }


    @wire(getTemplateId, { communityId : COMMUNITYID })
    wiredTemplateId ({ error, data }){
        if (data){
            console.log('data: ', data);
            this.templateURL =  data;
        } else if (error){
            console.log('ERROR: ', error);
        }
    }

    /* This function will parse the uploaded file in order to extract the Quantity, Material, UPC, and Model values to be used later on */
    async handleFileUpload(event){

        this.objectList = [];
        let hasError = false;
        let missingField = '';
        if (event.target.files.length > 0){
            // Get the file type and check that it's CSV
            this.fileUploaded = event.target.files[0];
            let fileType = this.fileUploaded.type;
            if (fileType !== 'text/csv'){
                this.hasContent = false;
                const toastEvt = new ShowToastEvent({
                    title: LABEL_INVALID_FILE_TYPE,
                    message : LABEL_INVALID_FILE_TYPE_DETAIL,
                    variant : 'error'
                });
                this.dispatchEvent(toastEvt);
            } else {
                this.parsedData = await this.parseFile(this.fileUploaded, null);
                this.parsedData = this.parsedData.splice(1, this.parsedData.length - 1);// Remove the first entry, those are headers
                if (this.parsedData.length > 0 && this.parsedData[0].length > 2) { // Ensure that we have at least 1 row with more than 2 pieces of information in it
                    let dataClone = [];
                    dataClone = JSON.parse(JSON.stringify(this.parsedData));
                    dataClone.forEach(item => {
                        let newObj = {};
                        newObj.quantity = item[0] != '' ? item[0] : 0;
                        newObj.material = item[1] != '' ? item[1] : '' ;
                        newObj.upc = item[2] != '' ? item[2] : '';
                        newObj.model = item[3] != '' ? item[3] : '';
                        this.objectList.push(newObj);
                    });
                    this.processData(this.objectList);
                } else {
                    this.hasContent = false;
                    const toastEvt = new ShowToastEvent({
                        title : '',
                        message : LABEL_EMPTY_FILE_TEXT,
                        variant : 'error'
                        });
                    this.dispatchEvent(toastEvt);
                }
            }
        } else {
            const toastEvt = new ShowToastEvent({ title: '', message : LABEL_NO_FILE_SELECTED, variant : 'error'});
            this.dispatchEvent(toastEvt);
            this.isLoading = false;
        }
    }

/* Function to handle the parsing of the uploaded CSV file */
    parseFile(inputFile, callback){
        return new Promise(resolve => {
            var reader = new FileReader();
            reader.onload = function () {
                var lines = this.result.split('\n');
                let result = lines.map(function (line) {
                    line = line.replace('\r','');
                    return line.split(',');
                });
                resolve(result);
            }
            reader.readAsText(inputFile);
        });
    }

    get acceptedFormats() {
        return ['.csv'];
    }

    retrieveInfo(){
        getInfo({
            userId : USERID,
            effectiveAccountId : this.effectiveAccountId,
            communityId : COMMUNITYID,
            webstoreId : null
        })
        .then(result => {
            this.cartId = result.cartId;
            this.webstoreId = result.webstoreId;
            this.maxUploadRows = result.maxUploadRows;
            // MParrella | 07-07-2022 | Commenting this out and passing in effectiveAccountId via experience builder parameter
//            this.effectiveAccountId = result.effectiveAccountId;
        })
        .catch(error => {
            this.isLoading = false;
            console.log('Error in retrieveInfo() function: ', error);
            const toastEvt = new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error'
            });
            this.dispatchEvent(toastEvt);
        });
    }

    processData(data){
        this.isLoading = true;
        if (data){
            processData({
                uploadData : JSON.stringify(data),
                userId : USERID,
                webstoreId : this.webstoreId,
                effectiveAccountId : this.effectiveAccountId,
                cartId : this.cartId
            })
            .then(result => {
                this.isLoading = false;
                if (result.messages) {
                    let messages = JSON.parse(JSON.stringify(result.messages));
                    this.showProcessLog = false;

                    // process error messages
                    this.processLog = this.processErrorMsgs(messages);

                    this.showProcessLog = true;

                    // Refresh the cart icon in the event we have partial success (for example: 2 out of 3 products were found and added to the cart, but 1 had a cart failure)
                    this.dispatchEvent(new CustomEvent("cartchanged", {
                        bubbles: true,
                        composed: true
                    }));

                    // If 100% successful, redirect to cart page
                    if (this.isSuccess) {
                        // redirect to cart page
                        this[NavigationMixin.Navigate]({
                           type: 'standard__webPage',
                           attributes : {
                               url: '/cart/' + this.cartId
                           }
                        });
                    }
                }
            })
            .catch(error => {
                this.showProcessLog = true;
                let processLog = '';
                processLog += '<ul>';
                let msgClass = 'slds-text-color_error';
                let formattedMsg = '';
                formattedMsg = '<li class=\'' + msgClass + '\'>' + error.message + '</li>';
                this.isLoading = false;
                console.log('ERROR: ', error);
               this.dispatchEvent(new ShowToastEvent({
                   title : '',
                   message: error.message,
                   variant: 'error'
               })) ;
            });
        } else {
            this.isLoading = false;
        }
    }
    processErrorMsgs(data) {
        let processLog = '';
        processLog += '<ul>';
        let msgClass = '';

        for (var i = 0; i < data.length; i++) {
            var message = data[i];
            if (message.row === 0) { // Generic error
                if (message.severity === 'success') {
                    this.highLvlErrorMsg = message.title;
                    this.isSuccess = true;

                } else {
                    this.isSuccess = false;
                    this.highLvlErrorMsg = LABEL_ERROR_ENCOUNTERED_TEXT + ': ' + message.title;
                    processLog += '<li class=\'slds-text-color_error slds-p-around_large slds-text-heading_medium\'>' + message.message + '</li>';
                }
            } else {
                if (message.severity === 'info') {
                    msgClass = 'slds-text-color_error';
                }
                if (message.severity === 'success') {
                    msgClass = 'slds-text-color_success';
                }
                if (message.severity === 'warn') {
                    msgClass = 'slds-text-color_default';
                }
                if (message.severity === 'error') {
                    msgClass = 'slds-text-color_error';
                }
                msgClass += ' slds-p-around_large slds-text-heading_medium';
                let formattedMsg = '';
                formattedMsg = '<li class=\'' + msgClass + '\'>' + message.message + '</li>';
                processLog += formattedMsg;
            }
        }
        processLog += '</ul>';
        return processLog;
    }

}