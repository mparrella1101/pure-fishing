import { api, LightningElement, track } from 'lwc';

const IS_ACTIVE = 'active';

export default class Paginator extends LightningElement {

    @api pagedata = [];
    @api title = '';

    @track enablePagingButtons = true;
    @track enableBackButton = true;
    @track enableNextButton = true;
    @api disableBackButton = false;
    @api disableNextButton = false;

    @track currentPageIndex = 0;
    @track maxNumberOfPages = 0;
    @api entriesToShowPerPage = 5;
    @api maxPageButtonsToShow = 5;
    @api hideOrDisableButtons = 'disable';

    _pageRange = [];

    @api
    get currentlyShown() {
        const currentEntry = this.entriesToShowPerPage * this.currentPageIndex;
        const pageStartRange = currentEntry >= this.pagedata.length ? this.pagedata.length - this.entriesToShowPerPage : currentEntry;
        const pageEndRange = this.currentPageIndex === 0 ? this.entriesToShowPerPage : pageStartRange + this.entriesToShowPerPage;
        return this.pagedata.slice(pageStartRange, pageEndRange);
    }

    @api
    get currentVisiblePageRanges() {
        if (this._pageRange.length === 0) {
            this._pageRange = this._fillRange(this.currentPageIndex * this.maxPageButtonsToShow);
        }
        return this._pageRange;
    }

    set currentVisiblePageRanges(nextRange) {
        const lastPossibleRange =
                        (this.maxPageButtonsToShow > this.maxNumberOfPages
                ? this.maxNumberOfPages
            : nextRange + this.maxPageButtonsToShow);
        this._pageRange = this._fillRange(
            lastPossibleRange - this.maxPageButtonsToShow
        );
    }

    renderedCallback() {
        console.log('this.pagedata', this.pagedata);
        debugger;
        this.maxNumberOfPages = Math.ceil(!!this.pagedata
            ? this.pagedata.length / this.entriesToShowPerPage
            : 0);
        this.currentShownPages =
            this.maxNumberOfPages <= this.maxPageButtonsToShow
                ? this.maxNumberOfPages
                : this.maxPageButtonsToShow;
        this.dispatchEvent(new CustomEvent('pagerchanged'));
        if ([...this.template.querySelectorAll('button.active')].length === 0) {
            //first render
            this._highlightPageButtonAtIndex(0);
        }
    }

    handlePrevious() {
        this.currentPageIndex =
            this.currentPageIndex > 0 ? this.currentPageIndex - 1 : 0;
        this.currentVisiblePageRanges =
            this.currentPageIndex - 1 <= 0 ? 1 : this.currentPageIndex - 1;
        this.dispatchEvent(new CustomEvent('pagerchanged'));
        this._highlightPageButtonAtIndex(
            this.currentPageIndex <= 0 ? 0 : this.currentPageIndex
        );
    }

    handleNext() {
        this.currentPageIndex =
            this.currentPageIndex < this.maxNumberOfPages
                ? this.currentPageIndex + 1
                : this.maxNumberOfPages - 1;
        this.currentVisiblePageRanges = this.currentPageIndex;
        // this.currentPageIndex <= this.maxNumberOfPages
        //     ? this.currentPageIndex
        //     : this.currentPageIndex + 1;
        this.dispatchEvent(new CustomEvent('pagerchanged'));
        this._highlightPageButtonAtIndex(
            this.currentPageIndex >= this.maxNumberOfPages
                ? this.maxNumberOfPages
                : this.currentPageIndex
        );
    }

    handleClick(event) {
        this.currentPageIndex = parseInt(event.target.innerHTML) - 1;
        this.currentVisiblePageRanges = this.currentPageIndex;
        this._clearCurrentlyActive();
        event.target.classList.toggle(IS_ACTIVE);
        this._enableOrDisableChevronButtons();
    }

    _clearCurrentlyActive() {
        const alreadySelected = [...this.template.querySelectorAll('.' + IS_ACTIVE)];
        if (alreadySelected.length === 1) {
            alreadySelected[0].classList.toggle(IS_ACTIVE);
        }
    }

    _fillRange(start) {   //, end) {
        this.maxNumberOfPages = Math.ceil(!!this.pagedata ? this.pagedata.length / this.entriesToShowPerPage : 0);
        console.log('_fillRange entered:', start, 'max#ofPages:', this.maxNumberOfPages);

        // console.log('_fillRange entered:', start, end, 'max#ofPages:', this.maxNumberOfPages);
        if (start <= 0) {
            start = 0;
        }
        let end = start + this.maxPageButtonsToShow;  // - 1;
        console.log('end:',end);
        if (end > this.maxNumberOfPages) {
            end = end > this.maxPageButtonsToShow ? this.maxNumberOfPages + 1 : this.maxNumberOfPages;
            start = end - this.maxPageButtonsToShow;
            if (start < 0) {
                start = 0;
            }
        }

        console.log('adjusted:', start, end, 'max#ofPages:', this.maxNumberOfPages);
        const rangeArray = Array(end - start)
            .fill()
            .map((_, index) => (start === 0 ? 1 + index : start + index));
        console.log('rangeArray:', rangeArray);
        return rangeArray;
    }

    _highlightPageButtonAtIndex(pageNumber) {
        pageNumber = pageNumber + 1;
        this._clearCurrentlyActive();
        const pageButtons = [...this.template.querySelectorAll('button')];
        const firstButton = pageButtons.filter(
            (button) => button.textContent === String(pageNumber)
        );
        if (firstButton.length === 1) {
            firstButton[0].classList.toggle(IS_ACTIVE);
        }
        this._enableOrDisableChevronButtons();

    }

    _enableOrDisableChevronButtons() {

        if (this.pagedata.length <= this.entriesToShowPerPage) {
            this.enablePagingButtons = false;
        } else {
            this.enablePagingButtons = true;

            this.enableBackButton = true;
            this.enableNextButton = true;
            this.disableBackButton = false;
            this.disableNextButton = false;
            if (this.currentPageIndex <= 0) {
                // this.enableBackButton = false;
                if (this.hideOrDisableButtons == 'hide') {
                    this.enableBackButton = false;
                } else {
                    this.disableBackButton = true;
                }
            }
            if (this.currentPageIndex + 1 >= this.maxNumberOfPages) {
                // this.enableNextButton = false;
                if (this.hideOrDisableButtons == 'hide') {
                    this.enableNextButton = false;
                } else {
                    this.disableNextButton = true;
                }
            }
        }

    }
}