/**
 * Created by Matt on 3/29/2022.
 */

import { LightningElement, api } from 'lwc';
import { ReduceError } from 'c/ldsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import COMMUNITY_ID from '@salesforce/community/Id';

import doInventoryCallout from '@salesforce/apex/MasterGridController.doInventoryCallout';
import getInfo from '@salesforce/apex/QuickOrderUploadController.getInfo';

/* Custom Label Imports: START */
import LABEL_MODEL_NAME from '@salesforce/label/c.cc_mg_product_info_model_label';
import LABEL_MATERIAL_NUMBER from '@salesforce/label/c.cc_mg_product_info_material_label';
import LABEL_UPC_NUMBER from '@salesforce/label/c.cc_mg_product_info_upc_label';
import LABEL_CASEPACK_NUMBER from '@salesforce/label/c.cc_mg_product_info_casepack_label';
import LABEL_IN_STOCK_LABEL from '@salesforce/label/c.cc_mg_product_info_inventory_in_stock';
import LABEL_INVENTORY_STATUS_LABEL from '@salesforce/label/c.cc_mg_product_info_inventory_status_label';
import LABEL_OUT_OF_STOCK_LABEL from '@salesforce/label/c.cc_mg_product_info_inventory_out_of_stock';
import getProductDetail from '@salesforce/apex/MasterGridController.getProductDetail';
/* Custom Label Imports: STOP */

export default class cc_mg_product_info extends LightningElement {
  @api material;
  @api model;
  @api upc;
  @api casePack;
  @api inventoryStatus;
  @api recordId;
  @api isRecordDetailPage;
  fontClass = 'product-info-container product-table-display';

  // Display Text vars
  modelName = LABEL_MODEL_NAME + ':';
  materialNumber = LABEL_MATERIAL_NUMBER + ' #:';
  upcNumber = LABEL_UPC_NUMBER + ':';
  casepackNumber = LABEL_CASEPACK_NUMBER + ':';
  inventoryStatusLabel = LABEL_INVENTORY_STATUS_LABEL + ':';
  inStockLabel = LABEL_IN_STOCK_LABEL;
  outOfStockLabel = LABEL_OUT_OF_STOCK_LABEL;

  cartId;
  webstoreId;
  productIds = [];
  @api effectiveAccountId;

  connectedCallback() {
      if (this.isRecordDetailPage) {
          this.fontClass = 'product-info-container product-detail-display';
          this.getProductDetails();
          this.getInventoryStatus();
      }
  }

  async getInventoryStatus() {
     let communityData = await this.retrieveInfo();
      return new Promise((resolve,reject) => {
           doInventoryCallout({
               accountId : this.effectiveAccountId,
               productIds : this.productIds
           })
           .then(result => {
               if (result.isSuccess){
                   let objMap = new Map(Object.entries(result.recordData));
                   this.inventoryStatus = objMap.get(this.recordId);
                   resolve(result);
               } else {
                   this.inventoryStatus = 'Error retrieving inventory';
                   this.dispatchEvent(new ShowToastEvent({
                       title: 'Error Retrieving Inventory',
                       message: result.errorMsg,
                       variant: 'error'
                   }));
                   reject(result.errorMsg);
               }
           })
           .catch(error => {
               this.inventoryStatus = 'Error retrieving inventory';
               let errorMsg = ReduceError(error);
               console.log('Inventory Callout ERROR: ', errorMsg[0]);
               reject(errorMsg[0]);
           })
      });
  }

  retrieveInfo(){
      return new Promise((resolve,reject) => {
          getInfo({
              userId : null,
              effectiveAccountId : this.effectiveAccountId,
              communityId : COMMUNITY_ID,
              webstoreId : null
          })
          .then(result => {
                  let data = {
                      cartId : result.cartId,
                      webstoreId : result.webstoreId,
                      effectiveAccountId : result.effectiveAccountId
                  };
                  this.cartId = result.cartId;
                  this.webstoreId = result.webstoreId;
                  this.effectiveAccountId = result.effectiveAccountId;
              resolve(data);
          })
          .catch(error => {
              let errorMsgs = ReduceError(error);
              console.log('Error in retrieveInfo() function: ', errorMsgs);
              const toastEvt = new ShowToastEvent({
                 title: '',
                 message: errorMsgs[0],
                 variant: 'error'
              });
              this.dispatchEvent(toastEvt);
              reject(errorMsgs[0]);
          });
      })
  }

  getProductDetails() {
      getProductDetail({
          productId: this.recordId
      })
      .then(result => {
          this.productIds.push(this.recordId);
          this.material = result.recordData.material;
          this.model = result.recordData.model;
          this.casePack = result.recordData.casepack;
          this.upc = result.recordData.upc;
          this.inventoryStatus = result.recordData.inventory_status;
      })
      .catch(error => {
        console.log('ERROR: ', error);
      });
  }
}