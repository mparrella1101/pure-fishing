import { LightningElement, api, wire, track } from 'lwc';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import { updateRecord } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import { ReduceError } from 'c/ldsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getProductDetails from '@salesforce/apex/AddOrderController.getProductDetails';
import productQuery from '@salesforce/apex/AddOrderController.productQuery';
import productValidation from '@salesforce/apex/AddOrderController.productValidation';
import getPricing from '@salesforce/apex/AddOrderController.getPricing';
import getInventory from '@salesforce/apex/AddOrderController.getInventory';
import pricebookCheck from '@salesforce/apex/AddOrderController.pricebookCheck';
import getOrderTotal from '@salesforce/apex/AddOrderController.getOrderTotal';
import submitOrderItems from '@salesforce/apex/AddOrderController.submitOrderItems';
import ORDER_OBJECT from '@salesforce/schema/Order';
import ORDER_ID from '@salesforce/schema/Order.Id';
import ORDER_REASON_CODE from '@salesforce/schema/Order.Reason_Code__c';
import orderInformation from '@salesforce/label/c.Order_Information';
import addProduct from '@salesforce/label/c.Add_Product';
import productCode from '@salesforce/label/c.Product_Code';
import description from '@salesforce/label/c.Description';
import image from '@salesforce/label/c.Image';
import unitPrice from '@salesforce/label/c.Unit_Price';
import extPrice from '@salesforce/label/c.Ext_Price';
import currentInventory from '@salesforce/label/c.Current_Inventory';
import action from '@salesforce/label/c.Action';
import backToOrder from '@salesforce/label/c.Back_to_Order';
import completeOrder from '@salesforce/label/c.Complete_Order';
import quantity from '@salesforce/label/c.Quantity';
import deleteProduct from '@salesforce/label/c.Delete';
import error from '@salesforce/label/c.Error';
import save from '@salesforce/label/c.Save';
import deleteButton from '@salesforce/label/c.Delete';
import addToOrder from '@salesforce/label/c.Add_To_Order';
import selectedProducts from '@salesforce/label/c.Selected_Products';
import productUploadErrors from '@salesforce/label/c.Product_Upload_Errors';
import clearProducts from '@salesforce/label/c.Clear_Products';
import uploadCSV from '@salesforce/label/c.Upload_a_File_csv';
import orderReasonCode from '@salesforce/label/c.Order_Reason_Code';
import enterSKU from '@salesforce/label/c.Enter_SKU';
import materialModelNum from '@salesforce/label/c.Material_Model_no';

const actions = [
    { label: 'Add Product', name: 'add' },
    { label: 'Delete', name: 'delete' }
];

const errorColumns = [
    { 
        label: 'Product',
        fieldName: 'product',
        // cellAttributes: { 
        //     class: 'slds-text-color_success slds-text-title_caps'
        // },
        hideDefaultActions: true,
        editable: true
    },
    { 
        label: 'Quantity',
        fieldName: 'quantity',
        // cellAttributes: {
        //     class: 'slds-text-color_success slds-text-title_caps'
        // },
        hideDefaultActions: true,
        editable: true
    },
    { 
        label: 'Error',
        fieldName: 'error',
        // cellAttributes: {
        //     class: 'slds-text-color_success slds-text-title_caps'
        // },
        hideDefaultActions: true
    }
    // {
    //     label: 'Action',
    //     type: 'action',
    //     // cellAttributes: {
    //     //     class: 'slds-text-color_success slds-text-title_caps'
    //     // },
    //     hideDefaultActions: true,
    //     typeAttributes: {
    //         rowActions: actions,
    //         menuAlignment: 'left'
    //     }
    // }
];

export default class Cc_quick_product_add extends NavigationMixin(LightningElement) {

    label = {
        orderInformation,
        addProduct,
        productCode,
        description,
        image,
        unitPrice,
        extPrice,
        currentInventory,
        action,
        backToOrder,
        completeOrder,
        quantity,
        deleteProduct,
        error,
        save,
        deleteButton,
        addToOrder,
        selectedProducts,
        productUploadErrors,
        clearProducts,
        uploadCSV,
        orderReasonCode,
        enterSKU,
        noSelectedProductsHeader : 'No Added Products',
        noSelectedProductsBody : 'Search for products via SKU, enter a quantity, and they will appear here.',
        materialModelNum
    };

    productTableHeadings = [
        this.label.productCode,
        this.label.description,
        this.label.image,
        this.label.quantity,
        this.label.unitPrice,
        this.label.extPrice,
        this.label.currentInventory,
        this.label.action
    ];
    errorTableHeadings = [
        this.label.productCode,
        this.label.quantity,
        this.label.error,
        this.label.save,
        this.label.deleteButton
    ]
    errorColumns = errorColumns;
    activeTab = 'productsTab';

    hideScreen = false;
    loading = false;
    @api orderId;
    currencyCode;
    reasonCodeOptions;
    productCode = '';
    quantity = 0;
    productQuantity = 0;
    @track addedProducts = [];
    productList = [];
    // pricingProducts = [];
    productMap = new Map();
    pricebookEntriesMap = new Map();
    productsSelected = false;
    @track uploadErrorList = [];
    uploadErrors = false;
    rowsSelected = false;
    errorStagingMap = new Map();
    url;

    acctId;

    currencyCode;

    // SAP reqs
    division;
    customerNumber;
    distributionChannel;
    salesOrganization;
    orderType = 'OR';
    orderReason;
    shipmentDate;
    productMapSAP = new Map();
    materialMap = new Map();
    upcMap = new Map();
    modelMap = new Map();
    pricingMapSAP = new Map();
    accountId;
    orderMin = 0;
    productIds = [];
    inventoryMapSAP = new Map();
    updateOrderHeader = false;

    orderTotal = 'Calculating...';
    numProducts = 0;
    showModal = false;
    backModal = false;
    clearModal = false;
    inputsDisabled = false;

    @wire(getObjectInfo, { objectApiName: ORDER_OBJECT })
    orderObj;

    @wire(getPicklistValues, {
        recordTypeId: "$orderObj.data.defaultRecordTypeId",
        fieldApiName: ORDER_REASON_CODE,
    })
    getReasonCodes({ error, data }){
        if(error){
            this.error = error;
            this.showToast('error getting order reason codes', error.message, 'error');
        } else if(data){
            this.reasonCodeOptions = data.values.map(item => {
                return {
                    label: item.label,
                    value: item.value
                };
            });
        }
    }   



    connectedCallback(){
        this.setIsLoading(true);
        this.getProductDetails();
        this.getOrderTotal();
        this.generateRecordUrl();

    }

    generateRecordUrl(){
        let orderRecordId = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.orderId,
                objectApiName: ORDER_OBJECT.objectApiName,
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](orderRecordId)
        .then(url => this.url = url);
    }
    
    setIsLoading(value) {
        this.loading = value;
        this.inputsDisabled = value;
    }

    getOrderTotal() {
        getOrderTotal({
            orderId : this.orderId
        })
        .then(result => {


            this.orderTotal = result['TotalAmount'];
            this.distributionChannel = result['distChannel'];
        })
        .catch(error => {
            console.log('getOrderTotal() error: ' + error);
        });
    }

    getProductDetails(){ // get all variation products and load into map to draw from later, set callout defaults from account + order fields, etc.

        getProductDetails({ orderId: this.orderId })
        .then(result => {
            this.accountId = result.account.Id; // storing to send in SAP inventory call.
            this.orderMin = result.account.Customer_Group__r.Min_Order__c;
            this.division = result.account.Division_SAP__c;
            this.customerNumber = result.account.SAP_Customer__c;
            //this.distributionChannel = result.account.Distribution_Channel__c;

            this.distributionChannel = result.order.Distribution_Channel__c || this.distributionChannel;
            if(this.distributionChannel=='08'){
//            this.orderMin='500';
                this.orderMin= result.account.Customer_Group__r.Min_Order__c; // MParrella | 10-13-2022 | TSK-00231931 | Removing Plano order minimum
            }
            this.salesOrganization = result.account.Sales_Org__c;
            this.currencyCode = result.order.CurrencyIsoCode;
            // this.orderType = result.order.Type;
            this.orderReason = result.order.Reason_Code__c;
            if(this.orderReason=='125') {
                this.orderMin='0';
            }
            this.shipmentDate = result.order.EffectiveDate;
            this.currencyCode = result.order.CurrencyIsoCode;
            this.setIsLoading(false);
        })
        .catch(error => {
            console.log(error);
            this.showToast('error', 'error getting products', 'error');
            this.setIsLoading(false);
        });
    }

    addProduct(event){
        this.productCode = event.detail.value;
    }

    checkValidity() {
        let isValid = true;
        let qtyInputField = this.template.querySelector('.quantity-input');
        let skuInputField = this.template.querySelector('.sku-input');
        if (qtyInputField.value === '0' || qtyInputField.value === "" || qtyInputField.value.indexOf('-') !== -1){
            qtyInputField.setCustomValidity("Please enter a quantity value > 0.");
            qtyInputField.reportValidity();
            this.setFocusToQtyField();
            isValid = false;
        } else {
            qtyInputField.setCustomValidity("");
            qtyInputField.reportValidity();
        }
        if (skuInputField.value === "") {
            skuInputField.setCustomValidity("Please enter a SKU.");
            skuInputField.reportValidity();
            this.setFocusToSKUField();
            isValid = false;
        } else {
            skuInputField.setCustomValidity("");
            skuInputField.reportValidity();
        }

        return isValid;

    }

    addQuantity(event){
        this.productQuantity = parseInt(event.detail.value);
    }

    calculateOrderTotal() {
        let temp = 0.00;
        if (this.addedProducts.length > 0) {
            this.addedProducts.forEach(item => {
                if (item.extPrice){
                    temp += +item.extPrice;
                }
            });
            this.orderTotal = temp;
        } else {
            this.getOrderTotal();
        }
    }

    updateAddedProductsCount() {
        this.numProducts = (this.addedProducts || []).length;
    }

    productQuery(){
        if (this.checkValidity()){
            this.setIsLoading(true);
            productQuery({ productCode: this.productCode })
            .then(result => {
                this.handleProductQueryResult(result, this.productQuantity, null);
            })
            .catch(error => {
                this.showToast('error', error, 'error');
                this.setIsLoading(false);
            });
        }
    }

    handleProductQueryResult(result, quantity, index){
        if(!result.productValid){
            this.showToast('', 'Product code not found, please try again.', 'error');
                // this.resetInputFields();
            this.setFocusToSKUField();
            this.setIsLoading(false);   
            if(!!index){
                this.activeTab = 'errorTab';
            }            
        } else {
            let product = result.product;
            let productId = product.Id;
            let acctId = this.accountId;
            let productName = product.Name;
            let productCode = product.ProductCode;
            let materialNumber = product.Material__c;
            let caseQuantity = !result.product.Case_Quantity__c ? 1 : result.product.Case_Quantity__c;
            let imageString = product.Hero_Image__c.split("\"");                    
            if(!this.checkCaseQuantity(quantity, caseQuantity)){
                this.showToast('error', `${productName} quantity must be sold in increment of ${caseQuantity}`, 'error');
                if(!index){
                    // this.resetInputFields();
                } else {
                    this.activeTab = 'errorTab';
                }
                this.setFocusToQtyField();
                this.setIsLoading(false);
            } else {
                this.productMap.set(productCode, product); // setting this map for retrieval from product map reference, using SKU field entry as key.
                this.productMapSAP.set(materialNumber, product); // setting this map for retrieval from SAP pricing call, using materialCode as key.
                let products = [{
                    materialNumber: materialNumber,
                    quantity: quantity.toString()
                }];
                let productIds = [productId];
                this.pricebookCheck(productIds)
                .then(result => {
                    if(result.length > 0){
                        Promise.all([this.pricingCall(products), this.inventoryCall(productIds)])
                        .then(() => {
                            // let imageString = this.productMap.get(this.productCode).Hero_Image__c.split("\"");
                            // let materialNumber = this.productMap.get(this.productCode).Material__c;
                            let newUnitPrice = (this.pricingMapSAP.get(materialNumber) / quantity);
                            this.addedProducts.push({
                                orderId: this.orderId,
                                productId: productId,
                                productCode: productCode,
                                productName: productName,
                                materialNumber: materialNumber,
                                image: {
                                    url: imageString[1],
                                    altText: imageString[3]
                                },
                                quantity: quantity,
                                // unitPrice: this.pricebookEntriesMap.get(this.productMap.get(this.productCode).Id).UnitPrice,
                                unitPrice: newUnitPrice.toFixed(2),
                                extPrice: this.pricingMapSAP.get(materialNumber),
                                currentInventory: this.inventoryMapSAP.get(materialNumber),
                                accountId : this.accountId // MParrella | TSK-00236400
                            });
                            this.calculateOrderTotal();
                            this.updateAddedProductsCount();
                            this.setIsLoading(false);
                            if(!index){
                                this.resetInputFields();
                            } else {
                                this.uploadErrorList.splice(index, 1);
                                this.uploadErrors = this.uploadErrorList.length > 0;
                            }
                            
                            this.productsSelected = this.addedProducts.length > 0 ? true : false;
                            this.setIsLoading(false);
                        })
                    } else {
                        this.showToast('error', 'Product not found in pricebook', 'error');
                        this.setIsLoading(false);
                    }
                })                
                .catch(error => {
                    let errorsArray = ReduceError(error);
                    console.log(error);
                    // console.log(errorsArray);
                    // this.showToast('', 'An exception occurred during the callout to SAP. Please retry.', 'error');
                    // this.showToast('error', error, 'error');
                    this.setIsLoading(false);
                });
            }        
        }
        
    }

    async handleFileUpload(event){
        this.setIsLoading(true);
        if(event.detail.files.length > 0){
            this.updateOrderHeader = true;
//            this.setIsLoading(false);
            let parsedData;
             parsedData = await this.parseFile(event.target.files[0]);
            parsedData = parsedData.splice(1, parsedData.length - 2);
            // let productCodes = [];
            let productCodes = {};
            let upcList = [];
            let materialList = [];
            let modelList = [];
            parsedData.forEach(item => {
                // let productCode = !item[1] ? '' : item[1];
                let quantity = !item[0] ? 0 : item[0];
                // console.log('productCode: ' + productCode);
                // if(productCode){
                //     productCodes.push(productCode);
                // }
                // if(!!item[1]){
                //     productCodes.push(item[1]);
                // }
                // if(!!item[2]){
                //     productCodes.push(item[2]);
                // }
                // if(!!item[3]){
                //     productCodes.push(item[3]);
                // }
                if(!!item[1]){
                    // productCodes.push(item[1]);
                    materialList.push(item[1]);
                }
                if(!!item[2]){
                    // productCodes.push(item[2]);
                    upcList.push(item[2]);
                }
                if(!!item[3]){
                    // productCodes.push(item[3]);
                    modelList.push(item[3]);
                }
            });
            productCodes = {
                modelList : modelList,
                upcList: upcList,
                materialList : materialList
            };
            // productValidation({ productCodes: productCodes })
            productValidation({ dataModel: JSON.stringify(productCodes) })
            .then(result => {
                if(result.validProductList.length > 0){
                    let validProductList = result.validProductList;
                    let validProductIds = validProductList.map(product => product.Id);
                    let validProductCodes = validProductList.map(product => product.ProductCode);
                    let invalidProductCodes = [];
                    let validPricebookProductCodes = [];
                    this.pricebookCheck(validProductIds)
                    .then(result => {
                        if(result.length > 0){
                            result.forEach(productCode => {
                                if(!validProductCodes.includes(productCode)){
                                    invalidProductCodes.push(productCode);
                                    this.uploadErrorList.push({
                                        product: productCode,
                                        quantity: '',
                                        error: `${productCode} not found in pricebook.`,
                                        editable: true
                                    });
                                }
                                validPricebookProductCodes = validProductCodes.filter(code => !invalidProductCodes.includes(code));
                            })
                        } else {
                            this.showToast('error', 'No products exist in price book', 'error');
                            this.setIsLoading(false);
                        }
                    })
                    let validProducts = validProductList.filter(validProduct => !invalidProductCodes.includes(validProduct.Id));
                    result.validProductList.forEach(product => {
                        this.productMap.set(product.ProductCode, product);
                        this.materialMap.set(product.Material__c, product);
                        this.upcMap.set(product.UPC_Code__c, product);
                        this.modelMap.set(product.Model_Number__c, product);
                    });
                    parsedData.forEach(item => {
                        let items = [item[1], item[2], item[3]];
                        // let productCode;
                        // items.filter(code => !!code).forEach(code => {
                        //     productCode = code;
                        //     return;
                        // });
                        // if(!this.productMap.has(productCode)){
                        let mapResult = this.checkAllMapsForCode(items);
                        let originatingMap = mapResult.originatingMap;
                        let code = mapResult.code;
                        let mapContainsKey = mapResult.mapContainsKey;
                        let productCode = mapContainsKey ? this.setProductCode(code, originatingMap) : code;
                        // this.setProductMap();
                        if(!mapContainsKey){
                            this.uploadErrors = true;
                            this.uploadErrorList.push({
                                product: productCode,
                                quantity: item[0],
                                error: `${productCode} not found.`,
                                editable: false
                                // incorrectCaseQuantity: true
                            });
                        } else {
                            // this.productMap.set(product.ProductCode, product);
                            let productName = this.productMap.get(productCode).Name;
                            let materialNumber = this.productMap.get(productCode).Material__c;
                            let imageString = this.productMap.get(productCode).Hero_Image__c.split("\"");
                            let caseQuantity = !this.productMap.get(productCode).Case_Quantity__c ? 1 : this.productMap.get(productCode).Case_Quantity__c;
                            if(!this.checkCaseQuantity(item[0], caseQuantity)){
                                this.uploadErrorList.push({
                                    // id: errorIndex++,
                                    product: productCode,
                                    quantity: item[0],
                                    error: `${productCode} quantity must be sold in increments of ${caseQuantity}`,
                                    editable: true
                                    // incorrectCaseQuantity: false
                                });
                            } else {
                                this.addedProducts.push({
                                    orderId: this.orderId,
                                    productId: this.productMap.get(productCode).Id,
                                    productCode: productCode,
                                    productName: productName,
                                    materialNumber: materialNumber,
                                    image: {
                                        url: imageString[1],
                                        altText: imageString[3]
                                    },
                                    quantity: item[0]
                                });
                            }
                        }
                    });
                    this.updateAddedProductsCount();
                    if(this.addedProducts.length > 0){

                        this.productsSelected = true;
                        this.calculateOrderTotal();
                        this.bulkChange();
                    }
                    if(this.uploadErrorList.length > 0){
                        this.uploadErrors = true;
                        this.showToast('File uploaded with errors', 'Please make corrections on error tab', 'warning');
                    } else {
                        this.showToast('', 'File uploaded successfully. Retrieving SAP data...', 'success');
                    }
//                    this.setIsLoading(false);
                } else {
                    this.showToast('No products found', 'Please try again', 'error');
                    this.setIsLoading(false);
                }
            })
            .catch(error => {
                this.setIsLoading(false);
                let errorsArray = ReduceError(error);
                console.log(errorsArray[0]);
                this.showToast('error', 'error uploading csv', 'error');
            });
        }
    }

    checkAllMapsForCode(items){
        let mapContainsKey = false;
        let code;
        let originatingMap = 'Product';
        switch(true){
            case !!items[0]:
                code = items[0];
                originatingMap = 'Material'
                break;
            case !!items[1]:
                code = items[1];
                originatingMap = 'UPC';
                break;
            case !!items[2]:
                code = items[2];
                originatingMap = 'Model';
                break;
        }
        if(!!code){
            switch(originatingMap){
                case 'Material':
                    mapContainsKey = this.materialMap.has(code);
                    break;
                case 'UPC':
                    mapContainsKey = this.upcMap.has(code);
                    break;
                case 'Model':
                    mapContainsKey = this.modelMap.has(code);
                    break;
                default:
                    mapContainsKey = false;
                    break;
            }
        }
        return {
            mapContainsKey: mapContainsKey,
            originatingMap: originatingMap,
            code: code
        }
    }

    setProductCode(code, map){
        switch(map){
            case 'Material':
                return this.materialMap.get(code).ProductCode;
            case 'UPC':
                return this.upcMap.get(code).ProductCode;
            case 'Model':
                return this.modelMap.get(code).ProductCode;
        };
    }

    bulkChange(){
        this.setIsLoading(true);
        // let productCodes = [];
        let productList = []; // hold the list of products for pricing call
        let productIds = []; // hold the list of product Ids for inventory call
        this.addedProducts.forEach(item => {
            // productCodes.push(item.ProductCode);
            let productCode = item.productCode;
            let quantity = item.quantity;
            productList.push({
                materialNumber: this.productMap.get(productCode).Material__c,
                quantity: quantity
                //quantity: 1
            });
            productIds.push(this.productMap.get(productCode).Id);
        });
        this.clearCalloutMaps();
        Promise.all([this.pricingCall(productList), this.inventoryCall(productIds)])
        .then(() => {
            this.addedProducts.forEach(product => {
                let materialNumber = product.materialNumber;
                let quantity = product.quantity;
                 product.unitPrice = (this.pricingMapSAP.get(materialNumber) / quantity).toFixed(2);
                //product.extPrice = this.pricingMapSAP.get(materialNumber) * quantity;
                product.extPrice = this.pricingMapSAP.get(materialNumber);
                // product.extPrice = this.pricingMapSAP.get(materialNumber);
                product.currentInventory = this.inventoryMapSAP.get(materialNumber);
                this.updateOrderHeader = false;
            });
            this.calculateOrderTotal();
            this.updateAddedProductsCount();
            this.setIsLoading(false);
        })
        .catch(error => {
            let errorsArray = ReduceError(error);
            console.log(error);
            console.log(errorsArray);
            // this.showToast('', 'An exception occurred during the callout to SAP. Please retry.', 'error');
            this.setIsLoading(false);
        });        
    }

    resetInputFields(){
        this.productCode = '';
        this.quantity = '0';
        this.productQuantity = '0';
    }

    clearCalloutMaps(){
        this.pricingMapSAP.clear();
        this.inventoryMapSAP.clear();
    }

    checkCaseQuantity(productQuantity, caseQuantity){
        return productQuantity % caseQuantity == 0;
    }

    pricebookCheck(productIds){
        return new Promise((resolve, reject) => {
            let productData = {
                orderId: this.orderId,
                productIds: productIds,
                currencyCode: this.currencyCode
            }
            pricebookCheck({ productData: JSON.stringify(productData) })
            .then(result => {
                resolve(result)
                // if(result.length > 0){
                //     resolve(result);
                // } else {
                //     reject(error);
                // }
            })
        })
    }

    pricingCall(products){
        return new Promise((resolve, reject) => {
            let productObj = {
                division: this.division,
                customerNumber: this.customerNumber,
                distributionChannel: this.distributionChannel,
                salesOrganization: this.salesOrganization,
                // products: [{
                //     materialNumber: this.productMap.get(this.productCode).Material__c,
                //     quantity: this.productQuantity.toString()
                // }],
                products: products,
                orderType: this.orderType,
                orderReason: this.orderReason,
                shipmentDate: this.shipmentDate,
                accountId : this.accountId
            };
            getPricing({ productData: JSON.stringify(productObj) })
            .then(result => {
                result.forEach( line => {
                    this.pricingMapSAP.set(line.materialNumber, line.price);
                });
                resolve(result);
            })
            .catch(error => {
                this.setIsLoading(false);
                let errorsArray = ReduceError(error);
                console.log('Pricing Error: ', errorsArray);
                this.showToast('error getting pricing', error.body.message, 'error');
                reject(error);
                // reject(errorsArray[0]);
            });
        });
    }

    openModal(event) {
        // check if modal was opened from 'clear' button or 'back to order' button
        let eventSource = event.target.dataset.id;
        if (eventSource === 'clear'){
            this.clearModal = true;
            this.backModal = false;
        }
        else if (eventSource === 'backToOrder') {
            this.backModal = true;
            this.clearModal = false;
        }
        this.showModal = true;
    }

    closeModal() {
        this.showModal = false;
    }

    inventoryCall(productIds){
        return new Promise((resolve, reject) => {
            let inventoryData = {
                accountId: this.accountId,
                // productIds: [this.productMap.get(this.productCode).Id]
                productIds: productIds
            };
            getInventory({ inventoryData: JSON.stringify(inventoryData) })
            .then(result => {
                result.forEach(item => {
                    this.inventoryMapSAP.set(item.materialNumber, item.quantity);
                });
                resolve(result);
            })
            .catch(error => {
                let errorsArray = ReduceError(error);
                this.setIsLoading(false);
                this.showToast('error', error.body.message, 'error');
                reject(errorsArray[0]);
            });
        });
    }

    handleErrorCorrect(event){
        // event.preventDefault();
        // this.selectedTab = 'Error Tab';
        let selectedRow = event.currentTarget;
        let field = selectedRow.dataset.name;
        let index = selectedRow.dataset.id;
        let fieldValue = event.detail.value;
        this.errorStagingMap.set(`${index}${field}`, fieldValue);
    }

    setObjectProperty(event){
        let selectedRow = event.currentTarget;
        let field = selectedRow.dataset.name;
        let index = selectedRow.dataset.id;
        let fieldValue = this.errorStagingMap.get(`${index}${field}`);
        if(!!fieldValue){
            switch(field){
                case 'product':{
                    this.uploadErrorList[index].product = fieldValue;
                    break;
                }
                case 'quantity':{
                    this.uploadErrorList[index].quantity = fieldValue;
                    break;
                }
            }
        }
    }

    setFocusToSKUField() {
        setTimeout(() => {
            const skuField = this.template.querySelector('lightning-input[data-fieldname=\'sku-field\']');
            skuField.focus();
        }, 250);
    }

    setFocusToQtyField() {
        setTimeout(() => {
            const qtyField = this.template.querySelector('lightning-input[data-fieldname=\'qty-field\']');
            qtyField.focus();
        }, 250);
    }

    deleteLine(event){
        let selectedRow = event.currentTarget;
        let index = selectedRow.dataset.id;
        let table = selectedRow.dataset.name;
        switch(table){
            case 'Products':{
                this.addedProducts.splice(index, 1);
                this.productsSelected = this.addedProducts.length > 0 ? true : false;
                this.calculateOrderTotal();
                this.updateAddedProductsCount();
                break;
            }
            case 'Errors':{
                this.uploadErrorList.splice(index, 1);
                this.uploadErrors = this.uploadErrorList.length > 0 ? true : false;                
                this.calculateOrderTotal();
                this.updateAddedProductsCount();
                break;
            }
        }
    }

    saveLine(event){
        this.setIsLoading(true);
        let index = event.currentTarget.dataset.id
        // let index = this.uploadErrorList.findIndex(obj => {
        //     return obj.id == event.currentTarget.dataset.id;
        // });
        let productCode = this.uploadErrorList[index].product;
        let quantity =  this.uploadErrorList[index].quantity;
        productQuery({ productCode: productCode})
        .then(result => {
            this.handleProductQueryResult(result, quantity, index);
        })
        .catch(error => {
            let errorsArray = ReduceError(error);
            this.showToast('error', errorsArray[0], 'error');
            this.setIsLoading(false);
        });
    }

    handleReasonCodeChange(event){
        this.orderReason = event.detail.value;
        this.updateOrderHeader = true;
        if(this.addedProducts.length > 0){
            this.bulkChange();
        }        
    }

    handleRowAction(event){
    }

    handleRowSelection(event){
        this.rowsSelected = event.detail.selectedRows.length > 0 ? true : false;
    }

    handleRowSave(event){
    }

    printErrorStickyToasts(){ // multiple one line error sticky toasts.
        this.uploadErrorList.forEach(error => {
            if(error.error == 'Map'){
                this.showStickyToast('error on file upload', 'Product not found: ' + error.product, 'error');
            }
        });
    }

    printErrorList(){ //  single toast with this list of errors.
        let errors = '';
        this.uploadErrorList.forEach(error => {
            if(error.error == 'Map'){
                errors += 'Product not found: ' + error.product + '/n';
            }
        });
        return errors;
    }

    parseFile(inputFile){
        return new Promise(resolve => {
            var reader = new FileReader();
            reader.onload = function () {
                var lines = this.result.split('\n');
                let result = lines.map(function (line) {
                    line = line.replace('\r','');
                    return line.split(',');
                });
                resolve(result);
            }
            reader.readAsText(inputFile);
        });
    }

    backToOrder(event){
//        this.handleNavigate(false);
        this.dispatchEvent(
            new CustomEvent (
                'goback'
            )
        );
    }

   addToOrder(event){
       this.submitOrderItems(false);
   }

   completeOrder(event){
       this.submitOrderItems(true);
   }

    submitOrderItems(checkout){
        this.setIsLoading(true);
        let orderTotal = this.addedProducts.reduce(
            (total, price) => total + price.extPrice, 0
        );
            submitOrderItems({ dataModel: JSON.stringify(this.addedProducts) })
            .then(result => {
                if(result.numberOfErrors > 0){
                    this.showToast('Order items inserted with errors', 'Please check Product Upload Error tab', 'warning');
                    result.errorList.forEach(error => {
                        let errorString = error.split("-");
                        this.uploadErrorList.unshift({
                            product: errorString[1],
                            quantity: errorString[0],
                            error: errorString[2],
                            editable: false
                        });
                    });
                    this.activeTab = 'errorTab';
                    this.addedProducts = [];
                    this.productsSelected = false;
                    this.setIsLoading(false);
                } else {
                    // getRecordNotifyChange([{recordId : this.orderId}]);
                    this.showToast('success', 'order items created successfully!', 'success');
                    // if(this.updateOrderHeader){
                        const fields = {};
                        fields[ORDER_ID.fieldApiName] = this.orderId;      
                        fields[ORDER_REASON_CODE.fieldApiName] = this.orderReason; 
                        const recordInput = { fields };
                        updateRecord(recordInput)
                        .then(() => {
                            this.handleNavigate(checkout);
                        })
                    // } else {
                    //     this.handleNavigate(checkout);
                    // } 
                }                                       
            })
            .catch(error => {
                this.setIsLoading(false);
                let errorsArray = ReduceError(error);
                console.log('error inserting lines: ', errorsArray);
                this.showToast('', 'Error: ' + errorsArray[0], 'error');
            });
//        }
    }

    clearProducts(){
        this.showModal = false;
        this.addedProducts = [];
        this.productsSelected = false;
        this.uploadErrorList = [];
        this.uploadErrors = false;
        this.calculateOrderTotal();
        this.updateAddedProductsCount();


    }

    handleNavigate(checkout){
        if(checkout){
            // this.dispatchEvent(new CustomEvent('launchflow'));
            this[NavigationMixin.Navigate]({
                type: 'standard__component',
                attributes: {
                    componentName: 'c__ccCompleteOrderFlow' 
                },
                state: {
                    'c__id': this.orderId
                }
            })
        } else {
            this.dispatchEvent(new CustomEvent('navigatetorecord'));            
            // this[NavigationMixin.Navigate]({
            //     type: 'standard__recordPage',
            //     attributes: {
            //         recordId: this.orderId,
            //         objectApiName: ORDER_OBJECT.objectApiName,
            //         actionName: 'view'
            //     }
            // });
        }
        
    }

    @api launchFlow(start){
        this.hideScreen = start;
    }

    showToast(title, message, variant){
        this.dispatchEvent(new ShowToastEvent({
            title, message, variant
        }));
    }

    showStickyToast(title, message, variant){
        this.dispatchEvent(new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'sticky'
        }));
    }

}