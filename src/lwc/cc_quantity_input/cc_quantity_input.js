/**
 * Created by Matt on 4/25/2022.
 */

import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import LABEL_ADD_TO_WISHLIST from '@salesforce/label/c.cc_mg_page_totals_add_to_wishlist';
import LABEL_ADD_TO_CART from '@salesforce/label/c.cc_mg_page_totals_add_to_cart';
import LABEL_QUANTITY_LABEL from '@salesforce/label/c.cc_mg_master_grid_quantity_header';
import getProductPriceData from '@salesforce/apex/MasterGridController.getProductPriceData';
import getInfo from '@salesforce/apex/QuickOrderUploadController.getInfo';
import getProductDetail from '@salesforce/apex/MasterGridController.getProductDetail';
import addProductsToCart from '@salesforce/apex/MasterGridController.addProductsToCart';
import COMM_ID from '@salesforce/community/Id';
import LANG from '@salesforce/i18n/lang';

export default class cc_quantity_input extends NavigationMixin(LightningElement) {
    productPrice = 0;
    quantityVal = 0;
    quantityText = LABEL_QUANTITY_LABEL;
    @api recordId;
    productName;
    cartId;
    storeLanguage = LANG;
    addToCartBtnClass = '';
    layoutClass = '';
    cartBtnTxt = LABEL_ADD_TO_CART;
    wishlistBtnTxt = LABEL_ADD_TO_WISHLIST;
    webstoreId;
    btnDisabled = true;
    maxUploadRows;
    isLoading = false;
    showWishlistModal = false;
    @api effectiveAccountId;
    inputTimer;
    wishlistData = [];
    waitInterval = 400; // Used to control the debouncing of input field

    connectedCallback() {
        this.getStoreData();
        this.getPriceData();
        this.setStoreLanguageClass();
    }

    // Fixing button alignment in French Store
    setStoreLanguageClass() {
        if (this.storeLanguage == 'fr') {
            this.addToCartBtnClass = 'qty-input-cart-btn-fr';
            this.layoutClass = 'qty-input-layout-fr'
        } else {
            this.addToCartBtnClass = '';
            this.layoutClass = '';
        }
    }

    getStoreData() {
        getInfo({
            userId : null,
            effectiveAccountId : this.effectiveAccountId,
            communityId : COMM_ID,
            webstoreId : null
        })
        .then(result => {
                let data = {
                    cartId : result.cartId,
                    webstoreId : result.webstoreId,
                    effectiveAccountId : result.effectiveAccountId
                };
                this.cartId = result.cartId;
                this.webstoreId = result.webstoreId;
                this.maxUploadRows = result.maxUploadRows;
        })
        .catch(error => {

        });

    }

    getPriceData() {
        getProductPriceData({
            communityId: COMM_ID,
            productId: this.recordId,
            effectiveAccountId : this.effectiveAccountId
        })
        .then(result => {
            this.productPrice = result.recordData.your_price;
        })
        .catch(error => {
            console.log('ERROR: ', error);
        })
    }

    getRecordData() {
        getProductDetail({
            productId : this.recordId
        })
        .then(result => {
            // Store Product Name
            this.productName = result.prodData.productname;
        })
        .catch(error => {
            console.log('getRecordData() ERROR: ', error);
        });
    }

    handleQuantityChange(event) {
        clearTimeout(this.inputTimer);
        let inputValue = event.target.value;
        this.inputTimer = setTimeout(() => {
            this.quantity = inputValue;
            if (this.quantity == 0 || !this.quantity) {
                this.btnDisabled = true;
            } else {
                this.btnDisabled = false;
            }
        }, this.waitInterval);
    }

    showToastMessage(inputTitle, inputMsg, inputVariant) {
        this.dispatchEvent(new ShowToastEvent({
            title: inputTitle,
            message: inputMsg,
            variant: inputVariant
        }));
    }

    handleAddToCart() {
        this.isLoading = true;
        // Build JSON object to send to back-end
        try {
            let itemsList = [];
            let productObj = {
                productId: this.recordId,
                qty: this.quantity,
                type: '',
                productName: this.productName
            };
            itemsList.push(productObj);

            // Call Apex method to add product to cart
            addProductsToCart({
                JSONitemsList : JSON.stringify(itemsList),
                webstoreId : this.webstoreId,
                effectiveAccountId : this.effectiveAccountId,
                activeCartOrId : this.cartId
            })
            .then(result => {
                this.isLoading = false;
                if (result.messages) {
                    if (result.messages[0].severity === 'success'){
                        // Refresh the cart icon in the event we have partial success (for example: 2 out of 3 products were found and added to the cart, but 1 had a cart failure)
                           this.dispatchEvent(new CustomEvent("cartchanged", {
                                 bubbles: true,
                                 composed: true
                             }));
                        // redirect to cart page
                        this[NavigationMixin.Navigate]({
                           type: 'standard__webPage',
                           attributes : {
                               url: '/cart/' + this.cartId
                           }
                        });
                    } else {
                        let messages = JSON.parse(JSON.stringify(result.messages));
                        this.highLvlErrorMsg = messages[0].title;
                        let errorMsg = messages[0].message;
                        this.showToastMessage(
                            this.highLvlErrorMsg,
                            errorMsg,
                            'error'
                        );
                    }
                }
            })
            .catch(error => {
                this.isLoading = false;
                this.showToastMessage('', error.message, 'error');
            });
        }
        catch(e) {
            console.log('EXCEPTION CAUGHT: ', e);
            this.isLoading = false;
            this.showToastMessage('',e.message,'error');
        }
    }

    handleAddToWishlist() {
        let productObj = {
            productId : this.recordId,
            quantity : this.quantity
        };
        this.wishlistData.push(productObj);
        this.showWishlistModal = true;
    }

    closeModal() {
        this.showWishlistModal = false;
    }

}