/**
 * Created by Matt on 3/30/2022.
 */

import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import VIEW_DETAILS_LABEL from '@salesforce/label/c.cc_mg_view_details';
import PRODUCT_OBJECT from '@salesforce/schema/Product2';

export default class cc_mg_product_image_display extends NavigationMixin(LightningElement) {
    @api imgUrl;
    @api productId;
    @api productName;

    viewDetailsLabel = VIEW_DETAILS_LABEL;

    goToRecordDetail(event) {
        let targetProductId = event.target.dataset.id;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: targetProductId,
                objectApiName: PRODUCT_OBJECT,
                actionName: 'view'
            }
        });
    }
}