/**
 * Created by Matt on 1/5/2023.
 */

import { LightningElement, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import getTranslations from '@salesforce/apex/TranslationUtilities.getTranslatedProductFields';
import FIELD_VARIANT_PARENT from '@salesforce/schema/Product2.Variation_Parent__c';
import FIELD_ID from '@salesforce/schema/Product2.Id';

const productFields = ['Name'];

export default class cc_pdp_product_name extends LightningElement {
    @api recordId;
    @api textSize;
    textSizeClass;
    nameField;

    connectedCallback() {
        // Set the style based on the 'Text Size' selected in builder
        switch (this.textSize) {
            case 'Small':
                this.textSizeClass = 'small-text';
                break;

            case 'Medium':
                this.textSizeClass = 'medium-text';
                break;

            case 'Large':
                this.textSizeClass = 'large-text';
                break;

            case 'X-Large':
                this.textSizeClass = 'x-large-text';
                break;

            default:
                this.textSizeClass = 'medium-text';
                break;
        }
    }

    @wire(getRecord, { recordId: '$recordId', fields: [FIELD_ID, FIELD_VARIANT_PARENT]})
    wiredRecord({ error, data }) {
        let productId = '';
        if (data) {
            if (data.fields.Variation_Parent__c.value) {
                console.log('Variation product detected.');
                productId = data.fields.Variation_Parent__c.value;
            } else {
                console.log('Standard product detected.');
                productId = this.recordId;
            }

        // Call method to get the Name of the Variation Parent (if applicable)
        getTranslations({
            product2Id : productId,
            fields : productFields
        })
        .then(result => {
            this.nameField = result['Name'];
        })
        .catch(error => {
            this.nameField = '';
            console.log('Error retrieving name: ', error);
        });

        } else if (error) {
            console.log('error: ', error);
        }
    }
}