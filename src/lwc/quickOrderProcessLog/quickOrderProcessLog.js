/**
 * Created by Matt on 3/23/2022.
 */

import { LightningElement, api } from 'lwc';

export default class QuickOrderProcessLog extends LightningElement {
    @api showProcessLog;
    @api richText;
    @api titleError;
    @api isSuccess;
}