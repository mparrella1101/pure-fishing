/**
 * Created by Matt on 4/26/2022.
 */

import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import COMMUNITY_ID from '@salesforce/community/Id';
import getSuggestedOrders from '@salesforce/apex/ConnectAPIUtils.getSuggestedOrders';
import getInfo from '@salesforce/apex/QuickOrderUploadController.getInfo';
import addToSuggestedOrder from '@salesforce/apex/ConnectAPIUtils.addToSuggestedOrder';
import createSuggestedOrder from '@salesforce/apex/ConnectAPIUtils.createSuggestedOrder';
import addCartToNewOrder from '@salesforce/apex/MasterGridController.addCartToNewOrder';
import addCartToExistingOrder from '@salesforce/apex/MasterGridController.addCartToExistingOrder';
import addBulkItemsToWishList from '@salesforce/apex/MasterGridController.addBulkItemsToWishList';
import { ReduceError } from 'c/ldsUtils';

import LABEL_ADD_WISHLIST_HEADER_LABEL from '@salesforce/label/c.cc_add_wishlist_header_label';
import LABEL_ADD_WISHLIST_EXISTING_LABEL from '@salesforce/label/c.cc_add_wishlist_add_existing_label';
import LABEL_ADD_WISHLIST_CREATE_LABEL from '@salesforce/label/c.cc_add_wishlist_create_label';
import LABEL_ADD_WISHLIST_LOADING_TEXT from '@salesforce/label/c.cc_add_wishlist_loading_text';
import LABEL_ADD_WISHLIST_NO_EXISTING_LABEL from '@salesforce/label/c.cc_add_wishlist_no_existing_label';
import LABEL_ADD_WISHLIST_SELECT_PLACEHOLDER from '@salesforce/label/c.cc_add_wishlist_select_placeholder';
import LABEL_ADD_WISHLIST_NEW_ORDER_LABEL from '@salesforce/label/c.cc_add_wishlist_new_order_label';
import LABEL_ADD_WISHLIST_SELECT_LABEL from '@salesforce/label/c.cc_select_expiration_date_label';
import LABEL_ADD_WISHLIST_CANCEL_LABEL from '@salesforce/label/c.cc_add_wishlist_cancel_label';
import LABEL_ADD_WISHLIST_NEXT_LABEL from '@salesforce/label/c.cc_add_wishlist_next_label';
import LABEL_ADD_WISHLIST_SELECT_INPUT_ERROR from '@salesforce/label/c.cc_add_wishlist_select_input_error';
import LABEL_ADD_WISHLIST_TEXT_INPUT_ERROR from '@salesforce/label/c.cc_add_wishlist_text_input_error';
import LABEL_ADD_WISHLIST_SUCCESS_PART_1 from '@salesforce/label/c.cc_add_wishlist_success_part_1';
import LABEL_ADD_WISHLIST_SUCCESS_PART_2 from '@salesforce/label/c.cc_add_wishlist_success_part_2';
import LABEL_ADD_WISHLIST_SUCCESS_PART_3 from '@salesforce/label/c.cc_add_wishlist_success_part_3';

const CART_CHANGED_EVT = 'cartchanged';

export default class cc_add_to_wishlist extends NavigationMixin(LightningElement) {
    isLoading = false; // Controls loading spinner
    @api recordId;
    productIds = [];
    @api wishlistDataInput; // Stores Product2Id -> object data
    existingOrders = []; // Stores all existing Wishlists returned from Connect API callout
    wishlistListSelectDisabled = true; // Controls the dropdown input field
    newListInputDisabled = true; // Controls the text input field
    newListName = ''; // Stores the text entered by the user when creating a new list
    existingSelected = false; // TRUE when user selects to update an existing Wishlist
    newSelected = false; // TRUE when user selects to create a new Wishlist
    selectedList; // Stores the Id of the selected Wishlist that the user selected from the existing dropdown
    selectValidity; // Stores the validity status of the select input field
    textValidity; // Store the validity status of the text input field
    selectPlaceholder = LABEL_ADD_WISHLIST_LOADING_TEXT;
    @api isRecordDetailPage = false;
    @api isCartPage = 'false';
    cartId;
    errorMsgs = []; // Hold any and all error messages encountered
    webstoreId;
    @api effectiveAccountId;
    addToExistingWishlistText = LABEL_ADD_WISHLIST_EXISTING_LABEL;
    createNewWishlistText = LABEL_ADD_WISHLIST_CREATE_LABEL;
    newSuggestedOrderLabel = LABEL_ADD_WISHLIST_NEW_ORDER_LABEL;
    selectExpDateText = LABEL_ADD_WISHLIST_SELECT_LABEL;
    cancelText = LABEL_ADD_WISHLIST_CANCEL_LABEL;
    nextText = LABEL_ADD_WISHLIST_NEXT_LABEL;
    selectedListName;
    expirationDate;
    btnDisabled = true;

    // Custom label assignments
    headerText = LABEL_ADD_WISHLIST_HEADER_LABEL;

    setIsLoading(newValue) {
        this.isLoading = newValue;
    }

    closeModal() {
        this.dispatchEvent(new CustomEvent('closemodal'));
    }

    connectedCallback() {
        this.setupDefaultExpirationDate();
        this.retrieveInfo();
        this.init();
    }


    setupDefaultExpirationDate() {
        // Calculate + construct tomorrow's date to set as default expiration date
        let today = new Date();
        this.expirationDate = today.toISOString().split('T')[0];
        let splitDate = this.expirationDate.split('-');
        let tempDate = new Date(`${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`);
        this.expirationDate = new Date(tempDate.setDate(tempDate.getDate() + 1)).toISOString().split('T')[0];
    }

    async init() {
        this.selectPlaceholder = LABEL_ADD_WISHLIST_LOADING_TEXT;
        this.existingOrders = await this.retrieveSuggestedOrders();
        if (this.existingOrders.length === 0) {
            // Disable the 'existing' radio option
            let existingRadio = this.template.querySelector('.existingSelect');

            existingRadio.disabled = true;
            this.selectPlaceholder = LABEL_ADD_WISHLIST_NO_EXISTING_LABEL;
        } else {
            this.selectPlaceholder = LABEL_ADD_WISHLIST_SELECT_PLACEHOLDER;
        }
    }

    handleRadioSelect(event) {
        let selection = event.target.value;
        this.btnDisabled = false;
        // clear any persisted field validity messages
        let inputFields = this.template.querySelectorAll('.inputField');
        inputFields.forEach(field => {
            field.setCustomValidity("");
            field.reportValidity();
        });

        switch (selection) {
            case 'existing':
                this.existingSelected = true;
                this.newSelected = false;
                this.wishlistListSelectDisabled = false;
                this.newListInputDisabled = true;
                break;
            case 'new':
                this.existingSelected = false;
                this.newSelected = true;
                this.wishlistListSelectDisabled = true;
                this.newListInputDisabled = false;
                break;
            default:
                this.newListInputDisabled = true;
                this.wishlistListSelectDisabled = true;
        }

    }

    handleDateChange(event) {
        // Get Date value from event
        let selectedDate = event.detail.value;

        // Split into array
        let splitDate = selectedDate.split('-');

        // Convert back into Date object and assign
        this.expirationDate = new Date(`${splitDate[1]}-${splitDate[2]}-${splitDate[0]}`).toISOString().split('T')[0];
    }

    handleTextInput(event) {
        this.newListName = event.target.value;
    }

    checkValidity(selector) {
        let isValid = true;
        let inputField = this.template.querySelector(selector);
        if (selector === '.add-to-wishlist-select-input' && inputField.value === undefined) {
            inputField.setCustomValidity(LABEL_ADD_WISHLIST_SELECT_INPUT_ERROR);
            inputField.reportValidity();
            isValid = false;
        }
        else if (selector === '.add-to-wishlist-text-input' && inputField.value === "") {
            inputField.setCustomValidity(LABEL_ADD_WISHLIST_TEXT_INPUT_ERROR);
            inputField.reportValidity();
            isValid = false;
        }
        return isValid;
    }

    handleConfirm() {
        // Check to see which option is selected and perform specific checks
        if (this.existingSelected){
            if (this.checkValidity('.add-to-wishlist-select-input')) {
                // Pass Wishlist id to back-end to update existing Wishlist via ConnectApi
                this.updateWishlist();
            }
        }
        else if (this.newSelected) {
            if (this.checkValidity('.add-to-wishlist-text-input')) {
                // Pass entered name text to back-end to create new Wishlist via ConnectApi
                this.createWishList(this.newListName);
            }
        }
    }

    retrieveInfo() {
        getInfo({
            userId : null,
            effectiveAccountId : this.effectiveAccountId,
            communityId : COMMUNITY_ID,
            webstoreId : null
        })
        .then(result => {
                let data = {
                    cartId : result.cartId,
                    webstoreId : result.webstoreId,
//                    effectiveAccountId : result.effectiveAccountId
                };
                this.cartId = result.cartId;
                this.webstoreId = result.webstoreId;
//                this.effectiveAccountId = result.effectiveAccountId;
        })
        .catch(error => {
            this.errorMsgs = ReduceError(error);
            this.showToastMessage('retrieveInfo() Error', this.errorMsgs[0], 'error');
            console.log('Error in retrieveInfo() function: ', this.errorMsgs);
        });
    }

    retrieveSuggestedOrders() {
        return new Promise((resolve,reject) => {
            getSuggestedOrders({
                communityId : COMMUNITY_ID,
                webstoreId : null,
                effectiveAccountId : this.effectiveAccountId
            })
            .then(result => {
                resolve(result);
            })
            .catch(error => {
                this.errorMsgs = ReduceError(error);
                console.log('ERROR: ', this.errorMsgs);
                reject(this.errorMsgs[0]);
            });
        });
    }

    showToastMessage(inputTitle, inputMsg, inputVariant) {
        this.dispatchEvent(new ShowToastEvent({
            title: inputTitle,
            message: inputMsg,
            variant: inputVariant
        }));
    }

    createWishList(name) {
        this.setIsLoading(true);
        let tempArray = [];
        if (this.wishlistDataInput){
            tempArray = JSON.parse(JSON.stringify(this.wishlistDataInput));
        }

        // Check if we're firing from Cart Page, if so -- call 'addCartToWishlist' method
        if (this.isCartPage == "true") {
            // Create a new wishlist
            createSuggestedOrder({
                orderName : name,
                webstoreId : this.webstoreId,
                effectiveAccountId : this.effectiveAccountId,
                productQuantityMap : null,
                expirationDate : this.expirationDate
            })
            .then(result => {
                let newOrderId = result.Id;
                // Add CartItems to Wishlist
                addCartToNewOrder({
                    communityId : COMMUNITY_ID,
                    effectiveAccountId : this.effectiveAccountId,
                    activeCartOrId : this.recordId,
                    orderId : newOrderId
                })
                .then(result => {
                    if (result.isSuccess) {
                        this.setIsLoading(false);
                        this.showToastMessage('', LABEL_ADD_WISHLIST_SUCCESS_PART_1 + ' ' + name + ' ' + LABEL_ADD_WISHLIST_SUCCESS_PART_2, 'success');
                        this.handleCartUpdate();
                        this.navigateToList(newOrderId);
                        this.closeModal();
                    } else {
                        this.setIsLoading(false);
                        this.showToastMessage('', result.errorMsg, 'error');
                    }
                })

            })
            .catch(error => {
                this.setIsLoading(false);
                let errorMsgs = ReduceError(error);
                this.showToastMessage('', errorMsgs[0], 'error');
            });
        } else {
            createSuggestedOrder({
                orderName : name,
                webstoreId : this.webstoreId,
                effectiveAccountId : this.effectiveAccountId,
                productQuantityMap : JSON.stringify(tempArray),
                expirationDate : this.expirationDate
            })
            .then(result => {
                this.showToastMessage('', LABEL_ADD_WISHLIST_SUCCESS_PART_1 + ' ' + name + ' ' + LABEL_ADD_WISHLIST_SUCCESS_PART_2, 'success');
                this.navigateToList(result.Id);

                this.closeModal();
            })
            .catch(error => {
                this.setIsLoading(false);
                this.errorMsgs = ReduceError(error);
                this.showToastMessage('', this.errorMsgs[0], 'error');
                console.log('Create ERROR: ', this.errorMsgs);
            });
        }
    }

    handleWishlistSelect(event) {
        this.selectedListName = event.target.options.find(opt => opt.value === event.detail.value).label;
        this.selectedList = event.target.value;
    }

    updateWishlist() {
        this.setIsLoading(true);
        let tempArray = [];
        if (this.wishlistDataInput){
            tempArray = JSON.parse(JSON.stringify(this.wishlistDataInput));
        }
        // Check if we're on the Cart page, if so -- call the 'addCartToWishlist' apex method
        if (this.isCartPage === "true") {
            addCartToExistingOrder({
                communityId : COMMUNITY_ID,
                effectiveAccountId : this.effectiveAccountId,
                activeCartOrId : this.recordId,
                orderId : this.selectedList
            })
            .then(result => {
                if (result.isSuccess) {
                    this.setIsLoading(false);
                    this.handleCartUpdate();
                    this.showToastMessage('', LABEL_ADD_WISHLIST_SUCCESS_PART_3 + ' \'' + this.selectedListName + ' \'' + LABEL_ADD_WISHLIST_SUCCESS_PART_2, 'success');
                    this.navigateToList(this.selectedList);
                    this.closeModal();
                } else {
                    this.setIsLoading(false);
                    this.showToastMessage('', result.errorMsg, 'error');
                }
            })
            .catch(error => {
                this.setIsLoading(false);
                this.errorMsgs = ReduceError(error);
                console.log('Update ERROR: ', this.errorMsgs);
                this.showToastMessage('',this.errorMsgs[0],'error');
            });
        } else {
            // Check if the component is on a record detail page. If yes, we're only passing in 1 recordId to add to a wishlist
            if (this.isRecordDetailPage === "true"){
              addToSuggestedOrder({
                  webstoreId : this.webstoreId,
                  effectiveAccountId : this.effectiveAccountId,
                  orderId : this.selectedList,
                  productQuantityMap : JSON.stringify(tempArray)
              })
              .then(result => {
                   this.showToastMessage('', LABEL_ADD_WISHLIST_SUCCESS_PART_3 + ' \'' + this.selectedListName + ' \'' + LABEL_ADD_WISHLIST_SUCCESS_PART_2, 'success');
                  this.navigateToList(this.selectedList);
                  this.closeModal();
              })
              .catch(error => {
                  this.setIsLoading(false);
                  this.errorMsgs = ReduceError(error);
                  console.log('Update ERROR: ', this.errorMsgs);
                  this.showToastMessage('',this.errorMsgs[0],'error');
              });
            } else {
                // Component is on the Master Grid Page, meaning multiple recordIds will be passed into it for modifying or creating a Wishlist
                addBulkItemsToWishList({
                    webstoreId : this.webstoreId,
                    effectiveAccountId : this.effectiveAccountId,
                    orderId : this.selectedList,
                    productQuantityMap : JSON.stringify(tempArray)
                })
                .then(result => {
                    this.setIsLoading(false);
                    if (result.isSuccess){
                    this.showToastMessage('', LABEL_ADD_WISHLIST_SUCCESS_PART_3 + ' \'' + this.selectedListName + ' \'' + LABEL_ADD_WISHLIST_SUCCESS_PART_2, 'success');
                        this.navigateToList(this.selectedList);
                        this.closeModal();
                    } else {
                        this.showToastMessage('', result.errorMsg, 'error');
                    }
                })
                .catch(error => {
                    this.setIsLoading(false);
                    this.errorMsgs = ReduceError(error);
                    console.log('Wishlist Update ERROR: ', this.errorMsgs);
                    this.showToastMessage('',this.errorMsgs[0],'error');
                });
            }

        }
    }

    handleCartUpdate(){
      this.dispatchEvent(
              new CustomEvent(CART_CHANGED_EVT, {
                  bubbles: true,
                  composed: true
              })
          );
    }

    navigateToList(listId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/suggested-order/' + listId + '/detail'
            }
        });
    }




}