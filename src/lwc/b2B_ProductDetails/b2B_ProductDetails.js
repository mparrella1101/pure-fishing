import { LightningElement,api, track  } from 'lwc';
import getProduct from "@salesforce/apex/productDetailService.getProductDetails";//Replace class and method name and make sure method is aura enabled
 

export default class b2B_ProductDetails extends LightningElement {//replace component name
    @api recordId;
    @track product = [];
    connectedCallback(){
        this.getProductName();
    }

    getProductName(){
        getProduct({ recordId: this.recordId })
        .then((result) => {
            for(let key in result){
                 this.product.push({value:result[key], key:key});
            }
        })
        .catch((error) => {
            console.log(error);
        })
       
    }


}