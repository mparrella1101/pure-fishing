/**
 * Created by Matt on 5/24/2022.
 */

import { LightningElement, api } from 'lwc';

const QUANTITY_CHANGED_EVT = 'quantitychanged';
const WISHLIST_ITEM_REMOVE_EVT = 'removewishlistitem';
const ADD_TO_CART_EVT = 'singleaddtocart';

import LABEL_MATERIAL_TEXT from '@salesforce/label/c.cc_cart_item_material_label';
import LABEL_MODEL_TEXT from '@salesforce/label/c.cc_cart_item_model_label';
import LABEL_CASEPACK_TEXT  from '@salesforce/label/c.cc_cart_item_casepack_label';
import LABEL_INVENTORY_STATUS_LABEL from '@salesforce/label/c.cc_mg_product_info_inventory_status_label';
import LABEL_ADD_TO_CART_TEXT from '@salesforce/label/c.cc_mg_page_totals_add_to_cart';


export default class cc_wishlist_item extends LightningElement {
    @api item;
    inputsDisabled = false;
    @api currencyCode;
    isLoading = false;

    modelText = LABEL_MODEL_TEXT;
    materialText = LABEL_MATERIAL_TEXT;
    casePackText = LABEL_CASEPACK_TEXT;
    inventoryStatusText = LABEL_INVENTORY_STATUS_LABEL;
    addToCartTxt = LABEL_ADD_TO_CART_TEXT;

    inputTimer;
    waitInterval = 400;

    @api setIsLoading(value) {
        this.isLoading = value;
        this.inputsDisabled = value;
    }


    handleQuantityDecrease(event){
        if (this.item.quantity != 0){
          this.item = JSON.parse(JSON.stringify(this.item));
          let selectorInput = this.template.querySelector('.selector-input');
          let newQuantity = +this.item.quantity - 1;
          this.item.quantity = newQuantity;
          selectorInput.value = newQuantity;
          selectorInput.dispatchEvent(new Event('change'));
        }
    }

    handleQuantityIncrease(event){
        this.item = JSON.parse(JSON.stringify(this.item));
        let selectorInput = this.template.querySelector('.selector-input');
        let newQuantity = +this.item.quantity + 1;
        this.item.quantity = newQuantity;
        selectorInput.value = newQuantity;
        selectorInput.dispatchEvent(new Event('change'));
    }

    handleQuantityChange(event) {
        // Debounce the quantity change
        let val = event.target.value;
        clearTimeout(this.inputTimer);
        this.inputTimer = setTimeout(() => {
            this.setIsLoading(true);
           this.updateQuantity(this.item.orderItemId, val);
        }, this.waitInterval);
    }

    updateQuantity(orderItemId, quantity) {
        // fire event to handle on cart items lwc
        this.dispatchEvent(
            new CustomEvent(QUANTITY_CHANGED_EVT, {
                detail: {
                    orderItemId,
                    quantity
                }
            })
        );
    }

    handleDeleteWishlistItem(event) {
        let removedOrderItemId = event.target.dataset.orderitemid;
        this.dispatchEvent(
            new CustomEvent(WISHLIST_ITEM_REMOVE_EVT,
            {
                detail: {
                    orderItemId : removedOrderItemId
                }
            })
        );
    }

    handleSingleCartAdd(event) {
        this.dispatchEvent(
            new CustomEvent(ADD_TO_CART_EVT, {
                detail: {
                    productId : this.item.productId,
                    qty : this.item.quantity,
                    modelNumber : this.item.modelNumber,
                    type: '',
                    productName : this.item.productName
                }
            })
        );
    }

    handleProductDetailNavigation(event) {
        let productId = event.target.dataset.productid;
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/product/' + productId
            }
        });
    }
}