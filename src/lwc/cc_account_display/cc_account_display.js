/**
 * Created by Matt on 7/7/2022.
 */

import { LightningElement, api } from 'lwc';
import getAccountName from '@salesforce/apex/AccountDisplayController.getAccountName';

export default class cc_account_display extends LightningElement {
    @api accountId;
    @api effectiveAccountId; // Holds the {!CurrentUser.effectiveAccountId} value
    showAccountText = false; // When TRUE, will display message to usr indicating they're shopping under a different account
    accountName; // Holds the Account Name of the account the user is shopping under, if it differs from their default account

    connectedCallback() {
        // Need to trim effectiveAccountId down to 15-characters (since accountId parameter is 15-characters)
        this.effectiveAccountId = this.effectiveAccountId.substring(0,15);
        if (this.accountId !== this.effectiveAccountId){
            getAccountName({
                accountId : this.effectiveAccountId
            })
            .then(result => {
                this.accountName = result;
                this.showAccountText = true;
            })
            .catch(error => {
                console.log('ERROR: ', error);
            })
        } else {
            this.showAccountText = false;
        }
    }
}