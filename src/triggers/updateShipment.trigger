trigger updateShipment on OrderItem (before update) {
    System.debug('updateShipmentInvoice start');
    System.debug('updateShipmentInvoice isBatch: '+system.isBatch());
    System.debug('updateShipmentInvoice isFuture: '+system.isFuture());
    ApexGovernorLimits.printMaxLimits();
    //if(system.isBatch() || system.isFuture()) return;

    try {
        /*Shipment*/
        List<ShipmentSalesOrderItem> shipments = new List<ShipmentSalesOrderItem>();
        
        List<String> orderItemIDs = new List<String>();
        List<String> ECRecordNames = new List<String>();
        List<String> ShipmentNumbers = new List<String>();
        List<String> order_ids = new List<String>();
        
        ShipmentSalesOrderItem shipment = null;
        
        /*Invoice*/
        /*List<InvoiceSalesOrderItem> invoices = new List<InvoiceSalesOrderItem>();
        
        List<String> invoiceOrderItemIDs = new List<String>();
        List<String> invoiceECRecordNames = new List<String>();
        List<String> invoiceNumbers = new List<String>();
        List<String> invoiceOrder_ids = new List<String>();
        
        InvoiceSalesOrderItem invoice = null;*/
        
        Integer ind = 0;
        for (OrderItem record : Trigger.new) {
            /*if(record.Invoice_ECRecordName__c != null && record.Shipment_Number__c != null && record.Invoice_Number__c == null && record.EComStoreID__c != NULL) {
                System.debug('updateInvoice:'+record);
                
                InvoiceItem item = new InvoiceItem();
                item.qty = record.Quantity_Invoiced__c;
                item.order_item_id = record.MagentoId__c;
        
                InvoiceTrack track = new InvoiceTrack();
                track.invoice_number = record.JDE_Invoice_Number__c;
    
                Boolean tracking_duplicate_exists = false;
                //Check if tracking number already exists
    
                if(!tracking_duplicate_exists || invoice == null) {
                    invoiceECRecordNames.add(record.Invoice_ECRecordName__c);
                    invoiceNumbers.add(record.Invoice_Number__c);
                    invoiceOrder_ids.add(record.OrderId);
                    
                    System.debug('ind:'+ind + ' - record.Invoice_ECRecordName__c:' + record.Invoice_ECRecordName__c);
                    System.debug('ind:'+ind + ' - record.Invoice_Number__c:' + record.Invoice_Number__c);
                    System.debug('ind:'+ind + ' - record.OrderId:' + record.OrderId);
                    System.debug('ind:'+ind + ' - record.MagentoId__c:' + record.MagentoId__c);
                    System.debug('ind:'+ind + ' - record.Invoice_Number__c:' + record.Invoice_Number__c);
                    ind++;
                    
                    invoice = new InvoiceSalesOrderItem();
                    invoice.appendComment = false;
                    invoice.notify = false;
                    invoice.capture = true;
                    
                    invoice.items = new List<InvoiceItem>();
                    invoice.items.add(item);
                    
                    //invoice.tracks = new List<InvoiceTrack>();
                    //invoice.tracks.add(track);
                    invoices.add(invoice);
                    
                    //orderItemIDs = new List<String>();
                    invoiceOrderItemIDs.add(record.Id);
                    System.debug('In IF: invoiceOrderItemIDs:'+invoiceOrderItemIDs + ' record.Id:'+record.Id);
                }
                else {
                    invoiceOrderItemIDs.set(invoiceOrderItemIDs.size()-1,invoiceOrderItemIDs.get(invoiceOrderItemIDs.size()-1)+','+record.Id);
                    invoices.get(invoices.size()-1).items.add(item);
                    System.debug('In ELSE: invoiceOrderItemIDs:'+invoiceOrderItemIDs + ' record.Id:'+record.Id);
                }
                System.debug('>>invoice:'+invoice);
            } else */if(record.ECRecordName__c != null && record.Shipment_Number__c == null /*&& record.Invoice_Number__c == null */&& record.EComStoreID__c != NULL) {
                System.debug('updateShipment:'+record);
                
                ShipmentItem item = new ShipmentItem();
                item.qty = record.Quantity_Shipped__c;
                item.order_item_id = record.MagentoId__c;
        
                ShipmentTrack track = new ShipmentTrack();
                track.track_number = record.Tracking_Pro_Number__c;
                track.carrier_code = 'ups';
                track.title = 'Universal Parcel Service';
    
                Boolean tracking_duplicate_exists = false;
                //Check if tracking number already exists
                if(shipment != null) {
                    for(ShipmentTrack tr : shipment.tracks) {
                        if(tr.track_number == track.track_number) {
                            tracking_duplicate_exists = true;
                            break;
                        }
                    }
                }
                if(!tracking_duplicate_exists || shipment == null) {
                    ECRecordNames.add(record.ECRecordName__c);
                    ShipmentNumbers.add(record.Shipment_Number__c);
                    order_ids.add(record.OrderId);
                    
                    System.debug('ind:'+ind + ' - record.ECRecordName__c:' + record.ECRecordName__c);
                    System.debug('ind:'+ind + ' - record.Shipment_Number__c:' + record.Shipment_Number__c);
                    System.debug('ind:'+ind + ' - record.OrderId:' + record.OrderId);
                    System.debug('ind:'+ind + ' - record.MagentoId__c:' + record.MagentoId__c);
                    System.debug('ind:'+ind + ' - record.Tracking_Pro_Number__c:' + record.Tracking_Pro_Number__c);
                    ind++;
                    
                    shipment = new ShipmentSalesOrderItem();
                    shipment.appendComment = false;
                    shipment.notify = true;
                    
                    shipment.items = new List<ShipmentItem>();
                    shipment.items.add(item);
                    
                    shipment.tracks = new List<ShipmentTrack>();
                    shipment.tracks.add(track);
                    shipments.add(shipment);
                    
                    //orderItemIDs = new List<String>();
                    orderItemIDs.add(record.Id);
                    System.debug('In IF: orderItemIDs:'+orderItemIDs + ' record.Id:'+record.Id);
                }
                else {
                    orderItemIDs.set(orderItemIDs.size()-1,orderItemIDs.get(orderItemIDs.size()-1)+','+record.Id);
                    shipments.get(shipments.size()-1).items.add(item);
                    System.debug('In ELSE: orderItemIDs:'+orderItemIDs + ' record.Id:'+record.Id);
                }
                System.debug('>>shipment:'+shipment);
            }
    	}
        System.debug('>>shipments:'+shipments);
        System.debug('>>orderItemIDs:'+orderItemIDs);
/*
        System.debug('>>invoices:'+invoices);
        System.debug('>>invoiceOrderItemIDs:'+invoiceOrderItemIDs);
        System.debug('>>invoiceECRecordNames:'+invoiceECRecordNames);
        System.debug('>>invoiceNumbers:'+invoiceNumbers);
        */
        if(Test.isRunningTest()) {
            /*EpM2InvoiceUpdateSync.updateInvoiceSynchronously(invoices, invoiceOrderItemIDs, invoiceECRecordNames, InvoiceNumbers, invoiceOrder_ids);
            System.enqueueJob(new EpM2InvoiceUpdateJob(invoices, invoiceOrderItemIDs, invoiceECRecordNames, InvoiceNumbers, invoiceOrder_ids));
            */
            EpM2ShipmentUpdateSync.updateShipmentSynchronously(shipments, orderItemIDs, ECRecordNames, ShipmentNumbers, order_ids);
            System.enqueueJob(new EpM2ShipmentUpdateJob(shipments, orderItemIDs, ECRecordNames, ShipmentNumbers, order_ids));
        }
		//EpM2ShipmentUpdateSync.updateShipmentSynchronously(shipments, orderItemIDs, ECRecordNames, ShipmentNumbers, order_ids);
        /*for(Integer index=0; index<invoices.size(); index++)
        {
            if(invoiceECRecordNames.get(index) != null && invoiceNumbers.get(index) == null) {
                InvoiceSalesOrderItem inv = invoices.get(index);
                
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    Order order = [SELECT MagentoId__c, ECRecordName__c,EComStoreID__c FROM Order WHERE Id = :invoiceOrder_ids.get(index)];
                    ApexGovernorLimits.printLimitsUsed();
                    
                    String body = System.JSON.serialize(inv);
                    System.debug('invoice END JSON:'+body);
                    System.debug('invoiceECRecordName:'+invoiceECRecordNames.get(index));
    
                    if (Limits.getQueries() > Limits.getLimitQueries()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit Queries governor limits');
                        MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :order.EComStoreID__c];
                        ApexGovernorLimits.printLimitsUsed();
                        
                        String last_sync_time = store.LastSalesOrderSyncTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
                        System.debug('last_sync_time: ' + last_sync_time);
                        
                        List<String> itemList = new List<String>();
                        if(invoiceOrderItemIDs.get(index).contains(','))
                            itemList = invoiceOrderItemIDs.get(index).split(',');
                        else
                            itemList = new List<String>{invoiceOrderItemIDs.get(index)};
                                EpM2InvoiceUpdateSync.defensiveUpdateInvoice(invoiceECRecordNames.get(index), order.MagentoId__c,body, itemList, store.AccessToken__c, store.Name, last_sync_time, store.BaseURL__c, store.SupportEmail__c);   
                    }
                }
            }
        }*/
        for(Integer index=0; index<shipments.size(); index++)
        {
            if(ECRecordNames.get(index) != null && ShipmentNumbers.get(index) == null) {
                ShipmentSalesOrderItem shpmnt = shipments.get(index);
                if (Limits.getQueries() > Limits.getLimitQueries()) {
                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                } else {
                    System.debug('Continue processing. Not going to hit Queries governor limits');
                    Order order = [SELECT MagentoId__c, ECRecordName__c,EComStoreID__c FROM Order WHERE Id = :order_ids.get(index)];
                    ApexGovernorLimits.printLimitsUsed();

                    String body = System.JSON.serialize(shpmnt);
                    System.debug('END JSON:'+body);
                    System.debug('ECRecordName:'+ECRecordNames.get(index));
    
                    if (Limits.getQueries() > Limits.getLimitQueries()) {
                        System.debug('Need to stop processing to avoid hitting a governor limit.');
                    } else {
                        System.debug('Continue processing. Not going to hit Queries governor limits');
                        MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :order.EComStoreID__c];
                        ApexGovernorLimits.printLimitsUsed();
                        
                        String last_sync_time = store.LastSalesOrderSyncTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss', 'GMT');
                        System.debug('last_sync_time: ' + last_sync_time);
                        
                        List<String> itemList = new List<String>();
                        if(orderItemIDs.get(index).contains(','))
                            itemList = orderItemIDs.get(index).split(',');
                        else
                            itemList = new List<String>{orderItemIDs.get(index)};
                        EpM2ShipmentUpdateSync.defensiveUpdateShipment(ECRecordNames.get(index), order.MagentoId__c,body, itemList, store.AccessToken__c, store.Name, last_sync_time, store.BaseURL__c, store.SupportEmail__c);   
                    }
                }
            }
        }
    } catch(Exception ex) {
        Notifier.sendEmail('updateShipmentInvoice Exception: ' + ex.getMessage());
    }
    ApexGovernorLimits.printLimitsLeft();
    System.debug('updateShipmentInvoice end');
}