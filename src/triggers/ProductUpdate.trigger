/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
1.0         Arsee Khan           Coastal Cloud           06/14/2022            Initial Version
*/
// Product Image creation will trigger here on update and insert of Product
trigger ProductUpdate on Product2 (after update,after insert) {
    List<Id> productIdList = new List<Id>();
    List<Id> NewProdIdList = new List<Id>();
    if (Trigger.isAfter)
    {
        if (Trigger.isInsert) 
        {
            for (Product2 prod : Trigger.new)
            {
                If (prod.DAM_Set__c != NULL && prod.DAM_Set__c !='')
                {
                    //b2b_ProductImage.ProductImage(prod.Id);
                    productIdList.add(prod.Id);
                }
                NewProdIdList.add(prod.Id);
            }
            
        }
        if (Trigger.isUpdate)
        { 
            
            for (Product2 prodcut : Trigger.new) {
                Product2 oldProd = Trigger.oldMap.get(prodcut.Id);
                
                if (prodcut.DAM_Set__c != oldProd.DAM_Set__c) {
                    If (prodcut.DAM_Set__c != NULL && prodcut.DAM_Set__c !='') {
                        //b2b_ProductImage.ProductImage(prodcut.Id);
                        productIdList.add(prodcut.Id);
                    }
                }
            }
        }
        if(productIdList.size()>0)
        b2b_ProductImage.ProductImage(productIdList);
        if(NewProdIdList.size()>0)
        B2B_CreatePriceList.CreatePriceList(NewProdIdList);
    }
}