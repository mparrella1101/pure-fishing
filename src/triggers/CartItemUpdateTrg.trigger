trigger CartItemUpdateTrg on CartItem (after insert, before update) {

    list<CartValidationOutput> error = new list<CartValidationOutput>();
    List<CartValidationOutput> valiId = new list<CartValidationOutput>();
    for (CartItem cartItems : Trigger.new) {
        if(cartItems.Type == 'product'){
            Decimal prodCaseQty = [ Select Case_Quantity__c from product2 where id =:cartItems.Product2Id][0].Case_Quantity__c;
            if(math.mod((Integer) cartItems.Quantity, (Integer) prodCaseQty) !=  0){
                CartValidationOutput cartValidationError = new CartValidationOutput(
                    CartId = cartItems.CartId,
                    Level = 'Error',
                    Message = 'Quantity should be a multiple of ' +  prodCaseQty,
                    Name = (String)cartItems.CartId,
                    RelatedEntityId = cartItems.Id,
                    Type = 'Other'
                );
                error.add(cartValidationError);
                //insert cartValidationError;
            }
            else 
            {
                valiId=[Select id from CartValidationOutput where RelatedEntityId =:cartItems.Id];
            }
        }
        
    }
    if(error.size() > 0)
        insert error;
    if(valiId.size() > 0)
         delete valiId;
}