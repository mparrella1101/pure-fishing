/**
 * @description CartItem Trigger to execute custom logic on records during specific database interactions
 *
 *
 */
/*  Version      Author                  Company                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           05/19/2022            Initial Version
 */

trigger CartItemTrigger on CartItem (before update, after insert, after update, after delete) {
    if (Trigger.isBefore) {
        if (Trigger.isUpdate) {
            CartItemTriggerHandler.handleBeforeUpdate(Trigger.newMap, Trigger.oldMap);
        }

    }
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            CartItemTriggerHandler.handleAfterInsert(Trigger.newMap);
        }
        if (Trigger.isUpdate) {
            CartItemTriggerHandler.handleAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }
        if (Trigger.isDelete) {
            CartItemTriggerHandler.handleAfterDelete(Trigger.old);
        }
    }

}