/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 * * Modified by SM to add "Schema" due to issue with ShipmentItem object
 **/
trigger dlrs_ShipmentItemTrigger on ShipmentItem
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(Schema.ShipmentItem.SObjectType);
}