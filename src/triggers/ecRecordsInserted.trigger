trigger ecRecordsInserted on ECRecord__c (before insert, before update, after insert, after update) {
    System.debug('ecRecordsInserted start');
    System.debug('ecRecordsInserted isBatch: '+system.isBatch());
    System.debug('ecRecordsInserted isFuture: '+system.isFuture());
    
    if (Trigger.isInsert || Trigger.isUpdate) {
        if (Trigger.isBefore) {
            for (ECRecord__c record : Trigger.new) {
                try {
                    if(record.Type__c == 'Customer Create' && record.MagentoResponse__c == 'Success' && record.JDEResponse__c == null) {
                        EpSFContactSync.createSFContact(record);
                    } else if(record.Type__c == 'Sales Order Create' && 
                              record.MagentoResponse__c == 'Success' && 
                              record.JDEResponse__c == null) { 
						EpSFOrderSync.createSFOrder(record);
                    } else if(record.Type__c == 'Shipment Update' && 
                              record.JDEResponse__c == 'Success' && 
                              record.MagentoResponse__c == null) {
						EpSFSync.updateSFOrder(record);
                    }/* else if(record.Type__c == 'Invoice Update' && 
                              record.JDEResponse__c == 'Success' && 
                              record.MagentoResponse__c == null) { 
						EpSFInvoiceSync.updateSFOrder(record);
                    }*/
                } catch (Exception ex) {
                    System.debug('Exception ex: ' + ex.getMessage());
                    //record.SalesForceResponse__c = ex.getMessage();
                    //record.SalesForceObjectStatus__c = false;
                    Notifier.sendEmail('ecRecordsInserted Exception: ' + ex.getMessage());
                }
            }
        } else if (Trigger.isAfter) {
            for (ECRecord__c record : Trigger.new) {
                try {
                    if(record.Type__c == 'Customer Create' && record.MagentoResponse__c == 'Success' && record.JDEResponse__c == null) {
                        if (Limits.getQueries() > Limits.getLimitQueries()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit Queries governor limits');
                            Contact contact = [SELECT Id, ECRecordName__c FROM Contact WHERE Id = :record.SalesForceID__c];
                            ApexGovernorLimits.printLimitsUsed();
                        
                            contact.ECRecordName__c = record.Name;
                            if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                                System.debug('Need to stop processing to avoid hitting a governor limit.');
                            } else {
                                System.debug('Continue processing. Not going to hit DML governor limits');
                                update contact;
                                ApexGovernorLimits.printLimitsUsed();
                            }
                        }
                    } else if(record.Type__c == 'Sales Order Create' && record.MagentoResponse__c == 'Success' && record.JDEResponse__c == null) {
                            System.debug('ecRecordsInsertedAfter > record.SalesForceID__c: ' + record.SalesForceID__c);
                            System.debug('ecRecordsInsertedAfter > record.Name: ' + record.Name);
                        if (Limits.getQueries() > Limits.getLimitQueries()) {
                            System.debug('Need to stop processing to avoid hitting a governor limit.');
                        } else {
                            System.debug('Continue processing. Not going to hit Queries governor limits');
                            Order order = [SELECT Id, ECRecordName__c,EComStoreID__c FROM Order WHERE Id = :record.SalesForceID__c];
                            ApexGovernorLimits.printLimitsUsed();
                        
                            order.ECRecordName__c = record.Name;
                            if (Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                                System.debug('Need to stop processing to avoid hitting a governor limit.');
                            } else {
                                System.debug('Continue processing. Not going to hit DML governor limits');
                                update order;
                                ApexGovernorLimits.printLimitsUsed();
                            }
                        }
                    } else if(record.Type__c == 'Shipment Update' && record.JDEResponse__c == 'Success' && record.MagentoResponse__c == null) {
                        String[] orderItemIDs = null;
                        if(record.SalesForceID__c.contains(',')) {
                            orderItemIDs = record.SalesForceID__c.split(',');
                        } else {
                            orderItemIDs = new String[]{record.SalesForceID__c};
                        }
                        
                        List<OrderItem> orderItems = new List<OrderItem>();
                        if(orderItemIDs!=null 
                           && orderItemIDs.size()>0)
                        {
                            for(String orderItemID : orderItemIDs) {
                                if (Limits.getQueries() > Limits.getLimitQueries()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                } else {
                                    System.debug('Continue processing. Not going to hit Queries governor limits');
                                    List<OrderItem> orderItemz = [SELECT Id, ECRecordName__c,EComStoreID__c FROM OrderItem WHERE Id = :orderItemID];
                                    ApexGovernorLimits.printLimitsUsed();
                                    if(orderItemz!=null 
                                       && orderItemz.size()>0) {
                                        OrderItem orderItem = orderItemz.get(0);
                                        orderItem.ECRecordName__c = record.Name;
                                        orderItems.add(orderItem);
                                    }
                                }
                            }
                            if(!orderItems.isEmpty())
                            {
                                if (orderItems.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                } else {
                                    System.debug('Continue processing. Not going to hit DML governor limits');
                                    update orderItems;
                                    ApexGovernorLimits.printLimitsUsed();
                                }
                            }
                        }
                    }/* else if(record.Type__c == 'Invoice Update' && record.JDEResponse__c == 'Success' && record.MagentoResponse__c == null) {
                        String[] orderItemIDs = null;
                        if(record.SalesForceID__c.contains(',')) {
                            orderItemIDs = record.SalesForceID__c.split(',');
                        } else {
                            orderItemIDs = new String[]{record.SalesForceID__c};
                        }
                        
                        List<OrderItem> orderItems = new List<OrderItem>();
                        if(orderItemIDs!=null 
                           && orderItemIDs.size()>0)
                        {
                            for(String orderItemID : orderItemIDs) {
                                if (Limits.getQueries() > Limits.getLimitQueries()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                } else {
                                    System.debug('Continue processing. Not going to hit Queries governor limits');
                                    List<OrderItem> orderItemz = [SELECT Id, Invoice_ECRecordName__c,EComStoreID__c FROM OrderItem WHERE Id = :orderItemID];
                                    ApexGovernorLimits.printLimitsUsed();
                                    
                                    if(orderItemz!=null 
                                       && orderItemz.size()>0) {
                                        OrderItem orderItem = orderItemz.get(0);
                                        orderItem.Invoice_ECRecordName__c = record.Name;
                                        orderItems.add(orderItem);
                                    }
                                }
                            }
                            if(!orderItems.isEmpty())
                            {
                                if (orderItems.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()) {
                                    System.debug('Need to stop processing to avoid hitting a governor limit.');
                                } else {
                                    System.debug('Continue processing. Not going to hit DML governor limits');
                                    update orderItems;
                                    ApexGovernorLimits.printLimitsUsed();
                                }
                            }
                        }
                    }*/
                } catch (Exception ex) {
                    System.debug('EX: ' + ex);
                    System.debug('ex.getMessage(): ' + ex.getMessage());
                    Notifier.sendEmail('ecRecordsInsertedAfter: ' + ex.getMessage());
                }
            }
        }
    }
    System.debug('ecRecordsInserted end');
}