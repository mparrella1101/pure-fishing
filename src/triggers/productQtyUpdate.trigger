trigger productQtyUpdate on Product2 (after update) {
    System.debug('productQtyUpdate start');
    System.debug('productQtyUpdate isBatch: '+system.isBatch());
    System.debug('productQtyUpdate isFuture: '+system.isFuture());
    List<String> prodJdeIds = new List<String>();
    List<String> prodQtysAvailable = new List<String>();
    List<String> prodEComStoreIDs = new List<String>();
        
    for (Product2 prodcut : Trigger.new) {
        try {
            if(prodcut.JdeId__c != NULL &&
               prodcut.JdeId__c != '' &&
               prodcut.MagentoId__c!= NULL &&
               prodcut.EComStoreID__c != NULL) {
                prodJdeIds.add(prodcut.JdeId__c);
                prodQtysAvailable.add(String.valueOf(prodcut.Qty_Available__c));
                prodEComStoreIDs.add(prodcut.EComStoreID__c);
            }
        } catch(Exception ex) {
            System.debug('productQtyUpdate Exception: ' + ex.getMessage());
            Notifier.sendEmail('productQtyUpdate Exception: ' + ex);
        }
    }
    
    if(prodJdeIds.size()>0) {
        if(System.Test.isRunningTest()) {
            EpM2StockItemUpdateSync.defensiveStockItemUpdate(
                prodJdeIds.get(0),
                prodQtysAvailable.get(0),
                prodEComStoreIDs.get(0));
        }
        //EpM2StockItemUpdateSync.defensiveStockItemUpdateSynchronously(JSON.serialize(prodJdeIds), JSON.serialize(prodQtysAvailable), JSON.serialize(prodEComStoreIDs));
        /*if(System.isFuture()){
            System.debug('productQtyUpdate -> System.isFuture():'+System.isFuture());
            EpM2StockItemUpdateSync.stockItemUpdateSynchronously(JSON.serialize(prodJdeIds), JSON.serialize(prodQtysAvailable), JSON.serialize(prodEComStoreIDs));
        } else if(Limits.getFutureCalls() < Limits.getLimitFutureCalls()){
            System.debug('productQtyUpdate -> Limits.getFutureCalls():'+Limits.getFutureCalls());
            EpM2StockItemUpdateSync.stockItemUpdateSynchronouslyFuture(JSON.serialize(prodJdeIds), JSON.serialize(prodQtysAvailable), JSON.serialize(prodEComStoreIDs));
        } else {
            System.debug('productQtyUpdate -> @future call was denied');
            Notifier.sendEmail('productQtyUpdate -> @future call was denied');
        }*/
        System.debug('productQtyUpdate -> System.isFuture():'+System.isFuture());
        System.debug('productQtyUpdate -> Limits.getFutureCalls() < Limits.getLimitFutureCalls():'+(Limits.getFutureCalls() < Limits.getLimitFutureCalls()));
        //EpM2StockItemUpdateSync.stockItemUpdateSynchronously(JSON.serialize(prodJdeIds), JSON.serialize(prodQtysAvailable), JSON.serialize(prodEComStoreIDs));
        ApexGovernorLimits.printMaxLimits();
        Integer scheduledJobCount = [SELECT COUNT() FROM CronJobDetail WHERE JobType = '7'];
        ApexGovernorLimits.printLimitsUsed();
        if(scheduledJobCount<100) {
            try {
                Datetime dt;
                dt = Datetime.now();
                Integer hour;
                Integer min;
                Integer ss;
                
                hour = dt.hour();
                min = dt.minute();
                ss = dt.second() + 5;
                
                while(ss>59)
                {
                    min += 1;
                    ss = ss - 59;
                }
                
                while(min>59)
                {
                    hour += 1;
                    min = min - 59;
                }
                
                if(hour>23)
                {
                    hour = 0;
                }
                //parse to cron expression
                String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                System.debug('nextFireTime: ' + nextFireTime);
                System.schedule('ScheduledItemJob' + String.valueOf(Math.random()), nextFireTime, new EpM2StockItemUpdateSyncSchedulable(JSON.serialize(prodJdeIds), JSON.serialize(prodQtysAvailable), JSON.serialize(prodEComStoreIDs)));
                if(System.Test.isRunningTest())
                {
                    System.schedule('ScheduledItemJob' + String.valueOf(Math.random()), nextFireTime, new EpM2StockItemUpdateSchedulable(prodJdeIds, prodQtysAvailable, prodEComStoreIDs));
                }
            } catch(Exception ex) {
                System.debug('productQtyUpdate Exception: ' + ex.getMessage());
                Notifier.sendEmail('productQtyUpdate Exception: ' + ex);
            }
        } else {
            Notifier.sendEmail('productQtyUpdate - Limits reached - Cannot update sku:'+JSON.serialize(prodJdeIds));
        }
        ApexGovernorLimits.printLimitsLeft();
    }
    System.debug('productQtyUpdate end');
}