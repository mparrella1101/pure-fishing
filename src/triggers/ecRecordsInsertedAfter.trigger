trigger ecRecordsInsertedAfter on ECRecord__c (after insert, after update) {
    System.debug('ecRecordsInsertedAfter start');
  System.debug('ecRecordsInsertedAfter isBatch: '+system.isBatch());
    System.debug('ecRecordsInsertedAfter isFuture: '+system.isFuture());
    for (ECRecord__c record : Trigger.new) {
        try {
            if(record.Type__c == 'Customer Create' && record.MagentoResponse__c == 'Success' && record.JDEResponse__c == null) {Contact contact = [SELECT Id, ECRecordName__c FROM Contact WHERE Id = :record.SalesForceID__c];contact.ECRecordName__c = record.Name;update contact;
            } else if(record.Type__c == 'Sales Order Create' && record.MagentoResponse__c == 'Success' && record.JDEResponse__c == null) {
                    System.debug('ecRecordsInsertedAfter > record.SalesForceID__c: ' + record.SalesForceID__c);
                    System.debug('ecRecordsInsertedAfter > record.Name: ' + record.Name);
                    Order order = [SELECT Id, ECRecordName__c FROM Order WHERE Id = :record.SalesForceID__c];order.ECRecordName__c = record.Name;update order;
            } else if(record.Type__c == 'Shipment Update' && record.JDEResponse__c == 'Success' && record.MagentoResponse__c == null) {
                String[] orderItemIDs = null;
                if(record.SalesForceID__c.contains(',')) {
                    orderItemIDs = record.SalesForceID__c.split(',');
                } else {
                    orderItemIDs = new String[]{record.SalesForceID__c};
                }
                          
                List<OrderItem> orderItems = new List<OrderItem>();
                if(orderItemIDs!=null 
                   && orderItemIDs.size()>0)
                {
                    for(String orderItemID : orderItemIDs) {
                        List<OrderItem> orderItemz = [SELECT Id, ECRecordName__c FROM OrderItem WHERE Id = :orderItemID];
                        
                        if(orderItemz!=null 
                           && orderItemz.size()>0) {
                            OrderItem orderItem = orderItemz.get(0);
                            orderItem.ECRecordName__c = record.Name;
                            orderItems.add(orderItem);
                        }
                    }
                    update orderItems;
                }
            }
        } catch (Exception ex) {
            System.debug('EX: ' + ex);
            System.debug('ex.getMessage(): ' + ex.getMessage());
            //record.SalesForceResponse__c = ex.getMessage();
            //record.SalesForceObjectStatus__c = false;
            Notifier.sendEmail('ecRecordsInsertedAfter: ' + ex.getMessage());
        }
    }
    System.debug('ecRecordsInsertedAfter end');
}