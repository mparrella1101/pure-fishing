trigger orderCreated on Order (after insert,before update) {
    System.debug('orderCreated start');
    System.debug('orderCreated isBatch: '+system.isBatch());
    System.debug('orderCreated isFuture: '+system.isFuture());
    List<Id> OrderIdList = new List<Id>();
    List<Order> OrderUpd=new List<Order>();
   // B2B_UpdateOrders UpdOrd= new B2B_UpdateOrders();
    if(Trigger.isbefore)
    {
        if(trigger.isUpdate)
        {       
            for(Order order : Trigger.new)
            {
                if(order.Status =='Created')
                {
                    System.debug('order :' + order.id+' status before update to Created: '+ order.Status);
                    OrderIdList.add(order.id);
                }
            }
            if(OrderIdList != null)
            {
                B2B_UpdateOrders.UpdateOrders(OrderIdList);
            }
            
        }
    }
    if (Trigger.isAfter)
    {
        for (Order order : Trigger.new) { 
            if(order.EComStoreID__c != null) {
                try {
                    //System.enqueueJob(new EpJDECreateSalesOrderJob(order.Id));
        
                    MagentoStore__c store = [SELECT AccessToken__c,BaseURL__c,ECom_Store_Name__c,Id,LastSalesOrderSyncTime__c,Name,SupportEmail__c FROM MagentoStore__c WHERE Name = :order.EComStoreID__c];
                    if(Limits.getQueueableJobs() == 1 || Test.isRunningTest()){
                        Datetime dt = Datetime.now();
                        Integer hour, min, ss;
                        hour = dt.hour();
                        min = dt.minute(); 
                        ss = dt.second() + 5;
                        if(Test.isRunningTest()) {
                            hour = 24;
                            min = 59; 
                            ss = 59 + 5;
                            
                            //EpJDEOrderSync.defensiveCreateJDESalesOrder(order.Id);
                        }
                        if(ss>59)
                        {
                            min += 1;
                            ss = ss - 59;
                        }
                        
                        if(min>59)
                        {
                            hour += 1;
                            min = min - 59;
                        }
                        
                        if(hour>23)
                        {
                            hour = 0;
                        }
                        //parse to cron expression
                        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
                        System.debug('nextFireTime: ' + nextFireTime);
                        System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextFireTime, new EpJDECreateSalesOrderSchedulable(order.Id, store.BaseURL__c));
                    }else{
                        System.enqueueJob(new EpJDECreateSalesOrderJob(order.Id, store.BaseURL__c));
                    }
                } catch(Exception ex) {
                    Notifier.sendEmail('orderCreated Exception: ' + ex.getMessage() + ' >>> OrderID: ' + order.Id);
                }
             }
        }
        System.debug('orderCreated end');
    }
    
}